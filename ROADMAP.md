# roadmap

## 1. get to the point of dogfooding

i used to use pull requests and more complicated flows, but now i just go. it's
just me. no one is going to collaborate. no one is going to help. it's just me.
so i require relatively basic behavior for version control.

i also note here that i currently am fudging a monorepo-style code organization
as i break out the code that i purposefully shoved into the original ts-gib +
ionic-gib. in doing so, i am using `.gitignore` to separate repos.

### required vcs behavior

* [x] add files/folders
  * `--b2tfs-diff --apply`
* [x] add log message ("apply message")
  * `--b2tfs-diff --apply --msg="a short msg here"`
  * `--b2tfs-diff --apply`
    * if no `--text` flag (`--msg`, `--m`, maybe other synonyms), then opens up
      vim for the msg.
  * apply message is required
  * apply message is rel8d to the new `B2tFSItemIbGib_V1`
  * the apply message provides a context for an entire conversation of that
    punctiliar thread.
* [ ] save data
  * [ ] project from B2tFS back to the FS path
    * can act as a poor entity's backup
    * need to be able to navigate previous versions and project from
      a source address.
  * [ ] outerspace sync/push/merge spaces
    * re-use/compose existing poor aws dynamodb + s3 sync space?
    * push space is like a ff-only replication space
      * easiest to implement

### really useful vcs behavior

* [ ] view log messages/conversations
  * i use this daily to see where i left off after daily sessions

### notes on branching

i'm still not sure of the repercussions of this being a meta version control
protocol and the nature of branching. in some existing version control systems,
branching is a way of creating parallel bundles of timelines for multiparty
collaboration. any repo fs node space can merge changes from other fs node
spaces, with deltas/intermediate states of files/folders being special artifacts.

in ibgib in general, any ibgib can be merged from multiple spaces, not just
b2tfs files/folders which have additional structured domain object definitions.

one common branching use case is feature branching. in this, the user creates
feature branch and then merges that feature branch back into the main branch.

## 2.
