#!/usr/bin/env node

import { execRCLI } from './witness/app/rcli/rcli.mjs';

await execRCLI();

console.log(`[index.mts] completed.`);
