/**
 * @module ibgib-helper
 *
 * temp here atow i think. just need a place for this global helper
 */

import { extractErrorMsg } from "@ibgib/helper-gib/dist/helpers/utils-helper.mjs";
import { IbGib_V1 } from "@ibgib/ts-gib/dist/V1/types.mjs";
import { mut8 } from "@ibgib/ts-gib/dist/V1/transforms/mut8.mjs";
import { getIbGibAddr } from "@ibgib/ts-gib/dist/helper.mjs";
import { SpecialIbGibType } from "@ibgib/core-gib/dist/common/other/other-types.mjs";
import { MetaspaceService } from "@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs";
import { IbGibSpaceAny } from "@ibgib/core-gib/dist/witness/space/space-base-v1.mjs";
import { getSpecialConfigKey } from "@ibgib/core-gib/dist/common/other/ibgib-helper.mjs";

import { GLOBAL_LOG_A_LOT } from './ibgib-constants.mjs';
import { CompoundStringVariations } from "./ibgib-types.mjs";

/**
 * used in verbose logging (across all ibgib libs atow)
 */
const logalot = GLOBAL_LOG_A_LOT;

/**
 * helper function that does /camelCasing/hyphenating from a given string
 */
export function getCompoundStringVariations({
    rawString
}: {
    rawString: string
}): CompoundStringVariations {
    const lc = `[${getCompoundStringVariations.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 32e75c033a0e3871c6dbaa2397505d23)`); }
        if (!rawString) { throw new Error(`rawString required (E: a222e3339ca2aacd7d6ef81f1d3f1b23)`); }

        const upperFirstChar = (s: string) =>
            s.length > 1 ? s[0].toUpperCase() + s.slice(1) : s.toUpperCase()
        const lowerFirstChar = (s: string) =>
            s.length > 1 ? s[0].toUpperCase() + s.slice(1) : s.toUpperCase()

        // thisIsCamelCased-and-hyphenated_snakeaddon
        const regex = /[A-Z\-_]/g;

        const separators = rawString.match(regex);
        // separators = ['I', 'C', 'C', '-', '-', '_']

        if (!separators) {
            // no separators so just return the rawString
            return {
                raw: rawString,
                camelCased: lowerFirstChar(rawString),
                hyphenated: rawString.toLowerCase(),
                pascalCased: upperFirstChar(rawString),
                snakeCased: rawString.toLowerCase(),
                upperSnakeCased: rawString.toUpperCase(),
            }; /* <<<< returns early */
        }

        const piecesSansSeparators = rawString.split(regex);
        // piecesSansSeparators = ['this', 's', 'amel', 'ased', 'and', 'hyphenated', 'snakeaddon']

        let pieces = piecesSansSeparators.splice(0, 1);
        // pieces = ['this']
        // piecesSansSeparators = ['s', 'amel', 'ased', 'and', 'hyphenated', 'snakeaddon']
        // separators = ['I', 'C', 'C', '-', '-', '_']
        for (let i = 0; i < separators.length; i++) {
            let separator = separators[i];
            if (['-', '_'].includes(separator)) {
                pieces.push(piecesSansSeparators[i]);
            } else {
                pieces.push(separator + piecesSansSeparators[i]);
            }
        }
        // pieces = ['this', 'Is', 'Camel', 'Cased', 'and', 'hyphenated', 'snakeaddon']

        pieces = pieces.map(x => x.toLowerCase());
        // pieces = ['this', 'is', 'camel', 'cased', 'and', 'hyphenated', 'snakeaddon']

        const pascalCasedPieces = pieces.map(s => upperFirstChar(s));
        // (7) ['This', 'Is', 'Camel', 'Cased', 'And', 'Hyphenated', 'Snakeaddon']

        const pascalCased = pascalCasedPieces.join('');
        // 'ThisIsCamelCasedAndHyphenatedSnakeaddon'

        const camelCased = [
            lowerFirstChar(pieces[0]),
            ...pascalCasedPieces.slice(1), // remaining pieces
        ].join('');
        // 'thisIsCamelCasedAndHyphenatedSnakeaddon'

        const hyphenated = pieces.map(s => s.toLowerCase()).join('-');
        // 'this-is-camel-cased-and-hyphenated-snakeaddon'

        const snakeCased = pieces.map(s => s.toLowerCase()).join('_');
        // 'this_is_camel_cased_and_hyphenated_snakeaddon'

        const upperSnakeCased = snakeCased.toUpperCase();
        // 'THIS_IS_CAMEL_CASED_AND_HYPHENATED_SNAKEADDON'

        return {
            raw: rawString,
            camelCased,
            hyphenated,
            pascalCased,
            snakeCased,
            upperSnakeCased,
        };
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * mut8s the special ibgib and does extra plumbing specific to special ibgibs.
 *
 * todo: move this into core-gib
 *
 * @returns mutated special ibgib
 */
export async function mut8SpecialIbGib<TData>({
    specialType,
    specialIbGib,
    dataToAddOrPatch,
    metaspace,
    space,
}: {
    specialType: SpecialIbGibType,
    specialIbGib?: IbGib_V1,
    dataToAddOrPatch: Partial<TData>,
    metaspace: MetaspaceService,
    space?: IbGibSpaceAny,
}): Promise<IbGib_V1> {
    const lc = `[${mut8SpecialIbGib.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 57fac28b6d7e37a0ea6496bbb26ffe23)`); }

        if (logalot) { console.log(`${lc} newly created index so initializing it... (I: 12dab62e6e164005b08b61f39093e33e)`); }

        if (!specialIbGib) {
            // if (!space) {
            //     if (logalot) { console.log(`${lc} space falsy. getting it. (I: def17546c7551965aab217033380cd23)`); }
            //     space = await metaspace.getLocalUserSpace({ lock: true });
            // }
            specialIbGib = await metaspace.getSpecialIbGib({ type: specialType, space }) ?? undefined;
        }
        if (!specialIbGib) { throw new Error(`specialIbGib falsy and couldn't get it. specialType: ${specialType} (E: 7902091494798623a24f5d1dab9bbb23)`); }
        // const dataToAddOrPatch: B2tFSIndexData_V1 = {
        //     mySpaceId: space.data.uuid,
        //     ancestorSpaceId: ancestorSpaceId ?? undefined,
        //     // activeSpaceId: , // undefined at this point
        // };
        const resNewSpecial = await mut8({
            src: specialIbGib,
            dataToAddOrPatch,
        });
        const newSpecialIbGib = resNewSpecial.newIbGib;

        // perform extra plumbing when editing special ibgib.  need to create a
        // mut8 analog to metaspace.rel8ToSpecialIbGib so this plumbing is done
        // automatically and centralized (DRY). (I copied this from that rel8ToSpecialIbGib in space-helper)

        // persist
        await metaspace.persistTransformResult({
            resTransform: resNewSpecial, space,
        });

        // return the new special address (not the incoming new ibGib)
        const newSpecialAddr = getIbGibAddr({ ibGib: newSpecialIbGib });

        // update the space ibgib which contains the special/config information
        const configKey = getSpecialConfigKey({ type: specialType });
        await metaspace.setConfigAddr({ key: configKey, addr: newSpecialAddr, space, });

        // register it creates a metastone and publishes the change to the
        // tjpAddr pubsub
        await metaspace.registerNewIbGib({ ibGib: newSpecialIbGib, space, });

        return newSpecialIbGib;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}
