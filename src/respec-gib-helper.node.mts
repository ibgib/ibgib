import { ChildProcess, exec, ExecException } from 'node:child_process';
import { execPath, cwd, } from 'node:process';

import { GLOBAL_LOG_A_LOT } from './ibgib-constants.mjs';
import { delay, extractErrorMsg } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';

const logalot: boolean | number = GLOBAL_LOG_A_LOT;

/**
 * execNode result
 */
export interface ExecNodeResultInfo {
    err: ExecException | null;
    std_out: string;
    std_err: string;
}

export async function execRCLIInNode({
    execPath,
    argsString,
    logContext,
    dontLogStdErr,
    timeoutMs = 10_000,
}: {
    execPath: string,
    argsString: string,
    logContext: string,
    dontLogStdErr?: boolean,
    timeoutMs?: number,
}): Promise<[Promise<ExecNodeResultInfo>, Promise<ChildProcess>]> {
    const lc = `${logContext}[${execRCLIInNode.name}]`;
    if (logalot) { console.time(lc); }
    try {
        if (logalot) { console.log(`${lc} starting... (may take awhile!!) (I: 56b107c56c8d23ab354ff03948b00f23)`); }

        /**
         * we're returning both the exec promise and child process. This child
         * process will be set inside the promise though.
         */
        let resChildProcess: ChildProcess | undefined = undefined;

        timeoutMs ??= 10_000;
        const execCallbackPromiseAndChildProcess = new Promise<ExecNodeResultInfo>((resolve, reject) => {
            const lcPromise = `${lc}[inner exec]`;
            try {
                const controller = new AbortController();
                const { signal } = controller;
                let complete = false;
                let timeout: NodeJS.Timeout | undefined;
                resChildProcess = exec(`node ${execPath} ${argsString}`, { signal }, (err, std_out, std_err) => {
                    if (logalot) {
                        console.log(`${lcPromise} err?: ${!!err}`);
                        console.log(`${lcPromise} cwd: ${cwd()}`);
                        console.log(`${lcPromise} BEGIN std_out:\n${std_out ?? '[falsy]'}`);
                        console.log(`${lcPromise} END std_out`);
                    }
                    if (std_err && !dontLogStdErr) {
                        console.error('\x1b[31m%s\x1b[0m', `${lcPromise} BEGIN std_err:\n${std_err ?? '[falsy]'}`);
                        console.error('\x1b[31m%s\x1b[0m', `${lcPromise} END std_err`);
                    } else if (std_err && logalot) {
                        console.log(`${lcPromise} BEGIN expected std_err:\n${std_err ?? '[falsy]'}`);
                        console.log(`${lcPromise} END expected std_err`);
                    } else if (logalot) {
                        console.log(`${lcPromise} BEGIN std_err:\n${std_err ?? '[falsy]'}`);
                        console.log(`${lcPromise} END std_err`);
                    }

                    complete = true;
                    if (timeout) { clearTimeout(timeout); }
                    resolve({ err, std_out, std_err });
                });

                if (timeoutMs > 0) {
                    timeout = setTimeout(() => {
                        if (!complete) { controller.abort(`${timeoutMs}ms timed out. (E: 3ea6f7f2df5347d98ac2de6e239002a6)`); }
                    }, timeoutMs);
                }
            } catch (error) {
                const emsg = `${lcPromise} ${extractErrorMsg(error)}`;
                console.error(emsg);
                reject(new Error(emsg));
            }
        });

        let delayCount = 0;
        while (!resChildProcess) {
            // doesn't get here afaict (11/2023). delete this while loop eventually
            delayCount++;
            console.log(`${lc} delaying waiting for child... (${delayCount})`)
            await delay(100);
        }

        return [execCallbackPromiseAndChildProcess, resChildProcess];
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
        if (logalot) { console.timeEnd(lc); }
    }
}
