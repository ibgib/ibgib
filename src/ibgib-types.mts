/**
 * @module ibgib-types
 *
 * temp here atow i think. just need a place for this global helper interface
 */

/**
 * gets variations of a raw string, camelCase, PascalCase, hyphenated and
 * snake_cased
 */
export interface CompoundStringVariations {
    /**
     * someStringHere_Yo
     */
    raw: string;
    /**
     * some-string-here-yo
     */
    hyphenated: string;
    /**
     * someStringHereYo
     */
    camelCased: string;
    /**
     * SomeStringHereYo
     */
    pascalCased: string;
    /**
     * some_string_here_yo
     */
    snakeCased: string;
    /**
     * SOME_STRING_HERE_YO
     */
    upperSnakeCased: string;
}
