import { Gib, Ib, IbGibAddr, TransformResult } from '@ibgib/ts-gib/dist/types.mjs';
import {
    IbGib_V1, ROOT, Factory_V1 as factory, Rel8n,
    IbGibRel8ns_V1, isPrimitive, IbGibData_V1, Factory_V1, rel8, mut8,
} from '@ibgib/ts-gib/dist/V1/index.mjs';

import { CommentIbGib_V1 } from '@ibgib/core-gib/dist/common/comment/comment-types.mjs';
import { TAGGED_REL8N_NAME } from '@ibgib/core-gib/dist/common/tag/tag-constants.mjs';
import {
    RobbotData_V1, RobbotRel8ns_V1, RobbotIbGib_V1,
    SemanticId, SemanticHandler,
    RobbotPropsData,
    RobbotInteractionIbGib_V1,
    SemanticInfo,
} from '@ibgib/core-gib/dist/witness/robbot/robbot-types.mjs';
import { DEFAULT_ROOT_REL8N_NAME } from '@ibgib/core-gib/dist/common/root/root-constants.mjs'

import { GLOBAL_LOG_A_LOT } from '../../../ibgib-constants.mjs';

const logalot = GLOBAL_LOG_A_LOT;


export const DEFAULT_UUID_ROLLY_ROBBOT = '';
export const DEFAULT_NAME_ROLLY_ROBBOT = 'Rolly';
export const DEFAULT_DESCRIPTION_ROLLY_ROBBOT =
    `Rolly is the default RCLI helper robbot that basically is the brains in the v1 ibgib RCLI environment.`;
export const DEFAULT_OUTPUT_PREFIX_ROLLY_ROBBOT = 'yo: ';
export const DEFAULT_OUTPUT_SUFFIX_ROLLY_ROBBOT = '--rolly';
/**
 * look rel8n names are those named edges that the robbot can travel along when
 * looking at what it's shown by the user using the :eyes: button.
 *
 * I'm trying out defaulting to empty and defaulting to pics/comments/links
 * and related (e.g. tag targets).
 *
 * The idea is that you could just say look at this one ibgib and the robbot
 * could analyze the entire sub-graph along certain edges, including those we do
 * want to see (e.g. yes look at sub comments & pics) and excluding those we
 * don't (e.g.  don't look at the dna).
 *
 * But then again, this is a more complex scenario and we may want the robbot
 * just to look at the specific ones we say. I'm thinking about this wrt to
 * analysis.
 */
// export const DEFAULT_LOOK_REL8N_NAMES_ROLLY_ROBBOT: string = [].join(',');
export const DEFAULT_LOOK_REL8N_NAMES_ROLLY_ROBBOT = [
    'pic', 'comment', 'link',
    'result', 'import',
    'tagged',
    TAGGED_REL8N_NAME,
    DEFAULT_ROOT_REL8N_NAME,
].join(',');
export const ROLLY_V1_DEFAULT_REQUEST_TEXT = 'help';

export interface RollyRobbotData_V1 extends RobbotData_V1 {
    /**
     * comma-delimited string of rel8n names.
     *
     * These are the rel8n names that the robbot can search through its own
     * ibgibs that it has seen. I'm sure that's worded poorly, so...
     *
     * For example, when you look at a comment ibgib, that ibgib may
     * have other ibgibs "inside" (rel8d) to it via various rel8n names.
     * If you want to search for comments within comments, then include
     * the 'comment' rel8n name. If you want to search for pics also,
     * include 'pic' rel8n name.
     *
     * @see {@link RollyRobbot_V1.getAllIbGibsWeCanLookAt}
     */
    lookRel8nNames: string;
}

export interface RollyRobbotRel8ns_V1 extends RobbotRel8ns_V1 {
    session?: IbGibAddr[];
}

export type RollySemanticId =
    // "semantic_done" |
    SemanticId;
export const RollySemanticId = {
    ...SemanticId,
    // done: "semantic_done" as RollySemanticId,
}


export interface RollyRobbotPropsData extends RobbotPropsData<RollySemanticId> {
}

/**
 * For use with narrowing down in lex search.
 */
export type RollyContextFlag = 'all';
