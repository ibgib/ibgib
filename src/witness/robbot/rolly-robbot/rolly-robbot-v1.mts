import {
    COMMA_DELIMITED_SIMPLE_STRINGS_REGEXP,
    COMMA_DELIMITED_SIMPLE_STRINGS_REGEXP_DESCRIPTION, DEFAULT_DATA_PATH_DELIMITER
} from '@ibgib/helper-gib/dist/constants.mjs';
import { SpeechBuilder } from '@ibgib/helper-gib/dist/helpers/lex-helper.mjs';
import {
    addTimeToDate, clone, getTimestamp, getUUID, pretty,
    getIdPool, getSaferSubstring, getTimestampInTicks, pickRandom, replaceCharAt, unique, extractErrorMsg
} from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { Gib, Ib, IbGibAddr, TransformResult } from '@ibgib/ts-gib/dist/types.mjs';
import {
    IbGib_V1, ROOT, Factory_V1 as factory, Rel8n,
    IbGibRel8ns_V1, rel8, mut8,
} from '@ibgib/ts-gib/dist/V1/index.mjs';
import { getGib, getGibInfo } from '@ibgib/ts-gib/dist/V1/transforms/transform-helper.mjs';
import { validateIbGibIntrinsically } from '@ibgib/ts-gib/dist/V1/validate-helper.mjs';
import { getIbAndGib, getIbGibAddr } from '@ibgib/ts-gib/dist/helper.mjs';
import { RobbotBase_V1 } from '@ibgib/core-gib/dist/witness/robbot/robbot-base-v1.mjs';
import {
    RobbotCmdData, RobbotCmdIbGib, RobbotCmdRel8ns,
    StimulusForRobbot,
    SemanticId, SemanticHandler,
    SemanticInfo, toLexDatums_Semantics, DEFAULT_HUMAN_LEX_DATA_ENGLISH_SEMANTICS,
    AtomicId, DEFAULT_HUMAN_LEX_DATA_ENGLISH_ATOMICS,
    RobbotInteractionIbGib_V1, RobbotInteractionType,
    ROBBOT_INTERACTION_REL8N_NAME,
    ROBBOT_SESSION_REL8N_NAME, ROBBOT_SESSION_ATOM,
    RobbotSessionIbGib_V1, RobbotSessionData_V1, RobbotSessionRel8ns_V1,
    ROBBOT_ANALYSIS_ATOM,
    DEFAULT_ROBBOT_REQUEST_ESCAPE_STRING,
    SemanticHandlerResult,
} from '@ibgib/core-gib/dist/witness/robbot/robbot-types.mjs';
import {
    getInteractionIbGib_V1, getSpaceDelimitedSaferRequestText,
    getRobbotIb, getRobbotSessionIb, isRequestComment,
    parseInteractionIb, RobbotFormBuilder,
} from '@ibgib/core-gib/dist/witness/robbot/robbot-helper.mjs';
import { getGraphProjection, GetGraphResult } from '@ibgib/core-gib/dist/common/other/graph-helper.mjs';
import { CommentIbGib_V1 } from '@ibgib/core-gib/dist/common/comment/comment-types.mjs';
import { getFromSpace, getLatestAddrs } from '@ibgib/core-gib/dist/witness/space/space-helper.mjs';
import { getTjpAddr } from '@ibgib/core-gib/dist/common/other/ibgib-helper.mjs';
import { isComment } from '@ibgib/core-gib/dist/common/comment/comment-helper.mjs';
import { IbGibSpaceAny } from '@ibgib/core-gib/dist/witness/space/space-base-v1.mjs';
import { DynamicForm } from '@ibgib/core-gib/dist/common/form/form-items.mjs';
import { DynamicFormFactoryBase } from '@ibgib/core-gib/dist/witness/factory/dynamic-form-factory-base.mjs';
import { WitnessFormBuilder } from '@ibgib/core-gib/dist/witness/witness-form-builder.mjs';
import { DynamicFormBuilder } from '@ibgib/core-gib/dist/common/form/form-helper.mjs';
import { WitnessArgIbGib } from '@ibgib/core-gib/dist/witness/witness-types.mjs';
import { DEFAULT_ROBBOT_TARGET_REL8N_NAME } from '@ibgib/core-gib/dist/witness/robbot/robbot-constants.mjs';

import {
    DEFAULT_DESCRIPTION_ROLLY_ROBBOT, DEFAULT_LOOK_REL8N_NAMES_ROLLY_ROBBOT,
    DEFAULT_NAME_ROLLY_ROBBOT, DEFAULT_UUID_ROLLY_ROBBOT,
    DEFAULT_OUTPUT_PREFIX_ROLLY_ROBBOT, DEFAULT_OUTPUT_SUFFIX_ROLLY_ROBBOT,
    RollyContextFlag, RollyRobbotData_V1,
    RollyRobbotPropsData,
    RollyRobbotRel8ns_V1,
    RollySemanticId,
    ROLLY_V1_DEFAULT_REQUEST_TEXT,
} from './rolly-robbot-types.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../ibgib-constants.mjs';
const logalot: boolean | number = GLOBAL_LOG_A_LOT;


/**
 *
 */
export class RollyRobbot_V1 extends RobbotBase_V1<
    // lex props type
    RollyRobbotPropsData,
    // in
    any, IbGibRel8ns_V1, IbGib_V1<any, IbGibRel8ns_V1>,
    // out
    any, IbGibRel8ns_V1, IbGib_V1<any, IbGibRel8ns_V1>,
    // this
    RollyRobbotData_V1, RollyRobbotRel8ns_V1
> {
    protected lc: string = `[${RollyRobbot_V1.name}]`;

    protected _brainLookProjection: GetGraphResult | undefined;
    /**
     * When we go to get the current working ibgib, if there are none available, then this
     * will be set.
     */
    protected nothingToWorkOnAvailable: boolean = false;
    protected _brainCommentIbGibs: CommentIbGib_V1[] = [];

    protected session: RobbotSessionIbGib_V1 | undefined;
    protected sessionInteractions: RobbotInteractionIbGib_V1[] = [];
    /**
     * syntactic sugar for getting the most recent interaction.
     *
     * @returns most recent interaction if there are any, else returns undefined
     */
    protected get prevInteractionInSession(): RobbotInteractionIbGib_V1 | undefined {
        return this.sessionInteractions?.length > 0 ?
            this.sessionInteractions[this.sessionInteractions.length - 1] :
            undefined;
    }

    protected cachedLatestAddrsMap: { [addr: string]: string | null } = {};

    protected alreadyHandledContextChildrenAddrs: IbGibAddr[] = [];

    /**
     * The last handler to signal that it is expecting a response.
     */
    protected handlerExpectingResponse: SemanticHandler | undefined;

    // debugInterval: any;

    constructor(initialData?: RollyRobbotData_V1, initialRel8ns?: RollyRobbotRel8ns_V1) {
        super(initialData, initialRel8ns); // calls initialize
        const lc = `${this.lc}[ctor]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            // console.log(`${lc} still what?`)
            // getUUID().then(id => {
            // this.debugInterval = setInterval(() => {
            //     console.log(`${lc} ${id.substring(0, 8)} wordy robbot still kicking...`)
            // }, 3000);
            // })
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * Initializes to default space values.
     */
    protected async initialize(): Promise<void> {
        const lc = `${this.lc}[${this.initialize.name}]`;
        try {
            await super.initialize();

            if (logalot) { console.log(`${lc} starting...`); }
            if (!this.data) { this.data = clone(DEFAULT_ROLLY_ROBBOT_DATA_V1); }
            if (!this.rel8ns && DEFAULT_ROLLY_ROBBOT_REL8NS_V1) {
                this.rel8ns = clone(DEFAULT_ROLLY_ROBBOT_REL8NS_V1);
            }
            this.gib = await getGib({ ibGib: this });
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async initialize_lex(): Promise<void> {
        const lc = `${this.lc}[${this.initialize_lex.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 1bc666f643414efb850f688fee9c8403)`); }
            await super.initialize_lex();
            if (!this._robbotLex) { throw new Error(`robbotLex not initialized (E: 3cc7d2128a2e4254a5d829145cc71aee)`); }

            this._robbotLex.data[SemanticId.hello] = [
                {
                    texts: [
                        `$(hi)!`,
                        `It looks like I haven't seen anything yet.`,
                        `Navigate somewhere and let me look at that ibgib by clicking 👀.`,
                    ],
                    props: {
                        semanticId: SemanticId.hello,
                        blankSlate: true,
                    },
                },
                {
                    texts: [
                        `$(hi).`,
                        `What do we want to do? You can give me requests like 'learn', 'hoogle', 'boogle'.`,
                        `Just prefix it with "${DEFAULT_ROBBOT_REQUEST_ESCAPE_STRING}"`,
                        `For a full list of requests available, try "list" or "ls". Or you can also request 'help'.`,
                    ],
                    props: {
                        semanticId: SemanticId.hello,
                        onlyInSession: true,
                        freshStart: true, // explicit for readability
                    },
                },
                {
                    texts: [
                        `$(hi).`,
                        `$(session_in_progress)`,
                        `$(${SemanticId.ready})`,
                    ],
                    props: {
                        semanticId: SemanticId.hello,
                        onlyInSession: true,
                        freshStart: false, // explicit for readability
                    },
                },
            ];
            this._robbotLex.data['session_in_progress'] = [
                {
                    texts: [
                        'Session is in progress...',
                    ],
                }
            ];
            this._robbotLex.data[SemanticId.help] = [
                {
                    texts: [
                        `$(hi)! I'm $name, a Rolly Robbot here to help you use the ibgib RCLI.`,
                        ``,
                        `Now unlike most chatbots, I ain't a genie and your wish isn't my command.`,
                        `But I do take requests. I know a request when you start your statement with a '${DEFAULT_ROBBOT_REQUEST_ESCAPE_STRING}', like...`,
                        `$requestExamples`,
                        ``,
                        `For a complete list, just say '${DEFAULT_ROBBOT_REQUEST_ESCAPE_STRING}list' or '${DEFAULT_ROBBOT_REQUEST_ESCAPE_STRING}requests'.`,
                    ],
                    props: {
                        semanticId: SemanticId.help,
                        templateVars: `name,requestExamples`,
                    }
                }
            ];
            this._robbotLex.data[SemanticId.ready] = [
                ...toLexDatums_Semantics(SemanticId.ready, [
                    'I\'m ready.', 'I\'m awake.',
                ])
            ];
            this._robbotLex.data[SemanticId.list] = [
                {
                    texts: [
                        `Here are what requests are available right now:`,
                        `$requests`,
                    ],
                    props: {
                        semanticId: SemanticId.list,
                        templateVars: `requests`,
                    }
                }
            ];

            if (!this._userLex) { throw new Error(`userLex not initialized (E: d48f401054f24ed4b747d01fef4d723c)`); }

            this._userLex.data[SemanticId.hello] = [
                ...DEFAULT_HUMAN_LEX_DATA_ENGLISH_ATOMICS[AtomicId.hi].flatMap(datum => {
                    return {
                        texts: datum.texts?.concat(),
                        props: {
                            semanticId: SemanticId.hello,
                            isRequest: true,
                        } as RollyRobbotPropsData,
                    }
                }),
            ];
            this._userLex.data[SemanticId.help] = [
                ...DEFAULT_HUMAN_LEX_DATA_ENGLISH_SEMANTICS[SemanticId.help].flatMap(datum => {
                    return {
                        texts: datum.texts?.concat(),
                        props: {
                            semanticId: SemanticId.help,
                            isRequest: true,
                        } as RollyRobbotPropsData,
                    }
                }),
            ];
            this._userLex.data[SemanticId.list] = [
                ...[`list`, `ls`, `requests`, `reqs`].map(text => {
                    return {
                        texts: [text],
                        props: {
                            semanticId: SemanticId.list,
                            isRequest: true,
                        } as RollyRobbotPropsData,
                    };
                }),
            ];
            this._userLex.data[SemanticId.stop] = [
                ...[`stop`, `cancel`, `abort`].map(text => {
                    return {
                        texts: [text],
                        props: {
                            semanticId: SemanticId.stop,
                            isRequest: true,
                        } as RollyRobbotPropsData,
                    };
                }),
            ];

        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async initialize_semanticHandlers(): Promise<void> {
        const lc = `${this.lc}[${this.initialize_semanticHandlers.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 67f631100d084b2aa289e8ab24fe1b93)`); }

            super.initialize_semanticHandlers();
            const superHandlers = { ...this._semanticHandlers };

            this._semanticHandlers = {
                ...superHandlers,

                [SemanticId.hello]: [
                    {
                        handlerId: '4500b5bfd913458c938972da85e78e94',
                        semanticId: SemanticId.hello,
                        fnCanExec: async (info) => !this.nothingToWorkOnAvailable && !this.prevInteractionInSession,
                        fnExec: (info) => this.handleSemantic_hello_freshStart(info),
                    },
                    {
                        handlerId: '97430b85f9a4486da1d6ead87726108e',
                        semanticId: SemanticId.hello,
                        fnCanExec: async (info) => !this.nothingToWorkOnAvailable && !!this.prevInteractionInSession,
                        fnExec: (info) => this.handleSemantic_hello_continue(info),
                    },
                    {
                        handlerId: '8cc6ab10316c4386962e869df7387260',
                        semanticId: SemanticId.hello,
                        fnCanExec: async (info) => this.nothingToWorkOnAvailable,
                        fnExec: (info) => this.handleSemantic_NothingToWorkOn(info),
                    },
                ],
                [SemanticId.list]: [
                    {
                        handlerId: '10d7f575a5d947c7bfd64badc15eb00a',
                        semanticId: SemanticId.list,
                        fnCanExec: async (info) => true,
                        fnExec: (info) => this.handleSemantic_list(info),
                    },
                ],
                [SemanticId.help]: [
                    {
                        handlerId: '3ff55538d7264859adcb92977e799171',
                        semanticId: SemanticId.help,
                        fnCanExec: async (info) => true,
                        fnExec: (info) => this.handleSemantic_help(info),
                    },
                ],
                [SemanticId.stop]: [
                    {
                        handlerId: 'b316fdbf177b4645a3578d6b76e12495',
                        semanticId: SemanticId.stop,
                        fnCanExec: (info) => this.canHandleSemantic_stop(info),
                        fnExec: (info) => this.handleSemantic_stop(info),
                    },
                ],
                // more handlers here
            }
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async finalizeContext({
        arg,
    }: {
        // arg: RobbotCmdIbGib<IbGib_V1, RobbotCmdData, RobbotCmdRel8ns>,
        arg: WitnessArgIbGib<IbGib_V1, RollyRobbotData_V1, RollyRobbotRel8ns_V1> | undefined,
    }): Promise<void> {
        const lc = `${this.lc}[${this.finalizeContext.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: d9973fd4232d45d6802996a24996f350)`); }

            await this.completeSessionIfNeeded({ sayBye: false });

            // if (this.debugInterval) {
            //     clearInterval(this.debugInterval);
            //     delete this.debugInterval;
            // }

            return await super.finalizeContext({ arg });
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    // #region semantic handlers

    /**
     * We have started a new session, atow this is only when we click the Gib (chat) button.
     */
    private async handleSemantic_hello_freshStart(info: SemanticInfo): Promise<SemanticHandlerResult> {
        const lc = `${this.lc}[${this.handleSemantic_hello_freshStart.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: ee8525057fb145ea84742eed1cc64709)`); }

            let hello = this._robbotLex!.get(SemanticId.hello, {
                props: props =>
                    props.semanticId === SemanticId.hello &&
                    props.onlyInSession === true &&
                    props.freshStart === true,
            });
            if (!hello) {
                console.warn(`${lc} hello falsy? (W: 86c427a79b094c638d6667a200d091c1)`);
                hello = {
                    text: 'hello',
                    ssml: 'hello',
                    datum: {},
                    rawData: [],
                };
            }

            // get the first stimulation for the current comment
            const sb = SpeechBuilder.with()
                .text(hello.text)
                // .newParagraph()
                ;

            const data = await this.getRobbotInteractionData({
                type: RobbotInteractionType.greeting,
                commentText: sb.outputSpeech({}).text!,
            });

            const interaction = await getInteractionIbGib_V1({ data });
            return { interaction };
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    private async handleSemantic_hello_continue(info: SemanticInfo): Promise<SemanticHandlerResult> {
        const lc = `${this.lc}[${this.handleSemantic_hello_continue.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 82eaa62218e04bef918d21458d06d4ec)`); }

            let hello = this._robbotLex!.get(SemanticId.hello, {
                props: props =>
                    props.semanticId === SemanticId.hello &&
                    props.onlyInSession === true &&
                    !props.freshStart,
            });
            if (!hello) {
                console.warn(`${lc} hello falsy? (W: 2597593b5f264ff8ad5d9815eaded698)`);
                hello = {
                    text: 'hello',
                    ssml: 'hello',
                    datum: {},
                    rawData: [],
                };
            }

            const data = await this.getRobbotInteractionData({
                type: RobbotInteractionType.info,
                commentText: hello.text,
            });

            const interaction = await getInteractionIbGib_V1({ data });
            return { interaction };
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * Catchall when there is nothing to work on.
     */
    private async handleSemantic_NothingToWorkOn(info: SemanticInfo): Promise<SemanticHandlerResult> {
        const lc = `${this.lc}[${this.handleSemantic_NothingToWorkOn.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 1d31e703bfb84ec4825402a62e798ef5)`); }

            let hello = this._robbotLex!.get(SemanticId.hello, {
                props: props =>
                    props.semanticId === SemanticId.hello &&
                    props.blankSlate === true,
            });
            if (!hello) {
                console.warn(`${lc} hello falsy? (W: 0794065006a74a26aa0ba1832d1ea1b3)`);
                hello = {
                    text: 'hello',
                    ssml: 'hello',
                    datum: {},
                    rawData: [],
                };
            }

            const data = await this.getRobbotInteractionData({
                type: RobbotInteractionType.info,
                commentText: hello.text,
            });

            const interaction = await getInteractionIbGib_V1({ data });
            return { interaction };
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     *
     *
     * @returns e.g. ['help: help, help please, help me, ...', 'hi: hi, hello, greetings, ...', ... ]
     */
    private async getUserRequestTexts({
        contextFlags = ['all'],
        examplesOnly = false,
    }: {
        /**
         * NOT IMPL YET - Flags that indicates if only requests that are specific to the current context.
         * For example, if you're in the middle of a lines exercise, it wouldn't make sense to include
         * the 'hi' request, even though this would
         */
        contextFlags?: RollyContextFlag[],
        examplesOnly?: boolean,
    } = {
            contextFlags: ['all'],
            examplesOnly: false,
        }
    ): Promise<string[]> {
        const lc = `${this.lc}[${this.getUserRequestTexts.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 16f717bb875a444bb3fbff49016a5702)`); }

            if (contextFlags?.length !== 1 && contextFlags[0] !== 'all') { throw new Error(`only contextFlags === ['all'] implemented atm (E: 1614fb4082c347838a724586b2879dff)`); }

            /**
             * each entry is a mapping of "bare" semantic id => array of request synonyms
             *
             * @example [ ["list", ["list", "ls", "requests"]], ["hello", "hello", "hi", "greetings"] ]

             */
            let requestEntries: [string, string[]][] =
                Object.values(this._userLex!.data)
                    .filter(datas => datas.some(x => x.props?.isRequest && !!x.props.semanticId))
                    .map(x => {
                        let requests = x.filter(datum => datum.props?.isRequest && !!datum.props.semanticId);
                        if (requests.length === 0) { throw new Error(`(UNEXPECTED) my logic is wrong...expected at least one request after filter... ? (E: c351fc438af74179a661d5c93c06adae)`); }
                        const semanticIds = unique(requests.map(x => x.props!.semanticId));
                        if (semanticIds.length !== 1) { throw new Error(`inconsistent user lex data. expected all requests to have the same semantic id. semanticIds: ${semanticIds.join(', ')} (E: 0575b5e567f24b78b6022be66e869ca9)`); }
                        const semanticId = semanticIds[0]; // e.g. "semantic_hello"
                        const requestName = semanticId!.slice("semantic_".length); // e.g. "hello"
                        const requestAliases = requests.map(x => x.texts![0]);
                        // e.g. ["list", ["list", "ls", "requests"]]
                        return [requestName, requestAliases];
                    });
            let requestsText: string[];
            if (examplesOnly) {
                requestsText = requestEntries
                    .filter(([reqName, reqAliases]) => {
                        return reqAliases.some(reqAlias => ['hello', 'help', 'list'].includes);
                    })
                    .map(([reqName, reqAliases]) => {
                        return `${reqName}: ${reqAliases.join(',')}`; // e.g. "hello: hello, hi, hey, hey there, hi there"
                    });
            } else {
                requestsText = requestEntries.map(([reqName, reqAliases]) => {
                    return `${reqName}: ${reqAliases.join(',')}`; // e.g. "hello: hello, hi, hey, hey there, hi there"
                });
            }
            return requestsText;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    private async handleSemantic_list(info: SemanticInfo): Promise<SemanticHandlerResult> {
        const lc = `${this.lc}[${this.handleSemantic_list.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 5e1bf4e0511445619edd560e25bd10fd)`); }

            const requestTexts = await this.getUserRequestTexts({ contextFlags: ['all'] });

            // get the list lex and use template var
            const speech = this._robbotLex!.get(SemanticId.list, {
                props: props =>
                    props.semanticId === SemanticId.list,
                vars: { requests: requestTexts.join('\n') },
            });
            if (!speech) { throw new Error(`(UNEXPECTED) speech falsy? (E: d2f6539a7ef649588715a5215a508754)`); }

            const data = await this.getRobbotInteractionData({
                type: RobbotInteractionType.info,
                commentText: speech.text,
            });

            const interaction = await getInteractionIbGib_V1({ data });
            return { interaction };
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }
    private async handleSemantic_help(info: SemanticInfo): Promise<SemanticHandlerResult> {
        const lc = `${this.lc}[${this.handleSemantic_help.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 39ae2d172662437484782801006f79f5)`); }

            let requestTexts = await this.getUserRequestTexts({ contextFlags: ['all'], examplesOnly: true });
            // const requestsText = requestEntries.map(([reqName, reqAliases]) => {
            //     return `${reqName}: ${reqAliases.join(',')}`; // e.g. "hello: hello, hi, hey, hey there, hi there"
            // });
            // get the list lex and use template var
            const speech = this._robbotLex!.get(SemanticId.help, {
                props: props =>
                    props.semanticId === SemanticId.help,
                vars: {
                    name: this.data!.name,
                    requestExamples: requestTexts.join('\n'),
                },
            });
            if (!speech) { throw new Error(`(UNEXPECTED) speech falsy? couldn't get from robbotLex (E: 1ca8c29c3b4e2a8e92de1041db5f7223)`); }

            const data = await this.getRobbotInteractionData({
                type: RobbotInteractionType.info,
                commentText: speech.text,
            });

            const interaction = await getInteractionIbGib_V1({ data });
            return { interaction };
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    private async getPreviousInteractions({
        ibGib
    }: {
        ibGib: IbGib_V1
    }): Promise<RobbotInteractionIbGib_V1[]> {
        const lc = `${this.lc}[${this.getPreviousInteractions.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: cab20eef521e4873b33e1ae54fd74031)`); }
            // each stimulation interaction has a subject tjpgib in its ib field.

            const tjpGib = getGibInfo({ gib: ibGib.gib }).tjpGib;
            if (!tjpGib) { throw new Error(`(UNEXPECTED) tjpGib is falsy? (E: 8e22b3026962ce68575442af8f3b2323)`); }
            const interactionAddrs = (this.rel8ns![ROBBOT_INTERACTION_REL8N_NAME] ?? [])
                .filter(ibGibAddr => {
                    let { ib } = getIbAndGib({ ibGibAddr });
                    let info = parseInteractionIb({ ib }); // also validates the ib
                    return info.subjectTjpGibs?.includes(tjpGib);
                });
            if (interactionAddrs.length > 0) {
                if (logalot) { console.log(`${lc} YES, there ARE interaction addrs for the given ibGib, so get those interactions from the local space. (I: bd5ee68aa93e46ef98fcd0939abe7bc5)`); }
                let resGetInteractions = await this.ibgibsSvc!.get({ addrs: interactionAddrs });
                if (resGetInteractions.success && resGetInteractions.ibGibs?.length === interactionAddrs.length) {
                    const gottenIbGibs = resGetInteractions.ibGibs ?? [];
                    for (let i = 0; i < gottenIbGibs.length; i++) {
                        const gottenIbGib = gottenIbGibs[i];
                        let errors = await validateIbGibIntrinsically({ ibGib: gottenIbGib });
                        if ((errors?.length ?? 0) > 0) { throw new Error(`gotten interaction ibgib has validation errors: ${errors} (E: 983bcb9afefb48a0b556b120922609c9)`); }
                    }
                    return gottenIbGibs as RobbotInteractionIbGib_V1[];
                } else {
                    console.error(`${lc} full result: `);
                    console.dir(resGetInteractions);
                    throw new Error(`problem with getting interaction ibgibs (E: fd725a9736904545aa5f17674e1f4f85)`);
                }
            } else {
                if (logalot) { console.log(`${lc} NO, there are NOT any previous interactions (I: 058db4608fe7422282d2c263bf141b1e)`); }
                return [];
            }
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * loads this._currentWorkingComment for lines stimulation based on info.
     *
     * ## requirements
     *
     * In this case, we are loading a comment to stimulate with lines.
     *
     * ## future todo
     *
     * * check for scheduled comments
     */
    private async loadNextWorkingComment_lines({ info }: { info: SemanticInfo }): Promise<void> {
        const lc = `${this.lc}[${this.loadNextWorkingComment_lines.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 198757f179e54c5385172360f920594f)`); }

        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    private async canHandleSemantic_stop(info: SemanticInfo): Promise<boolean> {
        const lc = `${this.lc}[${this.canHandleSemantic_stop.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: f163c7679a1149a48b3219862a041b37)`); }
            if (this.nothingToWorkOnAvailable) {
                console.log(`${lc} nothing to work on. returning false. (I: 499a44f084be473d8a3d8c48762f7bce)`)
                return false;
            }
            return !!this.handlerExpectingResponse;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    private async handleSemantic_stop(info: SemanticInfo): Promise<SemanticHandlerResult> {
        const lc = `${this.lc}[${this.handleSemantic_stop.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 8e50c4c101604f0f9bd229c3df77a023)`); }

            // do stop things...
            throw new Error(`not impl (E: 960f4776773e4f4fe41f9b1cc52e0923)`);
            // // cancel stimulation
            // if (this.handlerExpectingResponse?.fnCancelResponse) {
            //     if (logalot) { console.log(`${lc} calling cancel for handler (${this.handlerExpectingResponse.semanticId}, ${this.handlerExpectingResponse.handlerId}) (I: 555f4f4a598548c5abb5dc3a2251e16e)`); }
            //     try {
            //         await this.handlerExpectingResponse.fnCancelResponse();
            //     } catch (error) {
            //         console.error(`${lc} there was an error when trying to cancel. ${error.message ?? 'unknown error'} (E: 6981e4e0c7ca45edb22d2ed45824b115)`)
            //     }
            // }
            // delete this.handlerExpectingResponse;

            // what else? todo: extra stimulation cancellation when stop issued?...

            const speech = this._robbotLex!.get(SemanticId.stop, {
                props: props =>
                    props.semanticId === SemanticId.stop,
            });
            if (!speech) { throw new Error(`(UNEXPECTED) speech falsy (E: 581613c559c6c76151e8c18ddf378323)`); }

            const data = await this.getRobbotInteractionData({
                type: RobbotInteractionType.info,
                commentText: speech!.text,
                expectingResponse: false,
            });

            const interaction = await getInteractionIbGib_V1({ data });
            return { interaction };
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }
    // #endregion semantic handlers

    protected async doCmdActivate({
        arg,
    }: {
        arg: RobbotCmdIbGib,
    }): Promise<IbGib_V1> {
        const lc = `${this.lc}[${this.doCmdActivate.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            let result = await super.doCmdActivate({ arg });

            await this.initialized;

            await this.completeSessionIfNeeded({ sayBye: false });
            await this.initializeContext({
                arg: (arg as any),
                rel8nName: this.data?.defaultRel8nName ?? 'comment',
            });
            if (!this._brainCommentIbGibs) { await this.initializeBrain(); }
            await this.startSession();

            return result;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    // protected async doCmdDeactivate({
    //     arg,
    // }: {
    //     arg: RobbotCmdIbGib,
    // }): Promise<IbGib_V1> {
    //     const lc = `${this.lc}[${this.doCmdDeactivate.name}]`;
    //     try {
    //         if (logalot) { console.log(`${lc} starting...`); }
    //         let result = await super.doCmdDeactivate({ arg });
    //         return result;
    //     } catch (error) {
    //         console.error(`${lc} ${extractErrorMsg(error)}`);
    //         throw error;
    //     } finally {
    //         if (logalot) { console.log(`${lc} complete.`); }
    //     }
    // }

    protected async doDefault({
        ibGib,
    }: {
        ibGib: IbGib_V1,
    }): Promise<IbGib_V1> {
        const lc = `${this.lc}[${this.doDefault.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            await this.rel8To({ ibGibs: [ibGib] });
            return ROOT;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * In this robbot, the ib command looks at given ibGib(s) and remembers
     * it/them (i.e. the robbot rel8s the ibgibs to itself).
     *
     * @returns ROOT if successful, else throws
     */
    protected async doCmdIb({
        arg,
    }: {
        arg: RobbotCmdIbGib<IbGib_V1, RobbotCmdData, RobbotCmdRel8ns>,
    }): Promise<IbGib_V1> {
        const lc = `${this.lc}[${this.doCmdIb.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }

            await this.initialized;

            if (!arg.ibGibs) { throw new Error(`arg.ibGibs required (E: dc8be6a06d4dcc953f536bb1a8697e23)`); }

            await this.lookAt({ ibGibs: arg.ibGibs });

            await this.initializeBrain(); // shotgun approach here...

            return ROOT;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * Start speaking...
     *
     * Using our analysis, provide a comment based on what we've said in the past.
     *
     * ## notes
     *
     * there are many possibilities for this.
     *
     * * show individual words
     * * show individual words and a random neighbor of that word
     * * show some % of common words vs % uncommon words within texts
     *   * "common" WRT individual ibgib texts or aggregate corpus
     * * show phrases with blanked out words
     * * show words with blanked out letters
     * * show paragraphs with blanked out lines
     *
     * * show based on a fixed time schedule
     * * show % review vs already shown
     * * somehow hook up NN (just listing here for sngs)
     * * hard code strategy in robbot
     * * configurable strategy but robbot-pervasive
     * * configurable at time of review/speaking.
     *
     * ### cases
     *
     * * comments
     *   * word/phrase/song/poem in english/foreign language
     *   * meaning/translation in same comment
     *   * meaning/translation in subcomment
     *   * word/phrase/song/poem meaning/reverse translation in parent comment
     * * link to url (non-comment link ibgib)
     *
     *
     * ### simplified first run
     *
     * 1. choose a source ibgib
     *   a. keep track of source ibgibs
     *   b. keep track of interpreted metadata per source ibgib
     *   c. choose based on interpreted data and settings of robbot.
     * 2. stimulate ibgib connection
     *   a. choose strategy based on source ibgib and its metadata
     * 3. listen for response
     *   a. if user "spoke", interpret based on state
     *   b. handle if robbot spoke
     * 4. repeat #2 or end conversation with summary
     *
     * @returns ROOT if all goes well, otherwise throws an error.
     */
    protected async doCmdGib({
        arg,
    }: {
        arg: RobbotCmdIbGib<IbGib_V1, RobbotCmdData, RobbotCmdRel8ns>,
    }): Promise<IbGib_V1> {
        const lc = `${this.lc}[${this.doCmdGib.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: ed7a1ab7cfff4a45908e409ab06a1787)`); }

            await this.initialized;

            await this.completeSessionIfNeeded({ sayBye: true });
            await this.initializeContext({
                arg: arg as any,
                rel8nName: this.data?.defaultRel8nName ?? 'comment',
            });
            if (!this._brainCommentIbGibs) { await this.initializeBrain(); }
            await this.startSession();
            await this.promptNextInteraction({ isClick: true });

            // const textInfo = this.getTextInfo({ srcIbGib: this._currentWorkingComment });

            // if (textInfo.lines.length === 1) {
            //     // just a single line comment, do a fill-in-the-blank for now and clear the current ibgib.
            //     let words = textInfo.text
            //         .split(/\b/) // split into words
            //         .map(x => x.trim()) // trim those that are just blank spaces
            //         .filter(x => x.length > 2)
            //         ; // filter out those blank space trimmed, now-empty strings
            //     if (words.length > 0) {
            //         // we have a decent enough word
            //         let aWord = pickRandom({ x: words });
            //         let textWithBlank = textInfo.text.replace(aWord, '______');
            //         await this.createCommentAndRel8ToContextIbGib({ text: textWithBlank, contextIbGib: this._currentWorkingContextIbGib });
            //     } else {
            //         // only short words or just empty space
            //     }

            //     // since it's just a one liner, we're done working this particular comment.
            //     delete this._currentWorkingComment;
            // } else if (textInfo.paragraphs.length === 1) {
            //     // multiple lines, single paragraph
            // } else {
            //     // multiple lines & paragraphs
            // }

            return ROOT;

        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }

    }

    /**
     * In this robbot, the ibgib command will think (analyze known ibgibs) and
     * prepare what it's going to say next (its future output ibgib(s)).
     *
     * for this, we need
     *   * to create intermediate ibgibs
     *   * track which ibgibs (timelines?) we've analyzed
     *   * track which ibgibs we've chunked
     *   * keep stats
     *     * count internal number of words
     *     * count internal number of unique words
     *     * count internal number of phrases
     *     * ...
     *
     * So we need to analyze, create the stats, reassess, shake things up,...
     * hmm...
     * output the stats and internal state
     */
    protected async doCmdIbgib({
        arg,
    }: {
        arg: RobbotCmdIbGib<IbGib_V1, RobbotCmdData, RobbotCmdRel8ns>,
    }): Promise<IbGib_V1> {
        const lc = `${this.lc}[${this.doCmdIbgib.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: f1fb94e51214476ba919f43a695ade65)`); }

            await this.initialized;

            // const space = await this.ibgibsSvc.getLocalUserSpace({ lock: true });
            if (!this._brainCommentIbGibs) { await this.initializeBrain(); }

            // at this point, we should have all of the related ibgibs that we
            // care about loaded into this.cacheIbGibs and this.cachedLatestAddrsMap is populated.
            // we should be able to do any analysis on them that we wish.

            return ROOT;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * This is like loading a scene in a video game. We are initializing what
     * the robbot knows insofar as ibgibs that it has seen and analyses.
     */
    private async initializeBrain(): Promise<void> {
        const lc = `${this.lc}[${this.initializeBrain.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 4eadb117b5564cb195e1570c1a8b6430)`); }

            delete this._brainLookProjection;

            const space = await this.ibgibsSvc!.getLocalUserSpace({ lock: true });
            if (!space) { throw new Error(`(UNEXPECTED) couldn't get local user space? space falsy (E: 3b9a33414d6b350dc5950d6e52967323)`); }

            if (!this._brainLookProjection) {
                this._brainLookProjection = await this.getAllIbGibsWeCanLookAt({ space });
            }
            if (!this._brainLookProjection) { throw new Error(`(UNEXPECTED) unable to get current working look projection? (E: 3398f425f33e436c9560590391f4ad27)`); }

            if (!this._brainCommentIbGibs) {
                this._brainCommentIbGibs = await this.getCommentIbGibs({
                    lookProjection: this._brainLookProjection,
                });
            }
            if (!this._brainCommentIbGibs) { throw new Error(`(UNEXPECTED) unable to get current working comment ibgibs? (E: ccdaa1428b544620a4320670533518f9)`); }
            if (this._brainCommentIbGibs.length === 0) {
                if (logalot) { console.info(`${lc} nothing available to work on (haven't looked at any ibgibs) (I: 3142a6914af742cd9ca215129f810510)`); }
                this.nothingToWorkOnAvailable = true;
            } else {
                if (logalot) { console.info(`${lc} we do have comment ibgibs in our brain (that we have looked at). (I: a993b5d6f09f4f3e9100dac0de5e1d91)`); }
                this.nothingToWorkOnAvailable = false;
            }

            // check for any currently scheduled

            // when choosing a comment, we should know what comments need choosing...
            // or pick random one at start...hmmm
            // this._currentWorkingComment =
            //     pickRandom({ x: this._brainCommentIbGibs });
            // if (logalot) { console.log(`${lc} current working comment set. addr: ${getIbGibAddr({ ibGib: this._currentWorkingComment })} (I: c7c936b27df6441d83a027ce2664a6c0)`); }
            // this._currentWorkingCommentTjpAddr =
            //     getTjpAddr({ ibGib: this._currentWorkingComment, defaultIfNone: 'incomingAddr' });
            // load/create the analysis for the current working comment

            // this._currentWorkingCommentAnalysis =
            //     await this.getTextAnalysisForIbGib({ ibGib: this._currentWorkingComment, createIfNone: true });
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async handleNewContextChild({ newChild }: { newChild: IbGib_V1 }): Promise<void> {
        const lc = `${this.lc}[${this.handleNewContextChild.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 49a9adea4a804cfeacc7ace2c8f29678)`); }

            const addr = getIbGibAddr({ ibGib: newChild });
            if (this.alreadyHandledContextChildrenAddrs.includes(addr)) {
                if (logalot) { console.log(`${lc} already handled, returning early: ${addr} (I: 23b0988bfccd4c0d9ef08789c3542be5)`); }
                return; /* <<<< returns early */
            } else {
                this.alreadyHandledContextChildrenAddrs.push(addr);
            }

            if (this.isMyComment({ ibGib: newChild })) {
                if (logalot) { console.log(`${lc} my comment, returning early: ${addr} (I: 7db101d240874297b7f4eb1ecb9d3c6c)`); }
                return; /* <<<< returns early */
            }

            // if it's not a comment, then we're just going to look at it/remember it,
            // i.e. add it to our rel8d ibgibs. if it IS a comment, then we need to
            // handle it depending on what our state is. Usually it's either a request for
            // us or an answer/response to something we've done.
            if (isRequestComment({ ibGib: newChild, requestEscapeString: this.data!.requestEscapeString })) {
                await this.promptNextInteraction({ ibGib: newChild, isRequest: true });
            } else if (isComment({ ibGib: newChild }) && this.session) {
                if (!!this.handlerExpectingResponse) {
                    // in the middle of a session and someone else's comment has come to us
                    // there will be an issue if the robbot chooses to import a request ibgib...hmm
                    await this.promptNextInteraction({ ibGib: newChild, isRequest: false });
                } else {
                    if (logalot) { console.log(`${lc} new nonRequest ibgib detected, but this.expectingResponse is false. ignoring. (I: 63ca184372a34896b583a32f6c3bfc0f)`); }
                    return; /* <<<< returns early */
                }
            } else {
                // not a request and not a comment during a session
                console.warn(`${lc} (UNEXPECTED) hmm, i thought this would only trigger while a session is in play... (W: 5)`)
            }
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    private async getCommentIbGibs({
        lookProjection,
    }: {
        lookProjection: GetGraphResult,
    }): Promise<CommentIbGib_V1[]> {
        const lc = `${this.lc}[${this.getCommentIbGibs.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 47fe9dd70f6343a586b1b3d109a334f0)`); }

        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
        const commentIbGibs = Object.keys(lookProjection)
            .filter(addr => addr.startsWith('comment '))
            .map(addr => lookProjection[addr] as CommentIbGib_V1);
        const commentAddrs = commentIbGibs.map(ibGib => getIbGibAddr({ ibGib }));

        if (commentAddrs.length === 0) { return []; /* <<<< returns early */ }

        let space = await this.ibgibsSvc!.getLocalUserSpace({ lock: true });
        if (!space) { throw new Error(`(UNEXPECTED) couldn't get localUserSpace? (E: c87663c94adb38d3e97314310b37b723)`); }

        let resGetLatestAddrs = await getLatestAddrs({ ibGibs: commentIbGibs, space });
        const { latestAddrsMap } = resGetLatestAddrs.data!;
        if (!latestAddrsMap) { throw new Error(`(UNEXPECTED) latestAddrsMap not returned? (E: 6dfdabafcf765102f27a3ed4a2543223)`); }
        if (Object.values(latestAddrsMap).some(x => x === null)) {
            throw new Error(`(UNEXPECTED) hmm, latestAddrsMap null value? should  (E: ee8babe2ebf47c8f59400a1743d42523)`);
        }
        this.cachedLatestAddrsMap = {
            ...this.cachedLatestAddrsMap,
            ...latestAddrsMap!,
        };
        const latestAddrsToGet: IbGibAddr[] = [];
        for (let i = 0; i < commentAddrs.length; i++) {
            const commentAddr = commentAddrs[i];
            let latestAddr = latestAddrsMap[commentAddr];
            if (!latestAddr) {
                console.warn(`${lc} (UNEXPECTED) commentAddr falsy in latestAddrsMap? (W: a2a4fc7081ca43c0803bf61074759b6a)`);
                latestAddr = commentAddr;
            }
            if (!this._cacheIbGibs[latestAddr] && !latestAddrsToGet.includes(latestAddr)) {
                latestAddrsToGet.push(latestAddr);
            }
        }
        if (latestAddrsToGet.length > 0) {
            const resGetLatestIbGibs = await getFromSpace({ addrs: latestAddrsToGet, space });
            if (resGetLatestIbGibs.success && resGetLatestIbGibs.ibGibs?.length === latestAddrsToGet.length) {
                for (let i = 0; i < resGetLatestIbGibs.ibGibs.length; i++) {
                    const latestIbGib = resGetLatestIbGibs.ibGibs[i];
                    this._cacheIbGibs[getIbGibAddr({ ibGib: latestIbGib })] = latestIbGib;
                }
            } else {
                console.error(`${lc} full result: `);
                console.dir(resGetLatestIbGibs);
                throw new Error(`problem with getting latest ibgibs (E: 65bada4803244723ad86997dbf2f8081)`);
            }
        }
        return commentIbGibs;
    }

    /**
     *
     * builds a projection of ibgibs we can look, starting with the ibgibs we
     * immediately look at via user interaction. From those ibgibs as the sources, we then look along
     * their rel8ns via `this.data.lookRel8nNames`.
     *
     * @returns graph of ibgibs we can look at, i.e., a map of ibgibs indexed by their addrs.
     */
    private async getAllIbGibsWeCanLookAt({
        space
    }: {
        space: IbGibSpaceAny
    }): Promise<GetGraphResult> {
        const lc = `${this.lc}[${this.getAllIbGibsWeCanLookAt.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: aa325fa988fc4e6cbc1bc23b5276077e)`); }

            const rel8dIbGibsMap = await this.getRel8dIbGibs({
                rel8nNames: [this.data!.defaultRel8nName!],
            });
            const allRel8dIbGibs = Object.values(rel8dIbGibsMap).flatMap(x => x);
            allRel8dIbGibs.forEach(x => { this._cacheIbGibs[getIbGibAddr({ ibGib: x })] = x; });

            /**
             * we want to get all of the children of our ibgibs
             */
            const lookProjection = await getGraphProjection({
                ibGibs: allRel8dIbGibs,
                onlyRel8nNames: this.data!.lookRel8nNames.split(','),
                space
            });
            Object.values(lookProjection).forEach(x => { this._cacheIbGibs[getIbGibAddr({ ibGib: x })] = x; });
            return lookProjection;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    // #region validate

    protected async validateWitnessArg(arg: RobbotCmdIbGib): Promise<string[]> {
        const lc = `${this.lc}[${this.validateWitnessArg.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            const errors = await super.validateWitnessArg(arg) ?? [];
            if (!this.ibgibsSvc) {
                errors.push(`this.ibgibsSvc required (E: df38362a558949feb5c005ce6dd02406)`);
            }
            if (arg.data!.cmd) {
                // perform extra validation for cmds
                if ((arg.ibGibs ?? []).length === 0) {
                    errors.push(`ibGibs required. (E: a626439bbb9848c0ae4ec32caee45fbb)`);
                }
            }
            return errors;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async validateThis(): Promise<string[]> {
        const lc = `${this.lc}[${this.validateThis.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            const errors = [
                ...await super.validateThis(),
            ];
            const { data } = this;
            //
            return errors;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    // #endregion validate

    // #region session

    protected async startSession(): Promise<void> {
        const lc = `${this.lc}[${this.startSession.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: e1ac71e72ec2454bbbd6e59151cf0b72)`); }

            if (this.session) {
                console.warn(`${lc} session already in progress. closing... (W: b7f96c381b1d44c7a0d3b16f42a63689)`);
                await this.completeSessionIfNeeded({ sayBye: false });
            }

            if (!this._currentWorkingContextIbGib) { throw new Error(`current working context should already be initialized (E: 2426e5f4a02a4e2db751ba22466c062c)`); }

            const contextIbGib = this._currentWorkingContextIbGib;

            const timestamp = getTimestamp();
            const timestampInTicks = getTimestampInTicks(timestamp);
            const contextTjpAddr = getTjpAddr({
                ibGib: contextIbGib,
                defaultIfNone: 'incomingAddr',
            }) as IbGibAddr;
            const contextTjpGib = getIbAndGib({ ibGibAddr: contextTjpAddr }).gib;
            const sessionId = await getUUID();

            // build custom ibgib (always remember to calculate the gib!)
            // no dna for sessions, because it doesn't make sense
            const data: RobbotSessionData_V1 = {
                timestamp,
                uuid: sessionId,
                n: 0,
                isTjp: true,
                '@context': getIbGibAddr({ ibGib: contextIbGib }),
                '@contextTjp': contextTjpAddr,
            };
            const rel8ns: RobbotSessionRel8ns_V1 = {
                ancestor: [`${ROBBOT_SESSION_ATOM}^gib`],
            }
            const sessionIbGib: RobbotSessionIbGib_V1 = {
                ib: getRobbotSessionIb({ robbot: this, timestampInTicks, contextTjpGib, sessionId }),
                data,
                rel8ns,
            };
            sessionIbGib.gib = await getGib({ ibGib: sessionIbGib, hasTjp: true })

            this.session = sessionIbGib;
            this.sessionInteractions = [];

            // rel8 to the session itself
            await this.rel8To({ ibGibs: [sessionIbGib], rel8nName: ROBBOT_SESSION_REL8N_NAME })
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * save the current session if there are any interactions. if not, then warn
     * and just delete it. (there should be interactions unless there were
     * intermediate exceptions thrown)
     *
     * adds to the context
     *
     * this function will delete this.session and this.interactions, even if it throws.
     */
    protected async completeSessionIfNeeded({ sayBye }: { sayBye: boolean }): Promise<void> {
        const lc = `${this.lc}[${this.completeSessionIfNeeded.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 14efe384d22f4041885d51612d7a5536)`); }
            if (!this.session) {
                if (logalot) { console.log(`${lc} no session active. returning early... (I: f8be762bf1a44065aaa5ce9b2fc094e9)`); }
                if (this.sessionInteractions.length > 0) { throw new Error(`(UNEXPECTED) this.session falsy but this.interactions.length > 0? (E: 8006213289b1481fa8dd00d9b7aad79d)`); }
                return; /* <<<< returns early */
            }

            if (this.sessionInteractions.length === 0) {
                if (logalot) { console.log(`${lc} this.session is truthy but no interactions, so deleting without saving. (I: 1fbd01cbc9ba4c32bf5bae660dc707d2)`); }
                return; /* <<<< returns early */
            }

            // we have a session and at least one interaction.
            // so save the interactions, and then save the entire session.
            let resPut = await this.ibgibsSvc!.put({
                ibGibs: [
                    // ...this.interactionsInSession,
                    this.session,
                ]
            });
            if (!resPut.success) { throw new Error(`put session/interactions failed: ${resPut.errorMsg ?? 'unknown error 101ee5e794bf49788664d280d17c30f0'} (E: 12687d7ca6d347fbb131323fbf56139b)`); }

            if (sayBye) {
                if (logalot) { console.log(`${lc} saying bye... (I: f886e1c843dd4dbbba7a24a3717ff00f)`); }

                let bye = this._robbotLex!.get(SemanticId.bye);
                if (!bye) { throw new Error(`(UNEXPECTED) bye not found in robbotLex (E: 3f605155dc6cf75eebe50bd7ac817a23)`); }
                let { text } = bye;
                if (!text) {
                    console.error(`${lc} (UNEXPECTED) lex didn't work? tried to get 'bye' and text is falsy. (E: 5213bd0514d5448099ce12c9f39ff8b0)`);
                    text = 'bye';
                }
                if (!this._currentWorkingContextIbGib) {
                    throw new Error(`(UNEXPECTED) this._currentWorkingContextIbGib falsy? (E: 605d8388335fcf041adc4ffa8697e623)`);
                }
                await this.createCommentAndRel8ToContextIbGib({
                    text,
                    contextIbGib: this._currentWorkingContextIbGib!,
                    rel8nName: 'comment',
                });
            }
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} deleting session/interactions (I: a6697635e48e47d486d47fa21f84ac79)`); }
            delete this.session;
            this.sessionInteractions = [];
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async updateSessionWithInteraction({
        interaction
    }: {
        interaction: RobbotInteractionIbGib_V1
    }): Promise<RobbotSessionIbGib_V1> {
        const lc = `${this.lc}[${this.updateSessionWithInteraction.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 5d505ff5dcb14a62816044b76005ec65)`); }

            const interactionAddr = getIbGibAddr({ ibGib: interaction });

            // rel8 interaction to the current session
            const resRel8Interaction = await rel8({
                type: 'rel8',
                src: this.session,
                rel8nsToAddByAddr: { [ROBBOT_INTERACTION_REL8N_NAME]: [interactionAddr], },
                dna: true,
                nCounter: true,
            });

            // save the session (to the current local user space)
            await this.ibgibsSvc!.persistTransformResult({ resTransform: resRel8Interaction });

            // return it
            return resRel8Interaction.newIbGib as RobbotSessionIbGib_V1;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    // #endregion session

    /**
     * Takes the current robbot's state (e.g. analysis), context state of call
     * (is a click, is a request, what kind of request, etc.) and reacts to this
     * stimulus via an interaction in the Context ibgib (the ibgib within which
     * we're talking with the robbot).
     *
     * "Applying" this interaction to the Context ibgib most likely equates to
     * adding a comment from the robbot to the context ibgib like, e.g., a comment
     * with text "Hi. Your next line is ________" or similar.
     *
     * So the "prompt" in this function name is like "add comment to context".
     */
    protected async promptNextInteraction({
        ibGib,
        isRequest,
        isClick,
    }: StimulusForRobbot): Promise<void> {
        const lc = `${this.lc}[${this.promptNextInteraction.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: c32379336a0143e3b6f8b4ab4add05ed)`); }

            // get next interaction per stimulus, but don't execute yet
            const interaction = await this.getNextInteraction({ ibGib, isRequest, isClick });

            if (!interaction) { throw new Error(`no interaction produced (E: 457835647fd34636a8c982a6d75bfe7b)`); }

            // save the interaction, no need to register with local space via
            // `this.ibgibsSvc.registerNewIbGib` call, because we don't need a
            // timeline as we are not intending on updating the interaction
            // going forward.
            await this.ibgibsSvc!.put({ ibGib: interaction });

            // create the interaction output and apply it to the current
            // context. atow, this means create a comment with text based on the
            // interaction and relate that comment to the context.
            await this.applyInteractionToContext({ interaction });

            // update the session with the interaction to reflect that we have
            // executed it in the current context
            this.session = await this.updateSessionWithInteraction({ interaction });
            // we rel8 only timelines to this robbot, so no need to rel8 to the new session

            // we DO want to rel8 to the interaction, because each one is its
            // own stone (i.e. non-timeline)
            await this.rel8To({ ibGibs: [interaction], rel8nName: ROBBOT_INTERACTION_REL8N_NAME });
            this.sessionInteractions.push(interaction);
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async applyInteractionToContext({
        interaction,
    }: {
        interaction: RobbotInteractionIbGib_V1,
    }): Promise<void> {
        const lc = `${this.lc}[${this.applyInteractionToContext.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: ffeaad157aab43ef9a1e3fb7f3867621)`); }
            await this.createCommentAndRel8ToContextIbGib({
                text: await this.getOutputText({
                    text: interaction.data!.commentText!,
                }),
                contextIbGib: this._currentWorkingContextIbGib!,
                rel8nName: this.data?.defaultRel8nName ?? 'comment',
            });
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async getNextInteraction({
        ibGib,
        isRequest,
        isClick,
    }: StimulusForRobbot): Promise<RobbotInteractionIbGib_V1> {
        const lc = `${this.lc}[${this.getNextInteraction.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 88ef365260dd4bc389e9a91582bbd717)`); }
            if (!ibGib && !isClick) { throw new Error(`either ibGib or isClick required. (E: 767f53b925524dbcba9c301817bcd1af)`); }
            if (ibGib && isClick) { throw new Error(`(UNEXPECTED) ibGib expected to be falsy if isClick is true. (E: 1ea286b818db4d9bbc77b4fd27fb5ef5)`); }

            let resInteraction: RobbotInteractionIbGib_V1;

            if (isClick) {
                if (!this.session) { throw new Error(`(unexpected) session expected to exist at this point (E: e698997b624c4cbf92637a0d1ca9a43c)`); }
                if (this.prevInteractionInSession) {
                    // the user doesn't know there is already a session started? Or
                    // something else? just ping the user that a session is in
                    // progress and ask what's up, give the short help command
                    resInteraction = await this.getNextInteraction_PerRequest({
                        semanticId: SemanticId.help,
                    });
                } else {
                    // just starting session.
                    resInteraction = await this.getNextInteraction_PerRequest({
                        semanticId: SemanticId.hello,
                    });
                }
            } else if (isRequest && isComment({ ibGib: ibGib! })) {
                // someone has issued a request
                resInteraction =
                    await this.getNextInteraction_PerRequest({ request: ibGib as CommentIbGib_V1 });
            } else if (this.handlerExpectingResponse) {
                // ibgib added to context that may be a response to a
                // question, or is not relevant (but it isn't a request)
                resInteraction = await this.getNextInteraction_PerNonRequest({ nonRequest: ibGib });
            } else {
                throw new Error(`(UNEXPECTED) not sure what to do with the incoming stimulus? It's not a click, not a request and we're not expecting a child + have a handler. (E: 6496a703e0774ecfb426eab6df695896)`);
            }

            if (!resInteraction) { throw new Error(`(UNEXPECTED) resInteraction expected to be truthy. (E: bea29a7fd13542018b990d9b426f3d3b)`); }
            return resInteraction;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async getNextInteraction_PerRequest({
        request,
        semanticId,
    }: {
        request?: CommentIbGib_V1,
        semanticId?: SemanticId,
    }): Promise<RobbotInteractionIbGib_V1> {
        const lc = `${this.lc}[${this.getNextInteraction_PerRequest.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 12d35b2990ee4e5a983a417f9eca3b6a)`); }

            // validate
            if (!request && !semanticId) { throw new Error(`either request or semanticId required (E: d085ae8b44f9449b9a18b97dcf797b01)`); }

            // get the semanticId
            if (!semanticId) {
                let info = this.parseRequest({ request: request! });
                semanticId = info.semanticId;
            }
            if (!semanticId) { throw new Error(`(UNEXPECTED) semanticId not provided and not able to be gotten? (E: 562aca579896441ab2c20c5459a929e2)`); }

            // get our handlers that correspond to this semanticId at this point in time
            const info: SemanticInfo = { semanticId, request, };
            const handlersThatCanExecute =
                await this.getHandlersThatCanExecute({ semanticId, info });
            if (handlersThatCanExecute.length === 0) { throw new Error(`no handlers anywhere and what up with no default handler? (E: e09760464aa04581a8c54473849e05b0)`); }

            // get the interaction produced in our pipeline of handlers that can
            // execute. in the future, this can be smarter than simply returning
            // the first truthy interaction, including executing multiple paths
            // concurrently and choosing among them depending on, e.g., an
            // evaluation metric
            const interaction = await this.getInteractionFromHandlerPipeline({
                info,
                handlerPipeline: handlersThatCanExecute,
            });
            if (!interaction) { throw new Error(`no interaction produced. (E: 6f82b7f482174685ac676ea7bc9efb7c)`); }

            // we're done
            return interaction;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected parseRequest({
        request,
    }: {
        request: IbGib_V1,
    }): {
        semanticId: RollySemanticId,
        primary?: string,
        remainder?: string,
    } {
        const lc = `${this.lc}[${this.parseRequest.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 7e23d764a5ae4d4aab3ac05b565f52ba)`); }

            let semanticId: RollySemanticId;
            const entireRequestText_safe_lower =
                getSpaceDelimitedSaferRequestText({ ibGib: request, lowercase: true }) ||
                ROLLY_V1_DEFAULT_REQUEST_TEXT.toLowerCase();
            /**
             * the primary raw text found that directly translates to the semanticId.
             */
            let primary: string;
            /**
             * the remainder of the text found that may act as parameterization.
             */
            let remainder: string;

            // we're getting naive requestId, but may be unnecessary as we also
            // check matching against the datum starting with the entire request
            const requestPieces = entireRequestText_safe_lower.split(' ');
            if (requestPieces.length === 0) {
                console.error(`${lc} (UNEXPECTED) requestPieces.length === 0? request is truthy but requestText splits to nothing? funky junk. (E: b26bb13856034f298c164a3717076295)`)
                return { semanticId: SemanticId.unknown }; /* <<<< returns early */
            }
            const requestId = requestPieces[0];

            // map from the request text to a semantic id
            // const semanticId: SemanticId = SemanticId.help;
            const resultsFind = this._userLex!.find({
                fnDatumPredicate: d => {
                    // paranoid/convenient place to check for malformed lex data
                    if ((d.texts ?? [])?.length === 0) {
                        console.error(`${lc} invalid lex data. datum is empty. d: ${pretty(d)} (E: 60b16d12999246cbbb887584ae5eb418)`);
                        return false;
                    }
                    // must account for when request is multiple words like
                    // "hello there".  RCLIs nowadays think we should have to
                    // surround things with quotes but we should be able to
                    // handle this (synonyms, spaces, ...).  but if the request
                    // text is 'h', then we shouldn't match up with any request
                    // that starts with h.
                    const datumIsRequest = d.props?.isRequest;
                    const datumFirstText = d.texts![0].toLowerCase();
                    const matchesRequestId = datumFirstText === requestId;
                    /** remove non word/keep only spaces, so regexp is guaranteed here */
                    const safeRequestText = getSaferSubstring({ text: entireRequestText_safe_lower, keepLiterals: [' '] });
                    /**
                     * Do the same for datum, so we can include things like
                     * apostrophes for contractions and still match up in the
                     * regular expression
                     */
                    const safeDatumFirstText = getSaferSubstring({ text: datumFirstText, keepLiterals: [' '] });
                    const requestStartsWithDatumText = !!safeRequestText.match(new RegExp(`^${safeDatumFirstText}(\\s|$)`));
                    const matches = datumIsRequest && (matchesRequestId || requestStartsWithDatumText);
                    if (matches) {
                        if (matchesRequestId) {
                            primary = safeRequestText;
                        } else {
                            primary = datumFirstText;
                            remainder = safeRequestText.substring(datumFirstText.length + 1);
                        }
                    };
                    return !!matches;
                }
            });

            const resultsFind_Ids = Object.keys(resultsFind ?? []);

            if (resultsFind_Ids?.length === 1) {
                const resId = resultsFind_Ids[0];
                // id found, but is it semantic id?
                const semanticIds = Object.values(RollySemanticId);
                if (logalot) { console.log(`${lc} semanticIds: ${semanticIds} (I: a3eb4c4db06a450db613af9ba29e38c7)`); }
                if (semanticIds.includes(resId)) {
                    // semantic id found
                    semanticId = resId as RollySemanticId;
                } else {
                    // text found but not a semantic id? equate this with not found
                    console.warn(`${lc} id found but not a known semantic id: ${resId} (W: 52f5a5f566d64301b056338cdf9272e2)`)
                    semanticId = SemanticId.unknown;
                }
            } else if (resultsFind_Ids?.length > 1) {
                // multiple found? this is a problem with the data
                throw new Error(`(UNEXPECTED) multiple ids found from user requestText (${entireRequestText_safe_lower})? todo: confirm what user said workflow not implemented yet (E: fbb7bc9381224731a1556d32b6701b58)`);
            } else {
                // not found
                semanticId = SemanticId.unknown;
            }

            return { semanticId };
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async getHandlersThatCanExecute({
        semanticId,
        info,
    }: {
        semanticId: string,
        info: SemanticInfo,
    }): Promise<SemanticHandler[]> {
        const lc = `${this.lc}[${this.getHandlersThatCanExecute.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 9987a0a6ecdf4d0890ae10865d630320)`); }

            let handlers = this._semanticHandlers[semanticId] ?? [];
            if (handlers.length === 0) { handlers = this._semanticHandlers[SemanticId.default] ?? []; }
            if (handlers.length === 0) { throw new Error(`semanticId (${semanticId}) not found and no SemanticId.default handler found either. (E: fa85075b1aa94cf6a794c6fe6ccae63e)`); }

            /** first determine which handlers can execute */
            const handlersThatCanExecute: SemanticHandler[] = [];
            const fnGetHandlers = async () => {
                for (let i = 0; i < handlers.length; i++) {
                    const handler = handlers[i];
                    const canExec = handler.fnCanExec ?
                        await handler.fnCanExec(info) :
                        true;
                    if (canExec) {
                        handlersThatCanExecute.push(handler);
                    } else {
                        if (logalot) { console.log(`${lc} handler canExec false (${handler.semanticId}, ${handler.handlerId}) (I: f4cf1a3a97e6404fa6ee086658187b05)`); }
                    }
                }
            };

            await fnGetHandlers();

            if (handlersThatCanExecute.length === 0) {
                // try again with semanticId of default
                handlers = this._semanticHandlers[SemanticId.default] ?? [];
                if (handlers.length === 0) { throw new Error(`found no handlers that could execute and default handler not found (E: fd2c831d904540d0b42b14577c638a39)`); }
                await fnGetHandlers();
            }
            return handlersThatCanExecute;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async getInteractionFromHandlerPipeline({
        info,
        handlerPipeline,
    }: {
        info: SemanticInfo,
        handlerPipeline: SemanticHandler[],
    }): Promise<RobbotInteractionIbGib_V1 | undefined> {
        const lc = `${this.lc}[${this.getInteractionFromHandlerPipeline.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: dedbb7810bd24bbdb340f445d574e692)`); }

            let interaction: RobbotInteractionIbGib_V1 | null = null;
            for (let i = 0; i < handlerPipeline.length; i++) {
                const handler = handlerPipeline[i];
                const lcHandler = `${lc}[handler][${handler.handlerId}]`
                if (logalot) { console.group(lcHandler); } // just trying out the grouping...
                try {
                    if (logalot) { console.log(`${lc} starting... (I: c87e12b3acdc40779dfc148ad3c11f14)`); }
                    const resHandler = await handler.fnExec(info);
                    interaction = resHandler.interaction;
                    if (interaction?.data?.expectingResponse) {
                        if (logalot) { console.log(`${lcHandler} complete. YES interaction found from handler (${handler.semanticId}, ${handler.handlerId}). breaking for loop... (I: f1f502fd3b5f4be6afa2486c4227f82b)`); }
                        this.handlerExpectingResponse = handler;
                        break;
                    } else {
                        if (logalot) { console.log(`${lcHandler} complete. NO interaction not found for handler (${handler.semanticId}, ${handler.handlerId}) (I: 75e70ffd9c154218a7fd2f7687556cad)`); }
                    }
                } catch (error) {
                    console.error(`${lcHandler} ${extractErrorMsg(error)}`);
                    throw error;
                } finally {
                    if (logalot) { console.groupEnd(); }
                }
            }
            return interaction ?? undefined;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async getNextInteraction_PerNonRequest({
        nonRequest,
    }: {
        nonRequest?: IbGib_V1,
    }): Promise<RobbotInteractionIbGib_V1> {
        const lc = `${this.lc}[${this.getNextInteraction_PerNonRequest.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 7cb225e5313147d0861cf036b22657bf)`); }

            if (!this.handlerExpectingResponse) { throw new Error(`(UNEXPECTED) this.handlerExpectingResponse is assumed to be truthy at this point. (E: a035bf5994a9442c9c9533fbff4868a8)`); }

            // validate

            // get our handlers that correspond to this semanticId at this point in time
            // const info: SemanticInfo = {other: nonRequest};
            // const handlersThatCanExecute =
            //     await this.getHandlersThatCanExecute({ semanticId, info });
            // if (handlersThatCanExecute.length === 0) { throw new Error(`no handlers anywhere and what up with no default handler? (E: 44c82571c4764bbbb3bdc133d613da63)`); }

            // get the interaction produced in our pipeline of handlers that can
            // execute. in the future, this can be smarter than simply returning
            // the first truthy interaction, including executing multiple paths
            // concurrently and choosing among them depending on, e.g., an
            // evaluation metric
            const interaction = await this.getInteractionFromHandlerPipeline({
                info: { other: nonRequest, isContinuation: true },
                handlerPipeline: [this.handlerExpectingResponse],
            });
            if (!interaction) { throw new Error(`no interaction produced. (E: ba72330dab96483b82110c9797469ee7)`); }

            // we're done
            return interaction;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

}

/**
 * Default data values for a ROLLY robbot.
 *
 * If you change this, please bump the version
 *
 * (but of course won't be the end of the world when this doesn't happen).
 */
const DEFAULT_ROLLY_ROBBOT_DATA_V1: RollyRobbotData_V1 = {
    version: '1',
    uuid: DEFAULT_UUID_ROLLY_ROBBOT,
    name: DEFAULT_NAME_ROLLY_ROBBOT,
    description: DEFAULT_DESCRIPTION_ROLLY_ROBBOT,
    classname: RollyRobbot_V1.name,
    defaultRel8nName: DEFAULT_ROBBOT_TARGET_REL8N_NAME,
    allRel8nNames: [
        DEFAULT_ROBBOT_TARGET_REL8N_NAME,
        'comment',
    ],

    lookRel8nNames: DEFAULT_LOOK_REL8N_NAMES_ROLLY_ROBBOT,

    // tagOutput: false,
    outputPrefix: '🤖: ',
    outputSuffix: '-Rolly',
    requestEscapeString: '?',

    persistOptsAndResultIbGibs: false,
    allowPrimitiveArgs: true,
    catchAllErrors: true,
    trace: false,
}
const DEFAULT_ROLLY_ROBBOT_REL8NS_V1: RollyRobbotRel8ns_V1 | undefined = undefined;

/**
 * factory for Rolly robbot.
 *
 * @see {@link DynamicFormFactoryBase}
 */
export class RollyRobbot_V1_Factory
    extends DynamicFormFactoryBase<RollyRobbotData_V1, RollyRobbotRel8ns_V1, RollyRobbot_V1> {

    protected lc: string = `[${RollyRobbot_V1_Factory.name}]`;

    getName(): string { return RollyRobbot_V1.name; }

    async newUp({
        data,
        rel8ns,
    }: {
        data?: RollyRobbotData_V1,
        rel8ns?: RollyRobbotRel8ns_V1,
    }): Promise<TransformResult<RollyRobbot_V1>> {
        const lc = `${this.lc}[${this.newUp.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            data = data ?? clone(DEFAULT_ROLLY_ROBBOT_DATA_V1);
            rel8ns = rel8ns ?? DEFAULT_ROLLY_ROBBOT_REL8NS_V1 ? clone(DEFAULT_ROLLY_ROBBOT_REL8NS_V1) : undefined;
            data!.uuid = data!.uuid || await getUUID();
            let { classname } = data!;

            const ib = getRobbotIb({ robbotData: data!, classname });

            const resRobbot = await factory.firstGen({
                ib,
                parentIbGib: factory.primitive({ ib: `robbot ${classname}` }),
                data: data,
                rel8ns,
                dna: true,
                linkedRel8ns: [Rel8n.ancestor, Rel8n.past],
                nCounter: true,
                tjp: { timestamp: true },
            }) as TransformResult<RollyRobbot_V1>;

            // replace the newIbGib which is just ib,gib,data,rel8ns with loaded
            // witness class
            const robbotDto = resRobbot.newIbGib;
            let robbotIbGib = new RollyRobbot_V1(undefined, undefined);
            await robbotIbGib.loadIbGibDto(robbotDto);
            resRobbot.newIbGib = robbotIbGib;
            if (logalot) { console.log(`${lc} robbotDto: ${pretty(robbotDto)} (I: 0c37cbf63f53458eab01a5ed7e5336dc)`); }

            return resRobbot;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    async witnessToForm({ witness }: { witness: RollyRobbot_V1; }): Promise<DynamicForm> {
        const lc = `${this.lc}[${this.witnessToForm.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            let { data } = witness;
            if (!data) { throw new Error(`(UNEXPECTED) data falsy? (E: 4c55f7e528240db6f3ffa10e3d6b6323)`); }
            // We do the RobbotFormBuilder specific functions first, because of
            if (logalot) { console.log(`${lc} data: ${pretty(data)} (I: cd435fd756484720a8423883b07c41fb)`); }
            const idPool = await getIdPool({ n: 100 });
            if (!data!.classname) { throw new Error(`invalid data. classname required (E: 720c9d210577f2762c806624a08ec223)`); }
            // type inference in TS! eesh...
            let form = new RollyRobbotFormBuilder()
                .with({ idPool })
                .name({
                    of: data.name,
                    required: true,
                    defaultValue: DEFAULT_NAME_ROLLY_ROBBOT,
                })
                .description({
                    of: data.description ?? DEFAULT_DESCRIPTION_ROLLY_ROBBOT,
                    defaultValue: DEFAULT_DESCRIPTION_ROLLY_ROBBOT,
                })
                .and<RollyRobbotFormBuilder>()
                .lookRel8nNames({
                    of: data.lookRel8nNames,
                    // defaultValue: 'comment,pic,link,x,target',
                    defaultValue: DEFAULT_LOOK_REL8N_NAMES_ROLLY_ROBBOT,
                })
                .and<RobbotFormBuilder>()
                .outputPrefix({
                    of: data.outputPrefix ?? '',
                    defaultValue: DEFAULT_OUTPUT_PREFIX_ROLLY_ROBBOT,
                })
                .outputSuffix({
                    of: data.outputSuffix ?? '',
                    defaultValue: DEFAULT_OUTPUT_SUFFIX_ROLLY_ROBBOT,
                })
                .and<DynamicFormBuilder>()
                .uuid({ of: data.uuid, required: true })
                .classname({ of: data.classname })
                .and<WitnessFormBuilder>()
                .commonWitnessFields({ data })
                .outputForm({
                    formName: 'form',
                    label: 'Rolly Robbot',
                });
            return Promise.resolve(form);
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    async formToWitness({ form }: { form: DynamicForm; }): Promise<TransformResult<RollyRobbot_V1>> {
        // let robbot = new RollyRobbot_V1(null, null);
        let data: RollyRobbotData_V1 = clone(DEFAULT_ROLLY_ROBBOT_DATA_V1);
        this.patchDataFromItems({ data, items: form.items, pathDelimiter: DEFAULT_DATA_PATH_DELIMITER });
        let resRobbot = await this.newUp({ data });
        return resRobbot;
    }

}

export class RollyRobbotFormBuilder extends RobbotFormBuilder {
    protected lc: string = `[${RollyRobbotFormBuilder.name}]`;

    constructor() {
        super();
        this.what = 'robbot';
    }

    lookRel8nNames({
        of,
        defaultValue,
    }: {
        of: string,
        defaultValue?: string,
    }): RollyRobbotFormBuilder {
        this.addItem({
            // witness.data.outputPrefix
            name: "lookRel8nNames",
            description: `Every ibgib relates to other ibgibs via a "rel8n name". When you add a comment, this adds an ibgib via the "comment" rel8n name. So when you show your robbot an ibgib, do you want him/her/them to see sub-comments "inside" that ibGib? If so, include "comment". What about comments on pics? If so, also include "pic" or it won't see the comment under the pics. Basically, just leave this as-is unless you only want the robbot to look at the ibgib itself and no children, in which case blank this out.`,
            label: "Rel8n Names Visible",
            regexp: COMMA_DELIMITED_SIMPLE_STRINGS_REGEXP,
            regexpErrorMsg: COMMA_DELIMITED_SIMPLE_STRINGS_REGEXP_DESCRIPTION,
            dataType: 'text',
            value: of,
            defaultValue,
            required: false,
        });
        return this;
    }

}
