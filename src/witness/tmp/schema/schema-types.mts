/**
 * @module schema-types
 *
 * metadata to be attached to ibgib for various purposes used in that ibgib's
 * data.
 *
 * For example, if you want a schema for an ib, create that schema here and
 * rel8 the instantiated ibgib to that schema in its initial construction.
 *
 * ## notes
 *
 * * i've created many ib schemas without relating those ibgibs to the schema
 *   plumbing here.
 *   * i need to go back through and upgrade all of those ibgibs
 *     to relate to a specific schema.
 */

import { IbGibAddr } from '@ibgib/ts-gib/dist/types.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { WitnessWithContextData_V1, WitnessWithContextRel8ns_V1 } from '@ibgib/core-gib/dist/witness/witness-with-context/witness-with-context-types.mjs';
import { WitnessCmdData, WitnessCmdIbGib, WitnessCmdRel8ns } from '@ibgib/core-gib/dist/witness/witness-cmd/witness-cmd-types.mjs';
import { WitnessResultData, WitnessResultIbGib, WitnessResultRel8ns } from '@ibgib/core-gib/dist/witness/witness-types.mjs';

export type SchemaScope = "ibgib" | "ib" | "gib" | "data" | "rel8ns";
export const SchemaScope = {
    ibgib: "ibgib" as SchemaScope,
    ib: "ib" as SchemaScope,
    gib: "gib" as SchemaScope,
    data: "data" as SchemaScope,
    rel8ns: "rel8ns" as SchemaScope,
};
/**
 * values of {@link SchemaScope}
 */
export const SCHEMA_SCOPES: SchemaScope[] = Object.values(SchemaScope);
export type SchemaSubscope = "key" | "value";
export const SchemaSubscope = {
    /**
     * the key in a key/value pair.
     *
     * in ibgib.data, this would be a key in the data map.
     *
     * in ibgib.rel8ns, this would be the rel8nName in the ibgib.rel8ns map.
     */
    key: "key" as SchemaSubscope,
    /**
     * the value in a key/value pair.
     *
     * in ibgib.data, this would be a key in the data map.
     *
     * in ibgib.rel8ns, this would be the array of ibgib addresses in the ibgib.rel8ns map.
     */
    value: "value" as SchemaSubscope,
}
/**
 * values of {@link SchemaSubscope}
 */
export const SCHEMA_SUBSCOPES: SchemaSubscope[] = Object.values(SchemaSubscope);

/**
 * the primitive type associated with whatever the schema is describing.
 *
 * # notes
 *
 * * if the schema has no intrinsic payload/target that it's describing, i.e. it
 *   only exists to bring together multiple other schemas via the ibgib.rel8ns,
 *   then this should be set to "na".
 */
export type SchemaPayloadPrimitiveType = "string" | "number" | "any" | "na";
/**
 * constant to expose the enum {@link SchemaPayloadPrimitiveType}.
 *
 * ## notes
 *
 * * this is largely using TypeScript types, since that is what I'm using atow.
 *   * however, I'm trying to make this extensible, so that other values should
 *     be able to be created.
 */
export const SchemaPayloadPrimitiveType = {
    /**
     * this part is a text string.
     */
    string: 'string' as SchemaPayloadPrimitiveType,
    /**
     * this part is a number.
     */
    number: 'number' as SchemaPayloadPrimitiveType,
    /**
     * Use this if this schema description isn't sufficient for the value.
     */
    any: 'any' as SchemaPayloadPrimitiveType,
    /**
     * if the schema has no intrinsic payload/target that it's describing, i.e.
     * it only exists to bring together multiple other schemas via the
     * ibgib.rel8ns, then this should be set to "na".
     */
    na: 'na' as SchemaPayloadPrimitiveType,
}

export interface SchemaDefinition {
}

export interface SchemaData_V1 extends WitnessWithContextData_V1 {
    /**
     * human-readable name
     */
    name: string;
    /**
     * @optional additional human-readable information.
     *
     * this could contain intent of the schema, real use cases, etc.
     */
    description?: string;
    /**
     * is it a string/number/etc.
     */
    payloadType: SchemaPayloadPrimitiveType;
    /**
     * broad scope in an ibgib record. either it's the ib, gib, data, rel8ns, or ibgib
     * as a whole which requires constraints across multiple of these.
     */
    scope: SchemaScope;
    /**
     * further refines the scope if necessary.
     */
    subscope?: SchemaSubscope;
    /**
     * for "data" scope, the path would be the path into the data object.
     * whether it's the key or value of that path would be determined by the subscope.
     *
     * ## for rel8ns scope
     * path would be the rel8nName (I think)
     */
    path?: string;
    /**
     * If applicable, this relates to the 0-indexed position in a delimited string.
     *
     * @example so if scope is "ib" and the shape is [name][id][tx id] with "_"
     * as the delimiter, an example might be "bob_id123_tx24", the bob "name"
     * would be position 0, "id" would be position 1, "tx id" would be position
     * 2.
     */
    position?: number;
    /**
     * If applicable, this is the delimiter in this Schema Part type
     *
     * ## notes
     *
     * * the schema should not have parts that have the same delimiters in outer
     *   parts. for example, if you have the ib be space-delimited, you can't
     *   also have one of the ib parts be space-delimited.
     */
    delimiter?: string;
    /**
     * if applicable, the target of the schema should match this regular
     * expression.
     *
     * ## notes
     *
     * * note that this can also proscribe valid values (I don't think I need to
     *   add a separate field for this).
     */
    regexp?: string;
    /**
     * If true, then the given part may not exist at the given
     * scope+subscope+path+position.
     */
    optional?: boolean;
}

/**
 * If a schema hard links to other schemas via rel8ns, then those also should
 * apply (similar to additional constraints/requirements).
 */
export const REQUIRED_SCHEMA_REL8N_NAME = 'required_schema';
export const OPTIONAL_SCHEMA_REL8N_NAME = 'optional_schema';

export interface SchemaRel8ns_V1 extends WitnessWithContextRel8ns_V1 {
    /**
     * If a schema hard links to other schemas via this rel8n, then those also
     * are required (similar to additional constraints/requirements).
     *
     * @see {@link OPTIONAL_SCHEMA_REL8N_NAME}
     */
    [REQUIRED_SCHEMA_REL8N_NAME]?: IbGibAddr[];
    /**
     * If a schema hard links to other schemas via this rel8n, then those also
     * are required (similar to additional constraints/requirements).
     *
     * @see {@link REQUIRED_SCHEMA_REL8N_NAME}
     */
    [OPTIONAL_SCHEMA_REL8N_NAME]?: IbGibAddr[];
}

/**
 * Ibgib for schemas used in metadata.
 *
 * @see {@link SchemaData_V1}
 * @see {@link SchemaRel8ns_V1}
 */
export interface SchemaIbGib_V1 extends IbGib_V1<SchemaData_V1, SchemaRel8ns_V1> {

}

/**
 * Cmds for interacting with ibgib witnesses.
 *
 * Not all of these will be implemented for every witness.
 *
 * ## todo
 *
 * change these commands to better structure, e.g., verb/do/mod, can/get/addrs
 * */
export type SchemaCmd =
    'ib' | 'gib' | 'ibgib' | 'activate' | 'deactivate';
/** Cmds for interacting with ibgib spaces. */
export const SchemaCmd = {
    /**
     * it's more like a grunt that is intepreted by context from the schema.
     */
    ib: 'ib' as SchemaCmd,
    /**
     * it's more like a grunt that is intepreted by context from the schema.
     */
    gib: 'gib' as SchemaCmd,
    /**
     * third placeholder command.
     *
     * I imagine this will be like "what's up", but who knows.
     */
    ibgib: 'ibgib' as SchemaCmd,
}

/**
 * Flags to affect the command's interpretation.
 */
export type SchemaCmdModifier =
    'ib' | 'gib' | 'ibgib';
/**
 * Flags to affect the command's interpretation.
 */
export const SchemaCmdModifier = {
    /**
     * hmm...
     */
    ib: 'ib' as SchemaCmdModifier,
    /**
     * hmm...
     */
    gib: 'gib' as SchemaCmdModifier,
    /**
     * hmm...
     */
    ibgib: 'ibgib' as SchemaCmdModifier,
}

/** Information for interacting with spaces. */
export interface SchemaCmdData
    extends WitnessCmdData<SchemaCmd, SchemaCmdModifier> {
}

export interface SchemaCmdRel8ns extends WitnessCmdRel8ns {
}

/**
 * Shape of options ibgib if used for a schema.
 *
 * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface SchemaCmdIbGib<
    TIbGib extends IbGib_V1 = IbGib_V1,
    TCmdData extends SchemaCmdData = SchemaCmdData,
    TCmdRel8ns extends SchemaCmdRel8ns = SchemaCmdRel8ns,
> extends WitnessCmdIbGib<TIbGib, SchemaCmd, SchemaCmdModifier, TCmdData, TCmdRel8ns> {
}

/**
 * Optional shape of result data to app interactions.
 *
 * This is in addition of course to {@link SchemaResultData}.
 *
 * so if you're sending a cmd to this witness, this should probably be the shape
 * of the result.
 *
 * ## notes
 *
 * * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface SchemaResultData extends WitnessResultData {
}

/**
 * Marker interface rel8ns atm...
 *
 * so if you're sending a cmd to this witness, this should probably be the shape
 * of the result.
 *
 * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface SchemaResultRel8ns extends WitnessResultRel8ns { }

/**
 * Shape of result ibgib if used for a app.
 *
 * so if you're sending a cmd to this witness, this should probably be the shape
 * of the result.
 *
 * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface SchemaResultIbGib<
    TIbGib extends IbGib_V1,
    TResultData extends SchemaResultData,
    TResultRel8ns extends SchemaResultRel8ns
>
    extends WitnessResultIbGib<TIbGib, TResultData, TResultRel8ns> {
}
