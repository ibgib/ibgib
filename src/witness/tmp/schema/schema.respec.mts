import * as pathUtils from 'path';

import {
    firstOfAll, firstOfEach, ifWe, ifWeMight, iReckon,
    lastOfAll, lastOfEach, respecfully, respecfullyDear,
} from '@ibgib/helper-gib/dist/respec-gib/respec-gib.mjs';
const maam = `[${import.meta.url}]`, sir = maam;

import { delay, extractErrorMsg, getTimestampInTicks, getUUID } from '@ibgib/helper-gib';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { validateIbGibIntrinsically } from '@ibgib/ts-gib/dist/V1/validate-helper.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../ibgib-constants.mjs';
import {
    SchemaDefinition, SchemaPayloadPrimitiveType,
    SchemaData_V1, SchemaRel8ns_V1, SchemaIbGib_V1, SchemaScope,
} from './schema-types.mjs';
import { createSchema, validateSchemaData, validateSchemaItself } from './schema-helper.mjs';

const logalot: boolean | number = GLOBAL_LOG_A_LOT;

const lcFile: string = `[${pathUtils.basename(import.meta.url)}]`;

const VALID_SCHEMA_NAME = 'no_spaces_only_alphanumerics';

await respecfully(maam, lcFile, async () => {
    let lc = lcFile.concat();

    await respecfully(maam, `basics`, async () => {
        let testName = 'schemaName'

        await ifWe(maam, `createSchema`, async () => {
            const _resSchemaIbGib = await createSchema({
                name: VALID_SCHEMA_NAME,
                scope: SchemaScope.ibgib,
                payloadType: 'na',
            });
            iReckon(maam, `this didn't throw`).isGonnaBeTruthy();
        });

        await ifWe(maam, `validateSchemaData`, async () => {
            // the schema should not have parts that have the same delimiters in
            // outer parts. for example, if you have the ib be space-delimited,
            // you can't also have one of the ib parts be space-delimited.
            validateSchemaData({
                data: {
                    name: VALID_SCHEMA_NAME,
                    scope: SchemaScope.ibgib,
                    payloadType: 'na',
                }
            });
            iReckon(maam, `this didn't throw`).isGonnaBeTruthy();
        });

        await ifWe(maam, `validateSchemaItself`, async () => {
            // the schema should not have parts that have the same delimiters in
            // outer parts. for example, if you have the ib be space-delimited,
            // you can't also have one of the ib parts be space-delimited.
            const resSchemaIbGib = await createSchema({
                name: VALID_SCHEMA_NAME,
                scope: SchemaScope.ibgib,
                payloadType: 'na',
            });
            let schemaIbGib = resSchemaIbGib.newIbGib;
            await validateSchemaItself({ schemaIbGib });
            iReckon(maam, `this didn't throw`).isGonnaBeTruthy();
        });

    });


    // await respecfully(maam, `SchemaPartDefinition props`, async () => {

    //     await ifWe(maam, `version`, async () => {

    //     });
    //     await ifWe(maam, `name`, async () => {

    //     });
    //     await ifWe(maam, `description`, async () => {

    //     });
    //     await ifWe(maam, `partType`, async () => {

    //     });
    //     await ifWe(maam, `scope`, async () => {

    //     });
    //     await ifWe(maam, `subscope`, async () => {

    //     });
    //     await ifWe(maam, `path`, async () => {

    //     });
    //     await ifWe(maam, `position`, async () => {

    //     });
    //     await ifWe(maam, `delimiter`, async () => {

    //     });
    //     await ifWe(maam, `regexp`, async () => {

    //     });
    //     await ifWe(maam, `optional`, async () => {

    //     });

    // });

}, { logalot: !!logalot });
