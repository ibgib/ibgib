import { extractErrorMsg, getUUID, unique } from "@ibgib/helper-gib/dist/helpers/utils-helper.mjs";
import { getIbGibAddr } from "@ibgib/ts-gib/dist/helper.mjs";
import { validateIbGibIntrinsically } from "@ibgib/ts-gib/dist/V1/validate-helper.mjs";
import { Ib, IbGibAddr, TransformResult } from "@ibgib/ts-gib/dist/types.mjs";
import { IbGib_V1 } from "@ibgib/ts-gib/dist/V1/types.mjs";
import { rel8 } from "@ibgib/ts-gib/dist/V1/transforms/rel8.mjs";
import { Factory_V1 as factory } from "@ibgib/ts-gib/dist/V1/factory.mjs";
import { MetaspaceService } from "@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs";
import { IbGibSpaceAny } from "@ibgib/core-gib/dist/witness/space/space-base-v1.mjs";

import { GLOBAL_LOG_A_LOT } from '../../../ibgib-constants.mjs';
import {
    SchemaData_V1, SchemaIbGib_V1, SchemaRel8ns_V1,
    REQUIRED_SCHEMA_REL8N_NAME, OPTIONAL_SCHEMA_REL8N_NAME,
    SchemaPayloadPrimitiveType, SchemaScope, SchemaSubscope,
    SCHEMA_SCOPES, SCHEMA_SUBSCOPES,
} from "./schema-types.mjs";
import { DEFAULT_SCHEMA_IB_DELIMITER, MAX_SCHEMA_NAME_LENGTH } from "./schema-constants.mjs";

const logalot = GLOBAL_LOG_A_LOT;

export async function createSchema({
    name, description,
    payloadType,
    scope, subscope,
    path, position, delimiter, regexp,
    optional,
    requiredSchemas, requiredSchemaAddrs,
    optionalSchemas, optionalSchemaAddrs,
}: {
    /**
     * @see {@link SchemaData_V1.name}
     */
    name: string;
    /**
     * @optional additional human-readable information.
     *
     * this could contain intent of the schema, real use cases, etc.
     */
    description?: string;
    /**
     * is it a string/number/etc.
     */
    payloadType: SchemaPayloadPrimitiveType;
    /**
     * broad scope in an ibgib record. either it's the ib, gib, data, rel8ns, or ibgib
     * as a whole which requires constraints across multiple of these.
     */
    scope: SchemaScope;
    /**
     * further refines the scope if necessary.
     */
    subscope?: SchemaSubscope;
    /**
     * for "data" scope, the path would be the path into the data object.
     * whether it's the key or value of that path would be determined by the subscope.
     *
     * ## for rel8ns scope
     * path would be the rel8nName (I think)
     */
    path?: string;
    /**
     * If applicable, this relates to the 0-indexed position in a delimited string.
     *
     * @example so if scope is "ib" and the shape is [name][id][tx id] with "_"
     * as the delimiter, an example might be "bob_id123_tx24", the bob "name"
     * would be position 0, "id" would be position 1, "tx id" would be position
     * 2.
     */
    position?: number;
    /**
     * If applicable, this is the delimiter in this Schema Part type
     *
     * ## notes
     *
     * * the schema should not have parts that have the same delimiters in outer
     *   parts. for example, if you have the ib be space-delimited, you can't
     *   also have one of the ib parts be space-delimited.
     */
    delimiter?: string;
    /**
     * if applicable, the target of the schema should match this regular
     * expression.
     *
     * ## notes
     *
     * * note that this can also proscribe valid values (I don't think I need to
     *   add a separate field for this).
     */
    regexp?: string;
    /**
     * If true, then the given part may not exist at the given
     * scope+subscope+path+position.
     */
    optional?: boolean;
    /**
     * any additional externally-defined schemas required by this schema.
     *
     * ## notes
     *
     * * some schemas will only have extrinsic schemas, in which case
     *   `payloadType` should be 'na'.
     * * if both schema ibGibs and addrs are given, these will be combined and
     *   unique-ified.
     *
     * ## see also
     *
     * @see {@link requiredSchemaAddrs}
     * @see {@link optionalSchemas}
     * @see {@link optionalSchemaAddrs}
     */
    requiredSchemas?: SchemaIbGib_V1[],
    /**
     * @see {@link requiredSchemas}
     */
    requiredSchemaAddrs?: IbGibAddr[],
    /**
     * any additional externally-defined schemas referenced (but not required)
     * by this schema.
     *
     * ## notes
     *
     * @see notes on {@link requiredSchemas}
     *
     * ## see also

     * @see {@link requiredSchemas}
     * @see {@link requiredSchemaAddrs}
     * @see {@link optionalSchemaAddrs}
     */
    optionalSchemas?: SchemaIbGib_V1[],
    /**
     * @see {@link optionalSchemas}
     */
    optionalSchemaAddrs?: IbGibAddr[],
}): Promise<TransformResult<SchemaIbGib_V1>> {
    const lc = `[${createSchema.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 4f5dcccd624a46d39b7dc3f3a5b4ad72)`); }

        const uuid = await getUUID();
        const data: SchemaData_V1 = {
            uuid, name, description,
            payloadType,
            scope, subscope,
            delimiter, path, position, regexp,
            optional,
        };

        const validationErrors = validateSchemaData({ data });
        if (validationErrors) { throw new Error(`schema data provided invalid: ${validationErrors} (E: 2f1fdd5df23742d2bcae0991c8a19aa7)`); }

        let rel8ns: SchemaRel8ns_V1 | undefined = {};
        if ((requiredSchemaAddrs ?? []).length > 0 || (requiredSchemaAddrs ?? []).length > 0) {
            rel8ns[REQUIRED_SCHEMA_REL8N_NAME] = unique([
                ...(requiredSchemas ?? []).map(x => getIbGibAddr({ ibGib: x })),
                ...(requiredSchemaAddrs ?? []),
            ]);
        }
        if ((optionalSchemaAddrs ?? []).length > 0 || (optionalSchemaAddrs ?? []).length > 0) {
            rel8ns[OPTIONAL_SCHEMA_REL8N_NAME] = unique([
                ...(optionalSchemas ?? []).map(x => getIbGibAddr({ ibGib: x })),
                ...(optionalSchemaAddrs ?? []),
            ]);
        }

        if (Object.keys(rel8ns).length === 0) {
            rel8ns = undefined;
        }

        const resCreate = await factory.firstGen({
            parentIbGib: factory.primitive({ ib: 'schema' }),
            ib: getSchemaIb({ data }),
            data, rel8ns,
            dna: true,
            nCounter: true,
            tjp: { timestamp: true },
        }) as TransformResult<SchemaIbGib_V1>;

        return resCreate;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export function validateSchemaData({
    data,
}: {
    data: SchemaData_V1,
}): string[] | null {
    const lc = `[${validateSchemaData.name}]`;
    let errors: string[] = [];
    try {
        if (logalot) { console.log(`${lc} starting... (I: 921ca643511df99e8419d38e0eb70123)`); }

        if (!data) { throw new Error(`data required. (E: 855e5608ef5bf204b7cc8c293e322123)`); }

        const {
            uuid, name, description,
            payloadType,
            scope, subscope,
            path, position, delimiter, regexp,
        } = data;

        if (!uuid) { throw new Error(`uuid required (E: 5b9809e88477c11ef660583ddd511e23)`); }
        if (!name) { throw new Error(`name required (E: 3ce50f9fc48480f49ed0b583d77e5923)`); }
        if (name.length > MAX_SCHEMA_NAME_LENGTH) { throw new Error(`name too long. max length: ${MAX_SCHEMA_NAME_LENGTH} (E: 4bd5fa1f52676d5699c0925b22438223)`); }
        if (!payloadType) { throw new Error(`payloadType required (E: 3e20ae55a16af584d73f20e6b8dde423)`); }
        if (!scope) { throw new Error(`scope required (E: 2fcd211fc51188fc2aaaa5580800a323)`); }
        if (!SCHEMA_SCOPES.includes(scope)) { throw new Error(`invalid scope (${scope}). valid schema scopes: ${SCHEMA_SCOPES.join(', ')} (E: d7a2819aafc83f4ac1fda3a17ec94a23)`); }
        if (subscope) {
            if (!SCHEMA_SUBSCOPES.includes(subscope)) { throw new Error(`invalid subscope (${subscope}). valid schema subscopes: ${SCHEMA_SUBSCOPES.join(', ')} (E: 87acc8266c0448352fbe997a39052523)`); }
        }
        if (typeof position === 'number') {
            if (position < 0) { throw new Error(`position must be positive integer (E: e158b5002e83d221a653e6e3db48bd23)`); }
        }
        // todo: more schema validation
        return null;
    } catch (error) {
        // console.error(`${lc} ${extractErrorMsg(error)}`);
        errors.push(error.message);
        return errors;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export function getSchemaIb({
    data,
    ibDelimiter = DEFAULT_SCHEMA_IB_DELIMITER,
}: {
    data: SchemaData_V1,
    ibDelimiter?: string,
}): Ib {
    const lc = `[${getSchemaIb.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 83b2f1e745f4d3192e141b7e56091723)`); }

        ibDelimiter ??= DEFAULT_SCHEMA_IB_DELIMITER;

        let validationErrors = validateSchemaData({ data });
        if (validationErrors) { throw new Error(`schema data provided invalid: ${validationErrors} (E: c00e8917a16950295ea0f8a2defff523)`); }

        const {
            uuid,
            name, description,
            payloadType,
            scope, subscope,
            path, position, delimiter, regexp,
        } = data;

        if (name.includes(ibDelimiter)) { throw new Error(`name (${name}) includes ibDelimiter (${ibDelimiter}) (E: e6603ca2949229351f6e7436ead4f923)`); }
        if (scope.includes(ibDelimiter)) { throw new Error(`scope (${scope}) includes ibDelimiter (${ibDelimiter}) (E: bc867991526c406c8512e09fd3730d1a)`); }
        if (subscope?.includes(ibDelimiter)) { throw new Error(`subscope (${subscope})includes ibDelimiter (${ibDelimiter}) (E: 3dc6649bc9634f659def05308e539e92)`); }
        if (uuid!.includes(ibDelimiter)) { throw new Error(`uuid (${uuid}) includes ibDelimiter (${ibDelimiter}) (E: 056e3980ca56cfc90a283159ec4dd823)`); }

        // [name][scope][subscope][uuid]
        return `${name}${ibDelimiter}${scope}${ibDelimiter}${subscope ?? 'undefined'}${ibDelimiter}${uuid}`;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export interface SchemaIbInfo {
    name: string;
    scope: SchemaScope;
    subscope?: SchemaSubscope;
    uuid: string;
}

export function parseSchemaIb({
    ib,
    ibDelimiter = DEFAULT_SCHEMA_IB_DELIMITER,
}: {
    ib: Ib,
    ibDelimiter?: string,
}): SchemaIbInfo {
    const lc = `[${parseSchemaIb.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 4a7882ab7661908a25181c65f5c01923)`); }

        ibDelimiter ??= DEFAULT_SCHEMA_IB_DELIMITER;

        // return `${name}${ibDelimiter}${scope}${ibDelimiter}${subscope ?? 'undefined'}${ibDelimiter}${uuid}`;

        let [name, scope, subscope, uuid] = ib.split(ibDelimiter) as [
            string,
            SchemaScope,
            SchemaSubscope | undefined | 'undefined',
            string
        ];

        if (!name) { throw new Error(`name not found (E: 4ec42b493b3407d4aecd3878fcdfbb23)`); }
        if (!scope) { throw new Error(`scope not found (E: 10e579e4c4978b682bccc93762c5ad23)`); }
        if (!SCHEMA_SCOPES.includes(scope)) { throw new Error(`invalid scope (${scope}). valid scopes: ${SCHEMA_SCOPES.join(', ')} (E: 84c127f82f7cb332374abb6f024dc123)`); }
        if (subscope === 'undefined') { subscope = undefined; }
        if (subscope) {
            if (!SCHEMA_SUBSCOPES.includes(subscope)) { throw new Error(`invalid subscope (${subscope}). valid subscopes: ${SCHEMA_SUBSCOPES.join(', ')} (E: 96230e34313910fbd953fc6826841323)`); }
        }
        if (!uuid) { throw new Error(`uuid required (E: 3df1f85ca8ba440f6cccfa0c7c8ac323)`); }
        return { name, scope, subscope, uuid }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * validates the schema ibgib itself. this does NOT mean that
 * we are validating an ibgib against the schema. for that, use
 * one of the specialized `validateIbGibAgainstSchema...` helper functions.
 * @returns validation error if any, else returns undefined
 */
export async function validateSchemaItself({
    schemaIbGib,
}: {
    schemaIbGib: SchemaIbGib_V1,
}): Promise<string[] | null> {
    const lc = `[${validateSchemaItself.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: a672298930ea916c64748acd85891b23)`); }

        if (!schemaIbGib) { throw new Error(`schemaIbGib required (E: b34bec59ec3beeced3292d72581cb423)`); }
        if (!schemaIbGib.data) { throw new Error(`invalid schemaIbGib. data required. (E: 36fe0555a3e71f1db35e63b87988cd23)`); }

        let errors = await validateIbGibIntrinsically({ ibGib: schemaIbGib });
        if (errors) { return errors; /* <<<< returns early */ }

        errors = validateSchemaData({ data: schemaIbGib.data });
        if ((errors ?? []).length > 0) { return errors; /* <<<< returns early */ }

        return null;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export async function validateIbGibAgainstSchema({
    ibGib,
    schemaIbGibs,
    schemaIbGibAddrs,
    dontGetLatest,
    metaspace,
    space,
    alreadyValidated,
}: {
    /**
     * ibGib which we are checking against schemas.
     */
    ibGib: IbGib_V1,
    /**
     * if `schemaIbGibs` or `schemaIbGibAddrs` provided, will only check against
     * these schemas. Else, will check against the schemas rel8d to the given
     * `ibGib`.
     *
     * IOW, if this contains one or more schemas, then we're going to retrieve
     * any schema ibgibs in `schemaIbGibAddr` that aren't in `schemaIbGibs` and
     * check the given `ibGib` against **those schemas only**. We will ignore
     * any schema ibGibs listed in `ibGib.rel8ns.schema`.
     */
    schemaIbGibs?: SchemaIbGib_V1[],
    /**
     * if `schemaIbGibs` or `schemaIbGibAddrs` provided, will only check against
     * these schemas. Else, will check against the schemas rel8d to the given
     * `ibGib`.
     *
     * IOW, if this contains one or more schemas, then we're going to retrieve
     * any schema ibgibs in `schemaIbGibAddr` that aren't in `schemaIbGibs` and
     * check the given `ibGib` against **those schemas only**. We will ignore
     * any schema ibGibs listed in `ibGib.rel8ns.schema`.
     */
    schemaIbGibAddrs?: IbGibAddr[],
    /**
     * If true, will not perform a getLatest on each schema.
     * Else,
     */
    dontGetLatest?: boolean,
    /**
     * if provided, and if `space` is falsy, will use this `metaspace` to get
     * the local space in which to search for the given `ibGib`'s rel8d schema
     * (if any).
     */
    metaspace?: MetaspaceService,
    /**
     * If provided, will use this space to look for the given `ibGib`'s schemas
     * (if any).
     */
    space?: IbGibSpaceAny,
    /**
     *
     */
    alreadyValidated?: SchemaIbGib_V1[],
}): Promise<string[] | null> {
    const lc = `[${validateIbGibAgainstSchema.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 29e2e5f78ca47ddb48bd781487bd9423)`); }

        schemaIbGibs ??= [];
        schemaIbGibAddrs ??= [];
        alreadyValidated ??= [];


        // validate this first. if passes, then go for other rel8d schema ibgibs
        // - first required and then optional

        let ibGibsToDo: SchemaIbGib_V1[] = [];
        let ibGibAddrsToGet: IbGibAddr[] = [];
        let alreadyValidatedAddrs = alreadyValidated.map(x => getIbGibAddr({ ibGib: x }));
        ibGibAddrsToGet = schemaIbGibAddrs ?
            schemaIbGibAddrs.filter(addr => { throw new Error(`not impl (E: 04f2da39a55fd2f604df6da568db4123)`); }) :
            [];

        throw new Error(`not fully impl yet (E: 7e556457aea3020f4f7b21617e2ded23)`);
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * wrapper function that rel8's a `schemaIbGib` to a given `ibGib`.
 *
 * does NOT persist atow
 */
export async function rel8Schema({
    ibGib,
    schemaIbGib,
    isOptional,
    dna,
    nCounter,
    noTimestamp,
    linkedRel8ns,
}: {
    ibGib: IbGib_V1,
    schemaIbGib: SchemaIbGib_V1,
    isOptional?: boolean,
    dna?: boolean;
    nCounter?: boolean;
    noTimestamp?: boolean;
    linkedRel8ns?: string[];
}): Promise<TransformResult<IbGib_V1>> {
    const lc = `[${rel8Schema.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 15a1b820b7acce39b9bb316817dded23)`); }

        let resTransform = await rel8({
            type: 'rel8',

            // specific to schema
            src: ibGib,
            rel8nsToAddByAddr: {
                [isOptional ? OPTIONAL_SCHEMA_REL8N_NAME : REQUIRED_SCHEMA_REL8N_NAME]:
                    [getIbGibAddr({ ibGib: schemaIbGib })]
            },

            // pass through opts
            dna, nCounter, noTimestamp, linkedRel8ns,
        });

        return resTransform;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}
