/**
 * @module schema-constants
 */

import { IbGibData_V1, IbGibRel8ns_V1, IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { WitnessWithContextData_V1, WitnessWithContextRel8ns_V1 } from '@ibgib/core-gib/dist/witness/witness-with-context/witness-with-context-types.mjs';

/**
 * If a schema rel8s to other schema(s) via this rel8nName, then those also must
 * apply (similar to additional constraints/requirements).
 */
export const REQUIRED_SCHEMA_REL8N_NAME = 'required_schema';
/**
 * If a schema rel8s to other schema(s) via this rel8nName, then those also may
 * apply (similar to additional optional constraints/requirements).
 */
export const OPTIONAL_SCHEMA_REL8N_NAME = 'optional_schema';

/**
 * maximum number of chars in schema name
 */
export const MAX_SCHEMA_NAME_LENGTH = 64;

/**
 * atow by default, space is the schema-ib delimiter.
 */
export const DEFAULT_SCHEMA_IB_DELIMITER = ' ';
