import { cwd, chdir, } from 'node:process';
import { statSync } from 'node:fs';
import { mkdir, } from 'node:fs/promises';
import * as pathUtils from 'path';

import { delay, extractErrorMsg, getTimestampInTicks, getUUID } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';

import {
    firstOfAll, firstOfEach, ifWe, ifWeMight, iReckon,
    lastOfAll, lastOfEach, respecfully, respecfullyDear
} from '@ibgib/helper-gib/dist/respec-gib/respec-gib.mjs';
const maam = `[${import.meta.url}]`, sir = maam;

import { GLOBAL_LOG_A_LOT } from '../../../ibgib-constants.mjs';
import { RCLI_TEST_PATH } from './rcli-constants.mjs';

const logalot: boolean | number = GLOBAL_LOG_A_LOT;

const lcFile: string = `[${pathUtils.basename(import.meta.url)}]`;

export const initialCwd = cwd();
if (logalot) { console.log(`${lcFile} initialCwd: ${initialCwd} (I: 07f62b8d3f78ce20b68f1f18ac81f723)`); }
export const baseRespecPath = pathUtils.join(cwd(), RCLI_TEST_PATH);

async function initRespecPath(): Promise<void> {
    let lc: string = `${lcFile}[${initRespecPath.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 6cddbed3980c675aecb952bee3141723)`); }
        if (logalot) { console.log(`${lc} baseRespecPath: ${baseRespecPath} (I: f1455f7eb8c19fd0379e10c820c64b23)`); }

        let respecPathStat = statSync(baseRespecPath, { throwIfNoEntry: false })
        if (!respecPathStat) {
            await mkdir(baseRespecPath, { recursive: true });
        }
        chdir(baseRespecPath);
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}
await initRespecPath();

export async function createUniqueRespecSubpath(basePath: string = baseRespecPath): Promise<void> {
    const lc = `[${createUniqueRespecSubpath.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: b841532bcb2fdb4bcfa3819db1673c23)`); }
        let subpath = pathUtils.join(basePath || baseRespecPath, `${getTimestampInTicks()}_${(await getUUID()).slice(0, 5)}`);
        await mkdir(subpath, { recursive: true });
        chdir(subpath);
        if (logalot) { console.log(`${lc} cwd (after chdir): ${cwd()} (I: 0cb54386e6d1462182ffc8065021558f)`) }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }

}
