import { cwd, chdir, } from 'node:process';
import { statSync } from 'node:fs';
import { mkdir, readdir, } from 'node:fs/promises';
import * as pathUtils from 'path';

import { extractErrorMsg, getSaferSubstring, } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { RCLIArgInfo, RCLIArgType, } from "@ibgib/helper-gib/dist/rcli/rcli-types.mjs";
import { extractArgValue, } from "@ibgib/helper-gib/dist/rcli/rcli-helper.mjs";
import { promptForConfirm, promptForSecret, promptForSecret_node } from "@ibgib/helper-gib/dist/helpers/node-helper.mjs";
import { TransformResult } from '@ibgib/ts-gib/dist/types.mjs';
import { InnerSpace_V1, } from '@ibgib/core-gib/dist/witness/space/inner-space/inner-space-v1.mjs'
import { Metaspace_Innerspace } from '@ibgib/core-gib/dist/witness/space/metaspace/metaspace-innerspace/metaspace-innerspace.mjs';
import { BOOTSTRAP_IBGIB_ADDR } from '@ibgib/core-gib/dist/witness/space/bootstrap/bootstrap-constants.mjs';

import { PARAM_INFO_SPACE_NAME, } from '../rcli-constants.mjs';
import { alertUser, debugLogInnerSpace, extractArg_outputPath, promptForText, } from '../rcli-helper.mjs';
import { GLOBAL_LOG_A_LOT } from '../../../../ibgib-constants.mjs';
import { RCLICommentIbGib_V1 } from '../../../../common/rcli-comment/rcli-comment-types.mjs';
import { Metaspace_Nodespace } from '../../../space/metaspace/metaspace-nodespace/metaspace-nodespace.mjs';

/**
 * used in verbose logging (across all ibgib libs atow)
 */
const logalot = GLOBAL_LOG_A_LOT;


/**
 *
 * @returns
 */
export async function execInit_getOutputPath({
    argInfos,
}: {
    argInfos: RCLIArgInfo<RCLIArgType>[],
}): Promise<string> {
    const lc = `[${execInit_getOutputPath.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 0976ac0131830443ed3886baad68ba23)`); }
        if (logalot) { console.log(`${lc} cwd: ${cwd()} (I: 3da0ca2dac3b00f9693683128fd69e23)`); }

        // todo: do more validation on output path
        const outputPath = await extractArg_outputPath({ argInfos, warnIfExists: true, createUniqueSubdirIfExists: true });
        if (!outputPath) { throw new Error(`was unable to get output data path from the args. (E: 5f766a15bb454850868e5806a33b8a87)`); }

        // confirm if outputPath is not empty
        let stat = statSync(outputPath, { throwIfNoEntry: false });
        if (!stat) {
            // outputPath does not exist
            await mkdir(outputPath);
            if (logalot) { console.log(`${lc} cwd after mkdir: ${cwd()} (I: 4f8d8e82fa05e066476ef84313eabb23)`); }
        }
        stat = statSync(outputPath);
        if (!stat.isDirectory()) { throw new Error(`outputPath (${outputPath}) must be a directory. (E: f295b7e925534546819edfef9a750164)`); }
        let resRead = await readdir(outputPath);
        if ((resRead?.length ?? 0) > 0) {
            // throw if outputPath already contains a bootstrap^gib
            if (resRead.some(x => x.includes(BOOTSTRAP_IBGIB_ADDR))) {
                throw new Error(`can't init because ${BOOTSTRAP_IBGIB_ADDR} file already exists on outputPath (${outputPath}) (E: f1d98b5fe961b587be6035671a984123)`);
            } else {
                const resConfirm = await promptForConfirm({
                    msg: `outputPath (${outputPath}) is not empty. are you sure you want to init here?`
                });
                if (!confirm) { throw new Error(`cancelling...`); }
            }
            // } else {
            // outputPath is a valid, empty directory
        }

        return outputPath;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export async function execInit_nodeMetaspace({
    argInfos,
    resRCLICommentIbGib,
}: {
    argInfos: RCLIArgInfo<RCLIArgType>[],
    resRCLICommentIbGib: TransformResult<RCLICommentIbGib_V1>,
}): Promise<void> {
    const lc = `[${execInit_nodeMetaspace.name}]`;
    const initialCwd = cwd();
    try {
        if (logalot) { console.log(`${lc} starting... (I: 9f88eea0e79a49ea9e532ca2a16f9198)`); }

        // first ensure we have an output path that we can work with
        const outputPath = await execInit_getOutputPath({ argInfos });

        let spaceName = extractArgValue<string>({ paramInfo: PARAM_INFO_SPACE_NAME, argInfos }) as string | undefined;

        // default space name to dir name
        if (!spaceName) {
            const initialDirName_safe = getSaferSubstring({ text: pathUtils.basename(initialCwd) });
            if (logalot) { console.log(`${lc} space name not specified. there are two options at this point: 1) use the safer text of initialCwd (${initialCwd}) cwd folder (${initialDirName_safe}), and 2) prompt the user with probably defaulting to containing dir. atow (12/2023), we use option #1 (I: e48df240bc1b6c3fe99f6152a6772523)`); }
            spaceName = initialDirName_safe;
        }

        // prepare init
        if (logalot) { console.log(`${lc} changing execution context\initialCwd: ${initialCwd}\noutputPath: ${outputPath}. (I: d84e0d90a612f15ae18a033577fba723)`); }
        chdir(outputPath);
        if (logalot) { console.log(`${lc} cwd path after chdir(${outputPath}): ${cwd()} (I: 13374210397148ce8fbf31edf8ecbb5f)`); }

        //  initialize the ibgibs service
        if (logalot) { console.log(`${lc} creating metaspace... (I: ddeeb537aa994fc29a3bd15821702b8a)`); }
        let metaspace = new Metaspace_Nodespace(/*cacheSvc*/undefined);
        if (logalot) { console.log(`${lc} creating metaspace complete. initializing... (I: 616c2ae20c6a4e6e9612798726810cd5)`); }

        // initialize with node fs space for local space
        await metaspace.initialize({
            spaceName,
            metaspaceFactory: undefined,
            getFnAlert: () => { return ({ title, msg }) => alertUser({ title, msg, }) },
            getFnPrompt: () => { return ({ title, msg }) => promptForText({ title, msg, confirm: false }); },
            getFnPromptPassword: () => { return () => promptForSecret({ confirm: true }) },
        });
        // console.log('\n\n\n')
        // console.log(`metaspace.zerospace innerspace:\n${pretty(metaspace.zeroSpace)}`)
        // console.log('\n\n\n')
        // console.log(`metaspace localUserSpace:\n${pretty(await metaspace.getLocalUserSpace({ lock: false }))}`)
        // console.log('\n\n\n')
        if (logalot) { console.log(`${lc} initialize metaspace complete. (I: d850228ed6e548b0ac8950c87f7ccc47)`); }

        // save RCLI comment ibgib in space after initialized
        if (logalot) { console.log(`${lc} saving resRCLICommentIbGib (I: 25a45ca0ddee42bda29a9e6ce72eeea2)`); }
        await metaspace.persistTransformResult({ resTransform: resRCLICommentIbGib });
        if (logalot) { console.log(`${lc} saving resRCLICommentIbGib complete. (I: c27550042e5f4d9db746a7e21d0942f1)`); }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} changing back to initialCwd: ${initialCwd} (I: 17478213fda98ba9466a3d4a90172f23)`); }
        chdir(initialCwd)
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export async function execInit_inMemoryMetaspace({
    argInfos,
    resRCLICommentIbGib,
}: {
    argInfos: RCLIArgInfo<RCLIArgType>[],
    resRCLICommentIbGib: TransformResult<RCLICommentIbGib_V1>,
}): Promise<void> {
    const lc = `[${execInit_inMemoryMetaspace.name}]`;
    const initialCwd = cwd();
    try {
        if (logalot) { console.log(`${lc} starting... (I: 9f88eea0e79a49ea9e532ca2a16f9198)`); }
        throw new Error(`not impl yet (E: 4d2a1295b99d8a16e4573a9b9887bd23)`);
        console.warn(`${lc} IN-MEMORY NOT FULLY IMPLEMENTED. (W: 95d76dd332fc457da83609ba7328f4fe)`)
        console.warn(`${lc} IN-MEMORY NOT FULLY IMPLEMENTED. (W: 95d76dd332fc457da83609ba7328f4fe)`)
        console.warn(`${lc} IN-MEMORY NOT FULLY IMPLEMENTED. (W: 95d76dd332fc457da83609ba7328f4fe)`)

        const spaceName = extractArgValue<string>({ paramInfo: PARAM_INFO_SPACE_NAME, argInfos }) as string | undefined;

        // at this point of calling, this should be as simple as instantiating a
        // metaspace and calling its init.

        //  initialize the ibgibs service
        if (logalot) { console.log(`${lc} creating metaspace... (I: 5e618520841e44258e5fd282d49ddc4d)`); }
        let metaspace = new Metaspace_Innerspace(/*cacheSvc*/undefined);
        if (logalot) { console.log(`${lc} creating metaspace complete. initializing... (I: d988dda3413845ceae153cfa90bd91fc)`); }
        // in-memory flag provided, so change zerospace and local space
        // factory functions to use in-memory innerspaces.
        await metaspace.initialize({
            spaceName,
            // notice this returns the same instance and doesn't just pass
            // on the factory function.  this is because we want the space
            // in memory, but it has to persist across zerospace calls.
            metaspaceFactory: undefined,
            getFnAlert: () => { return ({ title, msg }) => alertUser({ title, msg, }) },
            getFnPrompt: () => { return ({ title, msg }) => promptForText({ title, msg, confirm: false }); },
            getFnPromptPassword: () => { return () => promptForSecret({ confirm: true }) },
        });
        if (logalot) {
            console.log(`metaspace.zerospace innerspace:`);
            await debugLogInnerSpace({ space: metaspace.zeroSpace as InnerSpace_V1 });
            console.log('\n\n\n')
            console.log(`metaspace localUserSpace:`)
            let localSpace = await metaspace.getLocalUserSpace({ lock: false });
            await debugLogInnerSpace({ space: localSpace as InnerSpace_V1 });
            console.log('\n\n\n')
        }
        if (logalot) { console.log(`${lc} initialize metaspace complete. (I: 0b703a0394fe4fb5810b023692bcd7ca)`); }

        // save RCLI comment ibgib in space after initialized
        if (logalot) { console.log(`${lc} saving resRCLICommentIbGib (I: 33bfed57f9524889ba6d2d030343560b)`); }
        await metaspace.persistTransformResult({ resTransform: resRCLICommentIbGib });
        if (logalot) { console.log(`${lc} saving resRCLICommentIbGib complete. (I: 89c16cd79ec64b6983f9ad5fbdad50e8)`); }

        // create app in space
        // create robbot in space

    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} changing back to initialCwd: ${initialCwd} (I: 17478213fda98ba9466a3d4a90172f23)`); }
        chdir(initialCwd)
        if (logalot) { console.log(`${lc} complete.`); }
    }
}
