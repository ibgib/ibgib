import { pretty } from "@ibgib/helper-gib/dist/helpers/utils-helper.mjs";

import { RCLI_DEFAULT_COMMAND_ESCAPE_STRING, RCLI_MAX_ESCAPE_STRING_LENGTH, PARAM_INFOS, RCLI_DEFAULT_OUTPUT_PATH } from "../rcli-constants.mjs";
import { DEFAULT_ROBBOT_REQUEST_ESCAPE_STRING } from "@ibgib/core-gib/dist/witness/robbot/robbot-types.mjs";

export function showHelp({ args }: { args: string[] }): void {
  console.error('1')
  console.error('2')
  console.error('3')
  const helpMsg = `
THIS IS INCOMPLETE, STUBBED HELP DOCUMENTATION ATOW. CHECK OUT RCLI.MTS AND
THE LIBRARY ITSELF.

howdy from ibgib!!

# mind shift

first off, think of the repl less as a rcli and more as a friendly chat where
every piece (and metapiece) of data is its own timeline. ignoring initialization
details for the moment, each chat has a context - itself being an ibgib
timeline.

a "context" is like the "current directory" within which you are working. as
such, any timeline ibgib can be a "directory".  when you enter a "command" or a
"request" or some other comment, you not only create that data (a comment ibgib
representing your text) but you also relate it to the current context. it's
kind of like creating a file and saving it to the current working directory.

the first comment ibgib, i.e. from the command + args invoked from the os
prompt, becomes the starting context ibgib. if that's an interactive repl
command, then the repl starts and subsequent commands, requests and others are
first converted to comment ibgibs and related to the context, thus updating the
context's timeline.

the rcli app itself is one participant of the chat. it is actually doing the
same thing you the user is doing - "listening" for updates to the context ibgib
and if it sees one that has a command (a comment that starts with the
${RCLI_DEFAULT_COMMAND_ESCAPE_STRING}), then it handles that command. this
usually involves creating ibgibs. if it updates the context ibgib, then that
will be reflected in the chat.

> pro tip: the app itself's internal data (like preferences/settings) is an ibgib timeline. and when we implement changing settings, etc., what we will be doing is mutating the app's timelines. **so the app is an instantiated object in memory hydrated with the app data you create on init**. in ibgib, any object that has behavior is a "witness".

the other participant in the chat is a single robbot chat agent named rolly from
request/robbot line interface (though you can change this name when you init).
rolly is here to help interface with the rcli app. rolly works the same way as
you the user and the app itself does - rolly watches the context ibgib for
changes and reacts accordingly, sometimes creating subsequent ibgibs.

# quickstart

## params, args and commands

the custom params/args plumbing is from @ibgib/helper-gib and is relatively
basic atm. here are some of the rules:

* all args must start with --
* most args should be in the form --arg-name=value
  * single or double quotes for values with spaces, e.g. --arg-name="some value whatever"
* boolean flags set to true can just be --some-boolean
* params have a primary name but also synonyms.
  * e.g. --help, --h
    * note the synonym still requires two dashes, so it's not -h
  * the hypernym that i use for name/synonyms is identifier
  * identifiers are **not** case sensitive.

> pro tip: the **app** interprets commands, robbots are more natural language.

## quick examples from os command line
_note: if working while developing with source code, replace the following "ibgib" commands with "node ."

* ibgib --help
  * shows the primary help for the rcli
* ibgib --init --output-path="abs/or/rel/path/to/directory"
  * initializes a local space in the given path
  * if output-path is not present, will prompt to use default ${RCLI_DEFAULT_OUTPUT_PATH}
* ibgib --repl --data-path="abs/or/rel/path/to/directory"
  * if data-path is not present, will prompt to use default ${RCLI_DEFAULT_OUTPUT_PATH}


# examples from within ibgib interactive repl

## to invoke commands to be handled by the chat app

* prefix with ${RCLI_DEFAULT_COMMAND_ESCAPE_STRING}
  * e.g. ${RCLI_DEFAULT_COMMAND_ESCAPE_STRING}fork --src-addr="ib^gib"**

## to "speak" directly to rolly, i.e. issue "requests"

* prefix with ${DEFAULT_ROBBOT_REQUEST_ESCAPE_STRING}
  * e.g. ${DEFAULT_ROBBOT_REQUEST_ESCAPE_STRING}hello


## overall workflow

# command params

* these app commands are implemented as boolean flag params.


## init

this bootstraps an ibgib locally in the given data-path. it should be empty,
but will warn if it isn't. it will fail if there's already a bootstrap^gib
in the (atow) meta folder.

when you issue any command, the first thing that occurs is that an RCLI comment
ibgib is created and added to the default local space. this even includes this
\`init\` request, though this happens after the space itself is created and
initialized.

# options

## data-path

usually a path to a root of a local ibgib space.

todo: flesh this out eh

for now, here are the PARAM_INFOS in src and possibly implemented:

${pretty(PARAM_INFOS)}

# examples

    ibgib --help
    ibgib --init
    ibgib --init --output-path="rel/or/abs/path/to/folder"

THIS IS INCOMPLETE, STUBBED HELP DOCUMENTATION ATOW. CHECK OUT RCLI.MTS AND
THE LIBRARY ITSELF. ATOW ALL NON-FLAG PARAMETER VALUES MUST BE IN DOUBLE QUOTES.
`;
  console.log(helpMsg);
}
