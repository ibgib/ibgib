import { cwd, chdir, } from 'node:process';
import { statSync } from 'node:fs';
import { readdir, } from 'node:fs/promises';
import * as pathUtils from 'path';

import { pretty, extractErrorMsg, pickRandom_Letters, clone, getUUID } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { RCLIArgInfo, RCLIArgType, } from "@ibgib/helper-gib/dist/rcli/rcli-types.mjs";
import { extractArgValue, } from "@ibgib/helper-gib/dist/rcli/rcli-helper.mjs";
import { promptForSecret, } from "@ibgib/helper-gib/dist/helpers/node-helper.mjs";
import { IbGibAddr, TransformResult } from '@ibgib/ts-gib/dist/types.mjs';
import { getIbAndGib, getIbGibAddr } from '@ibgib/ts-gib/dist/helper.mjs';
import { Factory_V1 as factory } from '@ibgib/ts-gib/dist/V1/factory.mjs';
import { Rel8n } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { createNewApp, getAppIb, getInfoFromAppIb, } from '@ibgib/core-gib/dist/witness/app/app-helper.mjs';
import { APP_REL8N_NAME } from '@ibgib/core-gib/dist/witness/app/app-constants.mjs'
import { IbGibSpaceAny } from '@ibgib/core-gib/dist/witness/space/space-base-v1.mjs';
import { getFromSpace } from '@ibgib/core-gib/dist/witness/space/space-helper.mjs';
import { MetaspaceService } from '@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs';
import { IBGIB_SPACE_NAME_DEFAULT, } from '@ibgib/core-gib/dist/witness/space/space-constants.mjs';
import { AppIbGib_V1, AppPromptResult } from '@ibgib/core-gib/dist/witness/app/app-types.mjs';
import { IbGibAppAny } from '@ibgib/core-gib/dist/witness/app/app-base-v1.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../../ibgib-constants.mjs';
import {
    PARAM_INFO_APP_ID, PARAM_INFO_LOCAL_SPACE_ID,
    PARAM_INFO_LOCAL_SPACE_NAME, RCLI_DEFAULT_OUTPUT_PATH,
} from '../rcli-constants.mjs';
import {
    alertUser, extractArg_dataPath, extractArg_space, promptForText,
} from '../rcli-helper.mjs';
import { RCLICommentIbGib_V1 } from '../../../../common/rcli-comment/rcli-comment-types.mjs';
import { Metaspace_Nodespace } from '../../../space/metaspace/metaspace-nodespace/metaspace-nodespace.mjs';
import { RCLIApp_V1, RCLIApp_V1_Factory } from '../rcli-app-v1.mjs';
import {
    DEFAULT_RCLI_APP_DATA_V1, DEFAULT_RCLI_APP_REL8NS_V1,
    RCLIAppData_V1, RCLIAppRel8ns_V1,
} from '../rcli-types.mjs';

/**
 * used in verbose logging (across all ibgib libs atow)
 */
const logalot = GLOBAL_LOG_A_LOT;

export async function execInteractive_RCLIApp({
    argInfos,
    resRCLICommentIbGib,
}: {
    argInfos: RCLIArgInfo<RCLIArgType>[],
    resRCLICommentIbGib: TransformResult<RCLICommentIbGib_V1>,
}): Promise<void> {
    const lc = `[${execInteractive_RCLIApp.name}]`;
    const initialPath = cwd();
    try {
        if (logalot) { console.log(`${lc} starting... (I: 3ca1138416ea4978ae1b8e2b16c43d58)`); }

        let { metaspace, localUserSpace } = await execInteractive_RCLIApp_setupInteraction({ argInfos, resRCLICommentIbGib, initialPath });

        // get RCLI app
        const rcliAppId = extractArgValue<string>({ paramInfo: PARAM_INFO_APP_ID, argInfos, throwIfNotFound: false }) as string | undefined;
        const rcliApp = await execInteractive_getRCLIApp({ metaspace, space: localUserSpace, rcliAppId });

        // now that we have the app, we can pass it the comment. the RCLI itself
        // can interpret the --interactive and create a chat session context
        // within which the user can operate.

        // but first, persist the RCLI comment ibgib (and graph) first...
        if (logalot) { console.log(`${lc} saving resRCLICommentIbGib (I: 19d585844819492b88786aeb9d87290e)`); }
        await metaspace.persistTransformResult({ resTransform: resRCLICommentIbGib });
        if (logalot) { console.log(`${lc} saving resRCLICommentIbGib complete. (I: ee1cb6c07cf84116b157e90efbf8ed65)`); }

        // ...now pass this to the RCLI app
        const rcliCommentIbGibFromUser = resRCLICommentIbGib.newIbGib;
        rcliApp.ibgibsSvc = metaspace;
        const _resRCLIApp = await rcliApp.witness(rcliCommentIbGibFromUser);

    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        // if (logalot) { console.log(`${lc} changing back to initialPath: ${initialPath} (I: 17478213fda98ba9466a3d4a90172f23)`); }
        // setTimeout(() => {
        //     chdir(initialPath)
        // });
        if (logalot) { console.log(`${lc} complete.`); }
    }
}
export async function execOneOff_RCLIApp({
    argInfos,
    resRCLICommentIbGib,
}: {
    argInfos: RCLIArgInfo<RCLIArgType>[],
    resRCLICommentIbGib: TransformResult<RCLICommentIbGib_V1>,
}): Promise<void> {
    const lc = `[${execOneOff_RCLIApp.name}]`;
    console.log(`${lc} cwd(): ${cwd()}`);
    const initialPath = cwd();
    try {
        if (logalot) { console.log(`${lc} starting... (I: 3ca1138416ea4978ae1b8e2b16c43d58)`); }

        let { metaspace, localUserSpace } =
            await execOneOff_RCLIApp_setupInteraction({ argInfos, initialPath });

        // get RCLI app
        const rcliAppId = extractArgValue<string>({ paramInfo: PARAM_INFO_APP_ID, argInfos, throwIfNotFound: false }) as string | undefined;
        const rcliApp = await execInteractive_getRCLIApp({ metaspace, space: localUserSpace, rcliAppId });

        // now that we have the app, we can pass it the comment. the RCLI itself
        // can interpret the --interactive and create a chat session context
        // within which the user can operate.

        // but first, adjust the comment ibgib because this is a one-off and
        // THEN persist the RCLI comment ibgib (and graph)...
        if (logalot) { console.log(`${lc} saving resRCLICommentIbGib (I: 19d585844819492b88786aeb9d87290e)`); }
        await metaspace.persistTransformResult({ resTransform: resRCLICommentIbGib });
        if (logalot) { console.log(`${lc} saving resRCLICommentIbGib complete. (I: ee1cb6c07cf84116b157e90efbf8ed65)`); }

        // ...now pass this to the RCLI app
        const rcliCommentIbGibFromUser = resRCLICommentIbGib.newIbGib;
        rcliCommentIbGibFromUser.oneOff = true;
        rcliApp.ibgibsSvc = metaspace;
        const _resRCLIApp = await rcliApp.witness(rcliCommentIbGibFromUser);

    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        // if (logalot) { console.log(`${lc} changing back to initialPath: ${initialPath} (I: 17478213fda98ba9466a3d4a90172f23)`); }
        // chdir(initialPath)
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export async function execInteractive_RCLIApp_setupInteraction({
    argInfos,
    resRCLICommentIbGib,
    initialPath,
}: {
    argInfos: RCLIArgInfo<RCLIArgType>[],
    resRCLICommentIbGib: TransformResult<RCLICommentIbGib_V1>,
    initialPath: string,
}): Promise<{ metaspace: MetaspaceService; localUserSpace: IbGibSpaceAny }> {
    const lc = `[${execInteractive_RCLIApp_setupInteraction.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 7dc6b7e185bcc34765a6065a59d67223)`); }

        // first ensure we have a data path that we can work with
        const dataPath = await execInteractiveOrOneOff_getDataPath({ argInfos });
        if (logalot) { console.log(`${lc} dataPath found: ${dataPath} (I: 605f7253eb3e4e7e73f277ad1b26c823)`); }

        // initialize our execution context using dataPath and load its metaspace
        if (logalot) { console.log(`${lc} changing execution context\ninitialPath: ${initialPath}\ndataPath: ${dataPath}. (I: 2c4f3244b8e241e5b6fdfb3a785f4263)`); }
        chdir(dataPath);
        if (logalot) { console.log(`${lc} cwd path after chdir(${dataPath}): ${cwd()} (I: 9407d5e22fdb4777ada66ac32f7eb5a3)`); }

        // create initialize the metaspace
        if (logalot) { console.log(`${lc} creating metaspace... (I: 3f5f2fff7392436596c05d918c9e1bb2)`); }
        let metaspace = new Metaspace_Nodespace(/*cacheSvc*/undefined);
        if (logalot) { console.log(`${lc} creating metaspace complete. initializing... (I: 46a001efbb04416ca48c11f13760e986)`); }

        // // initialize with node fs space for local space
        await metaspace.initialize({
            // spaceName: undefined,
            metaspaceFactory: undefined,
            getFnAlert: () => { return ({ title, msg }) => alertUser({ title, msg, }) },
            getFnPrompt: () => { return ({ title, msg }) => promptForText({ title, msg, confirm: false }); },
            getFnPromptPassword: () => { return () => promptForSecret({ confirm: true }) },
        });
        if (logalot) {
            console.log('\n\n\n')
            console.log(`${lc} metaspace.zerospace innerspace:\n${pretty(metaspace.zeroSpace)}`)
            console.log('\n\n\n')
            console.log(`${lc} metaspace localUserSpace:\n${pretty(await metaspace.getLocalUserSpace({ lock: false }))}`)
            console.log('\n\n\n')
            console.log(`${lc} initialize metaspace complete. (I: d6b7a59c1d124b14a357f4799b5ac920)`);
        }

        /**
         * space to work with (in case the user specifies a certain space).
         *
         * atow (July 30, 2023) can specify by space id
         */
        const localUserSpace = await extractArg_space({
            argInfos, metaspace,
            returnOnArgsFalsy: 'default',
            spaceIdParamInfo: PARAM_INFO_LOCAL_SPACE_ID,
            spaceNameParamInfo: PARAM_INFO_LOCAL_SPACE_NAME,
        });
        if (!localUserSpace) { throw new Error(`could not get localUserSpace from metaspace (E: bbbe51c6c4e5fc3b1dd0493f2d2fe423)`); }

        return { metaspace, localUserSpace };
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export async function execOneOff_RCLIApp_setupInteraction({
    argInfos,
    initialPath,
}: {
    argInfos: RCLIArgInfo<RCLIArgType>[],
    initialPath: string,
}): Promise<{ metaspace: MetaspaceService; localUserSpace: IbGibSpaceAny }> {
    const lc = `[${execOneOff_RCLIApp_setupInteraction.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 0b4b9c5382284bcd993609d201b4941f)`); }

        // first ensure we have a data path that we can work with
        const dataPath = await execInteractiveOrOneOff_getDataPath({ argInfos });
        if (logalot) { console.log(`${lc} dataPath found: ${dataPath} (I: 57193766920d4eb0a8f0ace2e3af99a3)`); }

        // initialize our execution context using dataPath and load its metaspace
        if (logalot) { console.log(`${lc} changing execution context\ninitialPath: ${initialPath}\ndataPath: ${dataPath}. (I: 69f0ac758d7d46109f29ee8ac3d086fb)`); }
        chdir(dataPath);
        if (logalot) { console.log(`${lc} cwd path after chdir(${dataPath}): ${cwd()} (I: 162104fab0d64c59836a46e24bf0039e)`); }

        // create initialize the metaspace
        if (logalot) { console.log(`${lc} creating metaspace... (I: c174db1cae124ad5babb07745a23cf0c)`); }
        let metaspace = new Metaspace_Nodespace(/*cacheSvc*/undefined);
        if (logalot) { console.log(`${lc} creating metaspace complete. initializing... (I: 23b23269935e4d2ca44f5197f5bebac1)`); }

        // // initialize with node fs space for local space
        await metaspace.initialize({
            // spaceName: undefined,
            metaspaceFactory: undefined,
            getFnAlert: () => { return ({ title, msg }) => alertUser({ title, msg, }) },
            getFnPrompt: () => { return ({ title, msg }) => promptForText({ title, msg, confirm: false }); },
            getFnPromptPassword: () => { return () => promptForSecret({ confirm: true }) },
        });
        if (logalot) {
            console.log('\n\n\n')
            console.log(`${lc} metaspace.zerospace innerspace:\n${pretty(metaspace.zeroSpace)}`)
            console.log('\n\n\n')
            console.log(`${lc} metaspace localUserSpace:\n${pretty(await metaspace.getLocalUserSpace({ lock: false }))}`)
            console.log('\n\n\n')
            console.log(`${lc} initialize metaspace complete. (I: 273b41c502144d05930d5c29c3311fcd)`);
        }

        /**
         * space to work with (in case the user specifies a certain space).
         *
         * atow (July 30, 2023) can specify by space id
         */
        const localUserSpace = await extractArg_space({
            argInfos, metaspace,
            returnOnArgsFalsy: 'default',
            spaceIdParamInfo: PARAM_INFO_LOCAL_SPACE_ID,
            spaceNameParamInfo: PARAM_INFO_LOCAL_SPACE_NAME,
        });
        if (!localUserSpace) { throw new Error(`could not get localUserSpace from metaspace (E: 4cd46cc21b5d4c82a2c6b5513d3687cd)`); }

        return { metaspace, localUserSpace };
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 *
 * @returns
 */
async function execInteractiveOrOneOff_getDataPath({
    argInfos,
}: {
    argInfos: RCLIArgInfo<RCLIArgType>[],
}): Promise<string> {
    const lc = `[${execInteractiveOrOneOff_getDataPath.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: fb09fdba3ff74eeca02c64669b6870c7)`); }
        if (logalot) { console.log(`${lc} cwd: ${cwd()} (I: 6e46229646344c72af2ca0d3eee3b156)`); }

        // todo: do more validation on datapath for interactive repl exec

        const dataPath = await extractArg_dataPath({
            argInfos, expectType: 'directory',
            throwIfNotExpected: false,
            promptIfNotExpected: true,
            defaultPath: RCLI_DEFAULT_OUTPUT_PATH,
        });
        if (!dataPath) { throw new Error(`could not find valid data path. --data-path arg should be either the bootstrap^gib.json file or the root directory of the ibgibs folder (which itself contains an initialized ibgib structure including the bootstrap^gib.json file) (E: 4842e8ef6b8edf42ab50d0aeece21523)`); }

        // confirm if dataPath is not empty
        let stat = statSync(dataPath, { throwIfNoEntry: false });
        if (!stat) { throw new Error(`(UNEXPECTED) dataPath does not exist? expected exist at this point. (E: d2eb3b37b0dea1e8b1a3a21593722723)`); }
        if (!stat.isDirectory()) { throw new Error(`dataPath (${dataPath}) must be a directory. (E: a1010570166c43e38223185224234923)`); }

        let resRead = await readdir(dataPath);

        if ((resRead?.length ?? 0) > 0) {
            // validate dataPath a little
            const ibgibSubDir = `ibgib`;
            stat = statSync(pathUtils.join(dataPath, ibgibSubDir));
            if (!stat) { throw new Error(`no ${ibgibSubDir} subdirectory (${ibgibSubDir}) found in dataPath (${dataPath}). an initialized dataPath includes an ibgib subdirectory, within which is a zerospace, which itself should contain a meta subdirectory with the bootstrap ibgib. (E: 0523e1f794413c4d149481a8643b3923)`); }
            if (!stat.isDirectory()) { throw new Error(`invalid dataPath. child ${ibgibSubDir} is not a directory? (E: 0404773eee234f51a4d4671d225d78dd)`); }

            resRead = await readdir(pathUtils.join(dataPath, ibgibSubDir));
            const zerospaceSubDir = `000_${IBGIB_SPACE_NAME_DEFAULT}`;
            if (!resRead.some(x => x === `000_${IBGIB_SPACE_NAME_DEFAULT}`)) {
                if (resRead.some(x => x === '000_default_space')) {
                    throw new Error(`zero space folder has changed from 000_default_space in the MVP to ${IBGIB_SPACE_NAME_DEFAULT}. please rename this dir (E: 2dcd619d254c1b0db7d95b1716edc623)`);
                } else {
                    throw new Error(`no zero space found (E: a2b07fcd91316e24f26143710bd99223)`);
                }
            }
            stat = statSync(pathUtils.join(dataPath, ibgibSubDir, zerospaceSubDir));
            if (!stat) { throw new Error(`no zerospace subdirectory (${zerospaceSubDir}) found in dataPath (${dataPath}). an initialized dataPath includes a zerospace, which itself should contain a meta subdirectory with the bootstrap ibgib. (E: 0523e1f794413c4d149481a8643b3923)`); }
            if (!stat.isDirectory()) { throw new Error(`invalid dataPath. child ${zerospaceSubDir} is not a directory? (E: 30a577152964cd8b671d039598a97623)`); }
        } else {
            throw new Error(`invalid dataPath (${dataPath}). no initialized zero space found. first initialize with the --init and then pass in that directory as the dataPath. (E: 2507c65b139ea0d48882b77c79d17723)`);
        }

        return dataPath;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * creates an app (and dependency graph).
 *
 * I'm unsure about the space used in this, as I believe this is supposed to
 * just generate and not persist the dependency graph. updating the bootstrap
 * and broadcasting is supposed to happen elsewhere... hmm...
 *
 * also, atow, this just asks for the name and assumes creating RCLI app (not a
 * todo/chat/etc. app). in the future, this should be a full-fledged
 * node-implementation that uses the dynamic form engine created in the mvp
 * (which currently is used in angular/web forms).
 *
 * @returns **prompt function** for creating the app
 */
function getFnPromptApp({
    space,
    ibGib,
}: {
    space: IbGibSpaceAny,
    ibGib: AppIbGib_V1 | null
}): (space: IbGibSpaceAny, ibGib: AppIbGib_V1 | null) => Promise<AppPromptResult | undefined> {
    const lc = `[${getFnPromptApp.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: bfcdfddedaad4e054cfee6a7012dae23)`); }
        if (!!ibGib) { throw new Error(`editing app ibgib not implemented yet. only create, i.e. pass in null for ibGib (E: 749bd8ffc3c860ca944a6905d45c3423)`); }

        console.warn(`${lc} right now, only RCLI app is implemented (W: e4fc25122dac41588be9cccfd35d377e)`);

        // let fnResult: (space: IbGibSpaceAny, ibGib: AppIbGib_V1 | null) => Promise<AppPromptResult | undefined> =
        return async () => {
            let name = await promptForText({
                title: 'App Name?',
                msg: 'Enter the name for the app. Can only contain alphanumerics.\nOR, hit ENTER for a randomly assigned name.',
                confirm: true,
            });
            if (name === '') {
                name = `${RCLIApp_V1.name}_${pickRandom_Letters({ count: 8 })}`;
                console.log(`${lc} generated random name: ${name}`);
            }
            const data = clone(DEFAULT_RCLI_APP_DATA_V1) as RCLIAppData_V1;
            data.name = name;
            data.uuid = await getUUID();

            // the following is taken from the createApp function in
            // core-gib space-helper.mts i can't use that though because it
            // does additional steps, whereas this prompt function is
            // supposed to just do the mechanics and not save in spaces.
            const rel8ns = DEFAULT_RCLI_APP_REL8NS_V1 ?
                clone(DEFAULT_RCLI_APP_REL8NS_V1) as RCLIAppRel8ns_V1 :
                undefined;
            const { classname } = data;

            const ib = getAppIb({ appData: data, classname });

            const resNewApp = await factory.firstGen({
                ib,
                parentIbGib: factory.primitive({ ib: `app ${classname}` }),
                data: data,
                rel8ns,
                dna: true,
                linkedRel8ns: [Rel8n.ancestor, Rel8n.past],
                nCounter: true,
                tjp: { timestamp: true },
            }) as TransformResult<IbGibAppAny>;

            let f = new RCLIApp_V1_Factory();
            f.newUp({});

            return resNewApp;
        };
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 *
 */
async function execInteractive_getRCLIApp({
    metaspace,
    space,
    rcliAppId,
}: {
    metaspace: MetaspaceService,
    space: IbGibSpaceAny,
    rcliAppId: string | undefined,
}): Promise<RCLIApp_V1> {
    const lc = `[${execInteractive_getRCLIApp.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: a0f295e2ebded3fda2ffb7f849727c23)`); }

        // get the apps index for the given space
        // let testing = await metaspace.getAppAppIbGibs({ createIfNone: false, fnPromptApp: getFnPromptApp({ space, ibGib: null }) });
        // let latestAddr = await metaspace.getLatestAddr({ ibGib: testing[0], space });
        let appsIbGib = await metaspace.getSpecialIbGib({
            type: "apps",
            initialize: false,
            lock: false,
            space,
        });
        if (!appsIbGib) { throw new Error(`(UNEXPECTED) no apps index special ibgib. not initialized as expected? (E: 1734f4bb0be39955d6da848a75426c23)`); }
        if (!appsIbGib.rel8ns) { throw new Error(`(UNEXPECTED) appsIbGib.rel8ns falsy? (E: f53ab66b5d7934e85f588f3209757723)`); }

        const appIbGibAddrs = appsIbGib.rel8ns[APP_REL8N_NAME] ?? [] as IbGibAddr[];
        const rcliAppAddrs =
            appIbGibAddrs
                .filter(ibGibAddr => {
                    let { ib } = getIbAndGib({ ibGibAddr });
                    let info = getInfoFromAppIb({ appIb: ib });
                    if (info.appClassname !== RCLIApp_V1.name) {
                        return false;
                    } else if (rcliAppId) {
                        return info.appId === rcliAppId;
                    } else {
                        // no app id specified, so accept all rcliApps
                        return true;
                    }
                });

        let rcliApp: RCLIApp_V1;
        let rcliAppAddr: IbGibAddr;

        if (rcliAppAddrs.length === 0) {
            if (rcliAppId) { throw new Error(`rcliAppId (${rcliAppId}) not found. (E: 15ebc8d3ccb4898f83a9c2c6a1d95523)`); }

            // the user doesn't have any RCLI apps in the space yet, so create one.
            console.log(`${lc} there were no RCLI app ibgibs found in the given space (${space.data!.uuid}).
SO! We must first create an RCLI app ibgib.
This is an ibgib kinda like we currently think of a profile or setting collection for an app.
This ibgib references a specific instantiation of the app.
NOTE: If you feel this is a mistake and you have previously created a RCLI app ibgib,
then ctrl+C out and check to be sure that you have a RCLI app in the space and that
you have specified the correct space in the args.`)
            rcliApp = await execInteractive_CreateAndSaveNewRCLIApp({ metaspace, space });
            rcliAppAddr = getIbGibAddr({ ibGib: rcliApp });
        } else if (rcliAppAddrs.length === 1) {
            rcliAppAddr = rcliAppAddrs[0];
        } else {
            // rcliAppAddrs.length > 1
            let list = rcliAppAddrs.map(x => getIbAndGib({ ibGibAddr: x }).ib).map((ib, idx) => `\n${idx}) ${ib}`);
            let idxPicked: number | undefined;
            let huh = false;
            do {
                let response = await promptForText({ title: 'Which RCLI?', msg: `${huh ? 'huh? ' : ''}Multiple RCLI apps found. which one?${list}` });
                let parsedInt = Number.parseInt(response);
                if (Number.isNaN(parsedInt)) {
                    huh = true;
                } else if (parsedInt >= rcliAppAddrs.length) {
                    huh = true;
                } else {
                    huh = false;
                    idxPicked = parsedInt;
                }
            } while (huh);
            rcliAppAddr = rcliAppAddrs[idxPicked!];
        }
        if (!rcliAppAddr) { throw new Error(`(UNEXPECTED) rcliAppAddr assumed to be set at this point. (E: 79f2badd4ad52c76ba113b865fcd0123)`); }
        let resGet = await getFromSpace({ addr: rcliAppAddr, space });
        if (resGet.success && resGet.ibGibs?.length === 1) {
            // the resGet only retrieves the dto. since the RCLIApp is a witness,
            // we need to load the dto into the object in memory.
            rcliApp = resGet.ibGibs[0] as RCLIApp_V1;
        } else {
            throw new Error(`couldn't get app (${rcliAppAddr}) in space (${space.data!.uuid}). error: ${resGet.errorMsg ?? 'unknown yo (E: 2f0e779a86894b40b8cad1c67f67b339)'} (E: bfc9ee928daa7612ca00fac85f88eb23)`);
        }

        // we want the full witness object, not just the ibgib dto, so check for
        // existence of the witness function.
        if (!rcliApp.witness) {
            const rcliAppWitness = new RCLIApp_V1(undefined, undefined);
            await rcliAppWitness.loadIbGibDto(rcliApp);
            rcliApp = rcliAppWitness;
        }

        return rcliApp;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export async function execInteractive_CreateAndSaveNewRCLIApp({
    metaspace,
    space,
}: {
    metaspace: MetaspaceService,
    space: IbGibSpaceAny,
}): Promise<RCLIApp_V1> {
    const lc = `[${execInteractive_CreateAndSaveNewRCLIApp.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: ce7d69e9db5505ad338659be686b1823)`); }

        // creates, persists, registers with special apps index in given space
        let app = await createNewApp({
            fnPromptApp: getFnPromptApp({ space, ibGib: null }),
            space,
            ibgibs: metaspace,
        });

        if (!app) { throw new Error(`failed to create app (falsy) (E: edcda57f9d65dc019b073869a48faa23)`); }
        if (app?.data?.classname !== RCLIApp_V1.name) { throw new Error(`(UNEXPECTED) app other than  (E: e8473512b09a67976452a0dc0e1ca923)`); }

        return app as RCLIApp_V1;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}
