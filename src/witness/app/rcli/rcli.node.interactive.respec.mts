import { cwd, chdir, } from 'node:process';
import { statSync } from 'node:fs';
import { mkdir, } from 'node:fs/promises';
import { ChildProcess, exec, ExecException } from 'node:child_process';
import * as pathUtils from 'path';

import {
    firstOfAll, firstOfEach, ifWe, ifWeMight, iReckon,
    lastOfAll, lastOfEach, respecfully, respecfullyDear,
} from '@ibgib/helper-gib/dist/respec-gib/respec-gib.mjs';
const maam = `[${import.meta.url}]`, sir = maam;

import { delay, extractErrorMsg, } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../ibgib-constants.mjs';
import { createUniqueRespecSubpath, initialCwd } from './rcli.respec.mjs';
import { execRCLIInNode } from '../../../respec-gib-helper.node.mjs';

const logalot: boolean | number = GLOBAL_LOG_A_LOT;

const lcFile: string = `[${pathUtils.basename(import.meta.url)}]`;


const testOutputPath = 'test_output_path';
const testSpaceName = 'test_space_name';

export async function finalizeChildProcess({
    childProc,
    fnResolveOrReject,
    resNodePromise,
    lc,
}: {
    childProc: ChildProcess,
    fnResolveOrReject: any,
    resNodePromise: any,
    lc: string,
}): Promise<void> {
    lc = `${lc}[${finalizeChildProcess.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 6c7455f2893605f20548198a4650ce24)`); }
        if (logalot) { console.time(lc); }
        if (logalot) { console.log(`${lc} starting... (I: 218f962088b44cd2b029c50a05ff0e5a)`) }
        childProc.kill('SIGKILL');
        let { std_out } = await resNodePromise;
        if (logalot) { console.log(`${lc} starting` + '...'); }
        // console.log(std_out.slice(0, 4000) + '...');
        if (logalot) { console.log(std_out); }
        if (logalot) { console.log(`${lc} complete.`) }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete. (I: 0457e28279834198bbae7d270155dc3f)`) }
        if (logalot) { console.timeEnd(lc); }
        fnResolveOrReject();
    }
}

/**
 * helper function.
 *
 * ## intent
 *
 * looking to parse non-echo lines atow, the rcli echos out just about
 * everything so we want to ignore that stuff.
 *
 * @param d data (e.g. text line)
 * @param s string to check in the data
 * @returns true if non echo and includes given string `s`
 */
function dataIsNonEchoAndIncludes(d: string, s: string, echoPrefix?: string): boolean {
    echoPrefix ??= '[echo]';
    return !d.startsWith(echoPrefix) && d.includes(s);
}

async function handleInitStuff(data: string, childProc: ChildProcess, robbotName: string, echoPrefix: string, initializeDone: boolean): Promise<[boolean, boolean]> {
    echoPrefix ??= '[echo]';
    if (!childProc.stdin) { throw new Error(`(UNEXPECTED) childProc.stdin falsy? (E: 229f6f9a9068c09d3974700b23a1cb23)`); }
    let handled = true;
    if (dataIsNonEchoAndIncludes(data, 'OR, hit ENTER for a randomly assigned name')) { // app
        childProc.stdin.write('\n');
    } else if (dataIsNonEchoAndIncludes(data, 'empty string entered. confirm')) {
        childProc.stdin.write('\n');
    } else if (dataIsNonEchoAndIncludes(data, 'What to call this robbot. Doesn\'t have to be unique, no spaces, up to 32 alphanumerics/underscores in length.] Name?')) {
        childProc.stdin.write(`${robbotName}\n`);
    } else if (dataIsNonEchoAndIncludes(data, 'Description/notes for this robbot.] Description?')) {
        childProc.stdin.write('test description here\n');
    } else if (dataIsNonEchoAndIncludes(data, 'this out.] Rel8n Names Visible?')) {
        childProc.stdin.write('comment,pic,link,x\n');
    } else if (dataIsNonEchoAndIncludes(data, 'the robbot.] Output Prefix?')) {
        childProc.stdin.write('well-\n');
    } else if (dataIsNonEchoAndIncludes(data, '(like a signature)] Output Suffix?')) {
        childProc.stdin.write(`- ${robbotName}\n`);
        initializeDone = true;
    } else if (!initializeDone) {
        // some other thing but we're not done initializing yet,
        // say it's "handled" but
        // handled = true;
        console.log(`${echoPrefix} ${data}`);
    } else {
        handled = false;
    }
    return [handled, initializeDone];
}

await respecfully(sir, lcFile, async () => {
    let lc = lcFile.concat();
    if (logalot) { console.log(`${lc} cwd: ${cwd()} (I: 6d1e2c1a81cc4246b52ebd26e3fc1e11)`) }

    firstOfEach(sir, async () => {
        await createUniqueRespecSubpath();
        // let subpath = pathUtils.join(baseRespecPath, `${getTimestampInTicks()}_${(await getUUID()).slice(0, 5)}`);
        // await mkdir(subpath, { recursive: true });
        // chdir(subpath);
        const [resNode_InitPromise] = await execRCLIInNode({
            execPath: initialCwd,
            argsString: `--init --output-path="${testOutputPath}" --space-name="${testSpaceName}"`,
            logContext: lc,
            timeoutMs: 20_000,
        }); // already inside unique test folder
        const resNode_Init = await resNode_InitPromise;
        if (resNode_Init.err) {
            throw new Error(`(UNEXPECTED) init errored? (E: 297a6ac1042562db3c9faf345e561423)`);
        }
        if (logalot) { console.log(`${lc} init complete (I: f0de43fd0de70136689bb0a851a77d23)`); }
    });

    lastOfEach(sir, async () => {
        chdir(initialCwd);
        if (logalot) { console.log(`${lc}[lastOfAll] cwd: ${cwd()} (I: 6d1e2c1a81cc4246b52ebd26e3fc1e11)`) }
    });

    await respecfully(sir, `execNode robbot one-offs`, async () => {

        // #region constants/fns reused in multiple respec blocks

        const userPrompt = '[user]>';
        const robbotName = 'RollyTestRobbot';
        /** we'll echo std out but dont want to trigger  */
        const echoPrefix = '[echo]';
        let initializeDone = false;

        // #endregion constants/fns reused in multiple respec blocks

        await ifWe(sir, `--interactive,  execNode not so great repl testing robbot hello`, async () => {
            let lc = `${lcFile}[--interactive default app/robbot names]`;
            try {
                if (logalot) { console.log(`${lc} starting... (I: b7352be8f6ce41449dc5dc421935778a)`); }
                const timeoutMs = 40_000;
                console.time(lc);

                let hadToForceTimeout = false;

                const [resNodePromise, childPromise] = await execRCLIInNode({
                    execPath: initialCwd,
                    argsString: `--interactive --data-path="${testOutputPath}"`,
                    logContext: lc,
                    timeoutMs,
                });

                // at this point, we have started an interactive repl-like
                // session with the rcli. do NOT await the resNodePromise
                // because this won't occur (until we quit/exit from the rcli.
                // So in order to interact, we must get/use the reference to the
                // spawned child process.
                const childProc = await childPromise;

                // now we can interact via std_in/std_out, etc. once we are done, we will
                // kill the childProc and THEN await the resNodePromise.

                console.timeStamp(lc)

                /** (d)ata is non-echo and has the string s */

                await new Promise<void>(async (resolve, reject) => {
                    let lineIndex = -1;
                    /**
                     * the expected output regexp isn't really implemented yet
                     */
                    const userInputsAndExpectedOutputRegExp: [string, RegExp][] = [
                        // ['?hello', /Rolly/g], // test request to robbot
                        ['?hello', /these regexp's are ignored atow in testing/g], // to show this regexp isn't impl yet
                        [':quit', /echo/], // if we don't quit, then repl will wait until timeout
                    ];
                    let outputSoFar: string[] = [];

                    /**
                     * flag that indicates if we're to parse incoming data. once
                     * we're done listening to the child proc and we try to kill it,
                     * we want to immediately ignore any further data output.
                     */
                    let listenToChildProcData = true;
                    childProc.stdout!.on('data', async (data: string) => {
                        await delay(200);
                        if (data.startsWith(echoPrefix)) { return; /* <<<< returns early */ }
                        if (!listenToChildProcData) { return; /* <<<< returns early */ }

                        if (!childProc.stdin) { throw new Error(`child.stdin falsy? (E: 6ed8425b5be4482a955aad3bf693bbea)`); }
                        if (!childProc.stdout) { throw new Error(`child.stdout falsy? (E: 418b37585c5b4d3085f10e8a213e17aa)`); }
                        if (!childProc.stderr) { throw new Error(`child.stderr falsy? (E: de54464f2ac14770adfc8429719c3261)`); }

                        // console.log(`[data][${new Date().toUTCString()}] ${data}`);

                        const [reshandledInitStuff, resinitializeDone] = await handleInitStuff(data, childProc, robbotName, echoPrefix, initializeDone);
                        initializeDone = resinitializeDone;
                        if (reshandledInitStuff) { return; /* <<<< returns early */ }

                        // at this point, we have some data being written out
                        // that is not the initialization step.
                        if (data.trim() === userPrompt) {
                            lineIndex++;
                            if (lineIndex < userInputsAndExpectedOutputRegExp.length) {
                                const [userInput, expectedRegExp] = userInputsAndExpectedOutputRegExp[lineIndex];
                                if (userInput) {
                                    // userInput is truthy, so write the next line
                                    await delay(300);
                                    outputSoFar = [];
                                    childProc.stdin.write(userInput + '\n');
                                } else {
                                    // userInput is empty string, i.e., skip.
                                    // this is because the robbot has its say also
                                    // and when it is done, the prompt will come up
                                    // again.
                                }
                            } else {
                                listenToChildProcData = false;
                                // await finalizeChildProc(childProc, () => resolve(), resNodePromise);
                                await finalizeChildProcess({
                                    childProc,
                                    fnResolveOrReject: () => resolve(),
                                    resNodePromise,
                                    lc,
                                });
                            }

                        } else if (data.trim().endsWith(userPrompt)) {
                            await delay(200);
                            childProc.stdin.write('\n');
                        } else if (dataIsNonEchoAndIncludes(data, userPrompt)) {
                            await delay(20);
                            console.log(`${echoPrefix} ${data}`);
                        } else if (data.includes(`[index.mts] completed.`)) {
                            // this could change but right now i have logging on
                            clearTimeout(forceFinalizeTimer);
                            // await finalizeChildProc(childProc, () => resolve(), resNodePromise);
                            await finalizeChildProcess({
                                childProc,
                                fnResolveOrReject: () => resolve(),
                                resNodePromise,
                                lc,
                            });
                            console.timeEnd(lc);
                        } else {
                            // echo it if we want to see these during the respec test
                            outputSoFar.push(data);
                            console.log(`${echoPrefix} ${data}`);
                        }
                    });

                    let forceFinalizeTimer = setTimeout(async () => {
                        hadToForceTimeout = true;
                        // await finalizeChildProc(childProc, () => reject('forceFinalizeTimer timeout reached'), resNodePromise);
                        await finalizeChildProcess({
                            childProc,
                            fnResolveOrReject: () => reject('forceFinalizeTimer timeout reached'),
                            resNodePromise,
                            lc,
                        });
                        console.timeEnd(lc);
                    }, timeoutMs);
                });

                iReckon(sir, hadToForceTimeout).isGonnaBeFalse();

            } catch (error) {
                console.error(`${lc} ${extractErrorMsg(error)}`);
                throw error;
            } finally {
                if (logalot) { console.log(`${lc} complete.`); }
            }
        });

    });

    await ifWe(sir, `--interactive execNode will time out because repl`, async () => {
        let lc = `${lcFile}[--interactive timeout]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: b7352be8f6ce41449dc5dc421935778a)`); }

            const [resNodePromise, childPromise] = await execRCLIInNode({
                execPath: initialCwd,
                argsString: `--interactive --data-path="${testOutputPath}"`,
                logContext: lc,
                timeoutMs: 5_000,
            });
            let { err } = await resNodePromise;
            iReckon(sir, err).isGonnaBeTruthy();
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    });

}, { logalot: !!logalot });
