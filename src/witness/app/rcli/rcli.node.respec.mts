import { cwd, chdir, } from 'node:process';
import { statSync } from 'node:fs';
import { mkdir, } from 'node:fs/promises';
import { exec, ExecException } from 'node:child_process';
import * as pathUtils from 'path';

import {
    firstOfAll, firstOfEach, ifWe, ifWeMight, iReckon,
    lastOfAll, lastOfEach, respecfully, respecfullyDear,
} from '@ibgib/helper-gib/dist/respec-gib/respec-gib.mjs';
const maam = `[${import.meta.url}]`, sir = maam;

import { GLOBAL_LOG_A_LOT } from '../../../ibgib-constants.mjs';
import { RCLI_TEST_PATH } from './rcli-constants.mjs';
import { extractErrorMsg, getTimestampInTicks, getUUID } from '@ibgib/helper-gib';
import {
    baseRespecPath, createUniqueRespecSubpath, initialCwd
} from './rcli.respec.mjs';
import { execRCLIInNode } from '../../../respec-gib-helper.node.mjs';

const logalot: boolean | number = GLOBAL_LOG_A_LOT;

const lcFile: string = `[${pathUtils.basename(import.meta.url)}]`;


await respecfully(sir, lcFile, async () => {
    let lc = lcFile.concat();
    if (logalot) { console.log(`${lc} cwd: ${cwd()} (I: 6d1e2c1a81cc4246b52ebd26e3fc1e11)`) }

    firstOfEach(sir, async () => {
        await createUniqueRespecSubpath();
    });

    lastOfEach(sir, async () => {
        chdir(initialCwd);
        if (logalot) { console.log(`${lc}[lastOfAll] cwd: ${cwd()} (I: 6d1e2c1a81cc4246b52ebd26e3fc1e11)`) }
    });

    await ifWe(sir, `--help`, async () => {
        let lc = `${lcFile}[--help]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: e8e9563e813dacb95f37a2814a709923)`); }
            const [resNodePromise] = await execRCLIInNode({
                execPath: initialCwd,
                argsString: '--help', logContext: lc
            });
            const resNode = await resNodePromise;
            iReckon(sir, resNode.err).isGonnaBe(null);
            iReckon(sir, resNode.std_out).includes("howdy from ibgib"); // "temporary" test here. need to get lex-gib incorporated
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    });

    await ifWe(sir, `--init`, async () => {
        let lc = `${lcFile}[--init]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: e8e9563e813dacb95f37a2814a709923)`); }
            const [resNodePromise] = await execRCLIInNode({
                execPath: initialCwd,
                argsString: '--init --output-path="test_output_path_yo" --space-name="test_space_name_yo"',
                logContext: lc,
                timeoutMs: 20_000,
            }); // already inside unique test folder
            const resNode = await resNodePromise;
            iReckon(sir, resNode.err).isGonnaBe(null);
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    });

    await ifWe(sir, `--init with bad space name`, async () => {
        let lc = `${lcFile}[--init with bad space name]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: e8e9563e813dacb95f37a2814a709923)`); }
            const [resNodePromise] = await execRCLIInNode({
                execPath: initialCwd,
                argsString: '--init --output-path="test_output_path_yo" --space-name="no spaces or ch$r$$ ()()Ps!"',
                logContext: lc,
                dontLogStdErr: true,
            }); // already inside unique test folder
            const resNode = await resNodePromise;
            iReckon(sir, resNode.err).isGonnaBeTruthy();
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    });

}, { logalot: !!logalot });
