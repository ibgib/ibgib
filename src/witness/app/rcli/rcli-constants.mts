/**
 * @module rcli constants for use with RcliApps
 */

import { RCLIParamInfo } from "@ibgib/helper-gib/dist/rcli/rcli-types.mjs"
import { COMMON_PARAM_INFOS, PARAM_INFO_NAME, } from "@ibgib/helper-gib/dist/rcli/rcli-constants.mjs";
import { clone } from "@ibgib/helper-gib/dist/helpers/utils-helper.mjs";
import { TransformOpts_Fork, TransformOpts_Mut8, TransformOpts_Rel8 } from "@ibgib/ts-gib/dist/types.mjs";
import { ROOT_ADDR } from "@ibgib/ts-gib/dist/V1/constants.mjs";
import { BOOTSTRAP_IBGIB_ADDR } from "@ibgib/core-gib/dist/witness/space/bootstrap/bootstrap-constants.mjs";

import { FileEncoding } from "./rcli-types.mjs";

/**
 * Used in npm test
 */
export const RCLI_TEST_PATH = 'test-rcli';

/**
 * the path used for the ibgib subpath if not set by args.
 */
export const RCLI_DEFAULT_OUTPUT_PATH = '.ibgib';

/**
 * used when prompting the user.
 */
export const DEFAULT_PROMPT_TEMPLATE = `[$id]> `;

/**
 * this is the thing that comes before a command.
 *
 * you can also just use the --[cmd] format (i think).
 */
export const RCLI_DEFAULT_COMMAND_ESCAPE_STRING = ':';

/**
 * the max length of a command escape string.
 */
export const RCLI_MAX_ESCAPE_STRING_LENGTH = 8;

/**
 * options that you start with when performing a fork operation.
 *
 * parts of this are what get overridden by parameters.
 */
export const DEFAULT_FORK_OPTIONS: TransformOpts_Fork = {
    type: 'fork',
    srcAddr: ROOT_ADDR,
    cloneData: true,
    cloneRel8ns: true,
    destIb: undefined,
    dna: true,
    linkedRel8ns: undefined,
    nCounter: true,
    noTimestamp: false,
    tjp: {
        timestamp: true,
        uuid: true,
    },
    uuid: undefined,
};

/**
 * options that you start with when performing a mut8 operation.
 *
 * parts of this are what get overridden by parameters.
 */
export const DEFAULT_MUT8_OPTIONS: TransformOpts_Mut8 = {
    type: 'mut8',
    dna: true,
    linkedRel8ns: undefined,
    nCounter: true,
    noTimestamp: false,
};

/**
 * options that you start with when performing a rel8 operation.
 *
 * parts of this are what get overridden by parameters.
 */
export const DEFAULT_REL8_OPTIONS: TransformOpts_Rel8 = {
    type: 'rel8',
    dna: true,
    linkedRel8ns: undefined,
    nCounter: true,
    noTimestamp: false,
};

/**
 * using when reading a file from the command line.
 *
 * ## notes
 *
 * * currently working on handle-reify-file.mts
 */
export const DEFAULT_FILE_ENCODING: FileEncoding = 'utf8';

/**
 * maximum number of chars when generating name
 */
export const MAX_GENERATE_FILE_NAME_LENGTH = 32;

// #region param info related

// #region command defs

/**
 * command typescript type literal.
 *
 * this is useful (i guess?) in locking down commands.
 *
 * # when creating new commands
 *
 * * Add it to the RCLICommand type here
 * * add to the constant of the same name
 * * add any synonyms
 */
export type RCLICommand =
    'help' | 'init' | 'quit' | 'cwd' |
    'fork' | 'mut8' | 'rel8' |
    'list-chat' |
    'add-comment' | // add-link, add-pic
    'add-stone' |
    'reify-file' |
    'generate-source-file' |
    'b2tfs-init' |
    'b2tfs-branch' |
    'b2tfs-activate-root-branch' |
    'b2tfs-info' |
    'b2tfs-diff'
    ;
export const RCLICommand = {
    help: 'help' as RCLICommand, init: 'init' as RCLICommand,
    quit: 'quit' as RCLICommand, cwd: 'cwd' as RCLICommand,
    fork: 'fork' as RCLICommand, mut8: 'mut8' as RCLICommand, rel8: 'rel8' as RCLICommand,
    add_comment: 'add-comment' as RCLICommand,
    list_chat: 'list-chat' as RCLICommand,
    create_stone: 'add-stone' as RCLICommand,
    reify_file: 'reify-file' as RCLICommand,
    generate_source_file: 'generate-source-file' as RCLICommand,
    info: 'info' as RCLICommand,
    b2tfs_init: 'b2tfs-init' as RCLICommand,
    b2tfs_branch: 'b2tfs-branch' as RCLICommand,
    b2tfs_activate_branch: 'b2tfs-activate-branch' as RCLICommand,
    b2tfs_info: 'b2tfs-info' as RCLICommand,
    b2tfs_diff: 'b2tfs-diff' as RCLICommand,
} satisfies { [key: string]: RCLICommand };
export const RCLI_COMMANDS: RCLICommand[] = Object.values(RCLICommand);

export const RCLI_COMMAND_SYNONYMS: { [key: string]: string[] } = {
    [RCLICommand.help]: ['h', 'huh', 'wat'],
    [RCLICommand.init]: ['initialize',],
    [RCLICommand.quit]: ['q', 'exit'],
    [RCLICommand.fork]: [],
    [RCLICommand.mut8]: [],
    [RCLICommand.rel8]: [],
    [RCLICommand.cwd]: ['pwd'],
    [RCLICommand.add_comment]: ['comment'],
    [RCLICommand.list_chat]: ['view-chat', 'replay', 'ls-chat'],
    [RCLICommand.create_stone]: ['new-stone'],
    [RCLICommand.reify_file]: ['reify'],
    [RCLICommand.generate_source_file]: [
        'generate-src', 'generate-src-file', 'g-src', 'g-src-file'
    ],
    [RCLICommand.info]: ['status', 'details', 'deets', 'cat'],
    [RCLICommand.b2tfs_init]: ['b2-init', 'fs-init', 'vcs-init'],
    [RCLICommand.b2tfs_branch]: [
        'b2-branch', 'fs-branch', 'vcs-branch',
    ],
    [RCLICommand.b2tfs_activate_branch]: [
        // 'b2tfs-activate-branch',
        'b2-activate-branch', 'fs-activate-branch', 'vcs-activate-branch',
        'b2tfs-activate', 'b2-activate', 'fs-activate', 'vcs-activate'
    ],
    [RCLICommand.b2tfs_info]: [
        'b2-info', 'fs-info', 'vcs-info',
        'b2tfs-status', 'b2-status', 'fs-status', 'vcs-status'
    ],
    [RCLICommand.b2tfs_diff]: [
        'b2-diff', 'fs-diff', 'vcs-diff',
    ],
};
export const RCLI_COMMAND_IDENTIFIERS: string[] = [
    ...RCLI_COMMANDS,
    ...Object.values(RCLI_COMMAND_SYNONYMS).flatMap(x => x),
];
// do a quick validation to avoid duplicate command names/synonyms.
if (RCLI_COMMAND_IDENTIFIERS.length !== new Set(RCLI_COMMAND_IDENTIFIERS).size) {
    throw new Error(`duplicate rcli command identifier found. (E: 8a962a60050a506e14ff49b81ffd2423)`);
}
if (RCLI_COMMAND_IDENTIFIERS.some(x => x === '')) {
    throw new Error(`empty string rcli command identifier found (E: 6bcff3c0f333544a583a24c47d161823)`);
}

/**
 * generated constant whose values are param infos for the command in
 * RCLICommand constant.
 *
 * ATOW all of these are boolean flags, some with synonyms, that do not allow
 * mutiple.
 */
export const RCLI_COMMAND_PARAM_INFOS: Record<RCLICommand, RCLIParamInfo> =
    Object
        .values(RCLI_COMMANDS)
        .reduce((agg: Record<RCLICommand, RCLIParamInfo>, cmdName: RCLICommand) => {
            agg[cmdName] = {
                name: cmdName,
                argTypeName: 'boolean',
                allowMultiple: false,
                isFlag: true,
                synonyms: RCLI_COMMAND_SYNONYMS[cmdName],
            } as RCLIParamInfo;
            return agg;
        }, {} as Record<RCLICommand, RCLIParamInfo>);
;

// #endregion command defs

/**
 * used as a general param in various cases when only a single ibgib addr is
 * expected.
 *
 * when fork, use this for srcAddr of ibGib that you're forking
 *
 * when doing --fs --init (atow in progress/not impl), this will reference
 * an existing fs tag, essentially like doing an import.
 */
export const PARAM_INFO_SRC_ADDR: RCLIParamInfo = {
    name: 'src-addr',
    argTypeName: 'string',
    synonyms: ['addr', 'from-addr', 'in-addr', 'input-addr'],
};
/**
 * used as a general param in various cases when executing operations where you
 * have to provide a source and destination and are referencing via ids (as
 * opposed to say, addrs).
 */
export const PARAM_INFO_SRC_ID: RCLIParamInfo = {
    name: 'src-id',
    argTypeName: 'string',
    synonyms: ['id', 'from-id', 'in-id', 'input-id'],
};
/**
 * used as a general param in various cases when executing operations where you
 * have to provide a source and destination and are referencing via addrs.
 */
export const PARAM_INFO_DEST_ADDR: RCLIParamInfo = {
    name: 'dest-addr',
    argTypeName: 'string',
    synonyms: ['addr', 'to-addr', 'out-addr', 'output-addr'],
};
/**
 * used as a general param in various cases when executing operations where you
 * have to provide a source and destination and are referencing via ids.
 */
export const PARAM_INFO_DEST_ID: RCLIParamInfo = {
    name: 'dest-id',
    argTypeName: 'string',
    synonyms: ['to-id', 'out-id', 'output-id'],
};
/**
 * used as a general param in various cases when executing operations where you
 * have to provide a source and destination and are referencing via names.
 *
 */
export const PARAM_INFO_DEST_NAME: RCLIParamInfo = {
    name: 'dest-name',
    argTypeName: 'string',
    synonyms: ['to-name', 'out-name', 'output-name'],
};

// #region param_info for commands

/**
 * inits/bootstraps a local space
 * should be used in conjunction with data-path, output-path
 * @see {@link PARAM_INFO_DATA_PATH}
 * @see {@link PARAM_INFO_OUTPUT_PATH}
 */
export const PARAM_INFO_INIT: RCLIParamInfo = RCLI_COMMAND_PARAM_INFOS[RCLICommand.init];
/**
 * execute a fork transform
 *
 * @see {@link TransformOpts_Fork}
 */
export const PARAM_INFO_FORK: RCLIParamInfo = RCLI_COMMAND_PARAM_INFOS[RCLICommand.fork];
/**
 * execute a mut8 transform
 *
 * @see {@link TransformOpts_Mut8}
 */
export const PARAM_INFO_MUT8: RCLIParamInfo = RCLI_COMMAND_PARAM_INFOS[RCLICommand.mut8];
/**
 * execute a rel8 transform
 *
 * @see {@link TransformOpts_Rel8}
 */
export const PARAM_INFO_REL8: RCLIParamInfo = RCLI_COMMAND_PARAM_INFOS[RCLICommand.rel8];
export const PARAM_INFO_QUIT: RCLIParamInfo = RCLI_COMMAND_PARAM_INFOS[RCLICommand.quit];
export const PARAM_INFO_CWD: RCLIParamInfo = RCLI_COMMAND_PARAM_INFOS[RCLICommand.cwd];
export const PARAM_INFO_ADD_COMMENT: RCLIParamInfo = RCLI_COMMAND_PARAM_INFOS[RCLICommand.add_comment];
export const PARAM_INFO_LIST_CHAT: RCLIParamInfo = RCLI_COMMAND_PARAM_INFOS[RCLICommand.list_chat];
export const PARAM_INFO_CREATE_STONE: RCLIParamInfo = RCLI_COMMAND_PARAM_INFOS[RCLICommand.create_stone];
/**
 * takes a file in a filesystem that is not an ibgib (with a hash) and reifies
 * it to an ibgib that includes the full ib^gib address with hash.
 *
 * @example if you have a '.ibgibignore' file, then you do not have any kind of
 * metadata/hash associated with that. this will take that file and convert it
 * into an ibgib file.
 */
export const PARAM_INFO_REIFY_FILE: RCLIParamInfo = RCLI_COMMAND_PARAM_INFOS[RCLICommand.reify_file];
export const PARAM_INFO_GENERATE_SOURCE_FILE: RCLIParamInfo = RCLI_COMMAND_PARAM_INFOS[RCLICommand.generate_source_file];
export const PARAM_INFO_INFO: RCLIParamInfo = RCLI_COMMAND_PARAM_INFOS[RCLICommand.info];

export const PARAM_INFO_B2TFS_INIT: RCLIParamInfo = RCLI_COMMAND_PARAM_INFOS[RCLICommand.b2tfs_init];
export const PARAM_INFO_B2TFS_BRANCH: RCLIParamInfo = RCLI_COMMAND_PARAM_INFOS[RCLICommand.b2tfs_branch];
export const PARAM_INFO_B2TFS_ACTIVATE_BRANCH: RCLIParamInfo = RCLI_COMMAND_PARAM_INFOS[RCLICommand.b2tfs_activate_branch];
export const PARAM_INFO_B2TFS_INFO: RCLIParamInfo = RCLI_COMMAND_PARAM_INFOS[RCLICommand.b2tfs_info];
export const PARAM_INFO_B2TFS_DIFF: RCLIParamInfo = RCLI_COMMAND_PARAM_INFOS[RCLICommand.b2tfs_diff];

// #endregion param_info for commands

// #region ibgib params


/**
 * when fork, use for "destIb"
 * when mut8, use for "mut8Ib"
 */
export const PARAM_INFO_IB: RCLIParamInfo = {
    name: 'ib',
    argTypeName: 'string',
};

/**
 * when rel8, use multiple of these for addrs
 *
 * the value should contain both rel8nName and addr with double vertical bar
 * delimiter.
 *
 * @example --+addr="comment||abc^123hash"
 */
export const PARAM_INFO_ADD_ADDR: RCLIParamInfo = {
    name: 'add-addr',
    argTypeName: 'string',
    allowMultiple: true,
    synonyms: ['+addr', '+@'],
};

/**
 * when rel8, use multiple of these for addrs to remove (unrel8)
 */
export const PARAM_INFO_RM_ADDR: RCLIParamInfo = {
    name: 'rm-addr',
    argTypeName: 'string',
    allowMultiple: true,
    synonyms: ['-addr', '-@'],
};

/**
 * tjp defaults to both uuid and timestamp.
 *
 * if one of this param is provided, this will be used **exclusively**. So if
 * you pass ing tjp="uuid", then the timestamp tjp will be falsy.
 *
 * @param {('uuid'|'timestamp'|'none')}
 */
export const PARAM_INFO_TJP: RCLIParamInfo = {
    name: 'tjp',
    argTypeName: 'string',
};

/**
 * flag that indicates when forking, the parent's data should be cloned.
 */
export const PARAM_INFO_CLONE_DATA: RCLIParamInfo = {
    name: 'clone-data',
    isFlag: true,
    argTypeName: 'boolean',
};
/**
 * flag that indicates when forking, the parent's rel8ns should be cloned.
 */
export const PARAM_INFO_CLONE_REL8NS: RCLIParamInfo = {
    name: 'clone-rel8ns',
    isFlag: true,
    argTypeName: 'boolean',
};

/**
 * json string for data key(s) to rename
 */
export const PARAM_INFO_DATA_TO_RENAME: RCLIParamInfo = {
    name: 'data-to-rename',
    argTypeName: 'string',
    synonyms: ['rename-data'],
};
/**
 * json string for data key(s) to remove
 */
export const PARAM_INFO_DATA_TO_REMOVE: RCLIParamInfo = {
    name: 'data-to-remove',
    argTypeName: 'string',
    synonyms: ['remove-data'],
};
/**
 * json string for data to add/patch when mut8ing
 */
export const PARAM_INFO_DATA_TO_ADD_OR_PATCH: RCLIParamInfo = {
    name: 'data-to-add-or-patch',
    argTypeName: 'string',
    synonyms: ['+data', 'add-data', 'patch-data'],
};
/**
 * json string for rel8ns to add options object
 */
export const PARAM_INFO_REL8NS_TO_ADD: RCLIParamInfo = {
    name: 'rel8ns-to-add',
    argTypeName: 'string',
    synonyms: ['+rel8ns', 'add-rel8ns', 'rel8ns-to-add-by-addr'],
};
/**
 * json string for rel8ns to remove options object
 */
export const PARAM_INFO_REL8NS_TO_REMOVE: RCLIParamInfo = {
    name: 'rel8ns-to-remove',
    argTypeName: 'string',
    synonyms: ['-rel8ns', 'remove-rel8ns', 'rm-rel8ns', 'rel8ns-to-remove-by-addr'],
};

/**
 * used when creating comments with comment command
 */
export const PARAM_INFO_TEXT: RCLIParamInfo = {
    name: 'text',
    synonyms: ['txt', 'message', 'm', 'msg'],
    argTypeName: 'string',
};

// #endregion ibgib params

/**
 * @todo
 * path to resolve the bootstrap ibgib.
 *
 * this can be used to use a specific file or a directory to use in
 * conjunction with the default bootstrap filename (bootstrap ibgib addr "bootstrap^gib").
 *
 * @see {@link BOOTSTRAP_IBGIB_ADDR}
 */
export const PARAM_INFO_BOOTSTRAP_PATH: RCLIParamInfo = {
    name: 'bootstrap-path',
    argTypeName: 'string',
};

/**
 * when using `--init`, this name will be used for the initialized space. if
 * this is not provided, the rcli will prompt the user for a space name.
 *
 * when using `--interactive`, this specifies space by name
 */
export const PARAM_INFO_SPACE_NAME: RCLIParamInfo = {
    name: 'space-name',
    argTypeName: 'string',
    allowMultiple: false,
};

/**
 * when using `--interactive`, this specifies space by id
 * when b2tfs, this can specify the space to act on/look in.
 */
export const PARAM_INFO_SPACE_ID: RCLIParamInfo = {
    name: 'space-id',
    argTypeName: 'string',
    allowMultiple: false,
};

/**
 * space id used to initialize the metaspace's local user space.
 *
 * note that this is not the id of the actual metaspace (which doesn't have an
 * id) but rather points to the local space that the metaspace will use.
 *
 * using this to differentiate from just bare --space-id which is used in
 * various commands.
 */
export const PARAM_INFO_LOCAL_SPACE_ID: RCLIParamInfo = {
    name: 'local-space-id',
    argTypeName: 'string',
    synonyms: ['context-space-id'],
    allowMultiple: false,
};

/**
 * space name used to initialize the metaspace's local user space.
 *
 * note that this is not the name of the actual metaspace (which doesn't have an
 * name) but rather points to the local space that the metaspace will use.
 *
 * using this to differentiate from just bare --name which is used in
 * various commands.
 */
export const PARAM_INFO_LOCAL_SPACE_NAME: RCLIParamInfo = {
    name: 'local-space-name',
    argTypeName: 'string',
    synonyms: ['context-space-name'],
    allowMultiple: false,
};

/**
 * specify an app id for a cmd
 */
export const PARAM_INFO_APP_ID: RCLIParamInfo = {
    name: 'app-id',
    argTypeName: 'string',
    allowMultiple: false,
};

/**
 * flag to indicate if, e.g. when getting an ibgib, we want to get the latest in
 * the timeline.
 */
export const PARAM_INFO_LATEST: RCLIParamInfo = {
    name: 'latest',
    argTypeName: 'boolean',
    synonyms: ['latest-only'],
    isFlag: true,
};

/**
 * @todo
 * when creating an ibgib, if we want to make it private, i.e. encrypt,
 * use this flag
 *
 * should be used in conjunction with data-path/data-string, output-path
 */
export const PARAM_INFO_PRIVATE: RCLIParamInfo = {
    name: 'private',
    isFlag: true,
    argTypeName: 'boolean',
};

/**
 * create a temporary in-memory space for ibgibs.
 * i.e., don't store anything on disk.
 */
export const PARAM_INFO_IN_MEMORY: RCLIParamInfo = {
    name: 'in-memory',
    isFlag: true,
    argTypeName: 'boolean',
};

/**
 * if true, start a repl.
 */
export const PARAM_INFO_INTERACTIVE: RCLIParamInfo = {
    name: 'interactive',
    isFlag: true,
    argTypeName: 'boolean',
    synonyms: ['repl'],
};

/**
 * specify encoding for a non-binary file.
 *
 * @see {@link PARAM_INFO_BINARY}
 */
export const PARAM_INFO_FILE_ENCODING: RCLIParamInfo = {
    name: 'file-encoding',
    argTypeName: 'string',
    synonyms: ['encoding', 'text-encoding'],
};

/**
 * flag to indicate if pretty print
 */
export const PARAM_INFO_PRETTY: RCLIParamInfo = {
    name: 'pretty',
    description: 'flag to indicate if pretty print output',
    argTypeName: 'boolean',
    synonyms: ['pretty-print'],
    isFlag: true,
};

/**
 * flag to indicate if verbose wordy lots of stuff
 */
export const PARAM_INFO_VERBOSE: RCLIParamInfo = {
    name: 'verbose',
    description: 'flag to indicate if verbose wordy lots of stuff',
    argTypeName: 'boolean',
    synonyms: ['v'],
    isFlag: true,
};

/**
 * if true, the command should treat a file/whatever as being binary.
 *
 * @see {@link PARAM_INFO_REIFY_FILE}
 */
export const PARAM_INFO_BINARY: RCLIParamInfo = {
    name: 'binary',
    isFlag: true,
    argTypeName: 'boolean',
    synonyms: ['is-binary'],
};

/**
 * boolean flag to indicate the output/whatever is a witness.
 *
 * i'm making this for * {@link PARAM_INFO_GENERATE_SOURCE_FILE}
 */
export const PARAM_INFO_WITNESS: RCLIParamInfo = {
    name: 'witness',
    isFlag: true,
    argTypeName: 'boolean',
    synonyms: ['is-witness', 'basic-witness'],
};

/**
 * boolean flag to indicate the output/whatever is specifically an app witness.
 *
 * i'm making this for * {@link PARAM_INFO_GENERATE_SOURCE_FILE}
 */
export const PARAM_INFO_APP: RCLIParamInfo = {
    name: 'app',
    isFlag: true,
    argTypeName: 'boolean',
    synonyms: ['is-app'],
};

/**
 * boolean flag to indicate the output/whatever is specifically a robbot
 * witness.
 *
 * i'm making this for * {@link PARAM_INFO_GENERATE_SOURCE_FILE}
 */
export const PARAM_INFO_ROBBOT: RCLIParamInfo = {
    name: 'robbot',
    isFlag: true,
    argTypeName: 'boolean',
    synonyms: ['is-robbot'],
};

/**
 * if true, skip doing respec unit tests
 *
 * ## use case
 *
 * i'm making this for * {@link PARAM_INFO_GENERATE_SOURCE_FILE}
 */
export const PARAM_INFO_NO_RESPEC: RCLIParamInfo = {
    name: 'no-respec',
    isFlag: true,
    argTypeName: 'boolean',
    synonyms: ['no-spec', 'no-specs', 'no-test', 'no-tests'],
};

/**
 * flag to indicate if we want to apply during some command.
 *
 * kind of the opposite of a dry-run.
 *
 * ## intent
 *
 * I'm making this for B2tFS diff. so when this flag is true, the diff should be
 * applied.
 */
export const PARAM_INFO_APPLY: RCLIParamInfo = {
    name: 'apply',
    argTypeName: 'boolean',
    synonyms: ['apply-cmd', 'apply-diff', 'apply-changes'],
    isFlag: true,
};

/**
 * flag to indicate if we want to add during some command.
 *
 * ## intent
 *
 * I'm making this for B2tFS branch. so when this flag is true, the cmd should
 * be adding a branh.
 */
export const PARAM_INFO_ADD: RCLIParamInfo = {
    name: 'add',
    description: 'adds something...not necessarily new, yada yada, todo here.',
    argTypeName: 'boolean',
    synonyms: [],
    isFlag: true,
};

/**
 * flag to indicate if we want to only do the fs (files/folders).
 *
 * ## intent
 *
 * I'm making this for B2tFS branch command similar to an export. so when this
 * flag is true, the cmd should only be snapshotting the files/folders into the
 * target folder and not copying the .ibgib folder.
 */
export const PARAM_INFO_FS_ONLY: RCLIParamInfo = {
    name: 'fs-only',
    description: 'only do the fs-side of things (not the ibgibs).',
    argTypeName: 'boolean',
    synonyms: ['files-only', 'files'],
    isFlag: true,
};

/**
 * customize the imported common param infos here:
 * * add documentation/description specific to this project's use case
 */
const CUSTOMIZED_COMMON_PARAM_INFOS = clone(COMMON_PARAM_INFOS) as RCLIParamInfo[];
CUSTOMIZED_COMMON_PARAM_INFOS.forEach(x => {
    if (x.name === PARAM_INFO_NAME.name) {
        const gSrcDesc = `PARAM_INFO_GENERATE_SOURCE_FILE
* name will be used as folder generated, as well as generated files, ibgib/data/rel8n/witness/class types.
* will automatically be converted to various casings
  (camelCase,PascalCase,hyphenated-case,snake_case, etc.) depending on where name is used.
  * e.g. camelCase for params/vars, PascalCase for class names, hyphenated-case for filenames, etc.
`;
        x.description += gSrcDesc;
    }
});

/**
 * Array of all parameters this library's RCLI supports.
 */
export const PARAM_INFOS: RCLIParamInfo[] = [
    ...CUSTOMIZED_COMMON_PARAM_INFOS,
    // atow... 11/2023
    // PARAM_INFO_BARE,
    // PARAM_INFO_HELP,
    // PARAM_INFO_DRY_RUN,
    // PARAM_INFO_DATA_PATH,
    // PARAM_INFO_INPUT_PATH,
    // PARAM_INFO_OUTPUT_PATH,
    // PARAM_INFO_DATA_STRING,
    // PARAM_INFO_DATA_INTEGER,
    // PARAM_INFO_DATA_BOOLEAN,
    // PARAM_INFO_NAME,
    //  * i'm adding this for {@link PARAM_INFO_GENERATE_SOURCE_FILE}, but should be
    //  * reusable. used for when you have a name. need to move this into the base lib
    //  * at some point.

    // command flags
    PARAM_INFO_INIT,
    PARAM_INFO_FORK,
    PARAM_INFO_MUT8,
    PARAM_INFO_REL8,
    PARAM_INFO_QUIT,
    PARAM_INFO_CWD,
    PARAM_INFO_ADD_COMMENT,
    PARAM_INFO_LIST_CHAT,
    PARAM_INFO_CREATE_STONE,
    PARAM_INFO_REIFY_FILE,
    PARAM_INFO_GENERATE_SOURCE_FILE,
    PARAM_INFO_INFO,

    // B2tFS
    PARAM_INFO_B2TFS_INIT, // command
    PARAM_INFO_B2TFS_BRANCH, // command
    PARAM_INFO_B2TFS_ACTIVATE_BRANCH, // command
    PARAM_INFO_B2TFS_INFO, // command
    PARAM_INFO_B2TFS_DIFF, // command

    // other flags
    PARAM_INFO_IN_MEMORY,
    PARAM_INFO_INTERACTIVE,
    PARAM_INFO_BINARY,
    PARAM_INFO_WITNESS,
    PARAM_INFO_APP,
    PARAM_INFO_NO_RESPEC,
    PARAM_INFO_APPLY,
    PARAM_INFO_ADD,
    PARAM_INFO_FS_ONLY,

    // ibgib
    PARAM_INFO_SRC_ADDR,
    PARAM_INFO_IB,
    PARAM_INFO_ADD_ADDR,
    PARAM_INFO_RM_ADDR,
    PARAM_INFO_TEXT,
    PARAM_INFO_SPACE_NAME,
    PARAM_INFO_SPACE_ID,
    PARAM_INFO_APP_ID,

    // IBGIB FLAGS
    PARAM_INFO_TJP,
    PARAM_INFO_CLONE_DATA,
    PARAM_INFO_CLONE_REL8NS,
    PARAM_INFO_DATA_TO_RENAME,
    PARAM_INFO_DATA_TO_REMOVE,
    PARAM_INFO_DATA_TO_ADD_OR_PATCH,
    PARAM_INFO_REL8NS_TO_ADD,
    PARAM_INFO_REL8NS_TO_REMOVE,
    PARAM_INFO_LATEST,

    // other
    PARAM_INFO_BOOTSTRAP_PATH,
    PARAM_INFO_FILE_ENCODING,
    PARAM_INFO_PRETTY,
    PARAM_INFO_VERBOSE,
];

// #endregion param info related
