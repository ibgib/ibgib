import * as readline from 'node:readline/promises';
import { cwd, stdin, stdout } from 'node:process'; // decide if use this or not

import { buildArgInfos } from '@ibgib/helper-gib/dist/rcli/rcli-helper.mjs';
import { argIs, getParamInfo } from '@ibgib/helper-gib/dist/rcli/rcli-helper.mjs';
import { PARAM_INFO_HELP } from '@ibgib/helper-gib/dist/rcli/rcli-constants.mjs';
import { clone, extractErrorMsg, getIdPool, getUUID, pretty } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { DEFAULT_DATA_PATH_DELIMITER } from '@ibgib/helper-gib/dist/constants.mjs';
import { promptForConfirm } from '@ibgib/helper-gib/dist/helpers/node-helper.mjs';
import { IbGib_V1, Rel8n, IbGibRel8ns_V1, } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { ROOT, } from '@ibgib/ts-gib/dist/V1/constants.mjs';
import { getIbGibAddr } from '@ibgib/ts-gib/dist/helper.mjs';
import { Factory_V1 as factory, } from '@ibgib/ts-gib/dist/V1/factory.mjs';
import { IbGibAddr, TransformResult } from '@ibgib/ts-gib/dist/types.mjs';
import { AppCmdData, AppCmdIbGib, AppCmdRel8ns, } from '@ibgib/core-gib/dist/witness/app/app-types.mjs';
import { AppBase_V1 } from '@ibgib/core-gib/dist/witness/app/app-base-v1.mjs';
import { ErrorIbGib_V1 } from '@ibgib/core-gib/dist/common/error/error-types.mjs';
import { RobbotCmd } from '@ibgib/core-gib/dist/witness/robbot/robbot-types.mjs';
import { CommentIbGib_V1 } from '@ibgib/core-gib/dist/common/comment/comment-types.mjs';
import { DynamicForm, FormItemInfo } from '@ibgib/core-gib/dist/common/form/form-items.mjs';
import { WitnessFormBuilder } from '@ibgib/core-gib/dist/witness/witness-form-builder.mjs';
import { getAppIb, AppFormBuilder } from '@ibgib/core-gib/dist/witness/app/app-helper.mjs';
import { DynamicFormBuilder, } from '@ibgib/core-gib/dist/common/form/form-helper.mjs';
import { WitnessArgIbGib } from '@ibgib/core-gib/dist/witness/witness-types.mjs';
import { isComment } from '@ibgib/core-gib/dist/common/comment/comment-helper.mjs';
import { WITNESS_CONTEXT_REL8N_NAME } from '@ibgib/core-gib/dist/witness/witness-constants.mjs';
import { isError } from '@ibgib/core-gib/dist/common/error/error-helper.mjs';
import { IbGibRobbotAny } from '@ibgib/core-gib/dist/witness/robbot/robbot-base-v1.mjs';
import { DynamicFormFactoryBase } from '@ibgib/core-gib/dist/witness/factory/dynamic-form-factory-base.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../ibgib-constants.mjs';
import {
    RCLIAppData_V1, RCLIAppRel8ns_V1, DEFAULT_RCLI_APP_DATA_V1,
    DEFAULT_RCLI_APP_REL8NS_V1, RCLIAppAddlMetadata, DEFAULT_DESCRIPTION_RCLI_APP,
    RCLI_ROBBOT_REL8N_NAME, RCLICommandTextInfo, RCLICommandHandler,
    RCLICommandHandlerArg,
    RCLIAppIbGib_V1,
} from './rcli-types.mjs';
// import { DynamicFormFactoryBase } from '../../tmp/dynamic-form-factory-base.mjs';
import { isRCLIComment, } from '../../../common/rcli-comment/rcli-comment-helper.mjs';
import { RCLICommentIbGib_V1 } from '../../../common/rcli-comment/rcli-comment-types.mjs';
import { RCLIFormBuilder, parseCommandRawTextIntoArgs, getPrompt, promptForText, validateArgInfos } from './rcli-helper.mjs';
import { RollyRobbot_V1, RollyRobbot_V1_Factory } from '../../robbot/rolly-robbot/rolly-robbot-v1.mjs';
import { RCLI_DEFAULT_COMMAND_ESCAPE_STRING, PARAM_INFOS, RCLICommand, RCLI_COMMANDS, RCLI_COMMAND_IDENTIFIERS } from './rcli-constants.mjs';
import { rcliCmdHandlerHelp } from './rcli-cmd-handlers/handle-help.mjs';
import { rcliCmdHandlerQuit } from './rcli-cmd-handlers/handle-quit.mjs';
import { rcliCmdHandlerFork } from './rcli-cmd-handlers/transforms/handle-fork.mjs';
import { rcliCmdHandlerMut8 } from './rcli-cmd-handlers/transforms/handle-mut8.mjs';
import { rcliCmdHandlerRel8 } from './rcli-cmd-handlers/transforms/handle-rel8.mjs';
import { rcliCmdHandlerCwd } from './rcli-cmd-handlers/handle-cwd.mjs';
import { rcliCmdHandlerAddComment } from './rcli-cmd-handlers/handle-add-comment.mjs';
import { rcliCmdHandlerListChat } from './rcli-cmd-handlers/handle-list-chat.mjs';
import { rcliCmdHandlerGenerateSourceFile } from './rcli-cmd-handlers/handle-generate-source-file/handle-generate-source-file.mjs';
import { rcliCmdHandlerInfo } from './rcli-cmd-handlers/handle-info.mjs';
import { rcliCmdHandler_B2tFS_init } from './rcli-cmd-handlers/b2tfs/handle-b2tfs-init.mjs';
import { rcliCmdHandler_B2tFS_branch } from './rcli-cmd-handlers/b2tfs/handle-b2tfs-branch.mjs';
import { rcliCmdHandler_B2tFS_info } from './rcli-cmd-handlers/b2tfs/handle-b2tfs-info.mjs';
import { rcliCmdHandler_B2tFS_activateBranch } from './rcli-cmd-handlers/b2tfs/handle-b2tfs-activate-branch.mjs';
import { rcliCmdHandler_B2tFS_diff } from './rcli-cmd-handlers/b2tfs/handle-b2tfs-diff.mjs';


const logalot = GLOBAL_LOG_A_LOT;

/**
 * the app ibgib that contains configuration (meta)data.
 */
export class RCLIApp_V1 extends AppBase_V1<
    // in
    any, IbGibRel8ns_V1, IbGib_V1<any, IbGibRel8ns_V1>,
    // out
    any, IbGibRel8ns_V1, IbGib_V1<any, IbGibRel8ns_V1>,
    // this
    RCLIAppData_V1, RCLIAppRel8ns_V1
> implements RCLIAppIbGib_V1 {
    protected lc: string = `[${RCLIApp_V1.name}]`;

    protected _rcliPrompt: string = '';
    get rcliPrompt(): string {
        if (!this._rcliPrompt) { this._rcliPrompt = getPrompt({ id: this.data!.name }); }
        return this._rcliPrompt;
    }
    _userPrompt: string = '';
    get userPrompt(): string {
        // todo: change user prompt once we have identity implemented
        if (!this._userPrompt) { this._userPrompt = getPrompt({ id: 'user' }); }
        return this._userPrompt;
    }

    /**
     * flag to indicate if we are currently prompting the user.
     */
    protected prompting = false;
    protected rl: readline.Interface | undefined;

    protected userFlaggedQuit: boolean = false;

    protected rliRobbot: IbGibRobbotAny | undefined;

    /**
     * when receiving an update to the context, we want to know if the incoming
     * ibgib child is one that we've already handled. this is for idempotence
     * (to avoid double-handling).
     */
    protected alreadyHandledContextChildrenAddrs: IbGibAddr[] = [];

    /**
     * handlers for routing commands.
     *
     * order matters. the pipeline tries the first handler for a cmd, then
     * the next handler and so on.
     */
    protected cmdHandlerFns: { [cmd: string]: RCLICommandHandler[] } = {
        [RCLICommand.help]: [rcliCmdHandlerHelp],
        [RCLICommand.fork]: [rcliCmdHandlerFork],
        [RCLICommand.mut8]: [rcliCmdHandlerMut8],
        [RCLICommand.rel8]: [rcliCmdHandlerRel8],
        [RCLICommand.quit]: [rcliCmdHandlerQuit],
        [RCLICommand.cwd]: [rcliCmdHandlerCwd],
        [RCLICommand.add_comment]: [rcliCmdHandlerAddComment],
        [RCLICommand.list_chat]: [rcliCmdHandlerListChat],
        [RCLICommand.reify_file]: [rcliCmdHandlerListChat],
        [RCLICommand.generate_source_file]: [rcliCmdHandlerGenerateSourceFile],
        [RCLICommand.info]: [rcliCmdHandlerInfo],
        [RCLICommand.b2tfs_init]: [rcliCmdHandler_B2tFS_init],
        [RCLICommand.b2tfs_branch]: [rcliCmdHandler_B2tFS_branch],
        [RCLICommand.b2tfs_activate_branch]: [rcliCmdHandler_B2tFS_activateBranch],
        [RCLICommand.b2tfs_info]: [rcliCmdHandler_B2tFS_info],
        [RCLICommand.b2tfs_diff]: [rcliCmdHandler_B2tFS_diff],
    };

    constructor(initialData?: RCLIAppData_V1, initialRel8ns?: RCLIAppRel8ns_V1) {
        super(initialData, initialRel8ns);
        const lc = `${this.lc}[ctor]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            this.initialize();
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * overridden to handle RCLI comment which invokes the RCLI.
     */
    protected async doNonArg({
        ibGib,
    }: {
        ibGib: IbGib_V1,
    }): Promise<IbGib_V1 | undefined> {
        const lc = `${this.lc}[${this.doNonArg.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: cf68de0802e2e6fc4ae78ca986fcc823)`); }

            if (isRCLIComment({ ibGib })) {
                return await this.doContextRCLIComment({ rcliCommentIbGib: ibGib as RCLICommentIbGib_V1 });
            } else {
                return await super.doNonArg({ ibGib });
            }
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * initializes/creates the accompanied RCLI robbot to interact with this
     * RCLI app.
     *
     * ## notes
     *
     * I'm having a hard time still distinguishing between an app and a robbot.
     * For now, the practicality of it equates to me using the app as something
     * that is fixed and the robbot as being more flexible and able to
     * potentially handle more robust commands.
     */
    protected async initializeRCLIRobbot(): Promise<void> {
        const lc = `${this.lc}[${this.initializeRCLIRobbot.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: a121ee114239483dda836b6713937523)`); }

            this.rliRobbot = await this.initializeRCLIRobbot_getExistingRCLIRobbot();

            if (!this.rliRobbot) {
                this.rliRobbot = await this.initializeRCLIRobbot_createNewRobbot();
            }

            if (this.rliRobbot) {
                await this.rliRobbot.initialized;
                if (logalot) { console.log(`${lc} this.rliRobbot and initialized (I: d1020c2e02f9f592848ceaec6a6a8e23)`); }
            } else {
                throw new Error(`(UNEXPECTED) couldn't initialize RCLI robbot? (E: 739a5bc7fb3e5af21b356d9b5aa3a223)`);
            }

        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async initializeRCLIRobbot_getExistingRCLIRobbot(): Promise<IbGibRobbotAny | undefined> {
        const lc = `${this.lc}[${this.initializeRCLIRobbot_getExistingRCLIRobbot.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 548ea720676eba58647515ba14d33323)`); }

            let rliRobbot: IbGibRobbotAny | undefined = undefined;

            let robbotAddrs = this.rel8ns?.[RCLI_ROBBOT_REL8N_NAME] ?? [];

            if (robbotAddrs.length > 0) {
                // robbot addr(s) already exists so first try getting that
                if (logalot) { console.log(`${lc} existing rli robbot addr(s) found. (I: 423fcd8c69a366363c40f419820a6623)`); }
                if (!this.ibgibsSvc) { throw new Error(`this.ibgibsSvc falsy. not initialized? (E: 6dd8966c212a4362ae543e796c9e842d)`); }
                const space = await this.ibgibsSvc.getLocalUserSpace({});
                if (!space) { throw new Error(`couldn't get local user space? (E: c70a3237ad20494c8b59f0be0ea1b475)`); }

                for (let i = 0; i < robbotAddrs.length; i++) {
                    const rliRobbotAddr = robbotAddrs[i];
                    const resGet = await this.ibgibsSvc.get({ addrs: [rliRobbotAddr] });
                    if (!resGet.success || resGet.ibGibs?.length !== 1) {
                        console.error(`${lc} failed to get robbot (${rliRobbotAddr}). (E: b38f0a25d92e44c0aa5cc3b038a43c6c)`);
                        continue;
                    } else {
                        rliRobbot = resGet.ibGibs![0] as IbGibRobbotAny;
                        break;
                    }
                }


                if (!rliRobbot) {
                    //     await rliRobbot.initialized;
                    // } else {
                    console.error(`${lc} ${robbotAddrs.length} existing robbot addrs rel8d but couldn't load any? (E: 4c969aa4da6d408d822c0968aaafbc30)`)
                }

                // currently, rliRobbot is only a DTO, so load the full witness
                let robbotWitness = new RollyRobbot_V1(undefined, undefined);
                await robbotWitness.loadIbGibDto(rliRobbot!);
                robbotWitness.ibgibsSvc = this.ibgibsSvc;

                // we're done
                return robbotWitness;
            } else {
                // no existing robbot addrs
                return undefined;
            }
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }
    /**
     * this should be in a node helper for the dynamic form plumbing.
     */
    protected async promptDynamicFormItem({
        item,
        /**
         * title for a group of prompts.
         */
        groupTitle,
    }: {
        /**
         *
         */
        item: FormItemInfo,
        /**
         * title for a group of prompts.
         */
        groupTitle: string,
    }): Promise<FormItemInfo> {
        const lc = `${this.lc}[${this.promptDynamicFormItem.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 871d6bb68f2e431b6b43ba4bc0cb5a23)`); }

            if (item.readonly) {
                item.value = item.defaultValue;
                return item; /* <<<< returns early */
            }

            let title = groupTitle ? `${groupTitle}] ${item?.label}?` : `${item?.label ?? item.name}?`;
            if (item.description) { title = `${title}\n${item.description}`; }

            let valid = false;

            let value: string | number | boolean | undefined;
            do {
                //   'text' | 'textarea' | 'select' | 'toggle' | 'number' | 'form';
                if (item.dataType === 'text' || item.dataType === 'textarea') {
                    value = await promptForText({
                        title,
                        msg: (item.label ?? item.name) + '?' + (item.defaultValue ? ` [ ${item.defaultValue} ]` : ''),
                        confirm: false,
                        noNewLine: false,
                        defaultValue: item.defaultValue?.toString(),
                    });

                } else if (item.dataType === 'number') {
                    value = await promptForText({
                        title,
                        msg: (item.label ?? item.name) + '?' + (item.defaultValue ? ` [ ${item.defaultValue} ]` : ''),
                        confirm: false,
                        noNewLine: false,
                    });
                    if (Number.isNaN(Number.parseInt(value))) {
                        console.error(`value is not a number. must be a number...hmm`);
                        valid = false;
                    }
                    value = Number.parseInt(value); // meh, was a string, now a number but we know this is possible in context
                } else if (item.dataType === 'toggle') {
                    console.log(`${lc} todo: need to improve/test this boolean prompt`);
                    value = await promptForConfirm({
                        msg: (item.label ?? item.name) + '?' + (item.defaultValue ? ` [ ${item.defaultValue} ]` : ''),
                        yesLabel: 'true',
                        noLabel: 'false',
                    }) as boolean;
                } else if (item.dataType === 'form') {
                    await this.promptDynamicFormItem({ item, groupTitle: groupTitle + (item.label ?? item.name), })
                } else if (item.dataType === 'select') {
                    if (!item.selectOptions && !item.selectOptionsWithIcons) { throw new Error(`(UNEXPECTED) invalid item. if type is 'select' must have either selectOptions or SelectOptionsWithIcons (E: 4d16dd9f88a7252264677cb32a865f23)`); }
                    let options = item.selectOptions ?
                        item.selectOptions!.concat() :
                        item.selectOptionsWithIcons!.map(x => `${x.label} - ${x.value}`);
                    let msg = (item.label ?? item.name) + '?' + (item.defaultValue ? ` [ ${item.defaultValue} ]` : '');
                    for (let i = 0; i < options.length; i++) {
                        const option = options[i];
                        msg += `\n${i}. ${option}`;
                    }
                    msg += '\nEnter the number of your selection.';
                    value = await promptForText({
                        title,
                        msg: msg,
                        confirm: false,
                        noNewLine: false,
                    });
                    let parsedSelectionIndex = Number.parseInt(value);
                    if (Number.isInteger(parsedSelectionIndex) && parsedSelectionIndex >= 0 && parsedSelectionIndex < options.length) {
                        value = item.selectOptions ?
                            item.selectOptions![parsedSelectionIndex] :
                            item.selectOptionsWithIcons![parsedSelectionIndex]!.value;
                    } else {
                        console.error(`Invalid selection. Please enter the NUMBER for your option.`);
                        value = undefined;
                    }
                } else {
                    throw new Error(`other item.dataType (${item.dataType}) not implemented yet in node prompt (E: bdadf312ed16054518a7fdb9a78ae723)`);
                }

                if (!value && item.defaultValue) { value = item.defaultValue as any; }

                let regexpIsValid = false;

                if (item.regexp) {
                    // regexp implies string
                    regexpIsValid = !!((value as string).match(item.regexp));
                    if (!regexpIsValid) {
                        console.error(item.regexpErrorMsg ?? item.defaultErrorMsg ?? 'invalid regexp');
                        console.error(`raw regexp source: ${item.regexp.source}`);
                    }
                } else {
                    regexpIsValid = true;
                }

                let fnValidIsValid = false;
                if (item.fnValid) {
                    fnValidIsValid = item.fnValid(item.value as any);
                    if (!fnValidIsValid) {
                        console.error(item.defaultErrorMsg ?? 'invalid value');
                    }
                } else {
                    fnValidIsValid = true;
                }

                valid = regexpIsValid && fnValidIsValid;
            } while (!valid)

            // use the item reference directly?
            item.value = value! ?? item.defaultValue ?? undefined;

            return item;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async initializeRCLIRobbot_createNewRobbot(): Promise<IbGibRobbotAny | undefined> {
        const lc = `${this.lc}[${this.initializeRCLIRobbot_createNewRobbot.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: ce78e26ece7f7b16ab62b3a24326e523)`); }
            let rollyFactory = new RollyRobbot_V1_Factory();
            let blank = (await rollyFactory.newUp({})).newIbGib;
            let form = await rollyFactory.witnessToForm({ witness: blank });
            let items = form.items;
            for (let i = 0; i < items.length; i++) {
                const item = items[i];
                if (!item.readonly) {
                    await this.promptDynamicFormItem({ item, groupTitle: RollyRobbot_V1.name });
                } else {
                    console.log(`${item.label ?? item.name}: ${item.value} (readonly)`);
                }
            }
            const resRollyGraph = await rollyFactory.formToWitness({ form });

            if (!this.ibgibsSvc) { throw new Error(`(UNEXPECTED) this.ibgibsSvc falsy? (E: 6d7046456f8a48237249d885ea509823)`); }
            await this.ibgibsSvc!.persistTransformResult({ resTransform: resRollyGraph });

            const rliRobbot = resRollyGraph.newIbGib;

            await this.ibgibsSvc.registerNewIbGib({
                ibGib: rliRobbot
            });

            await this.rel8To({
                ibGibs: [rliRobbot],
                rel8nName: RCLI_ROBBOT_REL8N_NAME,
                ibgibsSvc: this.ibgibsSvc,
                linked: false,
            });

            rliRobbot.ibgibsSvc = this.ibgibsSvc;

            return rliRobbot;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async doContextRCLIComment({
        rcliCommentIbGib
    }: {
        rcliCommentIbGib: RCLICommentIbGib_V1
    }): Promise<IbGib_V1> {
        const lc = `${this.lc}[${this.doContextRCLIComment.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 7f084169817731807e31a9d9ae133423)`); }

            if (!this.rliRobbot) { await this.initializeRCLIRobbot(); }

            if (this._currentWorkingContextIbGib) {
                // handling a comment but we've already established our context.
                console.log(`${lc} we're handling a comment but we've already established our context...not really tested yet...`);
                await this.finalizeContext({ arg: undefined });
            }

            if (logalot) { console.log(`${lc} rcliCommentIbGib addr: ${getIbGibAddr({ ibGib: rcliCommentIbGib })} (I: 2cb42e96aba7a48c9265338c43f38e23)`); }
            if (logalot) { console.log(`${lc} rcliCommentIbGib.data: ${pretty(rcliCommentIbGib.data)} (I: e056a45ee0cd463bb4064e7ff168093c)`); }
            // we haven't established a context, so this is the first
            // invoking request which we will use as our context.
            // initiate interaction with the context.
            await this.initializeContext({
                contextIbGib: rcliCommentIbGib,
                rel8nName: WITNESS_CONTEXT_REL8N_NAME,
            });

            if (rcliCommentIbGib.oneOff) {
                await this.createCommentAndRel8ToContextIbGib({
                    text: rcliCommentIbGib.data!.text,
                    contextIbGib: rcliCommentIbGib,
                    rel8nName: 'comment',
                    ibgibsSvc: this.ibgibsSvc,
                });

            } else {
                await this.createCommentAndRel8ToContextIbGib({
                    text: `${this.rcliPrompt} ${rcliCommentIbGib.data!.text}`,
                    contextIbGib: rcliCommentIbGib,
                    rel8nName: 'comment',
                    ibgibsSvc: this.ibgibsSvc,
                });

                do { await this.promptUser(); } while (!this.userFlaggedQuit)
            }

            return ROOT;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async paintPrompt(): Promise<void> {
        const lc = `${this.lc}[${this.paintPrompt.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: e09f4a60ae7f5b4162bcd37c2ae59c23)`); }

            if (this.rl) {
                stdout.write(`\x1b[2K\r${this.userPrompt} ${this.rl.line ?? ''}`);
            }
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }

    }
    /**
     * prompts the user, waiting for input. adds that input as comment and other
     * machinary will determine if it should quit.
     *
     * has this.prompting flag, with naive idempotence (i.e. not ref counted)
     */
    protected async promptUser(): Promise<void> {
        const lc = `${this.lc}[${this.promptUser.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 55e18bf4d352821bd7ba40098bef0323)`); }
            if (this.prompting) {
                await this.paintPrompt();
                return; /* <<<< returns early */
            } else {
                this.prompting = true;
                this.rl ??= readline.createInterface({
                    input: stdin,
                    output: stdout
                });
            }
            let response: string = '';
            do {
                response = await promptForText({
                    // title: this.rcliPrompt,
                    msg: this.userPrompt,
                    confirm: false,
                    noNewLine: true,
                    rl: this.rl,
                });
            } while (!response)

            // all input are handled as comments in the current context
            await this.createCommentAndRel8ToContextIbGib({
                text: response,
                rel8nName: 'comment',
                contextIbGib: this._currentWorkingContextIbGib!,
                ibgibsSvc: this.ibgibsSvc,
            });
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            this.prompting = false;
            if (this.rl) {
                this.rl.close();
                delete this.rl;
            }
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async initializeContext({
        arg,
        contextIbGib,
        rel8nName,
    }: {
        arg?: WitnessArgIbGib<IbGib_V1, any, any>,
        contextIbGib?: IbGib_V1,
        rel8nName: string,
    }): Promise<void> {
        const lc = `${this.lc}[${this.initializeContext.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 7c27c3414a3904f2c564a29b09ff1e23)`); }
            await super.initializeContext({ arg, contextIbGib, rel8nName });

            if (logalot) { console.log(`${lc} super.initializeContext complete. (I: d261f891467792e01cb6bbba784a8c23)`); }

            // prepare
            contextIbGib ??= await this.getContextIbGibFromArgOrAddr({ arg, latest: false });
            if (!contextIbGib) { throw new Error(`couldn't get contextIbGib? (E: 6d567ceb485b3b5a198edb23f8fdfd23)`); }
            if (!this.rliRobbot) { throw new Error(`(UNEXPECTED) this.rliRobbot falsy? (E: 97f641c974a4ec32c58fe4f1dd3eb323)`); }

            if (logalot) { console.log(`${lc} validation complete. (I: f0576cf2f9d3ed38b8a6766d608ff723)`); }

            // initialize the context of our helper robbot as well, as both
            // initially start out the same
            // in the future here, these may not be the same though...
            const argActivate = await this.rliRobbot.argy({
                argData: {
                    cmd: RobbotCmd.activate,
                    ibGibAddrs: [getIbGibAddr({ ibGib: contextIbGib })], // context
                },
                ibGibs: [contextIbGib], // context
            });
            const resCmd = await this.rliRobbot.witness(argActivate);

            if (!resCmd) { throw new Error(`resCmd is falsy. (E: ea2177ce94a44e16a846448c1aa07d97)`); }
            if (isError({ ibGib: resCmd })) {
                const errIbGib = resCmd as ErrorIbGib_V1;
                throw new Error(`errIbGib: ${pretty(errIbGib)} (E: 6c5d2dae462e450f91090369165ea76d)`);
            }
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * @see {@link handleContextUpdate}
     */
    protected async handleNewContextChild({ newChild }: { newChild: IbGib_V1 }): Promise<void> {
        const lc = `${this.lc}[${this.handleNewContextChild.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 88fe5e432fb872868f6faa46bc40d623)`); }

            const addr = getIbGibAddr({ ibGib: newChild });

            if (this.alreadyHandledContextChildrenAddrs.includes(addr)) {
                if (logalot) { console.log(`${lc} already handled, returning early: ${addr} (I: 57adca7ed6454402bc7298425b48596d)`); }
                return; /* <<<< returns early */
            } else {
                this.alreadyHandledContextChildrenAddrs.push(addr);
            }

            if (isComment({ ibGib: newChild })) {
                // console.log(`${lc} recvd yo`)
                // temporary here...need to get a better handle on what our
                // strategy is
                setTimeout(async () => {
                    await this.doComment({ ibGib: newChild as CommentIbGib_V1 });
                    console.log(`${this.rcliPrompt} echo: ${newChild.data!.text}`);
                });
                // console.log(`${lc} this._currentWorkingContextIbGib:`);
                // console.log(pretty(this._currentWorkingContextIbGib));
                // setTimeout(() => { console.log(this.userPrompt); }, 1000);
                setTimeout(() => {
                    if (this.prompting) { this.paintPrompt(); }
                }, 1000);
            }
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * overridden to convert comments to requests, if applicable.
     */
    protected async doComment({
        ibGib,
    }: {
        ibGib: CommentIbGib_V1,
    }): Promise<IbGib_V1 | undefined> {
        const lc = `${this.lc}[${this.doComment.name}]`;
        let result: IbGib_V1 | undefined = undefined;
        try {

            if (logalot) { console.log(`${lc} starting... (I: 2aff0c8c408280380e444937b51d2823)`); }

            if (!ibGib.data) { throw new Error(`(UNEXPECTED) ibGib.data falsy? (E: 797d98487fd87e3cace0879258ad4323)`); }
            const { text } = ibGib.data;
            if (!text) { throw new Error(`(unexpected) ibGib.data.text falsy? should be a comment (E: ed245746863d53bb1449350d41392323)`); }

            // app is hard-coded atow to start requests with the colon (:), e.g., ":quit"
            const cmdEscapeString = this.data!.cmdEscapeString ?? RCLI_DEFAULT_COMMAND_ESCAPE_STRING;
            if (isRCLICommandComment({ ibGib, cmdEscapeString })) {
                // put into command pipeline
                result = await this.routeAndDoRCLICommand({ commentIbGib: ibGib, cmdEscapeString });
            } else {
                // just echo atm. the ibgib itself has already been added to the
                // context via the "comment" rel8n name.
                // setTimeout(() => {
                //     // const msg = `echo: ${text}`;
                //     // stdout.write(`\x1b[2K\r${this.rcliPrompt} ${msg}`);
                //     console.log(`\n${text}`)
                // });

                result = ROOT;
            }

            return result;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            // don't rethrow ?
            // throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * If we've gotten here, the user/robbot/someone has added a comment to the
     * current context, and that comment has been interpreted as being a command
     * for this RCLI app to process.
     *
     * This is different than a request said to one of the robbots or someone
     * else, this is specifically a command for the app to handle.
     */
    protected routeAndDoRCLICommand({
        commentIbGib,
        cmdEscapeString,
    }: {
        commentIbGib: CommentIbGib_V1,
        cmdEscapeString: string,
    }): Promise<IbGib_V1 | undefined> {
        const lc = `${this.lc}[${this.routeAndDoRCLICommand.name}]`;
        return new Promise<IbGib_V1 | undefined>(async (resolve, reject) => {
            try {
                if (logalot) { console.log(`${lc} starting... (I: 206631a4d0dc2219f9d8b4dd24e2f923)`); }
                if (!this._currentWorkingContextIbGib) { throw new Error(`(UNEXPECTED) this._currentWorkingContextIbGib expected to be truthy/initialized. (E: 55f4538845b34b0b9760d588583fb02f)`); }

                if (logalot) {
                    console.log(`${lc} console.dir(commentIbGib)... (I: 784333205d9a737d7eed2b45d5edc123)`);
                    console.dir(commentIbGib);
                }

                let cmdInfo = await this.parseRCLICommandText({
                    ibGib: commentIbGib,
                    cmdEscapeString,
                });
                let { cmd, rawText, errorMsg, } = cmdInfo;
                // do on the next process loop after we have completed the
                // current handle next child...not sure i like this type of
                // hack. it seems to be because we're appending a child while
                // already handling a new context child. but we should
                // DEFINITELY be able to handle multiple children being added
                if (errorMsg) {
                    if (logalot) { console.log(`${lc} errorMsg(?): ${errorMsg} (I: 56ba860fe6431e204394519a27c1c923)`); }
                    // there was an error
                    await this.createCommentAndRel8ToContextIbGib({
                        text: `"${cmd ? this.data!.cmdEscapeString + cmd : rawText}" request not impl? error: ${errorMsg} (E: b2a5565f04734d4c80f504b3604b674f)`,
                        contextIbGib: this._currentWorkingContextIbGib!,
                        rel8nName: 'comment',
                        ibgibsSvc: this.ibgibsSvc,
                    });

                    resolve(ROOT);
                    return; /* <<<< returns early */
                }

                if (logalot) { console.log(`${lc} passed validation. routing... (I: bd0f1ce083aee606b2cde02433fc6f23)`); }
                // route
                // should be able to handle all RCLICommand's
                let result: IbGib_V1 | undefined = undefined;
                if (cmd === RCLICommand.quit) {
                    if (logalot) {
                        console.log(`${lc} cmd init. routing complete. (I: 5e62112423561ce74e8510db819bd723)`);
                        console.log(`${lc} cwd: ${cwd()} (I: 1fbbe4d4d0790325fc88f9bf621a5223)`);
                    }
                    this.userFlaggedQuit = true;
                    result = ROOT;
                } else if (cmd === RCLICommand.init) {
                    // already handled before handoff to this app
                    if (logalot) { console.log(`${lc} cmd init: routing complete. (I: 1176df79d23db197dc94f2c9c0959523)`); }
                    result = ROOT;
                } else {
                    if (logalot) { console.log(`${lc} sending cmd to handler pipeline. (I: cc0d1cb45073cfb5d392a043480bd623)`); }
                    result = await this.handleCommandViaHandlerPipeline({
                        cmdInfo,
                        cmdEscapeString,
                        contextIbGib: this._currentWorkingContextIbGib!,
                        ibGib: commentIbGib,
                        metaspace: this.ibgibsSvc!,
                        fnAddComment: (arg) => this.createCommentAndRel8ToContextIbGib(arg),
                    });
                    if (logalot) { console.log(`${lc} pipeline complete. (I: 027153226eeb31d8442e488726a50123)`); }
                }
                if (!result) {
                    if (logalot) { console.log(`${lc} result still falsy. routing to default command handler. (I: 6b89ea11a37cffbdf48bb8369305a423)`); }
                }
                result ??= await this.handleCommand_default({
                    cmdInfo,
                    cmdEscapeString,
                    contextIbGib: this._currentWorkingContextIbGib!,
                    ibGib: commentIbGib,
                    metaspace: this.ibgibsSvc!,
                    fnAddComment: (arg) => this.createCommentAndRel8ToContextIbGib(arg),
                });

                if (logalot) { console.log(`${lc} routing complete. (I: 8805ec40ae0c7d9e0f7ba8cce0eee323)`); }
                if (result?.ib === 'quit' || (commentIbGib as RCLICommentIbGib_V1).oneOff) {
                    this.userFlaggedQuit = true;
                }

                resolve(result);
            } catch (error) {
                console.error(`${lc} ${extractErrorMsg(error)}`);
                reject(error);
            } finally {
                if (logalot) { console.log(`${lc} complete.`); }
            }
        })
    }

    /**
     * Iterates through this.handlers until a result is produced.
     */
    protected async handleCommandViaHandlerPipeline(arg: RCLICommandHandlerArg): Promise<IbGib_V1 | undefined> {
        const lc = `${this.lc}[${this.handleCommandViaHandlerPipeline.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: f25ed09ef3814d9e9299a5c9c8f1efb3)`); }
            let { cmdInfo, cmdEscapeString, contextIbGib, ibGib, metaspace, } = arg;
            if (!cmdInfo.cmd) { throw new Error(`cmdInfo.cmd falsy? maybe this is valid but atow i'm thinking this should be truthy. (E: 2f5e8e57170c2909d5b4e55bad3abf23)`); }
            const handlers = this.cmdHandlerFns[cmdInfo.cmd] ?? [];
            if (handlers.length === 0) {
                console.warn(`${lc} no handlers found for valid command name ${cmdInfo.cmd} (W: 4edc0b9cdb4c4febb53b638dc3fd4587)`);
                return undefined; /* <<<< returns early */
            }

            for (let i = 0; i < handlers.length; i++) {
                const fnHandler = handlers[i];
                const result = await fnHandler(arg);
                return result; /* <<<< returns early */
            }

            // if we've gotten here, then there was no result produced
            return undefined;
        } catch (error) {
            // debugger; // in error block
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async handleCommand_default({
        cmdInfo,
        cmdEscapeString,
        contextIbGib,
        ibGib,
        metaspace,
    }: RCLICommandHandlerArg): Promise<IbGib_V1 | undefined> {
        const lc = `${this.lc}[${this.handleCommand_default.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 38693806745793a0636382d2d10fe423)`); }
            // unknown how to handle the command
            await this.createCommentAndRel8ToContextIbGib({
                text: `"${cmdEscapeString}${cmdInfo.cmd}" cmd not impl...or iow...huh?`,
                contextIbGib: this._currentWorkingContextIbGib!,
                rel8nName: 'comment',
                ibgibsSvc: this.ibgibsSvc,
            });
            return ROOT;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async parseRCLICommandText({
        ibGib,
        cmdEscapeString,
    }: {
        ibGib: CommentIbGib_V1,
        cmdEscapeString: string,
    }): Promise<RCLICommandTextInfo> {
        const lc = `${this.lc}[${this.parseRCLICommandText.name}]`;
        let rawText = ibGib.data?.text ?? '';
        try {
            if (logalot) { console.log(`${lc} starting... (I: e5bbbb50174a6bdfafceeddcde156523)`); }
            let showHelp: boolean = false;

            if (!ibGib.data) { throw new Error(`ibGib.data required for comment ibgibs (E: c6ff0bd4be1521aee826d79abe28c423)`); }

            let {
                validArgsString,
                args,
                cmd_ie_firstArgSansPrefix: rawCmdText,
                validationErrorMsg
            } = parseCommandRawTextIntoArgs({
                rawText,
                validCommandIdentifiers: RCLI_COMMAND_IDENTIFIERS.concat(),
                logErrors: !!logalot,
            });
            if (!validArgsString) { throw new Error(`validationErrorMsg: ${validationErrorMsg ?? '(UNEXPECTED) validationErrorMsg falsy? (E: ef73b6dd91614dfeb0beff5b45481987)'} (E: 32be53bf7417562d09d6596c5776b423)`); }

            // at this point, args and cmd should be truthy
            if (!args || args.length === 0) { throw new Error(`(UNEXPECTED) validArgsString but args is falsy/empty? (E: 99c62a49478f7076026962b5466f4223)`); }
            if (!rawCmdText) { throw new Error(`(UNEXPECTED) validArgsString but returned cmd is falsy? (E: b4e4e6ac3f060566166302f585385223)`); }

            // if (!rawText.startsWith(cmdEscapeString)) {
            //     if (rawText.startsWith('--')) {
            //         // maybe is a command...
            //         const firstArg = rawText.split(' ')[0].slice(2).toLowerCase(); // 2 to slice off -- prefix
            //         if (!RCLI_COMMAND_IDENTIFIERS.includes(firstArg)) {
            //             throw new Error(`(UNEXPECTED) at this point atow, cmdEscapeString is expected to start the command's raw text or start with a command arg with -- prefix (E: 69ad38b36daddcaaddba1edfedc54823)`);
            //         }
            //     }
            // }

            // args = rawText.split(' ') ?? [];
            // let args: string[] = rawText.split(' ') ?? [];
            // args_spaceDelimited = rawText.split(' --') ?? []; // doesn't work if there is a bare arg
            // const firstArg = args[0]!;
            // let rawCmdText: string;
            // if (firstArg.startsWith(cmdEscapeString)) {
            //     rawCmdText = firstArg.substring(cmdEscapeString.length);
            // } else if (firstArg.startsWith('--')) {
            //     rawCmdText = firstArg.substring('--'.length);
            // } else {
            //     throw new Error(`(UNEXPECTED) expected a valid command at this point. the first arg must be a valid command. any bare args or others must come after the command arg. valid commands must not contain spaces. current valid commands (and synonyms): ${RCLI_COMMAND_IDENTIFIERS} (E: 2524e642dfbcd075cca2aa94a109b123)`);
            // }
            // let cmdParamInfo = getParamInfo({
            //     argIdentifier: rawCmdText,
            //     paramInfos: PARAM_INFOS
            // });

            let cmdParamInfo = getParamInfo({
                argIdentifier: rawCmdText,
                paramInfos: PARAM_INFOS,
                throwIfNotFound: false,
            });

            if (!cmdParamInfo) {
                return {
                    rawText,
                    errorMsg: `unknown command ${rawCmdText}`,
                }; /* <<<< returns early */
            }

            if (!cmdParamInfo.isFlag) { throw new Error(`(UNEXPECTED) atow I'm expecting this to always be a flag (E: f2ecc77d4bcc13373a7fccb3c2709b23)`); }
            if (!RCLI_COMMANDS.includes(cmdParamInfo.name as RCLICommand)) {
                throw new Error(`(unexpected) unknown RCLI command. rawCmdText: ${rawCmdText}. cmdParamInfo.name: ${cmdParamInfo.name} (E: b9136afd19c12838c2201b34eac38723)`);
            }
            const cmd = cmdParamInfo.name as RCLICommand;

            // I have everything prefixed with the double dashes atow
            // but we removed those dashes. manually with args[0] and
            // broadly when we split with raw.split(' --')
            // args[0] = `--${cmd}`;
            // args[0] = cmd;
            // args = args.map(x => `--${x}`);

            if (logalot) { console.log(`${lc} args.join(' '): ${args.join(' ')}`); }

            console.warn(`${lc} todo: change logalot type to number | boolean in helper-gib`);
            // const argInfos = buildArgInfos({ args, paramInfos: PARAM_INFOS, logalot: !!logalot });
            const argInfos = buildArgInfos({
                args: args,
                paramInfos: PARAM_INFOS,
                logalot: !!logalot
            });

            const validationErrors = validateArgInfos({ argInfos });
            if (validationErrors && validationErrors.length > 0) {
                // there was an error, so return early
                return {
                    rawText,
                    errorMsg: `There were validation errors for command args: ${validationErrors}`,
                }; /* <<<< returns early */
            }

            // showHelp = (argInfos.some(x => argIs({
            showHelp = (args.some(x => argIs({
                arg: x,
                paramInfo: PARAM_INFO_HELP,
            })));

            if (logalot) {
                console.log(`${lc} here are the args received`);
                for (let i = 0; i < args.length; i++) {
                    const arg = args[i];
                    console.log(`arg ${i}: ${arg}`);
                }
            }

            return { cmd, rawText, argInfos, showHelp, };
        } catch (error) {
            return {
                rawText,
                errorMsg: `There was an error: ${extractErrorMsg(error)}`,
            }; /* <<<< returns early */
            // console.error(`${lc} ${extractErrorMsg(error)}`);
            // throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     *
     * @returns addlmetadata string (atow default impl in base class)
     */
    protected getAddlMetadata(): string {
        return super.getAddlMetadata();
    }
    protected parseAddlMetadataString<TParseResult>({ ib }: { ib: string; }): TParseResult {
        // const addlMetadataText = `${atom}_${classnameIsh}_${nameIsh}_${idIsh}`;
        if (!ib) { throw new Error(`ib required (E: 4ec93595c12cc15ee71d92dd8a4f4f23)`); }
        const lc = `[${this.parseAddlMetadataString.name}]`;
        try {
            const [atom, classnameIsh, nameIsh, idIsh] = ib.split('_');
            const result = { atom, classnameIsh, nameIsh, idIsh, } as RCLIAppAddlMetadata;
            return result as TParseResult; // i'm not liking the TParseResult...hmm
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        }
    }

    /**
     * Initializes to default space values.
     */
    protected async initialize(): Promise<void> {
        const lc = `${this.lc}[${this.initialize.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            if (!this.data) { this.data = clone(DEFAULT_RCLI_APP_DATA_V1); }
            if (!this.rel8ns && DEFAULT_RCLI_APP_REL8NS_V1) {
                this.rel8ns = clone(DEFAULT_RCLI_APP_REL8NS_V1);
            }
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async doDefault({
        ibGib,
    }: {
        ibGib: IbGib_V1,
    }): Promise<IbGib_V1> {
        const lc = `${this.lc}[${this.doDefault.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            if (this._currentWorkingContextIbGib) {
                const emsg = `(UNEXPECTED) ERROR: ${lc} hmm, i haven't impl ${RCLIApp_V1.name}.${this.doDefault.name}  (E: 10992627a19b4a338dc98bd201e77061)`;
                console.error(emsg);
                await this.createCommentAndRel8ToContextIbGib({
                    text: emsg,
                    contextIbGib: this._currentWorkingContextIbGib,
                    rel8nName: 'comment',
                    ibgibsSvc: this.ibgibsSvc,
                });
            }
            return ROOT;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * In this app, the ib command looks at given ibGib(s) and remembers
     * it/them (i.e. the app rel8s the ibgibs to itself).
     *
     * @returns ROOT if successful, else throws
     */
    protected async doCmdIb({
        arg,
    }: {
        arg: AppCmdIbGib<IbGib_V1, any, any, AppCmdData, AppCmdRel8ns>,
    }): Promise<IbGib_V1> {
        const lc = `${this.lc}[${this.doCmdIb.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            throw new Error(`not implemented (E: 09ce64dd7939ee2bd3a614ce80138d22)`);
            return ROOT;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * "Speak" one of the memorized ibgibs using the given arg.data.ibGibAddrs[0] as the context
     * ibGib.
     *
     * @returns ROOT if all goes well, otherwise throws an error.
     */
    protected async doCmdGib({
        arg,
    }: {
        arg: AppCmdIbGib<IbGib_V1, any, any, AppCmdData, AppCmdRel8ns>,
    }): Promise<IbGib_V1> {
        const lc = `${this.lc}[${this.doCmdGib.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            if (!this.ibgibsSvc) { throw new Error(`(UNEXPECTED) this.ibgibsSvc falsy. not initialized? (E: b737e5093985e810469a12d7ddd96b23)`); }

            const space = await this.ibgibsSvc.getLocalUserSpace({ lock: true });

            throw new Error(`not implemented yet (E: 2fee227028baa72a660cb5b60f020122)`);
            return ROOT;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }

    }
    protected async doCmdIbgib({
        arg,
    }: {
        arg: AppCmdIbGib<IbGib_V1, any, any, AppCmdData, AppCmdRel8ns>,
    }): Promise<IbGib_V1> {
        const lc = `${this.lc}[${this.doCmdIbgib.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            throw new Error(`not implemented yet (E: 425ef954b639e64202fe5fb9fdc94b22)`);
            return ROOT;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }

    }

    protected async validateWitnessArg(arg: AppCmdIbGib): Promise<string[]> {
        const lc = `${this.lc}[${this.validateWitnessArg.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            const errors = await super.validateWitnessArg(arg) ?? [];
            if (!this.ibgibsSvc) { throw new Error(`(unexpected) this.ibgibsSvc falsy. not initialized? (E: d8636847aa325fff2892621705b82923)`); }
            if ((arg.data as any).cmd) {
                // perform extra validation for cmds
                if ((arg.ibGibs ?? []).length === 0) {
                    errors.push(`ibGibs required. (E: a21da24eea0049128eeed253aae1218b)`);
                }
            }
            return errors;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async validateThis(): Promise<string[]> {
        const lc = `${this.lc}[${this.validateThis.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            const errors = [
                ...await super.validateThis(),
            ];
            const { data } = this;
            if (data) {
                if (!data.cmdEscapeString) {
                    errors.push('data.cmdEscapeString required (E: c18aac3fbdc44a90b090e1d77fac6378)');
                }
            }
            return errors;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

}

/**
 * factory for random app.
 *
 * @see {@link DynamicFormFactoryBase}
 */
export class RCLIApp_V1_Factory
    extends DynamicFormFactoryBase<RCLIAppData_V1, RCLIAppRel8ns_V1, RCLIApp_V1> {

    protected lc: string = `[${RCLIApp_V1_Factory.name}]`;

    getName(): string { return RCLIApp_V1.name; }

    async newUp({
        data,
        rel8ns,
    }: {
        data?: RCLIAppData_V1,
        rel8ns?: RCLIAppRel8ns_V1,
    }): Promise<TransformResult<RCLIApp_V1>> {
        const lc = `${this.lc}[${this.newUp.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            data ??= clone(DEFAULT_RCLI_APP_DATA_V1);
            data = data!;
            rel8ns = rel8ns ?? DEFAULT_RCLI_APP_REL8NS_V1 ? clone(DEFAULT_RCLI_APP_REL8NS_V1) : undefined;
            data.uuid ||= await getUUID();
            let { classname } = data;

            const ib = getAppIb({ appData: data, classname });

            const resApp = await factory.firstGen({
                ib,
                parentIbGib: factory.primitive({ ib: `app ${classname}` }),
                data,
                rel8ns,
                dna: true,
                linkedRel8ns: [Rel8n.ancestor, Rel8n.past],
                nCounter: true,
                tjp: { timestamp: true },
            }) as TransformResult<RCLIApp_V1>;

            // replace the newIbGib which is just ib,gib,data,rel8ns with loaded
            // witness class (that has the witness function on it)
            const appDto = resApp.newIbGib;
            let appIbGib = new RCLIApp_V1(undefined, undefined);
            await appIbGib.loadIbGibDto(appDto);
            resApp.newIbGib = appIbGib;
            if (logalot) { console.log(`${lc} appDto: ${pretty(appDto)} (I: af9d16de46d6e6d75b2a21312d72d922)`); }

            return resApp as TransformResult<RCLIApp_V1>;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    async witnessToForm({ witness }: { witness: RCLIApp_V1; }): Promise<DynamicForm> {
        const lc = `${this.lc}[${this.witnessToForm.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            let { data } = witness;
            if (!data) { throw new Error(`(UNEXPECTED) witness.data falsy? (E: ce56dc810773bef0ed80fb0f31e89c23)`); }
            // We do the AppFormBuilder specific functions first, because of
            if (logalot) { console.log(`${lc} data: ${pretty(data)} (I: d4dc7619a4da932badbd7738bb4ebd22)`); }
            const idPool = await getIdPool({ n: 100 });
            // type inference in TS! eesh...
            const form = new RCLIFormBuilder()
                .with({ idPool })
                .name({ of: data.name, required: false, })
                .description({ of: data.description ?? DEFAULT_DESCRIPTION_RCLI_APP })
                .and<RCLIFormBuilder>()
                .cmdEscapeString({
                    of: data.cmdEscapeString,
                    required: true,
                    defaultValue: data.cmdEscapeString ?? RCLI_DEFAULT_COMMAND_ESCAPE_STRING,
                })
                .and<AppFormBuilder>()
                .icon({ iconsList: ['alert'], of: data.icon ?? 'alert', required: true })
                .and<DynamicFormBuilder>()
                .uuid({ of: data.uuid, required: true })
                .classname({ of: data.classname! })
                .and<WitnessFormBuilder>()
                .commonWitnessFields({ data })
                .outputForm({
                    formName: 'form',
                    label: 'RCLI',
                });
            return Promise.resolve(form);
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    async formToWitness({ form }: { form: DynamicForm; }): Promise<TransformResult<RCLIApp_V1>> {
        // let app = new RCLIApp_V1(null, null);
        let data: RCLIAppData_V1 = clone(DEFAULT_RCLI_APP_DATA_V1);
        this.patchDataFromItems({ data, items: form.items, pathDelimiter: DEFAULT_DATA_PATH_DELIMITER });
        let resApp = await this.newUp({ data });
        return resApp;
    }

}

export function isRCLICommandComment({
    ibGib,
    cmdEscapeString = RCLI_DEFAULT_COMMAND_ESCAPE_STRING,
}: {
    ibGib: IbGib_V1,
    cmdEscapeString?: string,
}): boolean {
    const lc = `${isRCLICommandComment.name}]`;
    try {
        if (logalot) {
            console.log(`${lc} starting... (I: d7c49619ffb7a9c26d9d74959b91ae22)`);
        }
        if (!isComment({ ibGib })) {
            return false; /* <<<< returns early */
        }
        let { ib } = ibGib;
        if (!ib) {
            throw new Error(`ib or ibGib.ib required (E: d92c26b15fc143977955a167b8b67522)`);
        }
        cmdEscapeString ||= RCLI_DEFAULT_COMMAND_ESCAPE_STRING;

        let text = ibGib.data!.text!;
        // atow naively looks at start of text. todo: change this to either use a templating engine leveraging lex-gib or natural language processing ai

        // it's either a command that starts with a cmd escape string, or it starts with
        // the double-dash lines and the name is a boolean flag that is registered as one of
        // the command identifiers
        if (text.toLowerCase().startsWith(cmdEscapeString.toLowerCase())) {
            // yes command
            return true;
        } else if (text.startsWith('--')) {
            // maybe is a command...
            let firstArg = text.split(' ')[0].slice(2).toLowerCase(); // 2 to slice off -- prefix
            return RCLI_COMMAND_IDENTIFIERS.includes(firstArg);
        } else {
            // definitely not a command
            return false
        }
    }
    catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    }
    finally {
        if (logalot) {
            console.log(`${lc} complete.`);
        }
    }
}
