import {
    AppData_V1, AppRel8ns_V1,
    AppCmdData, AppCmdRel8ns, AppCmdIbGib,
    AppCmd, AppCmdModifier,
    AppResultData, AppResultRel8ns, AppResultIbGib,
    AppIbGib_V1,
} from "@ibgib/core-gib/dist/witness/app/app-types.mjs";
import { IbGibAddr } from "@ibgib/ts-gib";
import { RCLI_DEFAULT_COMMAND_ESCAPE_STRING, RCLICommand } from "./rcli-constants.mjs";
import { RCLIArgInfo, RCLIArgType } from "@ibgib/helper-gib/dist/rcli/rcli-types.mjs";
import { IbGib_V1 } from "@ibgib/ts-gib/dist/V1/types.mjs";
import { CommentIbGib_V1 } from "@ibgib/core-gib/dist/common/comment/comment-types.mjs";
import { MetaspaceService } from "@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs";
import { RCLIApp_V1 } from "./rcli-app-v1.mjs";
// import {
//     WitnessArgData, WitnessArgRel8ns, WitnessArgIbGib,
//     WitnessResultData, WitnessResultRel8ns, WitnessResultIbGib,
// } from "@ibgib/core-gib/dist/witness/witness-types.mjs";

export const DEFAULT_UUID_RCLI_APP = '';
export const DEFAULT_NAME_RCLI_APP = 'rcli_gib';
export const DEFAULT_DESCRIPTION_RCLI_APP =
    `A RCLI (Request/Command Line Interface) app done ibgib style. Instead of rigid RCLI-style
commands, uses more flexible natural language with chat robbots for request
routing to interface with ibgib data space(s).`;

/**
 * globalThis state specific only to ibgib.
 *
 * ## driving use case
 *
 * I want to be able to store the initial cwd and not have to pass this around.
 */
export interface IbGibGlobalThis {
    /**
     * initial `cwd()` when the rcli is started from the commandline.
     *
     * ## notes
     *
     * i am using this as the context path that the user is typing in commands from.
     * when this changes, i.e. when we get some kind of pass-through `cd` command
     * from within the interactive repl, thi
     */
    initialCwd: string;
}

export interface RCLIAppData_V1 extends AppData_V1 {
    /**
     * escape sequence when commanding this rcli app to do something.
     *
     * works basically the same as robbots' requestEscapeString.
     */
    cmdEscapeString: string;
}

export const RCLI_ROBBOT_REL8N_NAME = 'rliRobbot';
export interface RCLIAppRel8ns_V1 extends AppRel8ns_V1 {
    /**
     * robbot(s) that this RCLI uses to interact with the user.
     *
     * the first robbot takes precedence(?)...hmm
     */
    [RCLI_ROBBOT_REL8N_NAME]: IbGibAddr[];
}

export interface RCLIAppIbGib_V1 extends AppIbGib_V1<RCLIAppData_V1, RCLIAppRel8ns_V1> {

}

/**
 * Default data values for a random app.
 *
 * If you change this, please bump the version
 *
 * (but of course won't be the end of the world when this doesn't happen).
 */
export const DEFAULT_RCLI_APP_DATA_V1: RCLIAppData_V1 = {
    version: '1',
    uuid: DEFAULT_UUID_RCLI_APP,
    name: DEFAULT_NAME_RCLI_APP,
    description: DEFAULT_DESCRIPTION_RCLI_APP,
    classname: `RCLIApp_V1`,

    icon: 'code-slash',

    persistOptsAndResultIbGibs: false,
    allowPrimitiveArgs: true,
    catchAllErrors: true,
    trace: false,

    cmdEscapeString: RCLI_DEFAULT_COMMAND_ESCAPE_STRING,
}
export const DEFAULT_RCLI_APP_REL8NS_V1: RCLIAppRel8ns_V1 | undefined = undefined;

export interface RCLIAppAddlMetadata {
    /**
     * should be rcli
     */
    atom?: 'rcli';
    /**
     * classname of rcli **with any underscores removed**.
     */
    classnameIsh?: string;
    /**
     * name of rcli app witness **with any underscores removed**.
     */
    nameIsh?: string;
    /**
     * id of rcli app witness **with any underscores removed**.
     */
    idIsh?: string;
}

export type ExpectPathType = 'file' | 'directory' | 'exists' | undefined;

export interface RCLICommandTextInfo {
    /**
     * raw text from the command ibgib. (i.e. ibgib.data.text)
     */
    rawText: string,
    /**
     * If the command is valid, then this should be the command name (i.e.
     * paramInfo.name not a synonym if that was what was provided in the raw
     * text).
     */
    cmd?: RCLICommand,
    /**
     * If the command is valid, then this should be populated with
     * the argInfos generated from parsing the command.
     */
    argInfos?: RCLIArgInfo<RCLIArgType>[],
    /**
     * If true, then there was an error with the command, not necessarily with
     * its processing.
     *
     * I think most likely this is a validation error.
     */
    errorMsg?: string
    /**
     * If true, then the command is either the bare help command or
     * we are showing the help for the command.
     */
    showHelp?: boolean;
}

export interface RCLICommandHandlerAddCommentFunctionArgs {
    text: string,
    contextIbGib: IbGib_V1,
    rel8nName: string,
}

/**
 * RCLIApp_V1 handler for commands via comment ibgibs.
 */
export interface RCLICommandHandlerArg {
    fnAddComment: (arg: RCLICommandHandlerAddCommentFunctionArgs) => Promise<void>;
    cmdInfo: RCLICommandTextInfo;
    metaspace: MetaspaceService;
    contextIbGib: IbGib_V1;
    ibGib: CommentIbGib_V1;
    cmdEscapeString: string;
}

export type RCLICommandHandler =
    (arg: RCLICommandHandlerArg) => Promise<IbGib_V1 | undefined>;

/**
 * currently i'm just taking this from the definition of node's `Buffer` type.
 *
 * using this with handle-reify-file.mts when reading the file.
 * note that 'binary' may reoslve to 'latin1' per SO at https://stackoverflow.com/questions/46441667/reading-binary-data-in-node-js
 */
export type FileEncoding =
    | 'ascii'
    | 'utf8'
    | 'utf-8'
    | 'utf16le'
    | 'ucs2'
    | 'ucs-2'
    | 'base64'
    | 'base64url'
    | 'latin1'
    | 'binary'
    | 'hex';
export const FileEncoding = {
    ascii: 'ascii' as FileEncoding,
    utf8: 'utf8' as FileEncoding,
    utf_8: 'utf-8' as FileEncoding,
    ['utf-8']: 'utf-8' as FileEncoding,
    utf16le: 'utf16le' as FileEncoding,
    ucs2: 'ucs2' as FileEncoding,
    ucs_2: 'ucs-2' as FileEncoding,
    ['ucs-2']: 'ucs-2' as FileEncoding,
    base64: 'base64' as FileEncoding,
    base64url: 'base64url' as FileEncoding,
    latin1: 'latin1' as FileEncoding,
    binary: 'binary' as FileEncoding,
    hex: 'hex' as FileEncoding,
};
/**
 * valid file encoding values per {@link FileEncoding}
 */
export const FILE_ENCODINGS = Object.values(FileEncoding);
