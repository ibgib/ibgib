import { B2tFSTestConfig } from "./b2tfs-respec-types.mjs";

/**
 * this basically contains information aout the test-b2tfs (atow 01/2024)
 * folder, from which we combine .ibgib and repo for b2tfs test cases.
 */
export const B2TFS_DEFAULT_TEST_CONFIG: B2tFSTestConfig = {
    basePath: 'test-b2tfs',
    startState: {
        ibgibSnapshotsSubpath: 'ibgib-folders',
        repoSubpaths: {
            simple: 'repo-folders/simple',
            complex: 'repo-folders/complex',
        },
    },
    outputSubPath: 'b2tmp',
}
