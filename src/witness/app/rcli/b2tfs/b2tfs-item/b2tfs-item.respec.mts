/**
 * @module b2tfs-item respec
 *
 * we gotta test our b2tfs-item
 */

// import { cwd, chdir, } from 'node:process';
// import { statSync } from 'node:fs';
// import { mkdir, } from 'node:fs/promises';
// import { ChildProcess, exec, ExecException } from 'node:child_process';
import * as pathUtils from 'path';

import {
    firstOfAll, firstOfEach, ifWe, ifWeMight, iReckon,
    lastOfAll, lastOfEach, respecfully, respecfullyDear
} from '@ibgib/helper-gib/dist/respec-gib/respec-gib.mjs';
const maam = `[${import.meta.url}]`, sir = maam;

import { extractErrorMsg, getTimestampInTicks, getUUID, hash, } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../../../ibgib-constants.mjs';
import { getB2tFSItemIb, getB2tFSItemSaferName, parseB2tFSItemIb } from './b2tfs-item-helper.mjs';
import { B2TFS_ITEM_ATOM } from './b2tfs-item-constants.mjs';
import { B2tFSItemData_V1, B2tFSItemSaferNameVersion, B2tFSType } from './b2tfs-item-types.mjs';
/**
 * for verbose logging
 */
const logalot = GLOBAL_LOG_A_LOT; // change this when you want to turn off verbose logging

const lcFile: string = `[${pathUtils.basename(import.meta.url)}]`;

await respecfully(maam, `helpers`, async () => {

    await ifWe(maam, `getB2tFSItemIb`, async () => {
        const atomIn = B2TFS_ITEM_ATOM;
        const fsTypeIn: B2tFSType = 'file';
        const nameIn = 'hoogle';
        const saferNameIn = 'hoogle'; // same as nameIn in this case because already safe (no spaces/special chars)
        const saferNameVersionIn: B2tFSItemSaferNameVersion = 'v1';
        const nameHashIn = '0a891cca599eb4d5f167ad9c62e9d108c4853425676a197ebce4029fcb160921';

        // build the data (which drives the ib creation in `getB2tFSItemIb`)
        const data: B2tFSItemData_V1 = {
            uuid: await getUUID(),
            diffTransactionId: await getUUID(),
            fsType: fsTypeIn,
            name: nameIn,
            nameHash: nameHashIn,
            saferNameVersion: saferNameVersionIn,
        };

        // build the ib off data, then parse it...
        // atow 01/2024
        // const ib = `${B2TFS_ITEM_ATOM} ${fsType} ${saferName} ${saferNameVersion} ${nameHash}`;
        const ib = getB2tFSItemIb({ data })
        let {
            atom: atomOut,
            fsType: fsTypeOut,
            saferName: saferNameOut,
            saferNameVersion: saferNameVersionOut,
            nameHash: nameHashOut
        } = parseB2tFSItemIb({ ib });

        // the in vars should match the out vars
        iReckon(maam, atomIn).asTo('atom').isGonnaBe(atomOut);
        iReckon(maam, fsTypeIn).asTo('fsType').isGonnaBe(fsTypeOut);
        iReckon(maam, saferNameIn).asTo('saferName').isGonnaBe(saferNameOut);
        iReckon(maam, saferNameVersionIn).asTo('saferNameVersion').isGonnaBe(saferNameVersionOut);
        iReckon(maam, nameHashIn).asTo('nameHash').isGonnaBe(nameHashOut);
    });

    // this is a repeat of the previous unit test, but we're isolating just
    // the parse here. maybe overkill
    await ifWe(maam, `parseB2tFSItemIb`, async () => {

        const atomIn = B2TFS_ITEM_ATOM;
        const fsTypeIn: B2tFSType = 'folder';
        const nameIn = 'hoogle';
        const saferNameIn = 'hoogle'; // same as nameIn in this case because already safe (no spaces/special chars)
        const saferNameVersionIn: B2tFSItemSaferNameVersion = 'v1';
        const nameHashIn = '0a891cca599eb4d5f167ad9c62e9d108c4853425676a197ebce4029fcb160921';

        /**
         * manually construct the test ib
         *
         * this MUST MATCH the shape in {@link getB2tFSItemIb} implementation
         *
         * atow 01/2024
         *   const ib = `${B2TFS_ITEM_ATOM} ${fsType} ${saferName} ${saferNameVersion} ${nameHash}`;
         */
        const ib = `${B2TFS_ITEM_ATOM} ${fsTypeIn} ${saferNameIn} ${saferNameVersionIn} ${nameHashIn}`;

        // parse it
        let {
            atom: atomOut,
            fsType: fsTypeOut,
            saferName: saferNameOut,
            saferNameVersion: saferNameVersionOut,
            nameHash: nameHashOut
        } = parseB2tFSItemIb({ ib });

        // the in vars should match the out vars
        iReckon(maam, atomIn).asTo('atom').isGonnaBe(atomOut);
        iReckon(maam, fsTypeIn).asTo('fsType').isGonnaBe(fsTypeOut);
        iReckon(maam, saferNameIn).asTo('saferName').isGonnaBe(saferNameOut);
        iReckon(maam, saferNameVersionIn).asTo('saferNameVersion').isGonnaBe(saferNameVersionOut);
        iReckon(maam, nameHashIn).asTo('nameHash').isGonnaBe(nameHashOut);
    });
});
