/**
 * @module b2tfs-item types (and enums)
 */

import { IbGibAddr, TransformResult } from '@ibgib/ts-gib/dist/types.mjs';
import { IbGibData_V1, IbGibRel8ns_V1, IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { B2TFS_CHILD_ITEM_DIFF_REL8N_NAME, B2TFS_CHILD_ITEM_REL8N_NAME } from './b2tfs-item-constants.mjs';
import { BINARY_REL8N_NAME } from '@ibgib/core-gib/dist/common/pic/pic-constants.mjs';
import { OrderedManifestIbGib_V1 } from '../../../../../common/ordered-manifest/ordered-manifest-types.mjs';
import { SpaceQualifiedIbGibAddr } from '../common/b2tfs-common-helper.mjs';
import { B2TFS_NAME_REGEXP } from '../common/b2tfs-common-constants.mjs';
import { CommentIbGib_V1 } from '@ibgib/core-gib/dist/common/comment/comment-types.mjs';
import { FileEncoding } from '../../rcli-types.mjs';
// and just to show where these things are
// import { CommentIbGib_V1 } from "@ibgib/core-gib/dist/common/comment/comment-types.mjs";
// import { MetaspaceService } from "@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs";

// /**
//  * example enum-like type+const that I use in ibgib. sometimes i put
//  * these in the types.mts and sometimes in the const.mts, depending
//  * on context.
//  */
// export type SomeEnum =
//     'ib' | 'gib';
// /**
//  * @see {@link SomeEnum}
//  */
// export const SomeEnum = {
//     ib: 'ib' as SomeEnum,
//     gib: 'gib' as SomeEnum,
// } satisfies { [key: string]: SomeEnum };
// /**
//  * values of {@link SomeEnum}
//  */
// export const SOME_ENUM_VALUES: SomeEnum[] = Object.values(SomeEnum);

/**
 * atow, this is just file and folder, which map from an ibgib to a file/folder
 * projection in a conventional FS paradigm.
 *
 * in the future, this can map also to other types of finer-grained generated
 * artifacts, which ultimately can then be projected onto a conventional FS
 * paradigm.
 */
export type B2tFSType = 'file' | 'folder';
/**
 * @see {@link B2tFSType}
 */
export const B2tFSType = {
    /**
     * represents a standard FS file
     */
    file: 'file' as 'file',
    /**
     * represents a standard FS folder
     */
    folder: 'folder' as 'folder',
} satisfies { [key: string]: B2tFSType };
/**
 * values of {@link B2tFSType}
 */
export const B2TFS_TYPE_VALUES: B2tFSType[] = Object.values(B2tFSType);

export type B2tFSItemSaferNameVersion = 'v1';
export const B2tFSItemSaferNameVersion = {
    v1: 'v1' as B2tFSItemSaferNameVersion,
}
export const B2TFS_ITEM_SAFER_NAME_VERSION_VALUES = Object.values(B2tFSItemSaferNameVersion);

/**
 * ibgib's intrinsic data goes here.
 *
 * @see {@link IbGib_V1.data}
 */
export interface B2tFSItemData_V1 extends IbGibData_V1 {
    /**
     * raw name of the file/folder
     *
     * NOTE: atow (1/2024) name must pass relatively restrictive regexp.
     * @see {@link B2TFS_ITEM_NAME_REGEXP}
     */
    name: string;

    /**
     * hash of the raw file/folder name
     *
     * ## dev notes
     *
     * I need this here and not just in the ib because the hash function is
     * async. so I need this already calculated when the item's ib is
     * calculated.
     *
     * ## compared to fileHash
     *
     * fileHash is the hash of the file's contents. nameHash is the hash of the
     * file's name.
     *
     * @see {@link B2tFSItemData_V1.name}
     */
    nameHash: string;

    /**
     * atow (1/2024) this version is used when calculating the ib safer name
     * field.
     *
     * @see {@link getB2tFSItemIb}
     * @see {@link getB2tFSItemSaferName}
     */
    saferNameVersion: B2tFSItemSaferNameVersion;

    // /**
    //  * for use in item ib fields.
    //  *
    //  * @see {@link B2TFS_SAFER_NAME_REGEXP}
    //  */
    // saferName: string;

    // /**
    //  * hash of the raw name, used in the ib
    //  */
    // rawNameHash: string;

    /**
     * when we project this onto a filesystem, what type is it?
     *
     * i.e. is it a folder or a file?
     *
     * ## future
     *
     * there must eventually be a way to amalgamate types, such that we can
     * have, for example, multiple granular functions or constants that get
     * composed into a single file.
     */
    fsType: B2tFSType;

    /**
     * for items where {@link fsType} are of type 'folder', these patterns
     * provide what files/folders to exclude.
     *
     * ## notes
     *
     * * expressed by means of an .ibgibignore file
     *   * changes to that file should be reflected here
     *   * changes here should be propagated to that file.
     *
     * @see {@link B2TFS_DEFAULT_ITEM_FILTER_PATTERNS}
     */
    filterPatterns?: string[];

    /**
     * If {@link fsType} is 'file', this is the filename without the extension.
     */
    fileNameSansExt?: string;

    /**
     * If {@link fsType} is 'file', this is the file extension.
     */
    fileExt?: string;

    /**
     * If {@link fsType} is 'file', this is the hash of the current file data.
     *
     * _NOTE: this is like PicData_V1.binHash, and we are using "binary ibgibs",
     * but again like I"m noting everywhere, these are not really binaries. They
     * are more like externalized payloads._
     *
     * ## compared to nameHash
     *
     * fileHash is the hash of the file's contents. nameHash is the hash of the
     * file's name.
     */
    fileHash?: string;

    /**
     * if it's a file, we'll **try** to preserve the encoding.
     */
    fileEncoding?: FileEncoding | undefined;

    /**
     * identifier that helps to group together items into a transaction.
     *
     * ## intent
     *
     * any item is its own timeline. unlike a vcs repo, we cannot assume the
     * root branch is the only place that an item will exist. the item exists in
     * and of itself.
     *
     * so this transaction id helps group together items that were created as a
     * result of a single, "atomic" diff application. i put "atomic" in quotes
     * because i have no explicit code that ensures atomicity. hey i gotta leave
     * something for the next generation to do.
     *
     * so i don't actually plan on using this atow (01/2024) in this
     * implementation, but i think it will probably be useful to have in the
     * future. buuut it may end up being yagni (or the overall structure of
     * course could be less than ideal),
     */
    diffTransactionId: string;
}

/**
 * rel8ns (named edges/links in DAG) go here.
 *
 * @see {@link IbGib_V1.rel8ns}
 */
export interface B2tFSItemRel8ns_V1 extends IbGibRel8ns_V1 {
    /**
     * @optional relationships to child b2tfs items.
     *
     * this is optional because it may be an empty folder or this item may not
     * even be of {@link B2tFSItemData_V1.fsType} 'folder' (might be a file
     * b2tfs item ibgib).
     */
    [B2TFS_CHILD_ITEM_REL8N_NAME]?: IbGibAddr[];
    /**
     * If {@link B2tFSItemData_V1.fsType} is 'file', then this is the rel8nName
     * used to link to the "binary" ibgib (`BinIbGib_V1`).
     *
     * These are not "really" binaries. This is the link to the file.
     *
     * ALL versions of files are here. the most recent version should be last.
     */
    [BINARY_REL8N_NAME]?: IbGibAddr[];
}

/**
 * this is the ibgib object itself.
 *
 * If this is a plain ibgib data only object, this acts as a dto. We may also
 * want to generate a witness ibgib, which is slightly different, for ibgibs
 * that will have behavior (i.e. methods).
 *
 * @see {@link B2tFSItemData_V1}
 * @see {@link B2tFSItemRel8ns_V1}
 */
export interface B2tFSItemIbGib_V1 extends IbGib_V1<B2tFSItemData_V1, B2tFSItemRel8ns_V1> {
}

/**
 * info for a diff, including the actual ibgibs to apply.
 *
 * ## note on order of application
 *
 * **All {@link childDiffs}' `transformResults` must be applied (persisted and
 * registered) before the parent {@link transformResults}, because those
 * children are prerequisites to this transform result.**
 *
 * For example, if a child is mutated, then it will have a new address. we
 * literally can't evolve (via mut8/rel8 transforms) this parent until we
 * have that new child address.
 *
 * for now, we will not make this transactional, so there is the chance of
 * child diffs being applied and the parent diff failing. but this is a very
 * small chance since we are first persisting and then registering the
 * `transformResults` only after they are persisted.
 */
export interface B2tFSItemDiffInfo {
    /**
     * is it a file or folder.
     *
     * in the future, there may be additional chunked types. for example, if we
     * semantically chunk a file into e.g. functions.
     */
    fsType: B2tFSType;
    /**
     * corresponds to the fs file/folder raw name.
     */
    fsName: string;
    /**
     * when was the diff made.
     */
    timestampInTicks: string;
    /**
     * identifier that helps to group together items into a transaction.
     *
     * ## intent
     *
     * any item is its own timeline. unlike a vcs repo, we cannot assume the
     * root branch is the only place that an item will exist. the item exists in
     * and of itself.
     *
     * so this transaction id helps group together items that were created as a
     * result of a single, "atomic" diff application. i put "atomic" in quotes
     * because i have no explicit code that ensures atomicity. hey i gotta leave
     * something for the next generation to do.
     *
     * so i don't actually plan on using this atow (01/2024) in this
     * implementation, but i think it will probably be useful to have in the
     * future. buuut it may end up being yagni (or the overall structure of
     * course could be less than ideal),
     */
    diffTransactionId: string;
    /**
     * space-qualified address of the starting point (punctiliar ibgib in a timeline).
     *
     * usually, this will be the most recent/latest/HEAD of the file/folder
     * item's timeline. (in code atow 1/2024, this is also `existingItem`).
     *
     * more concretely, if truthy, this is the addr of the src that we're
     * diffing against. (e.g. `mut8(src, options)` or `rel8(src, options)`)
     *
     * if falsy, then there is no src ibgib, and consequently the diff should be
     * entirely made of additions.
     *
     * ## notes
     *
     * pedantically speaking, "no src ibgib" actually means the ROOT ib^gib
     * ({ib: "ib", gib: "gib"}).  this is the singularity of zero and infinity
     * (zero's dual).
     */
    existingItemSQAddr?: SpaceQualifiedIbGibAddr;

    /**
     * space-qualified address of the {@link B2tFSItemIbGib_V1} after the diff
     * would be applied (if the diff was not deferred).
     *
     * atow (01/2024) this should be space-qualified addr of the
     * `transformResults` last `newIbGib` (`transformResults.at(-1).newIbGib`).
     */
    newItemSQAddr: SpaceQualifiedIbGibAddr;

    /**
     * instead of doing a "version" property, let's try this.
     *
     * _NOTE: once we're programming in native ibgib, then this kind of
     * versioning happens automatically._
     */
    diffInterfaceName: 'B2tFSItemDiffInfo_Folder' | 'B2tFSItemDiffInfo_File' | string;

    /**
     * text of {@link applyCommentIbGib} (via `data.text`) associated with the
     * {@link newItem}.
     */
    applyCommentText?: string;

    /**
     * if the item diff has children, they go here.
     *
     * there should be no interdependencies among any of these diffs. by this i
     * mean that there should be no shared timelines, other than the
     * parent-child relationship like a folder & child file/folder. otherwise,
     * if these diffs are deferred/not immediately applied, then the shared
     * timeline would branch with the current implementation (atow 12/2023)
     * because both actions would mut8/rel8 against the same punctiliar ibgib
     * source.
     *
     * when applying this diffs, the children are/must be applied first and then
     * the parent ibgibs.
     *
     * ## notes
     *
     * at first (atow 12/2023), this will only be populated when this item diff
     * is a folder and the children are file/folder item diffs.
     *
     * in the future, once we start chunking individual files, this can also
     * pertain to those.
     */
    childDiffs?: B2tFSItemDiffInfo[];

    /**
     * if there is an existing item, this is it.
     */
    existingItem?: B2tFSItemIbGib_V1 | undefined;

    /**
     * the new item created from the diff.
     *
     * should be the same as `transformResults.at(-1).newIbGib`
     *
     * NOTE: this is optional in case we want a version of this info object that
     * does not contain references to objects.
     */
    newItem?: B2tFSItemIbGib_V1;

    /**
     * this is the array of transform results for this B2tFSItem only.
     *
     * NOTE: this is optional in case we want a version of this info object that
     * does not contain references to objects.
     */
    transformResults?: TransformResult<IbGib_V1>[];

    /**
     * this should be rel8d to the {@link newItem} via the 'comment' rel8n name.
     */
    applyCommentIbGib?: CommentIbGib_V1;
}

export interface B2tFSItemDiffInfo_Folder extends B2tFSItemDiffInfo {
    fsType: 'folder';
    diffInterfaceName: 'B2tFSItemDiff_Folder';
    childNamesAdded: string[]; // optional?
    childAddrsAdded: string[]; // optional?
    childNamesRemoved: string[]; // optional?
    childAddrsRemoved: string[]; // optional?
    childNamesModified: string[]; // optional?
    /**
     * map of old addrs to new addrs when a child item (file or folder) changes.
     */
    childAddrsModified: { [prevAddr: string]: string }; // optional? yagni?
    // /**
    //  * for future when we implement a "fs-move/rename" command that can track a
    //  * rename.  shouldn't be required though, just would help with keeping track
    //  * if the user wants to take the added trouble.
    //  *
    //  * also in the farther future, an ibgib-native os "filesystem" would do this
    //  * automatically and the childNamesAdded/Removed hack would not be needed.
    //  */
    // childAddrsMoved: string[];
    /**
     * the filterPatterns used in this folder to produce this diff. this
     * includes required patterns, as well as default patterns (if applicable)
     * and patterns found in files (e.g. .ibgibignore).
     */
    filterPatterns: string[];
}

export interface B2tFSItemDiffInfo_File extends B2tFSItemDiffInfo {
    fsType: 'file';
    diffInterfaceName: 'B2tFSItemDiff_File';
    /**
     * can get to this via existingItem's binIbGib. not sure if that should be a
     * property of this diff...
     */
    oldData?: string;
    newData: string;
}

// export interface B2tFSItemDiffData_V1 extends IbGibData_V1 {
// export interface B2tFSItemDiffData_V1<TDiffInfo extends B2tFSItemDiffInfo_Folder | B2tFSItemDiffInfo_File>
//     extends IbGibData_V1 {
//     info: TDiffInfo;
// }

// export interface B2tFSItemDiffRel8ns_V1 extends IbGibRel8ns_V1 {
//     [B2TFS_CHILD_ITEM_DIFF_REL8N_NAME]: IbGibAddr[];
// }

// export interface B2tFSItemDiffIbGib_V1<TDiffInfo extends B2tFSItemDiffInfo_Folder | B2tFSItemDiffInfo_File>
//     extends IbGib_V1<B2tFSItemDiffData_V1<TDiffInfo>, B2tFSItemDiffRel8ns_V1> {
// }
