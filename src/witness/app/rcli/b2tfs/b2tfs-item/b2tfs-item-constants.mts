/**
 * @module b2tfs-item constants
 *
 * constants are in this file!
 */

// /**
//  * example enum-like type+const that I use in ibgib. sometimes i put
//  * these in the types.mts and sometimes in the const.mts, depending
//  * on context.
//  */
// export type SomeEnum =
//     'ib' | 'gib';
// export const SomeEnum = {
//     ib: 'ib' as SomeEnum,
//     gib: 'gib' as SomeEnum,
// } satisfies { [key: string]: SomeEnum };
// export const SOME_TYPE_VALUES: SomeEnum[] = Object.values(SomeEnum);

/**
 * atom used in ibs
 */
export const B2TFS_ITEM_ATOM = 'b2tfs_item';

/**
 * these patterns are used if no filter patterns are explicitly defined.
 *
 * literal matches are wrapped in quotes
 *
 * include
 */
export const B2TFS_DEFAULT_ITEM_FILTER_PATTERNS: string[] = [
    // literal match is wrapped in quotes
    '.ibgib',
    'node_modules',
];

/**
 * filenames that provide include/exclude filter patterns.
 */
export const B2TFS_ITEM_FILTER_PATTERN_FILENAMES: string[] = [
    '.ibgibignore',
];

/**
 * rel8n name used when referencing filesystem child (file/folder) ibgibs in a
 * b2tfs item of type 'folder'.
 *
 * This includes the child items in a b2tfs root folder ibgib, as in this
 * regard, a branch ibgib is the same as other folder item ibgibs.
 *
 * ## notes
 *
 * * the value of this (atow 12/2023) has the word "child" instead of just
 *   "item" or similar because a hierarchical filesystem necessarily implies a
 *   parent/child relationship.
 *
 * @see {@link B2tFSItemRel8ns_V1}
 */
export const B2TFS_CHILD_ITEM_REL8N_NAME = 'b2tfs_child';

/**
 * in a diff dependency graph ({@link B2tFSItemDiffIbGib_V1}), this is the rel8n
 * name from a parent folder item diff to a child file/folder item diff.
 */
export const B2TFS_CHILD_ITEM_DIFF_REL8N_NAME = 'b2tfs_child_diff';

/**
 * known extensions where we expect utf8 encoding.
 *
 * atow (01/2024) just off the top of my head.
 *
 * atow (01/2024) only really for ibgib projects which use typescript + html5,
 * text files...basics.
 */
export const B2TFS_EXPLICIT_UTF8_EXTENSIONS: string[] = [
    'txt', 'md',
    'ts', 'mts', 'js', 'mjs',
    'html', 'css', 'scss', 'sass',
];

/**
 * known extensions where we expect binary/non-utf8 encoding.
 *
 * atow (01/2024) just off the top of my head.
 */
export const B2TFS_POSSIBLY_BINARY_EXTENSIONS: string[] = [
    'jpg', 'jpeg', 'png', 'gif', 'tiff',
];
