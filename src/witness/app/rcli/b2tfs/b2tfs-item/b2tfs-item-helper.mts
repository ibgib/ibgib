/**
 * @module b2tfs-item helper/util/etc. functions
 *
 * this is where you will find helper functions like those that generate
 * and parse ibs for b2tfs-item.
 */

import * as pathUtils from 'path';
import { Dirent, statSync } from 'node:fs';
import { readFile, readdir, mkdir, writeFile, } from 'node:fs/promises';
// import * as readline from 'node:readline/promises';
// import { stdin, stdout } from 'node:process'; // decide if use this or not

import {
    extractErrorMsg, delay, getSaferSubstring, getTimestampInTicks,
    getUUID, pretty, hash, getTimestamp,
} from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { CLASSNAME_REGEXP, UUID_REGEXP, } from '@ibgib/helper-gib/dist/constants.mjs';
import { Ib, IbGibAddr, TransformResult, } from '@ibgib/ts-gib/dist/types.mjs';
import { validateIbGibIntrinsically } from '@ibgib/ts-gib/dist/V1/validate-helper.mjs';
import { getGib } from '@ibgib/ts-gib/dist/V1/transforms/transform-helper.mjs';
import { getIbAndGib, getIbGibAddr } from '@ibgib/ts-gib/dist/helper.mjs';
import { Factory_V1 } from '@ibgib/ts-gib/dist/V1/factory.mjs';
import { mut8 } from '@ibgib/ts-gib/dist/V1/transforms/mut8.mjs';
import { rel8 } from '@ibgib/ts-gib/dist/V1/transforms/rel8.mjs';
import { MetaspaceService } from '@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs';
import { getBinIb } from '@ibgib/core-gib/dist/common/other/ibgib-helper.mjs';
import { BinIbGib_V1 } from '@ibgib/core-gib/dist/common/bin/bin-types.mjs';
import { BINARY_REL8N_NAME } from '@ibgib/core-gib/dist/common/pic/pic-constants.mjs';
import { IbGibSpaceAny } from '@ibgib/core-gib/dist/witness/space/space-base-v1.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../../../ibgib-constants.mjs';
import {
    B2tFSItemData_V1, B2tFSItemRel8ns_V1, B2tFSItemIbGib_V1,
    B2TFS_TYPE_VALUES, B2tFSType,
    B2tFSItemDiffInfo, B2tFSItemDiffInfo_File, B2tFSItemDiffInfo_Folder,
    B2tFSItemSaferNameVersion, B2TFS_ITEM_SAFER_NAME_VERSION_VALUES,
} from './b2tfs-item-types.mjs';
import {
    B2TFS_POSSIBLY_BINARY_EXTENSIONS, B2TFS_DEFAULT_ITEM_FILTER_PATTERNS,
    B2TFS_ITEM_ATOM, B2TFS_ITEM_FILTER_PATTERN_FILENAMES,
    B2TFS_CHILD_ITEM_REL8N_NAME,
} from './b2tfs-item-constants.mjs';
import { pathIsAsExpected, } from '../../rcli-helper.mjs';
import { B2TFS_MAX_SAFER_NAME_LENGTH, B2TFS_NAME_REGEXP, B2TFS_SAFER_NAME_REGEXP, } from '../common/b2tfs-common-constants.mjs';
import { B2tFSBranchIbGib_V1, } from '../b2tfs-branch/b2tfs-branch-types.mjs';
import { SpaceQualifiedIbGibAddr, getSpaceQualifiedIbGibAddr, isPathFiltered, parseSpaceQualifiedIbGibAddr } from '../common/b2tfs-common-helper.mjs';
import { CommentIbGib_V1 } from '@ibgib/core-gib/dist/common/comment/comment-types.mjs';
import { Encoding } from '@ibgib/core-gib/dist/witness/space/filesystem-space/filesystem-types.mjs';
import { FileEncoding } from '../../rcli-types.mjs';

/**
 * for verbose logging
 */
const logalot = GLOBAL_LOG_A_LOT;

export function validateCommonB2tFSItemData({
    data,
}: {
    data?: B2tFSItemData_V1,
}): string[] {
    const lc = `[${validateCommonB2tFSItemData.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting...`); }
        if (!data) { throw new Error(`data required (E: ac515a7c9bc50b813bb728b6977261d7)`); }
        const errors: string[] = [];
        const { name, nameHash, uuid, classname, fsType } = data;

        if (name) {
            if (!name.match(B2TFS_NAME_REGEXP)) {
                errors.push(`name must match regexp: ${B2TFS_NAME_REGEXP} (E: c92342553a8e6e26a109b3fbcde24947)`);
            }
        } else {
            errors.push(`name required. (E: 6ca7dbad8a2c4e6f804793d1aa4c8201)`);
        }

        if (nameHash) {
            if (!nameHash.match(UUID_REGEXP)) {
                errors.push(`nameHash must match regexp: ${UUID_REGEXP} (E: e7f8f96147f546949f2cb59afe3c3d71)`);
            }
        } else {
            errors.push(`nameHash required. (E: a745323851634de188beaebdcb85cbb9)`);
        }

        if (uuid) {
            if (!uuid.match(UUID_REGEXP)) {
                errors.push(`uuid must match regexp: ${UUID_REGEXP} (E: 518d4d921205861860103caccc69b54e)`);
            }
        } else {
            errors.push(`uuid required. (E: c1f76d21c9f54a34bf3c534a12a1acf7)`);
        }

        if (classname) {
            if (!classname.match(CLASSNAME_REGEXP)) {
                errors.push(`classname must match regexp: ${CLASSNAME_REGEXP} (E: 220949ca192c5d563e9601f635891424)`);
            }
        }

        if (fsType) {
            if (!B2TFS_TYPE_VALUES.includes(fsType)) {
                errors.push(`invalid fsType (${fsType}). Must be one of ${B2TFS_TYPE_VALUES.join(', ')}. (E: b940fce2089b40a6bda6ae5a1523ac76)`);
            }
        } else {
            errors.push(`fsType required (E: e9807f898a8a4e63b9ad5a15f91318e6)`);
        }

        return errors;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * performs individual validation of the ib, gib, data and rel8ns, as well as
 * additional holistic ibGib validation (e.g. does the ib match generated ib from data).
 *
 * @returns errors array if any found, else undefined
 */
export async function validateCommonB2tFSItemIbGib({
    ibGib,
}: {
    ibGib: B2tFSItemIbGib_V1,
}): Promise<string[] | undefined> {
    const lc = `[${validateCommonB2tFSItemIbGib.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: ab66b36cbd279291347627d08c6de917)`); }
        const intrinsicErrors: string[] = await validateIbGibIntrinsically({ ibGib: ibGib }) ?? [];

        if (!ibGib.data) { throw new Error(`B2tFSItem ibGib.data required (E: fd61b60771f8933b13c1b06ca0d6ddf3)`); }
        const dataErrors = validateCommonB2tFSItemData({ data: ibGib.data });

        const ibErrors: string[] = [];
        // only check ib errors if there are no data errors, because the ib is
        // based on the data
        if (dataErrors.length === 0) {
            let fsType: B2tFSType;
            let saferName: string;
            let nameHash: string;
            try {
                const parsedIb = parseB2tFSItemIb({ ib: ibGib.ib }); // throws on invalid
                fsType = parsedIb.fsType;
                saferName = parsedIb.saferName;
                nameHash = parsedIb.nameHash;

                const calculatedNameHash = await hash({ s: ibGib.data.name });
                if (calculatedNameHash !== nameHash) {
                    throw new Error(`calculatedNameHash !== nameHash. so we rehashed the ibGib.data.name and came up with a different value than what is stored in ibGib.data.nameHash (E: 648bc2b6dd76210a473a8e423ce29524)`);
                }
            } catch (error) {
                ibErrors.push(error);
            }
        }
        // double-check that the ib matches the data
        const calculatedIb = getB2tFSItemIb({ data: ibGib.data });
        if (calculatedIb !== ibGib.ib) {
            ibErrors.push(`calculated ib (${calculatedIb}) does not match ibGib.ib (${ibGib.ib}). (E: 7de115ba5e014100811fe67a1fd169f5)`);
        }

        const result = [...(intrinsicErrors ?? []), ...(ibErrors ?? []), ...(dataErrors ?? [])];
        if (result.length > 0) {
            return result;
        } else {
            return undefined;
        }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * DRY deterministic function to get a safer name from a given file/folder name.
 *
 * atow (01/2024) - alphanumerics, no spaces, hyphens, dots
 *     const B2TFS_SAFER_NAME_REGEXP = /^[a-zA-Z0-9_\-.]{1,16}$/;
 *     const B2TFS_MAX_SAFER_NAME_LENGTH = 16;
 *
 * ## intent
 *
 * i don't want to have to hash every filename and compare file/folder name
 * hashes (i think). so this should be quicker to compare safer names for
 * file/folder names that have, e.g., spaces in them.
 *
 * really, if your vcs files'/folders' names differs by only spaces (which get
 * replaced by underscores atow 01/2024), then you're playing with fire.
 *
 * @see {@link getB2tFSItemIb}
 * @see {@link B2TFS_MAX_SAFER_NAME_LENGTH}
 */
export function getB2tFSItemSaferName({
    name,
    version,
}: {
    name: string,
    /**
     * case-INSENSITIVE
     */
    version: B2tFSItemSaferNameVersion,
}): string {
    const lc = `[${getB2tFSItemSaferName.name}]`;
    // DON'T CHANGE THIS EVER!!!! (only add cases)
    switch (version.toLowerCase()) {
        case 'v1':
            return getSaferSubstring({
                text: name,
                keepLiterals: ['.', '-'],
                length: B2TFS_MAX_SAFER_NAME_LENGTH
            });
        default:
            throw new Error(`(UNEXPECTED) case-insensitive version (${version}) is unknown? expected one of ${B2TFS_ITEM_SAFER_NAME_VERSION_VALUES}. (E: 60c5391a6f8110809ffc5da5f1d4b824)`);
    }
}

/**
 * Current schema (01/2024) is `${B2TFS_ITEM_ATOM} ${fsType} ${saferName} ${saferNameVersion} ${nameHash}`
 *
 * NOTE this is space-delimited
 */
export function getB2tFSItemIb({
    data,
}: {
    data: B2tFSItemData_V1,
}): Ib {
    const lc = `[${getB2tFSItemIb.name}]`;
    try {
        const validationErrors = validateCommonB2tFSItemData({ data }); // validates name, uuid
        if (validationErrors.length > 0) { throw new Error(`invalid B2tFSItem data: ${validationErrors} (E: ba850094b1c20b886478ac76fce62d10)`); }

        const { fsType, name, nameHash, saferNameVersion } = data;

        const saferName = getB2tFSItemSaferName({
            name,
            version: saferNameVersion
        });

        return `${B2TFS_ITEM_ATOM} ${fsType} ${saferName} ${saferNameVersion} ${nameHash}`;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    }
}

/**
 * Current schema (01/2024):
 *
 *     `${B2TFS_ITEM_ATOM} ${fsType} ${saferName} ${saferNameVersion} ${nameHash}`
 *
 * NOTE this is space-delimited
 */
export function parseB2tFSItemIb({
    ib,
}: {
    ib: Ib,
}): {
    /**
     * atom identifies this ib/ibGib as a b2tfs item
     */
    atom: string,
    /**
     * file or folder
     */
    fsType: B2tFSType,
    /**
     * safer name that ensures space-delimited ib isn't screwed up with a
     * file/folder name that itself contains a space.
     */
    saferName: string,
    /**
     * version of algorithm used in getting `saferName`
     */
    saferNameVersion: B2tFSItemSaferNameVersion,
    /**
     * raw name's hash
     */
    nameHash: string,
} {
    const lc = `[${parseB2tFSItemIb.name}]`;
    try {
        if (!ib) { throw new Error(`B2tFSItem ib required (E: db7373063ae96addae3e66119a21c423)`); }

        // destructure space-delimited ib
        const [atom, fsType, saferName, saferNameVersion, nameHash] = ib.split(' ');

        // validate (maybe this should be elsewhere but hmm)
        if (!fsType) { throw new Error(`invalid b2tfs item ib. fsType falsy. (E: 85a2c2dcedd1e1308b60480a9ce53424)`); }
        if (!B2TFS_TYPE_VALUES.includes(fsType as B2tFSType)) { throw new Error(`invalid fsType (${fsType}). must be one of ${B2TFS_TYPE_VALUES} (E: 5fff9f2ab39e437dd834bab6565f5d23)`); }
        if (saferName) {
            if (!saferName.match(B2TFS_SAFER_NAME_REGEXP)) {
                throw new Error(`saferName does not match B2TFS_SAFER_NAME_REGEXP (${B2TFS_SAFER_NAME_REGEXP.source}) (E: 3223d2dd272dc348b819f21c50020d24)`);
            }
        } else {
            throw new Error(`invalid b2tfs item ib. saferName falsy. (E: 911fa5cdd1d2dccb0208f92287950f24)`);
        }
        if (nameHash) {
            if (!nameHash.match(UUID_REGEXP)) {
                throw new Error(`nameHash does not match UUID_REGEXP (${UUID_REGEXP.source}) (E: 644aaf9285b049d6a235a9f56d553695)`);
            }
        } else {
            throw new Error(`invalid b2tfs item ib. nameHash falsy. (E: e8405584f0e9422197bab6ee97e32604)`);
        }

        return {
            atom,
            fsType: fsType as B2tFSType,
            saferName,
            saferNameVersion: saferNameVersion === `${undefined}` ? undefined : (saferNameVersion as any),
            nameHash,
        }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    }
}

/**
 * creates the item then calculates the diffs for that item.
 *
 * if the {@link existingItem} is given, then the item will use that as the
 * source ibgib (when mut8/rel8). if it's falsy, then the diff will be composed
 * entirely of additions.
 *
 * @returns the new B2tFSItem ibgib and the diff(s) between that item and {@link existingItem}.
 */
export async function getB2tFSItemDiff({
    existingItem,
    inputPath,
    filterPatterns = B2TFS_DEFAULT_ITEM_FILTER_PATTERNS,
    deferred,
    applyCommentIbGib,
    diffTransactionId,
    metaspace,
    space,
}: {
    /**
     * if undefined, this will create a new item ibgib and calculate the
     * addition B2tFSDiff's using the given {@link inputPath}.
     *
     * if truthy, this will create an item ibGib with mut8/rel8 transforms with
     * this `existingItem` as the src. the diffs will be calculated against this
     * `existingItem`'s state.
     */
    existingItem: B2tFSItemIbGib_V1 | undefined,
    /**
     * src path that will map to the item.
     *
     * if the inputPath is a folder, then we will recursively iterate to add
     * items.
     */
    inputPath: string,
    /**
     * patterns to include/exclude children files/folders.
     *
     * * this will be the base patterns definition.
     * * any child folder that contains filter pattern files (e.g. .ibgibignore
     *   atow 12/2023) will be added to this pattern.
     * * successive patterns are last one wins.
     *   * e.g. if you have an exclude early in the array/file that excludes
     *     ".tmp" files and a later include pattern that explicitly includes
     *     ".tmp" files then the later one wins and ".tmp" files will be
     *     included.
     *
     * @see {@link B2tFSItemData_V1.filterPatterns}
     */
    filterPatterns?: string[],
    /**
     * if true, will not apply the diffs as we go.
     *
     * this is to be used in just a `--b2tfs-diff` command or one with
     * `--dry-run`-like flag.
     */
    deferred: boolean,
    /**
     * comment to associate with the diff if it is not deferred.
     *
     * if deferred is false, this must be defined or throws.
     */
    applyCommentIbGib: CommentIbGib_V1 | undefined,
    /**
     * transaction id of the entire diff (UUID). this should be set by the
     * consumer and then passed on to all recursive calls for child diff.
     */
    diffTransactionId: string,
    /**
     * @see {@link MetaspaceService}
     */
    metaspace: MetaspaceService,
    space: IbGibSpaceAny,
}): Promise<B2tFSItemDiffInfo | undefined> {
    const lc = `[${getB2tFSItemDiff.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 696c55227ad29ab693b6d9a6d0f06323)`); }

        // debugger; // getB2tFSItemDiff starting...

        if (!deferred && !applyCommentIbGib) { throw new Error(`(UNEXPECTED) !deferred and !applyCommentIbGib? comment ibgib required when applying the diff. (E: ca4f239c1fd83e68f91c01670457d724)`); }

        const pathExists = pathIsAsExpected({
            relOrAbsPath: inputPath,
            expectType: 'exists',
            throwIfNotExpected: false,
            warnIfNotExpected: false,
        });

        if (!pathExists) {
            if (logalot) { console.log(`${lc} inputPath (${inputPath}) does not exist. So there is not B2tFSItem to get. Returning undefined. (I: 1efc9ffd800909730f711c52c7846c23)`); }
            return undefined; /* <<<< returns early */
        }

        // if undefined, set - **BUT NOT IF EMPTY ARRAY**
        filterPatterns ??= B2TFS_DEFAULT_ITEM_FILTER_PATTERNS;

        if (isPathFiltered({ inputPath, filterPatterns })) {
            if (logalot) { console.log(`${lc} inputPath is filtered. returning undefined. (I: 6fe108753c5d068ff2067d619d4b3b23)`); }
            return undefined; /* <<<< returns early */
        }

        const stat = statSync(inputPath, { throwIfNoEntry: true }); // guaranteed to exist by preceding code
        const resDiff = stat.isFile() ?
            await getB2tFSItemDiff_File({
                existingItem, inputPath, diffTransactionId,
                deferred, applyCommentIbGib, metaspace, space
            }) :
            await getB2tFSItemDiff_Folder({
                existingItem, inputPath, filterPatterns, diffTransactionId,
                deferred, applyCommentIbGib, metaspace, space
            });
        if (logalot) {
            console.log(`${lc} console.dir(resDiff)... (I: fd8d71102c152336df2d009faaeede23)`);
            console.dir(resDiff);
            console.log(`${lc} (new ibgib if diff applied) console.dir(resDiff?.transformResults?.at(-1)?.newIbGib)... (I: 39f57622718249ccb06c45d61bbbee97)`);
            console.dir(resDiff?.transformResults?.at(-1)?.newIbGib);
        }

        if (resDiff && !deferred) {
            await applyDiff({ metaspace, diff: resDiff, space });
        }

        return resDiff;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * @internal subfunction for paths that correspond to files
 *
 * **NOT really binary. I'm just reusing that plumbing.**
 *
 * ## dev notes
 *
 * atow (1/2024) this isn't a pretty implementation. There is some shared code
 * when there is an existingItem and when there is not an existingItem. But for
 * now it'll have to do.
 */
async function getB2tFSItemDiff_File({
    existingItem,
    inputPath,
    deferred,
    applyCommentIbGib,
    diffTransactionId,
    metaspace,
    space,
}: {
    /**
     * if this is truthy, then we will use this item as our src and mut8/rel8
     * changes if any when diffing against current filesystem state.
     */
    existingItem: B2tFSItemIbGib_V1 | undefined,
    /**
     * at this point, this path should be guaranteed to existing, be a file, and
     * already have passed any exclusion filters.
     */
    inputPath: string,
    /**
     * @see {@link getB2tFSItemDiff} param.
     */
    diffTransactionId: string,
    /**
     * @see {@link getB2tFSItemDiff} param.
     */
    deferred: boolean,
    /**
     * comment to associate with the diff if it is not deferred.
     *
     * if deferred is false, this must be defined or throws.
     */
    applyCommentIbGib: CommentIbGib_V1 | undefined,
    /**
     * @see {@link MetaspaceService}
     */
    metaspace: MetaspaceService,
    /**
     * the space with our b2tfs ibgibs (e.g. {@link existingItem})
     */
    space: IbGibSpaceAny,
}): Promise<B2tFSItemDiffInfo_File | undefined> {
    const lc = `[${getB2tFSItemDiff_File.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 8dd476a08f428c6fdd742fe557504723)`); }

        // at this point, the path is guaranteed to exist, be a file, and passed
        // any exclude filters.

        // debugger; // getB2tFSItemDiff_File

        // compare the existing data with the calculated data from inputPath
        // const inputPathFileData: string = await readFile(inputPath, { encoding: 'utf8' });
        let {
            dataString: inputPathFileData,
            detectedEncoding,
            dataBuffer,
        } = await getFileDataAndEncoding({ inputPath });

        if (!inputPathFileData) {
            debugger; // just want to see this first time to see if it works
            if (!dataBuffer) { throw new Error(`(UNEXPECTED) both dataString and dataBuffer falsy? (E: 2bf9fd29adf73de21880107440d87924)`); }
            inputPathFileData = dataBuffer.toString('base64');
        }

        const inputPathFileHash = await hash({ s: inputPathFileData });

        let existingBinIbGib: BinIbGib_V1 | undefined = undefined;
        let existingFileData: string | undefined = undefined;
        const parsedInputPath = pathUtils.parse(inputPath);
        const rawFilename = parsedInputPath.base;
        const fileNameSansExt = parsedInputPath.name;
        const fileExt = parsedInputPath.ext.slice(1); // remove the dot

        if (logalot) { console.log(`${lc} read inputPathFileData.length: ${inputPathFileData.length} (I: 9650edb4989497fd483004ab429d3c23)`); }
        if (logalot) { console.log(`${lc} fileExt: "${fileExt}", inputPathFileHash: ${inputPathFileHash} (I: e7444cc7c2d3467f8bf1f55d07c85421)`); }

        // if not different, return undefined early
        if (existingItem && inputPathFileHash === existingItem.data!.fileHash) {
            if (logalot) { console.log(`${lc} inputPathFileHash === existingItem.data.fileHash, so no changes in this file. returning undefined early. (I: 66c531c920b2b1664f8ee99ceca51c23)`); }
            return undefined; /* <<<< returns early */
        }

        /**
         * if existingItem, we will set this
         */
        let existingItemSQAddr: string | undefined = !!existingItem ?
            getSpaceQualifiedIbGibAddr({ space, ibGib: existingItem }) :
            undefined;

        // #region throw if file is a binary tmp hack

        // right now just check to see if we're doing **real** binaries (and
        // not just reusing BinIbGib plumbing)
        if (B2TFS_POSSIBLY_BINARY_EXTENSIONS.includes(parsedInputPath.ext)) {
            throw new Error(`possibly binary extensions not implemented yet (E: b8a51a25f3e7f7d0afafc5d6e30a0423)`);
        } else {
            if (logalot) { console.log(`${lc} parsedPath.ext (${parsedInputPath.ext}) is not a registered binary extension. (I: 6671516a7ec8ae5649fb2066cd540523)`); }
        }

        // #endregion throw if file is a binary tmp hack

        /**
         * load the path's utf8 data and construct the bin ibgib off that
         *
         * i have this as a closure here because i don't want to do this for
         * every single file as it may be semi-expensive calculating the
         * binIbGib.gib. first we must check to see if the existingItem's file
         * hash is different than the current file's hash.
         *
         * NOTE: I'm reusing plumbing here. remember that these are not really
         * binaries. sigh. I've copied this somewhat from `createBinIbGib`
         * @returns [binIbGib, binAddr]
         */
        const binIb = getBinIb({ binHash: inputPathFileHash, binExt: fileExt });
        /**
         * this contains the actual data of the file.
         */
        const binIbGib: BinIbGib_V1 = { ib: binIb, data: inputPathFileData };
        // no tjp for binIbGibs as a rule/design decision. the timeline is the
        // ibgib that is associated to the binIbGib via the 'bin' rel8n to the
        // binAddr
        binIbGib.gib = await getGib({ ibGib: binIbGib, hasTjp: false });
        const binAddr = getIbGibAddr({ ibGib: binIbGib });
        if (logalot) { console.log(`${lc} binAddr: ${binAddr} (I: e4218520a16d63fe7635a8da653efa23)`); }

        /**
         * these are the ibgibs (dna, intermediate B2tFSItemIbGibs, BinIbGib,
         * and final new B2tFSItemIbGib) that are created if the resulting diff
         * are applied.
         *
         * note that these transform results are expected to be ordered, and
         * that the final new B2tFSItemIbGib should be the last item in the array
         * (`.at(-1).newIbGib`)
         */
        const transformResults: TransformResult<B2tFSItemIbGib_V1>[] = [];

        if (existingItem) {
            // #region existingItem truthy, get the transformResults that append to timeline

            if (!existingItem.data) { throw new Error(`(UNEXPECTED) existingItem.data falsy? (E: 5672a792ad7a41c296d539f34970a423)`); }
            if (existingItem.data.fsType !== 'file') {
                throw new Error(`existingItem.data.fsType !== 'file'? did you delete a file and then create a folder at the same location with the same name? this edge case has not been implemented yet. (E: 0276338b3fe5435427cc237cc9c48f23)`);
            }
            if (!existingItem.data.fileHash) { throw new Error(`(UNEXPECTED) existingItem.data.fileHash falsy? this is already known to be a file and so the fileHash should be truthy. (E: e54a0c9eb3e2e2554b3e6269853bff23)`); }

            // they are different, create the next punctiliar ibGibs in the
            // item ibgib's timeline and push those to transformResults
            if (logalot) { console.log(`${lc} inputPathFileHash (${inputPathFileHash}) !== existingItem.data.fileHash (${existingItem.data.fileHash}) so the file has changed. proceed to create new punctiliar ibgibs in B2tFSItemIbGib timeline and create the B2tFSItemDiff. (I: b6dc2282c9794c923f8a2768b092fd23)`); }

            // get the current bin ibGib & data (if existingItem)
            existingBinIbGib =
                await getBinIbGibFromB2tFSItemIbGib({ metaspace, item_File: existingItem, space });
            existingFileData = existingBinIbGib?.data ?? undefined;
            if (existingFileData === undefined) {
                throw new Error(`(UNEXPECTED) existingFileData === undefined? even if empty string shouldn't get here. (E: 8373b6b6566cc2b44d19e872de1cfb23)`);
            }

            /**
             * mut8 the intrinsic data to reflect the file's new hash (which
             * is guaranteed to be different at this point)
             */
            const resMut8_diffIdAndupdateFileHash = await mut8({
                src: existingItem,
                dataToAddOrPatch: {
                    diffTransactionId,
                    fileHash: inputPathFileHash,
                } satisfies Partial<B2tFSItemData_V1>,
                dna: true,
                nCounter: true,
            }) as TransformResult<B2tFSItemIbGib_V1>;

            let rel8nsToAddByAddr: Partial<B2tFSItemRel8ns_V1> = {
                bin: [binAddr],
            }
            if (applyCommentIbGib) {
                rel8nsToAddByAddr.comment = [getIbGibAddr({ ibGib: applyCommentIbGib })];
            }
            /**
             * rel8 the new bin addr, replacing the old bin addr via
             * setting `linkedRel8ns` to true.
             *
             * also rel8 the apply comment ibgib.
             */
            const resRel8_updateBinAddrAndApplyComment = await rel8({
                src: resMut8_diffIdAndupdateFileHash.newIbGib,
                rel8nsToAddByAddr,
                linkedRel8ns: ['bin'],
                dna: true,
                nCounter: true,
            }) as TransformResult<B2tFSItemIbGib_V1>;

            // #endregion existingItem truthy, get the transformResults that append to timeline
            transformResults.push(resMut8_diffIdAndupdateFileHash);
            transformResults.push(resRel8_updateBinAddrAndApplyComment);
        } else {
            // #region existingItem falsy, get the transformResult for new B2tFSItemIbGib

            const itemData: B2tFSItemData_V1 = {
                fsType: 'file',
                name: rawFilename,
                nameHash: await hash({ s: rawFilename }),
                saferNameVersion: 'v1',
                uuid: await getUUID(), // makes this ibgib unique even if the bin ibgib already exists
                diffTransactionId,
                fileExt,
                fileNameSansExt,
                fileHash: inputPathFileHash,
                fileEncoding: detectedEncoding,
            };
            const itemRel8ns: B2tFSItemRel8ns_V1 = applyCommentIbGib ?
                {
                    [BINARY_REL8N_NAME]: [binAddr],
                    comment: [getIbGibAddr({ ibGib: applyCommentIbGib })],
                } : {
                    [BINARY_REL8N_NAME]: [binAddr],
                };

            const resB2tFSItemIbGib = await Factory_V1.firstGen({
                parentIbGib: Factory_V1.primitive({ ib: B2TFS_ITEM_ATOM }),
                ib: getB2tFSItemIb({ data: itemData }),
                data: itemData,
                rel8ns: itemRel8ns,
                dna: true,
                nCounter: true,
                tjp: { timestamp: true, uuid: true },
            }) as TransformResult<B2tFSItemIbGib_V1>;

            // #endregion existingItem falsy, get the transformResult for new B2tFSItemIbGib
            transformResults.push(resB2tFSItemIbGib);
        }

        /**
         * validation for that thing we just made
         */
        const validationErrors = await validateCommonB2tFSItemIbGib({ ibGib: transformResults.at(-1)!.newIbGib });
        if ((validationErrors ?? []).length > 0) {
            throw new Error(`(UNEXPECTED) newly created b2tfs item ibgib had validation errors? validationErrors: ${validationErrors} (E: 6b62ad59c84440128d02ae0d95359cd5)`);
        }

        // add the binary ibgib to the intermediate ibgibs so that when the
        // transform result is persisted, the binary ibGib will also be
        // persisted.
        if (transformResults.length === 0) { throw new Error(`(UNEXPECTED) transformResults.length === 0? atow (1/2024) this is expected to always have at least one transformResult. (E: 67e45e4cc675de11fe265d9eed14e524)`); }
        transformResults.at(-1)!.intermediateIbGibs ??= [];
        transformResults.at(-1)!.intermediateIbGibs!.push(binIbGib);

        // get the new item sqaddr for result diff
        const newItem = transformResults.at(-1)!.newIbGib;
        const newItemSQAddr = getSpaceQualifiedIbGibAddr({ space, ibGib: newItem });

        /** we now have everything to create the diff info result object */
        const resDiff: B2tFSItemDiffInfo_File = {
            diffInterfaceName: 'B2tFSItemDiff_File',
            fsType: 'file',
            fsName: rawFilename,
            diffTransactionId,
            existingItem,
            newData: inputPathFileData,
            oldData: existingFileData, // undefined if existingItem falsy
            existingItemSQAddr, // undefined if existingItem falsy
            timestampInTicks: getTimestampInTicks(),
            applyCommentText: applyCommentIbGib?.data!.text,

            transformResults,
            newItem,
            newItemSQAddr,
            // childDiffs, // this is a file diff so no children
            applyCommentIbGib,
        }

        if (logalot) {
            console.log(`${lc} console.dir(resDiff)... (I: 594eb2109ffcd79794f0910c0b07ea24)`);
            console.dir(resDiff);
        }

        if (!deferred) {
            if (logalot) { console.log(`${lc} deferred is false. applying diff... (I: e09a86807d794f5f7102d9ca64106a24)`); }
            await applyDiff({ metaspace, diff: resDiff, space });
        }

        return resDiff;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * @internal subfunction for paths that correspond to folders
 *
 * ## existingItem falsy function branch (create new)
 *
 * creates a diff for this folder, including the requisite B2tFSItemIbGib_V1
 * transformResult.  creates diffs recursively for children.
 *
 * ## existingItem truthy function branch
 *
 * gets the existing children.
 *
 * have to check for:
 *
 * 1. additions (new files/folders)
 * 2. subtractions (deleted files/folders)
 * 3. changes (existing files/folders)
 *
 * For #1 & #2 we check by comparing folder's FS names against existing children
 * names via the child addrs (ib contains the name).
 *
 * In the case of #3, we delegate recursively to existing children.
 *
 * ## dev notes
 *
 * HUGE function here. I even have multiple sections of this jsdoc for this
 * function. should break up once I know wth I have this functioning.
 */
async function getB2tFSItemDiff_Folder({
    existingItem,
    inputPath,
    filterPatterns,
    diffTransactionId,
    deferred,
    applyCommentIbGib,
    metaspace,
    space,
}: {
    /**
     * if this is truthy, then we will use this item as our src and mut8/rel8
     * changes if any when diffing against current filesystem state.
     */
    existingItem: B2tFSItemIbGib_V1 | undefined,
    inputPath: string,
    filterPatterns: string[],
    /**
     * @see {@link getB2tFSItemDiff} param.
     */
    diffTransactionId: string,
    /**
     * if true, will not apply the diffs as we go.
     *
     * this is to be used in just a `--b2tfs-diff` command or one with
     * `--dry-run`-like flag.
     */
    deferred: boolean;
    /**
     * comment to associate with the diff if it is not deferred.
     *
     * if deferred is false, this must be defined or throws.
     */
    applyCommentIbGib: CommentIbGib_V1 | undefined,
    /**
     * @see {@link MetaspaceService}
     */
    metaspace: MetaspaceService,
    space: IbGibSpaceAny,
}): Promise<B2tFSItemDiffInfo_Folder | undefined> {
    const lc = `[${getB2tFSItemDiff_Folder.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 60ae4070a15c4e798a68ec0cd3e2f81c)`); }

        // at this point, the path is guaranteed to exist, be a folder, and
        // passed any exclude filters.

        // debugger; // getB2tFSItemDiff_Folder

        let resDiff: B2tFSItemDiffInfo_Folder;

        const rawFolderName = pathUtils.basename(inputPath);
        // const folderNameHash = await hash({s: folderName});
        // const folderName_Safer = getSaferSubstring({text: folderName});
        if (logalot) { console.log(`${lc} folderName: ${rawFolderName} (I: 28513a314ee2149a2feb1d4dd6e9e123)`); }

        const dirEntries: Dirent[] =
            await readdir(inputPath, { recursive: false, withFileTypes: true });

        // #region merge filterPatterns given with those found in folder's filterPattern file(s)


        /**
         * incoming filterPatterns plus any patterns found in file(s) inside
         * this folder. (we do NOT want to modify incoming `filterPatterns`)
         */
        // let filterPatterns_thisFolder: string[] = filterPatterns.concat();
        let filterPatterns_thisFolder: string[] =
            await getAggregateFilterPatternsThisFolder({
                filterPatterns,
                dirEntries,
                filterFilenames: B2TFS_ITEM_FILTER_PATTERN_FILENAMES,
                inputPath,
            });

        // const filterPatternFileEntries =
        //     dirEntries.filter(x => x.isFile() && B2TFS_ITEM_FILTER_PATTERN_FILENAMES.includes(x.name));
        // for (let i = 0; i < filterPatternFileEntries.length; i++) {
        //     const filterFileEntry = filterPatternFileEntries[i];
        //     const filterFilePath = pathUtils.join(inputPath, filterFileEntry.name);
        //     const fileContents = await readFile(filterFilePath, { encoding: 'utf8' });
        //     const fileFilterPatterns = fileContents.split('\n');
        //     filterPatterns_thisFolder = [
        //         ...filterPatterns_thisFolder,
        //         ...fileFilterPatterns,
        //     ];
        // }

        // #endregion merge filterPatterns given with those found in folder's filterPattern file(s)

        /**
         * these are the actual files/folders that are stored in the vcs.
         */
        const dirEntries_filtered = dirEntries.filter(x => !isPathFiltered({
            inputPath: pathUtils.join(inputPath, x.name),
            filterPatterns: filterPatterns_thisFolder,
        }));
        const symbolicLinkDirEntries = dirEntries_filtered.filter(x => x.isSymbolicLink());
        if (symbolicLinkDirEntries.length > 0) { throw new Error(`symbolic links are not implemented. inputPath: ${inputPath}, current symbolic links: ${symbolicLinkDirEntries.map(x => x.name).join(', ')} (E: d83829560e3243d49c97b96ce6af494b)`); }

        // #region diff info vars that we will populate

        /**
         * diffs that correspond to children files/folders found in this parent
         * folder.
         */
        const childDiffs: B2tFSItemDiffInfo[] = [];
        let childNamesAdded: string[] = [];
        let childAddrsAdded: string[] = [];

        let childNamesModified: string[] = [];
        /**
         * if existingItem children change, this provides mapping of old child
         * addr to new child addr.
         */
        let childAddrsModified: { [prevAddr: string]: string } = {};

        let childNamesRemoved: string[] = [];
        let childAddrsRemoved: IbGibAddr[] = [];

        /**
         * in the creation of this folder's b2tfs item ibgib, these are the addrs
         * to add rel8ns to.
         *
         * this could include both freshly added child addrs and modified
         * existing child addrs.
         */
        let childAddrsToRel8: string[] = [];
        /**
         * in the creation of this folder's b2tfs item ibgib, these are the addrs
         * to delete rel8ns to.
         *
         * this could include both deleted child addrs and old child addrs whose
         * items were modified/updated (and thus have new punctiliar addrs).
         */
        let childAddrsToUnrel8: string[] = [];

        /**
         * if existingItem truthy, this will point to it (space-qualified)
         */
        let existingItemSQAddr: SpaceQualifiedIbGibAddr | undefined = !!existingItem ?
            getSpaceQualifiedIbGibAddr({ space, ibGib: existingItem }) :
            undefined;
        /**
         * @see {@link B2tFSItemDiffInfo.transformResults}
         */
        let transformResults: TransformResult<B2tFSItemIbGib_V1>[] = [];

        // #endregion diff info vars that we will populate

        if (existingItem) {
            // #region existingItem truthy, get the transformResults that evolve the timeline

            /** existingItem's children */
            const existingChildAddrs = ((existingItem.rel8ns ?? {}).b2tfs_child ?? []);

            /** current (sub)folder names in fs inputPath */
            const dirChild_folderNames = dirEntries_filtered.filter(x => x.isDirectory()).map(x => x.name);
            /**
             * map from name to its hash, because we compare by hashes.
             *
             * ## intent
             *
             * this is to mitigate names that include spaces/other illegal
             * characters, because we want to be able to just look at the
             * ibGib.ib (in the addr) without loading the entire item ibgib.
             *
             * NOTE: this is not just a "hashmap", this is a mapping of name --> hash.
             */
            const dirChild_folderNameToHashMap: { [name: string]: string } = {};
            for (let i = 0; i < dirChild_folderNames.length; i++) {
                const dirChild_folderName = dirChild_folderNames[i];
                dirChild_folderNameToHashMap[dirChild_folderName] = await hash({ s: dirChild_folderName });
            }
            const dirChild_folderNameHashes = Object.values(dirChild_folderNameToHashMap);
            /** current filenames in fs inputPath */
            const dirChild_fileNames = dirEntries_filtered.filter(x => x.isFile()).map(x => x.name);
            /**
             * map from name to its hash, because we compare by hashes.
             *
             * ## intent
             *
             * this is to mitigate names that include spaces/other illegal
             * characters, because we want to be able to just look at the
             * ibGib.ib (in the addr) without loading the entire item ibgib.
             *
             * NOTE: this is not just a "hashmap", this is a mapping of name --> hash.
             */
            const dirChild_fileNameToHashMap: { [name: string]: string } = {};
            for (let i = 0; i < dirChild_fileNames.length; i++) {
                const dirChild_fileName = dirChild_fileNames[i];
                dirChild_fileNameToHashMap[dirChild_fileName] = await hash({ s: dirChild_fileName });
            }
            const dirChild_fileNameHashes = Object.values(dirChild_fileNameToHashMap);

            /**
             * ib info contains info on the child item: folder/file, name.
             */
            const existingChildIbInfos = existingChildAddrs
                .map(x => getIbAndGib({ ibGibAddr: x }).ib)
                .map(ib => parseB2tFSItemIb({ ib }));
            const existingChild_folderNameHashes = existingChildIbInfos.filter(x => x.fsType === 'folder').map(x => x.nameHash);
            const existingChild_fileNameHashes = existingChildIbInfos.filter(x => x.fsType === 'file').map(x => x.nameHash);

            // #region additions

            const folderNames_added = dirChild_folderNames.filter(x =>
                !existingChild_folderNameHashes.includes(dirChild_folderNameToHashMap[x]));
            const fileNames_added = dirChild_fileNames.filter(x =>
                !existingChild_fileNameHashes.includes(dirChild_fileNameToHashMap[x]));

            // const folderNames_added: string[] = [];
            // for (let i = 0; i < dirChild_folderNames.length; i++) {
            //     const dirChild_folderName = dirChild_folderNames[i];
            //     const dirChild_folderName_hash = await hash({ s: dirChild_folderName });
            //     if (!existingChild_folderNameHashes.includes(dirChild_folderName_hash)) {
            //         folderNames_added.push(dirChild_folderName);
            //     }
            // }
            // const fileNames_added: string[] = [];
            // for (let i = 0; i < dirChild_fileNames.length; i++) {
            //     const dirChild_fileName = dirChild_fileNames[i];
            //     const dirChild_fileName_hash = await hash({ s: dirChild_fileName });
            //     if (!existingChild_fileNameHashes.includes(dirChild_fileName_hash)) {
            //         fileNames_added.push(dirChild_fileName);
            //     }
            // }

            const all_added = folderNames_added.concat(fileNames_added);
            for (let i = 0; i < all_added.length; i++) {
                const childName = all_added[i];

                if (logalot) { console.log(`${lc} recursively getting diff for **ADDED** childName: ${childName} (I: d90f45e42d4b41d89f21bef3145fe6a1)`); }
                const childItemDiff = await getB2tFSItemDiff({
                    existingItem: undefined,
                    diffTransactionId,
                    inputPath: pathUtils.join(inputPath, childName),
                    filterPatterns: filterPatterns_thisFolder,
                    deferred,
                    applyCommentIbGib,
                    metaspace,
                    space,
                });

                if (!childItemDiff) { throw new Error(`(UNEXPECTED) childItemDiff falsy but new child file/folder name? all new child file/folders should create childItemDiffs are expected to be truthy because this is not a diff from a similar previous state, rather the previous state is completely falsy. So this should necessitate truthy additive childItemDiff. (E: d7dcfd9a5d824eeab96e914c77b1d31d)`); }
                if (!childItemDiff.transformResults) { throw new Error(`(UNEXPECTED) childItemDiff.transformResults falsy? (E: 59d176cfae8a4ef785213adad296f644)`); }
                if (childItemDiff.transformResults.length === 0) { throw new Error(`(UNEXPECTED) childItemDiff.transformResults.length === 0? at least one transform result is expected.  (E: 8cff8058f8484860998a231457a18cdf)`); }

                childDiffs.push(childItemDiff);
                childNamesAdded.push(childItemDiff.fsName);
                const childAddr = getIbGibAddr({ ibGib: childItemDiff.transformResults.at(-1)!.newIbGib });
                childAddrsAdded.push(childAddr);
            }

            // #endregion additions

            // #region deletions

            const folderNameHashes_deleted = existingChild_folderNameHashes.filter(x =>
                !dirChild_folderNameHashes.includes(x));
            const fileNameHashes_deleted = existingChild_fileNameHashes.filter(x =>
                !dirChild_fileNameHashes.includes(x));
            const allNameHashes_deleted = folderNameHashes_deleted.concat(fileNameHashes_deleted);
            const childAddrsDeleted = existingChildAddrs.filter(existingChildAddr => {
                const { ib, } = getIbAndGib({ ibGibAddr: existingChildAddr });
                const ibInfo = parseB2tFSItemIb({ ib });
                return allNameHashes_deleted.includes(ibInfo.nameHash);
            });

            // #endregion deletions

            // #region unchanged names but possible intrinsic changes

            const folderNameHashes_unchanged = existingChild_folderNameHashes.filter(x => dirChild_folderNameHashes.includes(x));
            const fileNameHashes_unchanged = existingChild_fileNameHashes.filter(x => dirChild_fileNameHashes.includes(x));
            const allNameHashes_unchanged = folderNameHashes_unchanged.concat(fileNameHashes_unchanged);
            for (let i = 0; i < allNameHashes_unchanged.length; i++) {
                const childNameHash = allNameHashes_unchanged[i];

                // first get existingItem for the child
                const existingChildAddr = existingChildAddrs.filter(x => {
                    const { ib } = getIbAndGib({ ibGibAddr: x });
                    const ibInfo = parseB2tFSItemIb({ ib });
                    return ibInfo.nameHash === childNameHash;
                }).at(0);
                if (!existingChildAddr) { throw new Error(`(UNEXPECTED) existingChildAddr falsy? atow (1/2024) the names are derived from the addrs so this should necessarily be truthy. (E: 0c9c6b6b76946d427926190560944824)`); }
                const resGetChild = await metaspace.get({ addr: existingChildAddr, space });
                // debugger; // looking for resGetChild results in getB2tFSItemDiff_Folder
                if (!resGetChild.success || !resGetChild.ibGibs?.at(0)) { throw new Error(`couldn't get existingChildAddr (${existingChildAddr}) in space (${space.data!.name} [${space.data!.uuid}]) errorMsg: ${resGetChild.errorMsg}. (E: a007420cf54788abebb19c6aee5d1e24)`); }
                const childItem: B2tFSItemIbGib_V1 = resGetChild.ibGibs!.at(0)! as B2tFSItemIbGib_V1;
                if (!childItem.data) { throw new Error(`(UNEXPECTED) childItem.data falsy? (E: 270dcabc4b7495614311b57c55dbff24)`); }

                /**
                 * recover childname from existing child item's data. (we only
                 * have the name hash and safer name in the address)
                 */
                const childName = childItem.data.name;

                // now get the child diff | undefined
                if (logalot) { console.log(`${lc} recursively getting possible diff for **UNCHANGED** childName: ${childNameHash} (I: 55a343d0bd3040eab83c1f06304c59f4)`); }
                const childPath = pathUtils.join(inputPath, childName);
                // debugger; // readme.md should be different!
                const childDiff = await getB2tFSItemDiff({
                    existingItem: childItem,
                    diffTransactionId,
                    inputPath: childPath,
                    filterPatterns: filterPatterns_thisFolder,
                    deferred,
                    applyCommentIbGib,
                    metaspace,
                    space,
                });

                if (childDiff) {
                    // validate/sanity
                    if (!childDiff.transformResults) { throw new Error(`(UNEXPECTED) childDiff.transformResults falsy? (E: b7db2bbfd7f44ff3b68172b76cc46972)`); }
                    if (childDiff.transformResults.length === 0) { throw new Error(`(UNEXPECTED) childDiff.transformResults.length === 0? at least one transform result is expected.  (E: 4824fbd2f3284b2f91efab15f110b142)`); }

                    // adjust our info for final result diff
                    childDiffs.push(childDiff);
                    childNamesModified.push(childDiff.fsName);
                    const newChildAddr = getIbGibAddr({ ibGib: childDiff.transformResults.at(-1)!.newIbGib });
                    childAddrsModified[existingChildAddr] = newChildAddr;
                } else {
                    if (logalot) { console.log(`${lc} childDiff for childName (${childName}) is undefined, so the child is unchanged. continuing to next child... (I: 3d41ad8d2da42907223e67b8cf02b524)`); }
                }
            }

            // #endregion unchanged names but possible intrinsic changes

            // #endregion existingItem truthy, get the transformResults that evolve the timeline

            // SANITY CHECK CHILDADDRSMODIFIED...REMOVE THIS ONCE WORKING
            if (logalot) { console.log(`${lc} sanity check on childAddrsModified...remove this once working (I: d8d958ab3b4ee28f5bff0359b2ff9c24)`); }
            for (let i = 0; i < Object.keys(childAddrsModified).length; i++) {
                const prevAddr = Object.keys(childAddrsModified)[i];
                const newAddr = childAddrsModified[prevAddr];
                if (prevAddr === newAddr) { throw new Error(`(UNEXPECTED) prevAddr === newAddr? (${prevAddr}) (E: d223b39dcb452edc054f86767917f524)`); }
            }

            childAddrsToUnrel8 = childAddrsDeleted.concat(Object.keys(childAddrsModified));
            childAddrsToRel8 = childAddrsAdded.concat(Object.values(childAddrsModified));

            if (childAddrsToRel8.length === 0 && childAddrsToUnrel8.length === 0) {
                if (logalot) { console.log(`${lc} no addrs to rel8 or to unrel8, so there are no changes. returning early undefined... (I: ee803c8a1c35fdef5ee51d6dc3d7a524)`); }
                // debugger;// just want to see this hit when there are no changes in diff
                return undefined; /* <<<< returns early */
            }

            // at this point, we are guaranteed to have changes to children

            // #region create b2tfs item ibgib transformResults for this folder

            // #region change rel8ns

            /**
             * always rel8 the apply comment ibgib
             */
            let rel8nsToAddByAddr: Partial<B2tFSItemRel8ns_V1> | undefined = undefined;
            if (applyCommentIbGib) {
                rel8nsToAddByAddr ??= {};
                rel8nsToAddByAddr.comment = [getIbGibAddr({ ibGib: applyCommentIbGib })];
            }
            if (childAddrsToRel8.length > 0) {
                rel8nsToAddByAddr ??= {};
                rel8nsToAddByAddr.b2tfs_child = childAddrsToRel8;
            }
            const resRel8 = await rel8({
                src: existingItem,
                rel8nsToAddByAddr,
                rel8nsToRemoveByAddr: childAddrsToUnrel8.length > 0 ? {
                    b2tfs_child: childAddrsToUnrel8
                } satisfies Partial<B2tFSItemRel8ns_V1> : undefined,
                dna: true,
                nCounter: true,
            }) as TransformResult<B2tFSItemIbGib_V1>;

            transformResults.push(resRel8);

            // #endregion change rel8ns

            // #region mut8 data for transaction id and maybe filterPatterns if changed
            const dataToAddOrPatch: Partial<B2tFSItemData_V1> = {
                diffTransactionId,
                // filterPatterns: filterPatterns_thisFolder.concat(),
            };

            const existingFilterPatterns = existingItem.data?.filterPatterns ?? [];
            if (JSON.stringify(existingFilterPatterns) !== JSON.stringify(filterPatterns_thisFolder)) {
                dataToAddOrPatch.filterPatterns = filterPatterns_thisFolder.concat();
            }

            const resMut8_idAndMaybeFilterPatterns = await mut8({
                src: transformResults.length > 0 ? transformResults.at(-1)?.newIbGib : existingItem,
                dataToAddOrPatch,
                dna: true,
                nCounter: true,
            }) as TransformResult<B2tFSItemIbGib_V1>;

            transformResults.push(resMut8_idAndMaybeFilterPatterns);

            // #endregion mut8 data for transaction id and maybe filterPatterns if changed

            // #endregion create b2tfs item ibgib transformResults for this folder
        } else {
            // #region existingItem falsy

            // #region get childItemDiffs for all children

            for (let i = 0; i < dirEntries_filtered.length; i++) {
                const entry = dirEntries_filtered[i];
                if (entry.isSymbolicLink()) { throw new Error(`symbolic link entry found but these are not allowed. inputPath: ${inputPath}. entry.name: ${entry.name} (E: 0107b3cc64f28a27aa38d023533a8823)`); }

                if (logalot) { console.log(`${lc} recursively creating childItemIbGib for entry.name: ${entry.name} (I: 9bc236741e95d14f8b2e585b207a0e23)`); }
                const childItemDiff = await getB2tFSItemDiff({
                    existingItem: undefined,
                    diffTransactionId,
                    inputPath: pathUtils.join(inputPath, entry.name),
                    filterPatterns: filterPatterns_thisFolder,
                    deferred,
                    applyCommentIbGib,
                    metaspace,
                    space,
                });

                if (!childItemDiff) { throw new Error(`(UNEXPECTED) childItemDiff but existingItem falsy? all childItemDiffs are expected to be truthy because this is not a diff from a similar previous state, rather the previous state is completely falsy. So this should necessitate truthy additive childItemDiff. (E: 50324bfe1e3d3f67e295ea24f3eb7924)`); }
                if (!childItemDiff.transformResults) { throw new Error(`(UNEXPECTED) childItemDiff.transformResults falsy? (E: 3f3fb3b349049f2058ddb6c4d461d224)`); }
                if (childItemDiff.transformResults.length === 0) { throw new Error(`(UNEXPECTED) childItemDiff.transformResults.length === 0? at least one transform result is expected.  (E: 26ca6dbf3dbad8b37b5ab271bf019924)`); }

                childDiffs.push(childItemDiff);
                childNamesAdded.push(childItemDiff.fsName);
                childAddrsAdded.push(getIbGibAddr({ ibGib: childItemDiff.transformResults.at(-1)!.newIbGib }));
            }

            // #endregion get childItemDiffs for all children

            // #region create b2tfs item ibgib transformResult for this folder

            const itemData: B2tFSItemData_V1 = {
                fsType: 'folder',
                name: rawFolderName,
                saferNameVersion: 'v1',
                nameHash: await hash({ s: rawFolderName }),
                diffTransactionId,
                uuid: await getUUID(),
                filterPatterns: filterPatterns_thisFolder.concat(),
            };
            /**
             * these will be added in the `firstGen` call (there will still be
             * other rel8ns like dna, past, etc.)
             */
            let itemRel8ns: B2tFSItemRel8ns_V1 | undefined = undefined;
            if (childAddrsAdded.length > 0) {
                itemRel8ns ??= {};
                itemRel8ns.b2tfs_child = childAddrsAdded;
            }
            if (applyCommentIbGib) {
                itemRel8ns ??= {};
                itemRel8ns.comment = [getIbGibAddr({ ibGib: applyCommentIbGib })];
            }
            const resCreateB2tFSItemIbGib = await Factory_V1.firstGen({
                parentIbGib: Factory_V1.primitive({ ib: B2TFS_ITEM_ATOM }),
                ib: getB2tFSItemIb({ data: itemData }),
                data: itemData,
                rel8ns: itemRel8ns,
                dna: true,
                nCounter: true,
                tjp: { timestamp: true, uuid: true },
            }) as TransformResult<B2tFSItemIbGib_V1>;

            transformResults.push(resCreateB2tFSItemIbGib);

            // #endregion create b2tfs item ibgib transformResult for this folder

            // #endregion existingItem falsy
        }

        // get the new item sqaddr for result diff
        if (transformResults.length === 0) { throw new Error(`(UNEXPECTED) transformResults.length === 0? atow (1/2024) this is expected to always have at least one transformResult. (E: b43c29991676409a810124e3aeb0f38a)`); }
        const newItem = transformResults.at(-1)!.newIbGib;
        const newItemSQAddr = getSpaceQualifiedIbGibAddr({ space, ibGib: newItem });

        resDiff = {
            fsType: 'folder',
            diffInterfaceName: 'B2tFSItemDiff_Folder',
            fsName: rawFolderName,
            diffTransactionId,
            existingItem: !!existingItem ? existingItem : undefined,
            childNamesAdded,
            childAddrsAdded,
            childNamesModified,
            childAddrsModified,
            childNamesRemoved,
            childAddrsRemoved,
            timestampInTicks: getTimestampInTicks(),
            applyCommentText: applyCommentIbGib?.data!.text,
            newItem,
            newItemSQAddr,
            transformResults,
            childDiffs: childDiffs.length > 0 ? childDiffs : undefined,
            existingItemSQAddr, // undefined if existingItem falsy
            filterPatterns: filterPatterns_thisFolder,
            applyCommentIbGib,
        };

        if (!resDiff) { throw new Error(`(UNEXPECTED) resDiff falsy? (E: 9344f23935eafa544343ddd253d7d424)`); }
        if (!resDiff.transformResults) { throw new Error(`(UNEXPECTED) resDiff.transformResults falsy? (E: e97164ed642d48157e0e500834247b24)`); }
        if (resDiff.transformResults.length === 0) { throw new Error(`(UNEXPECTED) resDiff.transformResults.length === 0? (E: 4aa40f7b19157f12f6fd58caaef92c24)`); }
        const resItemIbGib = resDiff.transformResults.at(-1)!.newIbGib as B2tFSItemIbGib_V1;
        if (!resItemIbGib) { throw new Error(`(UNEXPECTED) resItemIbGib falsy? (E: 2e7e274ee8fde1f036133d8132161e24)`); }
        const validationErrors = await validateCommonB2tFSItemIbGib({ ibGib: resItemIbGib });
        if ((validationErrors ?? []).length > 0) { throw new Error(`(UNEXPECTED) newly created B2tFSItemIbGib_V1 item ibgib had validation errors? validationErrors: ${validationErrors} (E: ba7c906e28be49e09589ff68dca33566)`); }

        if (!deferred) {
            if (logalot) { console.log(`${lc} deferred false. applying resDiff (item addr: ${getIbGibAddr({ ibGib: resItemIbGib })}) (I: dd63082b76fbcaf51e48970aa6084424)`); }
            await applyDiff({ metaspace, space, diff: resDiff });
        }

        return resDiff;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * "apply" (atow 1/2024) basically means to persist the ibgib dependency
 * graphs via `persistTransformResult` in the order in which they are
 * created and then register them with the given `space` via
 * `metaspace.registerNewIbGib`. this `registerNewIbGib` part updates the latest
 * addr pointer (via metastones in current impl but can be different in other
 * spaces) in the timeline, similar to pointing HEAD to a new commit (only in a
 * more DAG-y way), and publishes this update in the metaspace via its broadcast
 * function.
 *
 * @see {@link B2tFSItemDiffInfo} notes on applying child diffs before parents
 * @see {@link MetaspaceService}
 * @see {@link MetaspaceService.registerNewIbGib}
 */
export async function applyDiff({
    metaspace,
    diff,
    space,
}: {
    /**
     * @see {@link MetaspaceService}
     */
    metaspace: MetaspaceService,
    /**
     * diff that, among other info, contains the transformResult(s) to apply.
     *
     * note that these transformResults will be applied in the order in which
     * they appear in the array.
     *
     * @see {@link applyDiff}
     * @see {@link B2tFSItemDiffInfo}
     * @see {@link B2tFSItemDiffInfo.transformResults}
     */
    diff: B2tFSItemDiffInfo,
    /**
     * this is the space in which to apply the diff. note that usually, this is
     * a branch space and atow (1/2024) i haven't implemented any interspatial
     * dynamics to automaticallybring in dependencies when applying diffs to
     * other spaces. (that is actually more of a merge/sync operation).
     */
    space: IbGibSpaceAny,
}): Promise<void> {
    const lc = `[${applyDiff.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 2f39e3a121014388eb60bc02eb4be423)`); }

        /**
         * "umbrella" means the entire set of all diffs being applied.  each
         * individual timeline (file/[sub]folder) will have its own diff
         * applied, but this id is for the entire transaction.
         */
        const umbrellaDiffTransactionId = await getUUID();

        // after diff applied show previous umbrellaDiffTransactionId?
        // create diff/report/index ibgib that we rel8 to the root b2tfs item?
        // rel8 individual diffs to individual item ibgibs (i.e. timelines)?

        // we must first do the childDiffs
        let { childDiffs } = diff;
        childDiffs ??= [];
        for (let i = 0; i < childDiffs.length; i++) {
            const childDiff = childDiffs[i];
            await applyDiff({ metaspace, diff: childDiff, space });
        }

        const { transformResults } = diff;
        if (!transformResults) { throw new Error(`(UNEXPECTED) diff.transformResults falsy? (E: a30dfc8302775192ce130685cada6123)`); }
        if (transformResults.length === 0) { throw new Error(`(UNEXPECTED) diff.transformResults empty? (E: 5f45ac5e7cde42fab07f5c3c699b3072)`); }

        // FIRST, persist all...
        for (let i = 0; i < transformResults.length; i++) {
            const transformResult = transformResults[i];
            // persists in the given space before registering
            await metaspace.persistTransformResult({ resTransform: transformResult, space });
        }

        // ...THEN go through registering them
        for (let i = 0; i < transformResults.length; i++) {
            const transformResult = transformResults[i];
            // register the transform result in the given space to enable latest
            // addr index update
            await metaspace.registerNewIbGib({ ibGib: transformResult.newIbGib, space });
        }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * takes a given {@link B2tFSItemIbGib_V1} {@link item_File} which should be
 * {@link B2tFSType} === 'file', and returns its corresponding {@link
 * BinIbGib_V1} from the given {@link space} via the {@link metaspace}.
 *
 * @returns bin ibgib for the given `item_file`
 * @throws if bin ibgib can't be gotten or one of our assumptions is broken
 */
export async function getBinIbGibFromB2tFSItemIbGib({
    metaspace,
    item_File,
    space,
}: {
    /**
     * @see {@link MetaspaceService}
     */
    metaspace: MetaspaceService,
    /**
     * expected to be data.fsType === 'file'
     */
    item_File: B2tFSItemIbGib_V1,
    space: IbGibSpaceAny,
}): Promise<BinIbGib_V1> {
    const lc = `[${getBinIbGibFromB2tFSItemIbGib.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 15a8f35565ca56e9e4126816cf133b23)`); }

        if (!item_File.data) { throw new Error(`(UNEXPECTED) item.data falsy? (E: dfe227503fc55e807373206b092d4f23)`); }
        if (item_File.data.fsType !== 'file') { throw new Error(`(UNEXPECTED) item.data.fsType !== 'file'? (E: 81cf19d389b923370cd65098f84d0523)`); }
        if (!item_File.rel8ns) { throw new Error(`(UNEXPECTED) item.rel8ns falsy? (E: 8502fd07a09b00efbaee4a3e58465923)`); }
        if (!item_File.rel8ns.bin) { throw new Error(`(UNEXPECTED) item.rel8ns.bin falsy? (E: d5f9ce438d8d51bf2b9afccf40d1da23)`); }
        if (item_File.rel8ns.bin.length !== 1) { throw new Error(`(UNEXPECTED) item.rel8ns.bin.length !== 1? expect this to have length atow (12/2023) pointing to bin ibgib that contains the file's data. (E: 7109c3ea3d5708614fe974f75e308523)`); }

        const binAddr = item_File.rel8ns.bin.at(0);

        let resGet = await metaspace.get({ addr: binAddr, space });
        if (!resGet.success) { throw new Error(`couldn't get binAddr (${binAddr}) from space (${space.data!.name} ${space.data!.uuid}). errorMsg: ${resGet.errorMsg} (E: e7ac8b8b95d46363f66a4a9187595323)`); }

        if (resGet.ibGibs?.length !== 1) { throw new Error(`(UNEXPECTED) resGet.success but ibGibs.length !== 1? (E: 672886b90a9130d4b1c7d11c3c0eb323)`); }

        return resGet.ibGibs.at(0)! as BinIbGib_V1;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * simple helper that wraps some validation, metaspace.get plumbing, etc.
 * @returns B2tFSItemIbGib associated with branchIbGib/Space or undefined depending on {@link defaultIfNone}
 */
export async function getBranchRootItem({
    branchIbGib,
    branchSpace,
    latest,
    metaspace,
    defaultIfNone,
}: {
    branchIbGib: B2tFSBranchIbGib_V1,
    branchSpace: IbGibSpaceAny,
    latest: boolean,
    /**
     * @see {@link MetaspaceService}
     */
    metaspace: MetaspaceService,
    defaultIfNone: 'throw' | 'undefined',
}): Promise<B2tFSItemIbGib_V1 | undefined> {
    const lc = `[${getBranchRootItem.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: bf28d8f398d1e4a4f3db0c1888696b23)`); }

        // #region validate
        if (!branchSpace?.data?.uuid) { throw new Error(`(UNEXPECTED) branchSpace.data.uuid falsy? (E: 2645385328c9ccdf9b02e2e928f56b23)`); }
        const spaceId = branchSpace.data.uuid;
        if (!branchIbGib.data) { throw new Error(`(UNEXPECTED) branchIbGib.data falsy? (E: 8241c9ee886957bebe48733c72c50323)`); }
        if (!branchIbGib.rel8ns?.b2tfs_item) { throw new Error(`branch (${branchIbGib.data.name}, spaceId: ${spaceId}) rel8ns?.b2tfs_item falsy. (E: ea930cdfa2ddcd1f6478f9d2401e5823)`); }
        if (branchIbGib.rel8ns?.b2tfs_item.length !== 1) { throw new Error(`branch (${branchIbGib.data.name}, spaceId: ${spaceId}) rel8ns.b2tfs_item.length !== 1. length: ${branchIbGib.rel8ns?.b2tfs_item.length}. (E: ea930cdfa2ddcd1f6478f9d2401e5823)`); }
        // #endregion validate

        let branchRootItemAddr = branchIbGib.rel8ns?.b2tfs_item[0];

        if (latest) {
            branchRootItemAddr = await metaspace.getLatestAddr({
                addr: branchRootItemAddr,
                space: branchSpace,
            }) ?? branchRootItemAddr;
        }

        const resGet = await metaspace.get({
            addr: branchRootItemAddr,
            space: branchSpace,
        });

        if (!resGet.success || (resGet.ibGibs ?? []).length !== 1) {
            if (defaultIfNone === 'throw') {
                throw new Error(`could not get branchRootItem (${branchRootItemAddr}) from space (${spaceId}) (E: 42981ab8cf8d922fd8574172ceaf0323)`);
            } else {
                // undefined
                return undefined; /* <<<< returns early */
            }
        } else {
            return resGet.ibGibs?.at(0)! as B2tFSItemIbGib_V1;
        }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * atow (01/2024) this just gets a slightly formatted output
 * of previous data (all of it) and new data (all of it).
 *
 * need to change this to actually be a real diff.
 * @param param0
 * @returns
 */
export async function getDiffInfoText({
    itemDiff,
    indentLevel,
}: {
    itemDiff?: B2tFSItemDiffInfo,
    /**
     * spaces to indent.
     *
     * ## intent
     *
     * i'm using this to indent sub items within folders.
     * so if a file within some folder struture changes, each level deep shows
     * more indention.
     */
    indentLevel?: number,
}): Promise<string> {
    const lc = `[${getDiffInfoText.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 87259785c214875dbd3de7c38893e724)`); }

        if (!itemDiff) { return `[No differences found.]`; /* <<<< returns early */ }

        indentLevel ??= 0;

        const lines: string[] = [];
        const maxLineLength = 80;
        const addLine: (line: string, trim?: boolean) => void = (line, trim) => {
            const indentedLine = ''.padEnd(indentLevel ?? 0, ' ') + line;
            const formattedLine = indentedLine.length > maxLineLength && trim ?
                indentedLine.substring(0, maxLineLength - 3) + '...' :
                indentedLine;
            lines.push(formattedLine);
        }

        if (itemDiff.fsType === 'file') {

            const getFormattedData: (data: string | undefined) => string[] = (data) => {
                if (data) {
                    let lines = data.split('\n');
                    const maxLines = 10; // arbitrary hard-coded atm
                    const linesCount = lines.length;
                    if (linesCount > maxLines) {
                        lines = lines.slice(0, maxLines);
                        lines.push(`[Additional ${linesCount - maxLines} lines...]`);
                    }
                    return lines;
                } else {
                    return ['[data undefined? hmmm. (E: 1627d91fc5d84bd18a11336bdff04ad5)]'];
                }
            }

            // this is where we would get a good representation of changed
            // lines. for now just plop all of the info.

            const fileDiff = itemDiff as B2tFSItemDiffInfo_File;

            addLine(``);
            addLine(`${fileDiff.fsName}:`);
            addLine(``);
            addLine(`oldData:`);
            addLine(`----------------------`)
            getFormattedData(fileDiff.oldData).forEach(x => addLine(x, /*trim*/ true));
            addLine(`----------------------`)
            addLine(``);
            addLine(`newData:`);
            addLine(`++++++++++++++++++++++`)
            getFormattedData(fileDiff.newData).forEach(x => addLine(x, /*trim*/ true));
            addLine(`++++++++++++++++++++++`)
            addLine(``);

        } else if (itemDiff.fsType === 'folder') {

            const folderDiff = itemDiff as B2tFSItemDiffInfo_Folder;
            addLine(`[START] folder ${folderDiff.fsName}`);
            const childDiffs = folderDiff.childDiffs ?? [];
            for (let i = 0; i < childDiffs.length; i++) {
                const childDiff = childDiffs[i];
                const childDiffText = await getDiffInfoText({
                    itemDiff: childDiff,
                    indentLevel: (indentLevel ?? 0) + 2
                });
                addLine(childDiffText);
            }
            addLine(`[END] folder ${folderDiff.fsName}`);
            addLine(``);
        } else {
            throw new Error(`(UNEXPECTED) unknown itemDiff.fsType (${itemDiff.fsType})? atow only 'file' or 'folder' expected. (E: 75188db9fcb73ab26ae275ada6afb824)`);
        }

        return lines.join('\n');
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * writes a b2tfs item to FS given the {@link resolvedContextOutputDirPath}.
 *
 * if it's a folder, this makes a new directory using the item `ibgib.data.name`
 * under the {@link resolvedContextOutputDirPath} and recursively writes any
 * children that folder has.
 *
 * if it's a file, this writes that file {@link resolvedContextOutputDirPath},
 * using the b2tfs item.data.name (atow 01/2024).
 *
 * ## intent
 *
 * this is the implementation to be used when handling a branch command that is
 * exporting the branch's current state in the ibgib graph.
 *
 * so if there are changes in the working directory for the source branch, those
 * are not projected here, as this works strictly from the stored b2tfs item
 * ibgibs.
 */
export async function writeItemToFS({
    itemAddr,
    itemIbGib,
    metaspace,
    branchSpace,
    resolvedContextOutputDirPath,
}: {
    itemAddr?: IbGibAddr,
    itemIbGib?: B2tFSItemIbGib_V1,
    metaspace: MetaspaceService,
    branchSpace: IbGibSpaceAny,
    /**
     * the context folder path for the item (specified by {@link itemAddr}).
     *
     * if this is the root item, this will be an empty string.
     */
    resolvedContextOutputDirPath: string,
}): Promise<void> {
    const lc = `[${writeItemToFS.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 9f2d7db54429611de41daeaaa52f5d24)`); }

        // #region validate
        if (!itemAddr && !itemIbGib) { throw new Error(`(UNEXPECTED) !itemAddr && !itemIbGib? either one or the other is required. (E: a3cb367f0545e24b22e1d5c25afb9324)`); }
        if (itemAddr && itemIbGib) {
            if (itemAddr !== getIbGibAddr({ ibGib: itemIbGib })) { throw new Error(`(UNEXPECTED) itemAddr !== getIbGibAddr({ibGib: itemIbGib})? both addr and ibgib were provided, but they don't match up (E: 73640d94ed06afae8979be0473ea0a24)`); }
        }
        // #endregion validate

        if (!itemIbGib) {
            // get the ibgib from the addr
            // let itemIbGib: B2tFSItemIbGib_V1;
            const resGet = await metaspace.get({ addr: itemAddr, space: branchSpace, });
            if (resGet.success && resGet.ibGibs && resGet.ibGibs.length === 1) {
                itemIbGib = resGet.ibGibs.at(0)! as B2tFSItemIbGib_V1;
                if (!itemIbGib.data) { throw new Error(`(UNEXPECTED) !itemIbGib.data? (E: f5e2df38747345634d46370194d04224)`); }
            } else {
                throw new Error(`couldn't get itemAddr (${itemAddr}) from branchSpace (id: ${branchSpace.data?.uuid})? resGet.success: ${resGet.success}. resGet.errorMsg: ${resGet.errorMsg}. (E: 13f7a61340b4f7acbec825b50c5fe524)`);
            }
        }
        if (!itemIbGib) { throw new Error(`(UNEXPECTED) !itemIbGib? should be assured by now (E: 8c1e05fb2aeb5c60c5ab715703d10d24)`); }

        // build the resolved item path
        const { fsType, name, } = itemIbGib.data!;
        const resolvedItemPath = pathUtils.join(resolvedContextOutputDirPath, name);

        // depending on folder/file, do the fs operation (mkdir + recurse
        // children or write the file)
        switch (fsType) {
            case 'folder':
                await mkdir(resolvedItemPath, { recursive: true, });

                // iterate over child addrs (if any)
                const childAddrs = itemIbGib.rel8ns?.b2tfs_child ?? [];
                for (let i = 0; i < childAddrs.length; i++) {
                    const childAddr = childAddrs[i];
                    await writeItemToFS({
                        itemAddr: childAddr,
                        branchSpace,
                        metaspace,
                        resolvedContextOutputDirPath: resolvedItemPath,
                    });
                }
                break;
            case 'file':
                const binIbGib = await getBinIbGibFromB2tFSItemIbGib({ metaspace, item_File: itemIbGib, space: branchSpace });
                // atow (01/2024) we're only doing text files (utf8). in the
                // future, may have to do some conversion here.
                // const fileContents = binIbGib.data;
                try {
                    if (itemIbGib.data?.fileEncoding) {
                        await writeFile(resolvedItemPath, binIbGib.data, { encoding: itemIbGib.data!.fileEncoding });
                    } else {
                        await writeFile(resolvedItemPath, binIbGib.data);
                    }
                } catch (error) {
                    throw new Error(`there was an error trying to write the file. resolvedItemPath: ${resolvedItemPath}. binIbGib.data.length: ${binIbGib.data?.length}. writeFile error: ${extractErrorMsg(error)} (E: d0eb0c27d2274a7caf58380c89952cb4)`);
                }
                break;
            default:
                throw new Error(`(UNEXPECTED) unknown itemIbGib.data.fsType: ${fsType}? itemIbGib addr: ${getIbGibAddr({ ibGib: itemIbGib })} (E: dff00123cbc77e54b6947267d090b324)`);
        }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * combines both the existing `filterPatterns` already in effect and appends any
 * filter patterns found in files inside the given {@link dirEntries} (which should be
 * populated from the {@link inputPath}).
 * @returns aggregated filter patterns
 */
export async function getAggregateFilterPatternsThisFolder({
    filterPatterns,
    dirEntries,
    inputPath,
    filterFilenames = B2TFS_ITEM_FILTER_PATTERN_FILENAMES,
}: {
    filterPatterns: string[],
    filterFilenames: string[],
    dirEntries: Dirent[],
    inputPath: string,
}): Promise<string[]> {
    const lc = `[${getAggregateFilterPatternsThisFolder.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: e6dcd4c4646f756be7037c25db39e924)`); }

        filterFilenames ??= B2TFS_ITEM_FILTER_PATTERN_FILENAMES;

        let retFilterPatternsOfInputPath: string[] = filterPatterns.concat();

        const filterPatternFileEntries =
            dirEntries.filter(x => x.isFile() && filterFilenames.includes(x.name));
        for (let i = 0; i < filterPatternFileEntries.length; i++) {
            const filterFileEntry = filterPatternFileEntries[i];
            const filterFilePath = pathUtils.join(inputPath, filterFileEntry.name);
            const fileContents = await readFile(filterFilePath, { encoding: 'utf8' });
            const fileFilterPatterns = fileContents.split('\n');
            retFilterPatternsOfInputPath = [
                ...retFilterPatternsOfInputPath,
                ...fileFilterPatterns,
            ];
        }

        retFilterPatternsOfInputPath = retFilterPatternsOfInputPath
            .filter(x => !!x) // remove empty patterns
            .filter(x => !x.match(/^\s*#/)) // remove comments (start with #)
            ;
        return retFilterPatternsOfInputPath;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export interface FileDataInfo {
    inputPath: string;
    detectedEncoding?: FileEncoding | undefined;
    dataString?: string | undefined;
    dataBuffer?: Buffer | undefined;
}
export const B2TFS_DEFAULT_ENCODINGS_TO_TRY: (FileEncoding | undefined)[] = [
    FileEncoding.utf8,
    FileEncoding.utf16le,
    FileEncoding.base64,
    FileEncoding.binary,
    undefined,
]

export async function getFileDataAndEncoding({
    inputPath,
    encodingsToTry = B2TFS_DEFAULT_ENCODINGS_TO_TRY,
}: {
    inputPath: string,
    encodingsToTry?: (FileEncoding | undefined)[],
}): Promise<FileDataInfo> {
    const lc = `[${getFileDataAndEncoding.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: b4688e6e5f28efa9ed95729f10a6df24)`); }

        let resDataAndEncoding: FileDataInfo | undefined = undefined;
        let dataBuffer = await readFile(inputPath);

        encodingsToTry ??= B2TFS_DEFAULT_ENCODINGS_TO_TRY;

        const tryEncoding = async (encoding: FileEncoding | undefined) => {
            let res: [boolean, string | undefined];
            try {
                const str = dataBuffer.toString(encoding ?? undefined);
                /**
                 * doesn't include the node.js replacement unicode char
                 */
                const isProbablyCorrect = !str.includes('\ufffd');
                res = [isProbablyCorrect, str];
                return res;
            } catch (error) {
                debugger;
                res = [false, undefined];
            }
            return res;
        }

        for (let i = 0; i < encodingsToTry.length; i++) {
            const maybeEncoding = encodingsToTry[i];
            const [isProbablyCorrectEncoding, dataString] = await tryEncoding(maybeEncoding);
            if (isProbablyCorrectEncoding) {
                resDataAndEncoding = {
                    inputPath,
                    dataString,
                    detectedEncoding: maybeEncoding,
                    dataBuffer,
                };
                break;
            }
        }

        if (!resDataAndEncoding) {
            debugger; // want to see why resDataAndEncoding is still undefined
            resDataAndEncoding = {
                inputPath,
                detectedEncoding: undefined,
                dataString: undefined,
                dataBuffer,
            }
        }

        return resDataAndEncoding;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}
