export type B2tFSTestRepoType = 'simple' | 'complex';
export interface B2tFSTestConfig {
    /**
     * base path of the test
     */
    basePath: string;
    startState: {
        /**
         * path under `basePath` that contains .ibgib folder start states of the
         * ibgib data.
         *
         * atow (01/2024) the full path to here is `test-b2tfs/ibgib-folders` with
         * an example ibgib folder path being
         * `test-b2tfs/ibgib-folders/10_initialized`. this would be for an .ibgib
         * folder that has had the `ibgib --init` command executed only. (i.e. it
         * hasn't had other repl sessions or anything else that would grow the
         * contents of the .ibgib folder).
         *
         * ## notes
         *
         * when you are interacting with the ibgib rcli app, you are creating ibgibs
         * that are stored in ibgib spaces. all of this data lives in the .ibgib
         * folder (by default). this is the base subpath for snapshots of those
         * folder states for use in testing.
         *
         * atow (01/2024) i have this as 'ibgib-folders' under 'test-b2tfs'. so you
         * can look at these folders to grok what this is.
         */
        ibgibSnapshotsSubpath: string;
        /**
         * paths under base dir that point to the repo, depending on the complexity
         * of the example/test repo.
         */
        repoSubpaths: {
            /**
             * subpath that contains simple/contrived repos for very basic b2tfs
             * testing.
             */
            simple: string;
            /**
             * subpath that contains more complex repos for more advanced "real
             * world"-ish b2tfs testing.
             */
            complex: string;
        };
    };
    // pathPairs: [repoPath: string, ibgibPath: string][];
    /**
     * output working directory that we copy test start state (ibgib and repo
     * folders). each unit/integration test (respecfully or ifWe block) should have
     * its own terse subfolder directly underneath this directory.
     *
     * NOTE: this is as short as possible because path lengths make a difference
     * with storage of ibgibs. (limitations of OS's)
     */
    outputSubPath: string;
}

/**
 * a test start state combines a repo path with an ibgib path. this
 * provides permutations of start state for tests. this state will be
 * copied to a target test folder and then the test will begin with that
 * state.
 *
 * this allows composition of tests with repos of varying complexity and
 * ibgib data in varying states of progress.
 *
 * ## example
 *
 * so say you have a test repo with a single file 'readme.md'.  you would
 * normally nav to your repo folder and execute an init on the ibgib rcli
 * app itself. after this init, you would have an ibgib folder (.ibgib by
 * default) that contains the ibgib data.  we can snapshot this ibgib folder
 * at this point for testing.
 *
 * if we then do some other operations, like a successful b2tfs-init, in
 * which case the user has entered the name of the space, app, robbot, etc.
 * we can then snapshot this ibgib folder. we can then execute some tests on
 * this state, by again, copying the repo path and the ibgib folder to a new
 * folder and executing some test there.
 */
export interface B2tFSTestStartState {
    /**
     * used to generate the state of the .ibgibs folder for the b2tfs test case.
     */
    ibgibs: {
        /**
         * this name is used to subpath from the {@link B2tFSTestConfig.startState.ibgibSnapshotsSubpath}
         */
        name: string,
    },
    /**
     * used to generate the state of the repo for the b2tfs test case.
     */
    repo: {
        /**
         * determines which repo subpath to look for in * {@link B2tFSTestConfig.startState.repoSubpaths}
         */
        type: B2tFSTestRepoType,
        /**
         * determines the name of the subfolder of the repo to use in the b2tfs test case
         */
        name: string,
    }
}
