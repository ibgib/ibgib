/**
 * @module b2tfs constants
 *
 * constants are in this file!
 */

import { SpecialIbGibType } from "@ibgib/core-gib/dist/common/other/other-types.mjs";

// /**
//  * example enum-like type+const that I use in ibgib. sometimes i put
//  * these in the types.mts and sometimes in the const.mts, depending
//  * on context.
//  */
// export type SomeEnum =
//     'ib' | 'gib';
// export const SomeEnum = {
//     ib: 'ib' as SomeEnum,
//     gib: 'gib' as SomeEnum,
// } satisfies { [key: string]: SomeEnum };
// export const SOME_TYPE_VALUES: SomeEnum[] = Object.values(SomeEnum);

/**
 * meta special type used for indexing b2tfs roots.
 */
export type B2tFSSpecialIbGibType = 'b2tfs' | SpecialIbGibType;
/**
 * @see {@link B2tFSSpecialIbGibType} type
 */
export const B2tFSSpecialIbGibType = {
    ...SpecialIbGibType,
    /**
     * this is the type name that we use to index the B2tFS roots.
     */
    b2tfs: 'b2tfs' as B2tFSSpecialIbGibType,
} satisfies { [key: string]: B2tFSSpecialIbGibType }


/**
 * atow i'm making a tag ibgib to act as the index for the b2tfs roots.
 *
 * these roots are root "folders" akin to repos, which provide a mapping from
 * ibgibs to files/folders for the version control functionality.
 *
 * So a tag's ib schema is atow "tag [safe tag name]"
 */
export const B2TFS_INDEX_KEYWORD = 'B2tFSIndex'
export const B2TFS_INDEX_DESCRIPTION = 'Index used with the B2tFS functionality. Currently implemented as a tag, but perhaps should be a special ibgib or something else.'

// export const B2TFS_ACTIVE_BRANCH_SPACE_ID = 'active_root_space_id';

/**
 * rel8nName from a space's b2tfs index ibgib to its b2tfs root.
 *
 * each space has its own branch ibgib, and this is how you get to it from the
 * space.
 *
 * this differs from the {@link B2TFS_ACTIVE_BRANCH_SPACE_ID} in that this
 * points to the branch ibgib itself, while that points to what is possibly another
 * space.
 *
 * @see {@link B2TFS_ACTIVE_BRANCH_SPACE_ID}
 */
export const B2TFS_BRANCH_REL8N_NAME = 'b2tfs_root';
