/**
 * @module b2tfs types (and enums)
 */

import { IbGibAddr, TjpIbGibAddr } from '@ibgib/ts-gib/dist/types.mjs';
import { IbGibData_V1, IbGibRel8ns_V1, IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { SpaceId } from '@ibgib/core-gib/dist/witness/space/space-types.mjs';

import { B2TFS_BRANCH_TJP_REL8N_NAME } from '../common/b2tfs-common-constants.mjs';
import { B2tFSSpaceType } from '../common/b2tfs-common-types.mjs';
import { IbGibGlobalThis } from '../../rcli-types.mjs';
import { SOFT_ADDR_DATA_KEY_PREFIX_SPACE_QUALIFIED, SpaceQualifiedIbGibAddr } from '../common/b2tfs-common-helper.mjs';
import { B2tFSBranchInfo, B2tFSBranchInfo_Terse, } from '../b2tfs-branch/b2tfs-branch-types.mjs';

// and just to show where these things are
// import { CommentIbGib_V1 } from "@ibgib/core-gib/dist/common/comment/comment-types.mjs";
// import { MetaspaceService } from "@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs";

// /**
//  * example enum-like type+const that I use in ibgib. sometimes i put
//  * these in the types.mts and sometimes in the const.mts, depending
//  * on context.
//  */
// export type SomeEnum =
//     'ib' | 'gib';
// /**
//  * @see {@link SomeEnum}
//  */
// export const SomeEnum = {
//     ib: 'ib' as SomeEnum,
//     gib: 'gib' as SomeEnum,
// } satisfies { [key: string]: SomeEnum };
// /**
//  * values of {@link SomeEnum}
//  */
// export const SOME_ENUM_VALUES: SomeEnum[] = Object.values(SomeEnum);

/**
 * when you fork/branch/clone (different terms for the same process I mean) a
 * root, this creates a new root space and consequently a new b2tfs index and
 * new b2tfs root. this index is used for managing this level of information.
 *
 * so this b2tfs index ibgib's intrinsic data (this interface) largely has to do
 * with tracking b2tfs branch ibgibs and their corresponding b2tfs root spaces.
 *
 * @see {@link IbGib_V1.data}
 */
export interface B2tFSIndexData_V1 extends IbGibData_V1 {
    /**
     * atow (12/2023) i'm designing this to have a one-to-one relationship
     * between space & b2tfs index (this data definition's ibgib). this allows
     * to navigate from the index ibgib to the index's containing space, i.e., a
     * non-directed, id-based soft link instead of a directed, addr-based soft
     * link via `index.rel8ns` or even a directed, addr-based soft link via
     * `index.data`.
     */
    mySpaceId: SpaceId;
    /**
     * spaceId that corresponds to currently active root or branch space. when
     * b2tfs operations are performed (like "adding"/"syncing" the current
     * filesystem state), this is the space/b2tfs system that the operation acts
     * on.
     *
     * _note: you can get a reference to a space via
     * `metaspace.getLocalUserSpace({spaceId...})`._
     *
     * if this is falsy, {@link mySpaceId} is the active space.
     *
     * if this is truthy (and doesn't point to {@link mySpaceId} which
     * would be redundant), then these operations will be delegated to that
     * space. (i don't know atow 12/2023 what happens if that delegated space
     * itself can delegate further but my gut feeling is yes, it simply
     * delegates the operation further.)
     *
     * so if you performed an `add-branch` operation with a path
     * './some_subpath/', then this subpath would be relative to the active
     * space's relative path (not the metaspace or mySpaceId space).
     *
     * ## difference from other vcs' e.g. git
     *
     * most VCSs are file-based + repo-based only. this means that there is always
     * one root which is dictated by the repo. so when you say, e.g., `git add
     * path/file.txt`, it already knows that it has a repo and that repo has a
     * folder tree child structure.
     *
     * in ibgib, a space can "have" multiple roots, both from other "repos" and
     * from "branches" of the source root. so it should be evident that this
     * adds a layer of complexity (and power). but this should simplify overall
     * multi-"repo" composition and derivatives, e.g., devops.
     *
     * each space has only one b2tfs index, so it references other "contained"
     * roots via their space addrs. when synchronizing/merging, we should not be
     * sending index ibgibs (timelines/graphs) as these stay with the local
     * space, and the local space does not move.
     *
     * and when you branch a root, you create a separate space that copies the
     * current space (atow 12/2023 current implementation). if you activate that
     * branch (similar to, e.g., "checkout"/"switch" in git) then this
     * `activeSpaceId` will be set to that branch's space id.
     *
     * so you will continue to use the same shared metaspace, but operations
     * will be performed on the branch's space.
     */
    activeSpaceId?: SpaceId;
    /**
     * todo: not impl yet
     *
     * use this when branching
     *
     * if you created this index's space ({@link mySpaceId}) as a result
     * of a fork/branch/clone type of operation (idk atow 12/2023 what i'm calling
     * this yet) then that source space's id goes here.
     */
    ancestorSpaceId?: SpaceId;
    /**
     * space-qualified addresses to branch ibgib (should be branch ibgib tjp
     * addrs).
     */
    [`$@branch`]: SpaceQualifiedIbGibAddr[];
}

/**
 * rel8ns (named edges/links in DAG) go here.
 *
 * @see {@link IbGib_V1.rel8ns}
 */
export interface B2tFSIndexRel8ns_V1 extends IbGibRel8ns_V1 {
    /**
     * rel8n to the primary root that corresponds to this index's (and thus this
     * index's local space's) branch ibgib temporal junction point (tjp).
     */
    [B2TFS_BRANCH_TJP_REL8N_NAME]?: IbGibAddr[];
}

/**
 * This is a special indexing ibgib.
 *
 * If this is a plain ibgib data only object, this acts as a dto. You may also
 * want to generate a witness ibgib, which is slightly different, for ibgibs
 * that will have behavior (i.e. methods).
 *
 * @see {@link B2tFSIndexData_V1}
 * @see {@link B2tFSIndexRel8ns_V1}
 */
export interface B2tFSIndexIbGib_V1 extends IbGib_V1<B2tFSIndexData_V1, B2tFSIndexRel8ns_V1> {
}

/**
 * Shape of information presented to the user when cmd "b2tfs-info"
 * (status/report/other synonyms) is given.
 */
export interface B2tFSIndexReportInfo {
    /**
     * cwd of the given command
     */
    cwd: string;
    /**
     * global state
     */
    ibGibGlobalThis: IbGibGlobalThis;
    /**
     * id of the space which we're reporting on
     */
    spaceId: SpaceId;
    /**
     * name of the space which we're reporting on
     */
    spaceName: string;
    /**
     * true if the B2tFSIndex exists
     */
    indexExists?: boolean;
    /**
     * current index ibgib address.
     */
    indexAddr?: IbGibAddr;
    /**
     * index ibgib's temporal junction point address.
     */
    indexTjpAddr?: TjpIbGibAddr;
    /**
     * actual reference to index ibgib, for `console.dir(info)` usage.
     *
     * BE SURE TO DELETE THIS IF YOU NEED A NON-OBJECT, MEMOIZED ONLY INFO
     * OBJECT.
     */
    indexIbGib?: B2tFSIndexIbGib_V1;
    /**
     * branch info for all branches/subbranches in an index
     */
    branchInfos?: { [branchSQIAddr: string]: B2tFSBranchInfo | B2tFSBranchInfo_Terse };
}
