/**
 * @module b2tfs helper/util/etc. functions
 *
 * this is where you will find helper functions like those that generate
 * and parse ibs for b2tfs.
 */

// import * as pathUtils from 'path';
// import { statSync } from 'node:fs';
// import { readFile, } from 'node:fs/promises';
// import * as readline from 'node:readline/promises';
// import { stdin, stdout } from 'node:process'; // decide if use this or not

// import { extractErrorMsg, getTimestampInTicks, getUUID, pretty } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
// import { IbGibSpaceAny } from '@ibgib/core-gib/dist/witness/space/space-base-v1.mjs';

import { UUID_REGEXP } from '@ibgib/helper-gib/dist/constants.mjs';
import { extractErrorMsg } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { getIbAndGib, getIbGibAddr } from '@ibgib/ts-gib/dist/helper.mjs';
import { getTjpAddr } from '@ibgib/core-gib/dist/common/other/ibgib-helper.mjs';
import { MetaspaceService } from '@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs';
import { IbGibSpaceAny } from '@ibgib/core-gib/dist/witness/space/space-base-v1.mjs';
import { SpaceId } from '@ibgib/core-gib/dist/witness/space/space-types.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../../../ibgib-constants.mjs';
import { B2tFSIndexData_V1, B2tFSIndexIbGib_V1 } from './b2tfs-index-types.mjs';
import { B2tFSSpecialIbGibType } from './b2tfs-index-constants.mjs';
import { B2tFSBranchIbGib_V1, B2tFSBranchInfo } from '../b2tfs-branch/b2tfs-branch-types.mjs';
import { getB2tFSBranch, validateCommonB2tFSBranchIbGib, validateIntegrityOfB2tFSBranchGraph } from '../b2tfs-branch/b2tfs-branch-helper.mjs';
import { mut8SpecialIbGib } from '../../../../../ibgib-helper.mjs';
import { SpaceQualifiedIbGibAddr, getSpaceQualifiedIbGibAddr, parseSpaceQualifiedIbGibAddr } from '../common/b2tfs-common-helper.mjs';

/**
 * for verbose logging
 */
const logalot = GLOBAL_LOG_A_LOT;

/**
 *
 * @returns the latest B2tFS index
 */
export async function createNewB2tFSIndexIbGib({
    metaspace,
    space,
    ancestorSpaceId,
}: {
    metaspace: MetaspaceService,
    space?: IbGibSpaceAny,
    ancestorSpaceId?: SpaceId,
}): Promise<B2tFSIndexIbGib_V1> {
    const lc = `[${createNewB2tFSIndexIbGib.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: acc9f91147c442338ae1d2e7c1a57e34)`); }

        // validate
        space ??= await metaspace.getLocalUserSpace({ lock: true });
        if (!space) { throw new Error(`(UNEXPECTED) space arg falsy and couldn't get local user space? (E: 91dc7411088848a9ae240410fd76464c)`); }
        if (!space.data) { throw new Error(`(UNEXPECTED) space.data falsy? (E: 6110ce6cac1e4a76a370b04ba48d00ef)`); }
        if (ancestorSpaceId) {
            if (!ancestorSpaceId.match(UUID_REGEXP)) {
                throw new Error(`(UNEXPECTED) invalid ancestorSpaceId (${ancestorSpaceId})? does not match UUID_REGEXP (${UUID_REGEXP.source}) (E: e341fcb7be679fc8e15ab3bf44af5b23)`);
            }
        }

        // create the initial special ibgib via initialize: true
        let b2tfsIndexIbGib = await metaspace.getSpecialIbGib({
            type: B2tFSSpecialIbGibType.b2tfs,
            initialize: true,
            lock: true,
            space,
        }) as B2tFSIndexIbGib_V1;
        if (!b2tfsIndexIbGib) {
            throw new Error(`could not create B2tFSIndex ibGib not found in space (${space?.data?.name}: ${space?.data?.uuid}). If you have not done so, you must initialize the B2tFS Index in this space (spaceId: ${space?.data?.uuid}) first. if you think you have already initialized, maybe it was a different space? (E: a7296e77a39c46c9829db43f3301b06f)`);
        }

        // customize special ibGib
        if (logalot) { console.log(`${lc} newly created index so initializing it... (I: 12dab62e6e164005b08b61f39093e33e)`); }
        const dataToAddOrPatch: B2tFSIndexData_V1 = {
            mySpaceId: space.data.uuid,
            ancestorSpaceId: ancestorSpaceId ?? undefined,
            // activeSpaceId: , // undefined at this point
            "$@branch": [],
        };
        b2tfsIndexIbGib = await mut8SpecialIbGib({
            specialType: B2tFSSpecialIbGibType.b2tfs,
            specialIbGib: b2tfsIndexIbGib,
            dataToAddOrPatch,
            metaspace,
            space,
        }) as B2tFSIndexIbGib_V1;

        // return it
        return b2tfsIndexIbGib;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 *
 * @returns the latest B2tFS index
 */
export async function getB2tFSIndexIbGib({
    metaspace,
    space,
    throwIfNotFound,
}: {
    metaspace: MetaspaceService,
    space?: IbGibSpaceAny,
    throwIfNotFound?: boolean,
}): Promise<B2tFSIndexIbGib_V1 | undefined> {
    const lc = `[${getB2tFSIndexIbGib.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: edb4318fabc129425fa9405eb4167b23)`); }

        // validate
        space ??= await metaspace.getLocalUserSpace({ lock: true });
        if (!space) { throw new Error(`(UNEXPECTED) space arg falsy and couldn't get local user space? (E: 5e99c951caa93bb7761f5df84bc73923)`); }
        if (!space.data) { throw new Error(`(UNEXPECTED) space.data falsy? (E: 8007f38297f7f5db96b8896e2488cc23)`); }

        // get it
        const b2tfsIndexIbGib = await metaspace.getSpecialIbGib({
            type: B2tFSSpecialIbGibType.b2tfs,
            initialize: false,
            lock: true,
            space,
        }) as B2tFSIndexIbGib_V1;
        if (!b2tfsIndexIbGib) {
            if (throwIfNotFound) {
                throw new Error(`could not get B2tFSIndex ibGib not found in space (${space?.data?.name}: ${space?.data?.uuid}). If you have not done so, you must initialize the B2tFS Index in this space (spaceId: ${space?.data?.uuid}) first. if you think you have already initialized, maybe it was a different space? (E: cf4cb6dfdfd9897455a9c752851e0323)`);
            }
        }

        // return it
        return b2tfsIndexIbGib;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * registers a branch with a superSpace.
 *
 * ## intent
 *
 * when we create a branch, we put it in a different space. we can't then
 * directly hard-link/rel8 to that branch from the originating "super"-space,
 * because the branch ibgib is located in that new space and not in the
 * superSpace. we can, however, soft-rel8 the branch space (via spaceId atow
 * 12/2023) and there is a one-to-one correspondence between branch space to
 * branch ibgib via the branch's b2tfs index.
 */
export async function addB2tFSBranchRefToSuperSpace({
    metaspace,
    branchSpace,
    branchIndex,
    branchIbGib,
    superIndex,
    superSpace,
}: {
    metaspace: MetaspaceService,
    branchSpace: IbGibSpaceAny | undefined,
    /**
     * @optional B2tFS index ibgib. pass this in if you have a reference to it,
     * otherwise it will be gotten from the `metaspace`+`branchSpace`.
     */
    branchIndex: B2tFSIndexIbGib_V1 | undefined,
    branchIbGib: B2tFSBranchIbGib_V1 | undefined,
    superSpace: IbGibSpaceAny | undefined,
    /**
     * @optional B2tFS index ibgib. pass this in if you have a reference to it,
     * otherwise it will be gotten from the `metaspace`+`superSpace`.
     */
    superIndex?: B2tFSIndexIbGib_V1 | undefined,
}): Promise<void> {
    const lc = `[${addB2tFSBranchRefToSuperSpace.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 985b41300ab20548d6a0c341db434223)`); }

        // #region prepare branch related

        if (!branchSpace && !branchIndex) { throw new Error(`either branchSpace/Index required. (E: 28b812130b6295af57a9742b28bf3723)`); }

        let branchSpaceId: string | undefined;
        if (branchSpace) {
            branchSpaceId = branchSpace.data?.uuid;
        } else {
            if (!branchIndex!.data?.mySpaceId) { throw new Error(`(UNEXPECTED) branchIndex.data.mySpaceId falsy? (E: 7b9bcf6d798d13a15b85fe697ba6f723)`); }
            branchSpaceId = branchIndex!.data?.mySpaceId;
            branchSpace = await metaspace.getLocalUserSpace({ localSpaceId: branchSpaceId });
            if (!branchSpace) { throw new Error(`couldn't get branchSpace even via metaspace + id? branchSpaceId: ${branchSpaceId} (E: 242b286518311f579ee843d32156d123)`); }
        }

        // branch ibgib
        if (!branchIbGib) {
            const branchInfo = await getB2tFSBranch({ metaspace, branchSpace, branchIndex });
            branchIbGib = branchInfo?.ibGib;
        }
        if (!branchIbGib) { throw new Error(`couldn't get branchIbGib and wasn't provided by caller. (E: d876f2d8f73cd1bce74d8c1e01c1af23)`); }

        // validate branch ibgib before adding its reference to superspace via
        // branchSpace id
        const branchIbGibValidationErrors =
            await validateCommonB2tFSBranchIbGib({ ibGib: branchIbGib }) ?? [];
        if (branchIbGibValidationErrors.length > 0) { throw new Error(`invalid branchIbGib: ${branchIbGibValidationErrors} (E: a11be99dc8f8c936ee1a4aceb961be23)`); }
        const branchIbGibGraphIntegrityErrors =
            await validateIntegrityOfB2tFSBranchGraph({
                branchIbGib,
                metaspace,
                space: branchSpace,
            }) ?? [];
        if (branchIbGibGraphIntegrityErrors.length > 0) { throw new Error(`branchIbGib (${getIbGibAddr({ ibGib: branchIbGib })}) dependency graph has integrity errors: ${branchIbGibGraphIntegrityErrors} (E: 372683a605ff4bfa90667d74f7375875)`); }

        // #endregion prepare branch related

        // #region prepare superspace related

        if (!superSpace && !superIndex) { throw new Error(`either superSpace/Index required. (E: de2c8e439f5a42229f224f2e8b77c4c9)`); }

        let superSpaceId: string | undefined;
        if (superSpace) {
            superSpaceId = superSpace.data?.uuid;
        } else {
            if (!superIndex!.data?.mySpaceId) { throw new Error(`(UNEXPECTED) superIndex.data.mySpaceId falsy? (E: 793d4e5b603e43a2a20d9128acee76dc)`); }
            superSpaceId = superIndex!.data?.mySpaceId;
            superSpace = await metaspace.getLocalUserSpace({ localSpaceId: superSpaceId });
            if (!superSpace) { throw new Error(`couldn't get superSpace even via metaspace + id? superSpaceId: ${superSpaceId} (E: 2fbc73e3d24941d6b27f50ec5e283315)`); }
        }

        superIndex ??= await getB2tFSIndexIbGib({ metaspace, space: superSpace });
        if (!superIndex) { throw new Error(`couldn't get superIndex? (E: 5374ab98fd7b4e03cce60217358b3b23)`); }

        // #endregion prepare superspace related

        // we now have guaranteed branchSpace/SpaceId/Index/IbGib and superSpace/Index

        const branchAddr = getIbGibAddr({ ibGib: branchIbGib });
        const branchTjpAddr = getTjpAddr({ ibGib: branchIbGib, defaultIfNone: 'incomingAddr' })!;
        const branchTjpGib = getIbAndGib({ ibGibAddr: branchTjpAddr }).gib;

        /**
         * this is my initial attempt at a space+addr "qualified" ibgib address.
         * @see {@link parseSpaceQualifiedIbGibAddr}
         */
        const branchIbGibTjp_SpaceQualifiedAddr = getSpaceQualifiedIbGibAddr({
            spaceId: branchSpaceId,
            ibGibAddr: branchTjpAddr,
        });

        // check if root timeline is already in the b2tfs index via branch's
        // tjp. atow (10/2023) i'm thinking this should only be one timeline per
        // branch per index

        const existingBranchSpaceQualifiedAddrs: SpaceQualifiedIbGibAddr[] =
            superIndex?.data?.['$@branch'] ?? [];
        const timelineIsAlreadyIndexed = existingBranchSpaceQualifiedAddrs.some(existingSpaceQualifiedBranchAddr => {
            if (existingSpaceQualifiedBranchAddr === branchIbGibTjp_SpaceQualifiedAddr) {
                // space-qualified matches
                return true; /* <<<< returns early */
            }
            const { ibGibAddr, gib: existingTjpGib } =
                parseSpaceQualifiedIbGibAddr({ spaceQualifiedAddr: existingSpaceQualifiedBranchAddr });
            const addrMatchesExactly = ibGibAddr === branchAddr || ibGibAddr === branchTjpAddr;
            const addrSharesTjpGib = branchTjpGib === existingTjpGib;
            return addrMatchesExactly || addrSharesTjpGib;
        });
        if (timelineIsAlreadyIndexed) { throw new Error(`the branchIbGib timeline (${branchIbGibTjp_SpaceQualifiedAddr}) is already indexed by the superspace b2tFS index ibGib (${getSpaceQualifiedIbGibAddr({ spaceId: superSpaceId, ibGib: superIndex })}). (E: 207a59985fc2a2105432dd2b2ca8c223)`); }

        // not already indexed, so add the space-qualified addr reference to the
        // index's data.
        const _newB2tFSIndex = await mut8SpecialIbGib<B2tFSIndexData_V1>({
            specialType: B2tFSSpecialIbGibType.b2tfs,
            dataToAddOrPatch: {
                "$@branch": [...existingBranchSpaceQualifiedAddrs, branchIbGibTjp_SpaceQualifiedAddr],
            } satisfies Partial<B2tFSIndexData_V1>,
            metaspace,
            space: superSpace,
            specialIbGib: superIndex,
        });
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * gets the currently active branch in the given {@link space} or user's local
 * space for the given `metaspace`.
 *
 * @returns the currently active B2tFS branch IbGib
 */
export async function getActiveB2tFSBranchInfo({
    b2tfsIndexIbGib,
    metaspace,
    space,
    alreadySpaceIds = [],
}: {
    /**
     * @optional B2tFS index ibgib. pass this in if you have a reference to it,
     * otherwise it will be gotten from the `metaspace`+`space`.
     */
    b2tfsIndexIbGib?: B2tFSIndexIbGib_V1 | undefined | null,
    /**
     * hey it's meta
     */
    metaspace: MetaspaceService,
    /**
     * space to look in if you can't provide b2tfs index
     */
    space: IbGibSpaceAny,
    /**
     * DON'T PASS IN ANYTHING FOR THIS, AS THIS IS USED @internal -ly.
     * (I JUST DON'T WANT TO WRITE ANOTHER FUNCTION TO HIDE THIS)
     *
     * space ids already called for active branch info to avoid infinite
     * recursion.
     *
     * ## intent
     *
     * the reason i'm adding this is to avoid cyclic calls/infinite recursion.
     * If there is a space that delegates to another space that delegates to the
     * original space, then the cycle is invalid and an error must be thrown.
     */
    alreadySpaceIds?: SpaceId[],
}): Promise<B2tFSBranchInfo | undefined> {
    const lc = `[${getActiveB2tFSBranchInfo.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 4fae72b181a4b2feed63f8873b2bda23)`); }
        alreadySpaceIds ??= [];

        // initial validate and sanity
        let { data: spaceData, rel8ns: spaceRel8ns } = space;
        if (!spaceData) { throw new Error(`(UNEXPECTED) b2tfsIndexIbGib.data falsy? (E: 9235605b18b34f09b0d2e86a92a357aa)`); }
        if (!spaceData.uuid) { throw new Error(`(UNEXPECTED) spaceData.uuid falsy? (E: 103f32bb229a130cb39df6410c667523)`); }

        if (alreadySpaceIds.includes(spaceData.uuid)) {
            throw new Error(`cyclic active space error. this occurs if you have one space delegate to another active space that in turn points back to a previous space.  (E: 550e8caaba4c8c3a046fe19294a00f23)`);
        } else {
            alreadySpaceIds.push(spaceData.uuid);
        }

        // #region ensure index ibgib (and that it belongs to incoming space)
        if (b2tfsIndexIbGib) {
            if (logalot) { console.log(`${lc} b2tfsIndexIbGib truthy (provided by caller). confirming it matches incoming space. (I: 671b0133e6f26f7c2d523f0ce9f6d523)`); }

        } else {
            if (logalot) { console.log(`${lc} b2tfsIndexIbGib falsy. (I: 23bdbf1c2f532fe92bbf6a2f3eefad23)`); }
            // index NOT provided, so get that first
            b2tfsIndexIbGib = await getB2tFSIndexIbGib({ metaspace, space, throwIfNotFound: true });
        }
        if (logalot) { console.log(`${lc} b2tfsIndexIbGib addr: ${getIbGibAddr({ ibGib: b2tfsIndexIbGib })} (I: f0f17de9cc867e524cc977dc0c2af623)`); }
        if (b2tfsIndexIbGib?.data?.mySpaceId !== spaceData.uuid) {
            throw new Error(`(UNEXPECTED) caller provided a b2tfsIndexIbGib but it did not belong the given space (id: ${space?.data?.uuid})? (E: b20c532d3bc4a58a7a2295b715057c23)`);
        }
        // #endregion ensure index ibgib (and that it belongs to incoming space)

        // prepare/validate sanity
        let { data: indexData, rel8ns: indexRel8ns } = b2tfsIndexIbGib!;
        if (!indexData) { throw new Error(`(UNEXPECTED) b2tfsIndexIbGib.data falsy? (E: f315954b7947922354d3356f31d22423)`); }

        let { activeSpaceId, mySpaceId, ancestorSpaceId } = indexData;
        /**
         * first we need to get the active space. this may be the incoming
         * `space` or it could be delegated from given `space` to another space
         */
        let activeSpace: IbGibSpaceAny;
        let activeIndexIbGib: B2tFSIndexIbGib_V1;

        // may be falsy if we haven't added any roots/branches to the index/space or if
        // we removed our last one from the index/space
        if (!activeSpaceId) {
            if (logalot) { console.log(`${lc} index.data.activeSpaceId falsy. returning undefined (I: 52ef181043d65eaf184dc60c07428d23)`); }
            return undefined; /* <<<< returns early */
        }

        // we have an active space id...hey maybe it's the space provided by
        // caller which makes it easy
        if (activeSpaceId === spaceData.uuid) {
            if (logalot) { console.log(`${lc} activeRootSpaceId (${activeSpaceId}) matches given space. (I: 93eea3dcac756d7fefa781d21e216423)`); }
            activeSpace = space;
            activeIndexIbGib = b2tfsIndexIbGib;
        } else {
            // space delegates to another space
            if (logalot) { console.log(`${lc} delegating to a different root space but ensure we can get a reference to it (I: 6ba016903853b3d01be937e590c04823)`); }
            const resGetSpace = await metaspace.getLocalUserSpace({ localSpaceId: indexData.activeSpaceId });
            if (!resGetSpace) { throw new Error(`unable to get space via metaspace using index.data.activeRootSpaceId (addr: ${indexData.activeSpaceId}). (E: 2a3241b6eefaafaeb61696b61517b923)`); }
            // we do have a ref to the delegated root space
            activeSpace = resGetSpace;
            activeIndexIbGib = (await getB2tFSIndexIbGib({ metaspace, space: activeSpace, throwIfNotFound: true }))!;
        }

        // need to call recursively if the activeIndex itself has an activeSpaceId
        if (activeIndexIbGib.data?.activeSpaceId && activeIndexIbGib.data!.activeSpaceId !== activeSpace.data!.uuid) {
            // active index itself delegates to another space
            const delegatedSpaceId = activeIndexIbGib.data.activeSpaceId;
            if (logalot) { console.log(`${lc} active space (${activeSpace.data!.uuid}) index itself delegates to another delegatedSpaceId (${delegatedSpaceId})(I: 6701bbbf7c5197e617144cbebb810b23)`); }
            const delegatedSpace = await metaspace.getLocalUserSpace({ localSpaceId: delegatedSpaceId });
            if (!delegatedSpace) { throw new Error(`couldn't get delegated space. the current iteration's active space (${activeSpace.data!.uuid}) index itself has a delegated activeSpaceId (${delegatedSpaceId}). but this delegatedSpaceId could not be gotten via metaspace.getLocalUserSpace (E: 1b0711853d6e27f3de843de2cfccdb23)`); }
            return await getActiveB2tFSBranchInfo({ metaspace, space: delegatedSpace, });
        }

        // we have the actual active space/index (i.e. doesn't delegate)

        if (!activeIndexIbGib.rel8ns) { throw new Error(`(UNEXPECTED) activeIndexIbGib.rel8ns falsy? (E: 95044dc3d982b54981241b7ac71aa823)`); }
        const activeBranchTjpAddrs = activeIndexIbGib.rel8ns.branch_tjp;
        if (!activeBranchTjpAddrs) { throw new Error(`(UNEXPECTED) activeBranchTjpAddrs falsy? (E: a3c8a86bfabe4cda0bc4f541db516823)`); }
        if (activeBranchTjpAddrs.length !== 1) { throw new Error(`(UNEXPECTED) activeBranchTjpAddrs.length !== 1? (E: 58d7c1ac7ddca4ba77bf2f0cb676fa23)`); }

        const branchIbGibAddr = await metaspace.getLatestAddr({
            tjpAddr: activeBranchTjpAddrs[0],
            space: activeSpace,
        });
        const resGetBranchIbGib = await metaspace.get({
            addr: branchIbGibAddr,
            space: activeSpace,
        });
        if (!resGetBranchIbGib.success || (resGetBranchIbGib.ibGibs ?? []).length !== 1) {
            throw new Error(`resGetBranchIbGib.success falsy. emsg: ${resGetBranchIbGib.errorMsg ?? '[unknown error](E: 0f2100dfe5e74cc89079509f83b29816)'} (E: db511683999606b71b0cea5993095523)`);
        }

        const branchIbGib = resGetBranchIbGib.ibGibs![0] as B2tFSBranchIbGib_V1;

        if (logalot) {
            console.log(`${lc} console.dir(branchIbGib)... (I: 4a3275cdc474e20e42dada4579c02423)`);
            console.dir(branchIbGib);
        }

        return {
            indexAddr: getIbGibAddr({ ibGib: activeIndexIbGib }),
            name: branchIbGib.data!.name!,
            spaceId: activeSpaceId,
            spaceName: activeSpace.data!.name,
            ibGib: branchIbGib,
            index: activeIndexIbGib,
            space: activeSpace,
        };
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export async function activateB2tFSBranch({
    superSpace,
    metaspace,
    branchIndex,
    branchSpace,
    branchSpaceId,
}: {

    /**
     * the context space in which we are setting the active branch.
     *
     * if truthy, we mut8 this space's b2tfs index {@link B2tFSIndexData_V1.activeSpaceId}
     * to point to the incoming {@link branchSpaceId}.
     *
     * if falsy, the context space will be gotten via
     * `metaspace.getLocalUserSpace(...)`
     */
    superSpace?: IbGibSpaceAny,
    /**
     * our ibgib app's access to the local metaspace (kinda like a node).
     */
    metaspace: MetaspaceService,
    /**
     * b2tfs index of the branch that we wish to activate.
     *
     * if the caller has it, use this
     *
     * need either {@link branchSpace} or {@link branchIndex} or {@link branchSpaceId}
     */
    branchIndex?: B2tFSIndexIbGib_V1,
    /**
     * space that contains b2tfs index of the branch that we wish to activate.
     *
     * need either {@link branchSpace} or {@link branchIndex} or {@link branchSpaceId}
     */
    branchSpace?: IbGibSpaceAny,
    /**
     * id of space that contains b2tfs index of the branch that we wish to activate.
     *
     * if this is the only way to get the branch index, this will require
     * loading the space, then getting the b2tfs index from that space.
     *
     * need either {@link branchSpace} or {@link branchIndex} or {@link branchSpaceId}
     */
    branchSpaceId?: SpaceId,
}): Promise<B2tFSIndexIbGib_V1> {
    const lc = `[${activateB2tFSBranch.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: a8c8567d5a9d576fff4ffead11f4d823)`); }

        // validate
        if (!branchSpaceId && !branchIndex && !branchSpace) { throw new Error(`at least one branch identifier is required for this function. (E: 7dae46f678a7ec25bbeaaca51b9b7423)`); }

        if (superSpace) {
            if (logalot) { console.log(`${lc} superSpace truthy. (I: 228f75d1b5af234514d81d4330bb2a23)`); }
        } else {
            if (logalot) { console.log(`${lc} superSpace falsy. getting via metaspace (I: 2b205f45b9af758e830b575c446a0423)`); }
            superSpace = await metaspace.getLocalUserSpace({ lock: false });
            if (!superSpace) { throw new Error(`(UNEXPECTED) superSpace not provided and couldn't get from metaspace local user space? (E: 065c6b66fb4b5931c9a845e24f100323)`); }
        }

        const contextB2tFSIndex = (await getB2tFSIndexIbGib({
            metaspace, space: superSpace, throwIfNotFound: true
        }))!;

        if (!branchSpaceId) {
            if (branchSpace) {
                if (logalot) { console.log(`${lc} setting branchSpaceId via branchSpace (I: 35594ff6fdcca5d024ec55d1b7a23123)`); }
                if (!branchSpace.data) { throw new Error(`(UNEXPECTED) branchSpace.data falsy? (E: 411f8dade77843f0877a751bc301be23)`); }
                branchSpaceId = branchSpace.data.uuid!;
            } else {
                if (logalot) { console.log(`${lc} both branchSpace & branchSpaceId falsy. getting branch  (I: 9a95ab663bfe05580cb2e5d60f76be23)`); }
                if (!branchIndex!.data) { throw new Error(`(UNEXPECTED) branchB2tFSIndexIbGib.data falsy? (E: 77de85c4a433857c8e5b6f5f3005d223)`); }
                branchSpaceId = branchIndex!.data.mySpaceId;
            }
        }
        if (!branchSpaceId) { throw new Error(`(UNEXPECTED) branchSpaceId still falsy? (E: e8e6c40a95ce9dade8b57e681ea41223)`); }

        // make sure it's not already active
        if (!contextB2tFSIndex.data) { throw new Error(`contextB2tFSIndex.data falsy? (E: 1d9d22c60c56a5644e58157f4a303d23)`); }
        if (contextB2tFSIndex.data.activeSpaceId === branchSpaceId) {
            console.warn(`${lc} branchSpaceId already activated (W: 8d8a57ca8ddc484ea190e0f4e7344ec9)`);
            return contextB2tFSIndex; /* <<<< returns early */
        }

        // update the index
        const newContextB2tFSIndex = await mut8SpecialIbGib<B2tFSIndexData_V1>({
            specialType: B2tFSSpecialIbGibType.b2tfs,
            specialIbGib: contextB2tFSIndex,
            dataToAddOrPatch: {
                activeSpaceId: branchSpaceId,
            } satisfies Partial<B2tFSIndexData_V1>,
            metaspace,
            space: superSpace,
        });

        return newContextB2tFSIndex as B2tFSIndexIbGib_V1;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}
