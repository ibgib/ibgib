/**
 * @module b2tfs-branch helper/util/etc. functions
 *
 * this is where you will find helper functions like those that generate
 * and parse ibs for b2tfs-branch.
 */

import * as pathUtils from 'path';
import { cwd } from 'process';
// import { statSync } from 'node:fs';
// import { readFile, } from 'node:fs/promises';
// import * as readline from 'node:readline/promises';
// import { stdin, stdout } from 'node:process'; // decide if use this or not

import {
    extractErrorMsg, delay, getSaferSubstring,
    getTimestampInTicks, getUUID, pretty, getTimestamp,
} from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { RCLIArgInfo, RCLIParamInfo } from '@ibgib/helper-gib/dist/rcli/rcli-types.mjs';
import { extractArgValue } from '@ibgib/helper-gib/dist/rcli/rcli-helper.mjs';
import { PARAM_INFO_BARE, PARAM_INFO_NAME } from '@ibgib/helper-gib/dist/rcli/rcli-constants.mjs';
import { UUID_REGEXP } from '@ibgib/helper-gib/dist/constants.mjs';
import { Ib, TransformResult, } from '@ibgib/ts-gib/dist/types.mjs';
import { Factory_V1 } from '@ibgib/ts-gib/dist/V1/factory.mjs';
import { validateIbGibIntrinsically } from '@ibgib/ts-gib/dist/V1/validate-helper.mjs';
import { getIbGibAddr } from '@ibgib/ts-gib/dist/helper.mjs';
import { MetaspaceService } from '@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs';
import { IbGibSpaceAny } from '@ibgib/core-gib/dist/witness/space/space-base-v1.mjs';
import { SpaceId } from '@ibgib/core-gib/dist/witness/space/space-types.mjs';
import { CommentIbGib_V1 } from '@ibgib/core-gib/dist/common/comment/comment-types.mjs';
import { getTjpAddr, toDto } from '@ibgib/core-gib/dist/common/other/ibgib-helper.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../../../ibgib-constants.mjs';
import {
    B2tFSBranchData_V1, B2tFSBranchRel8ns_V1, B2tFSBranchIbGib_V1, B2tFSBranchInfo, B2tFSBranchDiff,
} from './b2tfs-branch-types.mjs';
import { B2TFS_BRANCH_ATOM, B2TFS_ITEM_REL8N_NAME, } from './b2tfs-branch-constants.mjs';
import { B2tFSItemIbGib_V1, B2tFSType } from '../b2tfs-item/b2tfs-item-types.mjs';
import {
    getB2tFSItemDiff,
    getBranchRootItem,
} from '../b2tfs-item/b2tfs-item-helper.mjs';
import { B2tFSIndexIbGib_V1, B2tFSIndexReportInfo } from '../b2tfs-index/b2tfs-index-types.mjs';
import { RCLICommandHandlerArg } from '../../rcli-types.mjs';
import { extractArg_space, pathIsAsExpected } from '../../rcli-helper.mjs';
import {
    PARAM_INFO_SPACE_ID, PARAM_INFO_SPACE_NAME,
    PARAM_INFO_SRC_ID, RCLI_DEFAULT_OUTPUT_PATH
} from '../../rcli-constants.mjs';
import { B2TFS_DEFAULT_ITEM_FILTER_PATTERNS } from '../b2tfs-item/b2tfs-item-constants.mjs';
import { B2TFS_BRANCH_TJP_REL8N_NAME, B2TFS_NAME_REGEXP } from '../common/b2tfs-common-constants.mjs';
import { getActiveB2tFSBranchInfo, getB2tFSIndexIbGib } from '../b2tfs-index/b2tfs-index-helper.mjs';
import { B2tFSSpecialIbGibType } from '../b2tfs-index/b2tfs-index-constants.mjs';
import { parseSpaceQualifiedIbGibAddr } from '../common/b2tfs-common-helper.mjs';


/**
 *
 * for verbose logging
 */
const logalot = GLOBAL_LOG_A_LOT;


/**
 * creates a new B2tFS branch ibgib that acts (atow) as the analog to a version
 * control system's root folder for a repo.
 *
 * So in existing version control systems, you have a repo and then you have
 * some other indexing facility to manage multiple repos.  For example with Team
 * Foundation Server, you have the TFS app manage repos that are located at
 * different locations on a filesystem. And in git, you have another provider,
 * e.g. GitHub or GitLab, that manages associating users to repos as well as other
 * configuration.
 *
 * But in ibGib, we enable more dynamic composition of timelines where there is
 * no outside management of the "repos". This is essentially a meta repo (a repo
 * of repos) and repos are timelines - and guess what: our system is designed
 * to handle timelines.
 *
 * So we create B2tFSBranchs to provide backwards compatibility to handle the concept
 * of some semblance of a hierarchical base location.
 *
 * ## notes
 *
 * * this **DOES**...
 *   * create the sub-items (files/folders) that may exist in the filesystem's
 *     given `inputPath` using filterPatterns (auto filters subspaces e.g.
 *     branches).
 *   * create the branch ibgib itself (and intermediate ibgibs/dna, i.e., root graph)
 *   * persist the root graph in the metaspace's local space
 *   * register the branch ibgib in the metaspace's local space
 *   * add the root to the local space's b2tfs index.
 *
 * @see {@link B2tFSIndexIbGib_V1}
 * @see {@link B2tFSBranchIbGib_V1}
 */
export async function createNewB2tFSBranchIbGib({
    absoluteBranchPath,
    filterPatterns = B2TFS_DEFAULT_ITEM_FILTER_PATTERNS,
    branchName,
    metaspace,
    branchSpace,
    branchIndex,
    applyCommentIbGib,
}: {
    /**
     * should e the absolute/resolved path that the user types  as the path to add the root.
     *
     * pathUtils.resolve(initial cwd + branchPath)
     */
    absoluteBranchPath: string,
    filterPatterns: string[],
    /**
     * must be "safe", i.e., simple regex of characters with a couple other
     */
    branchName: string,
    metaspace: MetaspaceService,
    /**
     * this is the space in which we are creating the branch.
     *
     * ## notes
     *
     * when creating a branch, we create a new space to house all of that
     * branch's ibgibs.
     */
    branchSpace: IbGibSpaceAny,
    /**
     * index must exist in the branch space before we create the branch ibgib.
     */
    branchIndex?: B2tFSIndexIbGib_V1,
    /**
     * comment ibgib for logging, e.g. "initial commit".
     *
     * note however that since all the data are ibgibs, we don't need to think
     * of just "one log message".
     */
    applyCommentIbGib: CommentIbGib_V1,
}): Promise<B2tFSBranchIbGib_V1> {
    const lc = `[${createNewB2tFSBranchIbGib.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 7bd54bf7fd75184dbc4bb8ee46ad4c23)`); }


        let diff = await getB2tFSItemDiff({
            existingItem: undefined,
            diffTransactionId: await getUUID(),
            inputPath: absoluteBranchPath,
            filterPatterns,
            deferred: false,
            applyCommentIbGib,
            metaspace,
            space: branchSpace,
        });
        if (!diff) { throw new Error(`(UNEXPECTED) diff falsy? we're creating a new item so diff should always be truthy (with only additions) (E: d3c22b0f9b9455475d4ab213e66a1824)`); }
        if (!diff.transformResults) { throw new Error(`(UNEXPECTED) diff.transformResults falsy? (E: 6e3286b55d4862239a72d4f33c70c724)`); }
        if (diff.transformResults.length === 0) { throw new Error(`(UNEXPECTED) diff.transformResults.length === 0? (E: 2763ee4ebb7f504d2124520fa98df824)`); }
        const b2tfsItemIbGib = diff.transformResults.at(-1)!.newIbGib as B2tFSItemIbGib_V1;
        if (logalot) {
            console.log(`${lc} b2tfsItemIbGib created. console.dir(b2tfsItemIbGib)... (I: fd6a6524e7f28ce28c2b0dc96f431c23)`);
            console.dir(b2tfsItemIbGib);
        }
        const b2tfsItemAddr = getIbGibAddr({ ibGib: b2tfsItemIbGib });

        // need to calculate the relative path from local user space to the
        // absoluteBranchPath
        const metaspacePath = cwd();
        const relativePathFromMetaspaceToFSPath =
            pathUtils.relative(metaspacePath, absoluteBranchPath);
        if (logalot) { console.log(`${lc} relativePathFromMetaspaceToFSPath: ${relativePathFromMetaspaceToFSPath} (I: 18859e4e52964f55a94156efb1b5e062)`) }

        const data: B2tFSBranchData_V1 = {
            name: branchName,
            uuid: await getUUID(),
            timestamp: getTimestamp(),
            relativePathFromMetaspaceToFSPath,
        };
        const errors = validateCommonB2tFSBranchData({ data });
        const rel8ns: B2tFSBranchRel8ns_V1 = {
            [B2TFS_ITEM_REL8N_NAME]: [b2tfsItemAddr],
        }

        if (errors.length > 0) { throw new Error(`B2tFS Branch Data has validation errors: ${errors.join('|')} (E: 53fa1a28bd8efcc32beca9c61b26e423)`); }

        const resBranchIbGib = await Factory_V1.firstGen({
            ib: getB2tFSBranchIb({ data }),
            parentIbGib: Factory_V1.primitive({ ib: B2TFS_BRANCH_ATOM }),
            data,
            rel8ns,
            dna: true,
            nCounter: true,
            tjp: { timestamp: true },
        }) as TransformResult<B2tFSBranchIbGib_V1>;

        const validationErrors = await validateCommonB2tFSBranchIbGib({ ibGib: resBranchIbGib.newIbGib });
        if ((validationErrors ?? []).length > 0) { throw new Error(`(UNEXPECTED) newly created branch ibgib had validation errors? validationErrors: ${validationErrors} (E: 70068e6f418b61c82cac65cabc20b623)`); }

        // persists in the local user space
        await metaspace.persistTransformResult({ resTransform: resBranchIbGib, space: branchSpace });

        // register it to be able for latest addr index tracking
        const newBranchIbGib = resBranchIbGib.newIbGib;
        await metaspace.registerNewIbGib({ ibGib: newBranchIbGib, space: branchSpace });

        // we have persisted the graph, just return the new ibgib
        if (logalot) {
            console.log(`${lc} console.dir(newBranchIbGib)... (I: 2f2ffa0765bdf85e0497a578b922a223)`);
            console.dir(newBranchIbGib);
        }

        // rel8 the new branch ibgib to the branch index
        branchIndex ??= await getB2tFSIndexIbGib({
            metaspace,
            space: branchSpace,
            throwIfNotFound: true,
        });
        const newBranchTjpIbGib = await metaspace.getTjpIbGib({
            ibGib: newBranchIbGib,
            space: branchSpace,
        });
        if (!newBranchTjpIbGib) { throw new Error(`(UNEXPECTED) just made newBranchIbGib (${getIbGibAddr({ ibGib: newBranchIbGib })}) and can't get newBranchTjpIbGib (${getTjpAddr({ ibGib: newBranchIbGib, defaultIfNone: 'undefined' })})? (E: 34cb32bfa0f8dd6b76af6b1590062823)`); }
        const _newBranchIndexAddr = await metaspace.rel8ToSpecialIbGib({
            type: B2tFSSpecialIbGibType.b2tfs,
            rel8nName: B2TFS_BRANCH_TJP_REL8N_NAME,
            space: branchSpace,
            ibGibsToRel8: [newBranchTjpIbGib],
        });
        return newBranchIbGib;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export function validateCommonB2tFSBranchData({
    data,
}: {
    data?: B2tFSBranchData_V1,
}): string[] {
    const lc = `[${validateCommonB2tFSBranchData.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting...`); }
        if (!data) { throw new Error(`b2tFSBranch data required (E: a577222c2685fc245a977591b75afa2e)`); }

        const errors: string[] = [];
        const { name, uuid, relativePathFromMetaspaceToFSPath: fsPathMapping_relativeFromSpace, } = data;

        if (name) {
            if (!name.match(B2TFS_NAME_REGEXP)) {
                errors.push(`name must match regexp: ${B2TFS_NAME_REGEXP} (E: ab4fd09e0f9e4966a7b286eb5bd9c720)`);
            }
        } else {
            errors.push(`name required. (E: a12c869b59634617bad91751209cd156)`);
        }

        if (uuid) {
            if (!uuid.match(UUID_REGEXP)) {
                errors.push(`uuid must match regexp: ${UUID_REGEXP} (E: 3e7bac98033640cfb04d63cf3a93a39d)`);
            }
        } else {
            errors.push(`uuid required. (E: c1f76d21c9f54a34bf3c534a12a1acf7)`);
        }

        if (!fsPathMapping_relativeFromSpace) {
            errors.push(`fsPathMapping_relativeFromSpace required (E: 8ad58797ff144e2492a367c95043ab45)`);
        }

        return errors;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export async function validateCommonB2tFSBranchIbGib({
    ibGib,
}: {
    ibGib: B2tFSBranchIbGib_V1,
}): Promise<string[] | undefined> {
    const lc = `[${validateCommonB2tFSBranchIbGib.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: a1bebe0f651a4ed3fb480e80438ad825)`); }

        const intrinsicErrors = await validateIbGibIntrinsically({ ibGib }) ?? [];

        const dataErrors = validateCommonB2tFSBranchData({ data: ibGib.data })

        const ibErrors: string[] = [];
        let { atom, name, id } = parseB2tFSBranchIb({ ib: ibGib.ib });
        if (name) {
            if (!name.match(B2TFS_NAME_REGEXP)) {
                ibErrors.push(`name must match regexp: ${B2TFS_NAME_REGEXP} (E: ea6f592137704672bcc8830ddb20e512)`);
            }
        } else {
            ibErrors.push(`name required. (E: a4ae1f4da95e490bb38da19f1b114319)`);
        }

        if (id) {
            if (!id.match(UUID_REGEXP)) {
                ibErrors.push(`uuid must match regexp: ${UUID_REGEXP} (E: d85cc9ead81b451f949a2cdf4d413f66)`);
            }
        } else {
            ibErrors.push(`uuid required. (E: 9f0acdf6f9aa48ecbc57cfe4c79344eb)`);
        }

        const result = [...intrinsicErrors, ...dataErrors, ...ibErrors];
        if (result.length > 0) {
            return result;
        } else {
            return undefined;
        }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * builds the branchIbGib.ib from given {@link data}.
 *
 * @returns `${B2TFS_BRANCH_ATOM} ${name} ${uuid}` atow (01/2024)
 */
export function getB2tFSBranchIb({
    data,
}: {
    data: B2tFSBranchData_V1,
}): Ib {
    const lc = `[${getB2tFSBranchIb.name}]`;
    try {
        const validationErrors = validateCommonB2tFSBranchData({ data }); // validates name, uuid
        if (validationErrors.length > 0) { throw new Error(`invalid b2tFSBranch data: ${validationErrors} (E: bcacbfef95286b806e9433534ee8fc66)`); }

        // ad hoc validation here. (if needed)

        const { name, uuid, } = data;
        return `${B2TFS_BRANCH_ATOM} ${name} ${uuid}`;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    }
}

/**
 * Current schema is `${B2TFS_BRANCH_ATOM} ${name} ${uuid}`
 *
 * NOTE this is space-delimited
 *
 * @see {@link B2TFS_BRANCH_ATOM}
 */
export function parseB2tFSBranchIb({
    ib,
}: {
    ib: Ib,
}): {
    atom: string,
    name: string,
    id: string,
} {
    const lc = `[${parseB2tFSBranchIb.name}]`;
    try {
        if (!ib) { throw new Error(`B2tFSBranch ib required (E: 6e04a44a62e63666dc57e908d26d7849)`); }

        const pieces = ib.split(' ');

        return {
            atom: pieces[0],
            name: pieces[1],
            id: pieces[2],
        };
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    }
}

/**
 * @internal helper to get a name from the given arg.
 *
 * if name isn't explicitly given, will use the input path's directory name.
 *
 * @returns name
 */
export async function getNameFromArgInfosOrUseInputPathDirName({
    arg,
    inputPath,
}: {
    arg: RCLICommandHandlerArg,
    inputPath: string,
}): Promise<string> {
    const lc = `[${getNameFromArgInfosOrUseInputPathDirName.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: d81438e9c4b9f87ccbf0c15a3c915523)`); }

        const { argInfos } = arg.cmdInfo;
        if (!argInfos) { throw new Error(`(UNEXPECTED) arg.cmdInfo.argInfos falsy? (E: 0d044e93c8ef39784b9e86baf37df723)`); }
        let name = extractArgValue({ paramInfo: PARAM_INFO_NAME, argInfos, throwIfNotFound: false, }) as string;

        if (!name) {
            // no explicit name given, so pull it from the folder name
            // const inputPath = extractArgValue({
            //     paramInfo: PARAM_INFO_INPUT_PATH,
            //     argInfos, throwIfNotFound: true
            // }) as string;

            const _pathIsFolder = pathIsAsExpected({
                relOrAbsPath: inputPath,
                expectType: 'directory',
                throwIfNotExpected: true,
            });
            // if (!pathIsFolder) { throw new Error(`inputPath is expected to be a directory/folder (E: 8650a62b7965c1b2c966d1942540d823)`); }

            // first resolve our bare input path
            const resolvedInputPath = pathUtils.resolve(inputPath);
            const resolvedInputPathBaseName = pathUtils.basename(resolvedInputPath);
            name = resolvedInputPathBaseName;

            /**
             * the default scenario is doing a branch from a conventional "repo"
             * mindset. This means that we are executing within our repo's root
             * folder which is by default initialized with the ibgib-related
             * space inside the ibgib subfolder (.ibgib atow 11/2023). So if
             * we're inside /user/my-repo/.ibgib, the user actually is working
             * from /user/my-repo and that is the "branchName" that we want to
             * get.
             */
            // let effectiveInputPath = resolvedInputPath.concat();

            // // check if it's relative (!absolute) and === .ibgib . if so adjust effective path
            // if (!pathUtils.isAbsolute(inputPath) &&
            //     resolvedInputPathBaseName === RCLI_DEFAULT_OUTPUT_PATH) {
            //     if (logalot) { console.log(`${lc} inputPath is being adjusted to account for inputPath being the RCLI_DEFAULT_OUTPUT_PATH (${RCLI_DEFAULT_OUTPUT_PATH}). This is because we expect this path to be buried one level inside of the actual path used. For example when you init a b2tFS space, the space itself is usually expected to be in the path/${RCLI_DEFAULT_OUTPUT_PATH} subpath. So if we're inside "/user/my-repo/.ibgib" then we actually want the path one level up ("my-repo") and not the ".ibgib" part. (I: 97de58ebace924b12d4ab61830533d23)`); }
            //     let rawOneLevelUpPath = pathUtils.join(resolvedInputPath, '..');
            //     effectiveInputPath = pathUtils.resolve(rawOneLevelUpPath);
            // }

            // name = pathUtils.basename(effectiveInputPath);
        }
        return name;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * go through the entire dependency graph of the branch to ensure at least that
 * all ibgibs exist in the space (local user space via `metaspace` if `space` is
 * not specified).
 *
 * The idea is that when you sync a branch ibgib to a sync space, you want to
 * verify that it is valid after synchronizing. also, when you pull from a sync
 * space to another local user space, you want to check the integrity before
 * adding the branch to your local b2tfs index.
 *
 * ## intent
 *
 * atow (12/2023) this is to be used when adding a branch to the index.
 *
 */
export async function validateIntegrityOfB2tFSBranchGraph({
    branchIbGib,
    metaspace,
    space,
}: {
    branchIbGib: B2tFSBranchIbGib_V1,
    metaspace: MetaspaceService,
    space: IbGibSpaceAny,
}): Promise<string[] | undefined> {
    const lc = `[${validateIntegrityOfB2tFSBranchGraph.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 347c15ec739a4fd024689e2669664523)`); }
        // let resGraph = await metaspace.getDependencyGraph({ ibGib: branchIbGib, space: space ?? null, });
        // throw new Error(`not impl yet (E: 955ef9d7bd3d753b54e95f850bb7c823)`);
        console.warn(`${lc} validation check not implemented yet. (W: bb8d756f76674c13b47e1f0671caec2d)`)
        return undefined;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * gets branch info (space, index, name) given one or more of these.
 *
 * if you pass in branchName, it will work entirely driven by that and ignore
 * space or index.
 *
 * if you pass in index and/or space, then it will go by those and ignore name.
 *
 * @returns branch info (space, index, name)
 */
export function getB2tFSBranch({
    metaspace,
    branchName,
    branchId,
    branchSpace,
    branchIndex,
}: {
    /**
     * hey it's meta
     */
    metaspace: MetaspaceService,
    /**
     * space to look in if you can't provide b2tfs index
     */
    branchSpace?: IbGibSpaceAny,
    /**
     * @optional B2tFS index ibgib. pass this in if you have a reference to it,
     * otherwise it will be gotten from the `metaspace`+`branchSpace`.
     */
    branchIndex?: B2tFSIndexIbGib_V1 | undefined,
    branchName?: string | undefined,
    branchId?: string | undefined,
}): Promise<B2tFSBranchInfo | undefined> {
    if (branchName) {
        return _getB2tFSBranch_ByName({ metaspace, branchName });
    } else if (branchId) {
        return _getB2tFSBranch_ById({ metaspace, branchId });
    } else if (branchIndex || branchSpace) {
        return _getB2tFSBranch_BySpaceOrIndex({ metaspace, branchSpace, branchIndex });
    } else {
        throw new Error(`either branchName/Id/Space/Index required (E: 8c558be420da8fcaffdf0157e2a78423)`);
    }
}

export async function _getB2tFSBranch_BySpaceOrIndex({
    metaspace,
    branchSpace,
    branchIndex,
}: {
    /**
     * hey it's meta
     */
    metaspace: MetaspaceService,
    /**
     * space to look in if you can't provide b2tfs index
     */
    branchSpace?: IbGibSpaceAny,
    /**
     * @optional B2tFS index ibgib. pass this in if you have a reference to it,
     * otherwise it will be gotten from the `metaspace`+`branchSpace`.
     */
    branchIndex?: B2tFSIndexIbGib_V1 | undefined,
}): Promise<B2tFSBranchInfo | undefined> {
    const lc = `[${_getB2tFSBranch_BySpaceOrIndex.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 611a6404bfd748d4a2dad3e19d0dd5fb)`); }

        // initial validate and sanity
        if (!branchSpace && !branchIndex) { throw new Error(`(UNEXPECTED) both branchSpace and branchIndex falsy? one of those is required. (E: 2ca46384e5eccc2a86352b1fb2fc8b23)`); }

        // ensure we have a branch index
        if (!branchIndex) {
            if (logalot) { console.log(`${lc} branchIndex falsy. will get from branchSpace (I: 8ca966ccd76d867cffecd317226dd823)`); }
            // branchSpace guaranteed truthy
            branchIndex = await getB2tFSIndexIbGib({ metaspace, space: branchSpace, throwIfNotFound: true });
        } else if (!branchSpace) {
            if (logalot) { console.log(`${lc} branchSpace falsy. will get via branchIbGib.data.mySpaceId (I: ef438c51b01fac5542b8c435bad75123)`); }
            if (!branchIndex.data?.mySpaceId) { throw new Error(`(UNEXPECTED) branchIndex.data?.mySpaceId falsy?. branchIndex.data.uuid: ${branchIndex?.data?.uuid} (E: 31ea4475adaf314f99323cb206dcfc23)`); }
            let branchSpaceId = branchIndex.data.mySpaceId;
            branchSpace = await metaspace.getLocalUserSpace({ localSpaceId: branchSpaceId });
            if (!branchSpace) { throw new Error(`branchSpace not provided and could not get it via metaspace. branchSpaceId: ${branchSpaceId} (E: c3a6c17f1f69a06f4209bdb8ff560223)`); }
        }

        // both index and space guaranteed at this point.

        // #region validate branchIndex/Space

        if (!branchIndex) { throw new Error(`couldn't get branchIndex (E: efd3dc4e0fcca2145c7c88c30cdaec23)`); }
        if (!branchSpace) { throw new Error(`couldn't get branchSpace (E: f5de270cb58211faef9c64a6df60c223)`); }

        let { data: spaceData, rel8ns: spaceRel8ns } = branchSpace;
        if (!spaceData) { throw new Error(`(UNEXPECTED) b2tfsIndexIbGib.data falsy ? (E: 598f3b49d35b48cb98a5d56ebfc1ed7a)`); }
        if (!spaceData.uuid) { throw new Error(`(UNEXPECTED) spaceData.uuid falsy ? (E: 22d21805d87e4d13b3c123cf4f1441fb)`); }

        let { data: indexData, rel8ns: indexRel8ns } = branchIndex;
        if (!indexData) { throw new Error(`(UNEXPECTED) branchIndex.data falsy ? (E: e2f9383d83b94935ac8dd6fececd712c)`); }

        if ((indexRel8ns?.branch_tjp ?? []).length === 0) { throw new Error(`branchIndex.rel8ns.branch_tjp empty/falsy? should have exactly one tjp addr here. branchId: ${indexData.uuid} (E: 38a2276de3076791145147d227403e23)`); }
        if ((indexRel8ns?.branch_tjp ?? []).length > 1) { throw new Error(`branchIndex.rel8ns.branch_tjp has more than one addr? should have exactly one tjp addr here. branchId: ${indexData.uuid}. indexRel8ns?.branch_tjp: ${indexRel8ns?.branch_tjp?.join(', ')} (E: 0437b27d2eeb4fbfaf9a25f5757aa870)`); }

        // #endregion validate branchIndex/Space

        // a branch index should have an associated branchIbGib via a
        // hard-linked rel8n with the branchIbGib's tjp
        const branchIbGibTjpAddr = indexRel8ns!.branch_tjp!.at(0);

        // get the latest addr/ibgib for that tjp
        const latestBranchIbGibAddr =
            await metaspace.getLatestAddr({ tjpAddr: branchIbGibTjpAddr, space: branchSpace });
        if (!latestBranchIbGibAddr) { throw new Error(`latestBranchIbGibAddr for branchIbGibTjpAddr (${branchIbGibTjpAddr}) is falsy. could not get this latest addr from branch space (${spaceData.uuid}) (E: c42e69606c4b2bb38bdc6dc54b5fd223)`); }
        const resGet = await metaspace.get({ addr: latestBranchIbGibAddr, space: branchSpace });
        if (!resGet.success || (resGet.ibGibs ?? []).length !== 1) {
            throw new Error(`could not get branch ibgib (${latestBranchIbGibAddr}) from branch space (${spaceData.uuid}) (E: e0a8b1ecb0fb562867336d3f85882123)`);
        }
        const branchIbGib = resGet.ibGibs!.at(0) as B2tFSBranchIbGib_V1;
        const { name: branchName } = parseB2tFSBranchIb({ ib: branchIbGib.ib });

        if (logalot) {
            console.log(`${lc} console.dir(branchIbGib)... (I: bf9eab0db6bb4a3a9804c73b1d8a4c0f)`);
            console.dir(branchIbGib);
        }

        // we now have all of the info
        return {
            indexAddr: getIbGibAddr({ ibGib: branchIndex }),
            name: branchName,
            spaceId: branchSpace.data!.uuid!,
            spaceName: branchSpace.data!.name!,
            space: branchSpace,
            index: branchIndex,
            ibGib: branchIbGib,
            // space: toDto({ ibGib: branchSpace }) as IbGibSpaceAny,
            // index: toDto({ ibGib: branchIndex }),
            // ibGib: toDto({ ibGib: branchIbGib }),
        };
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)} `);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * subfunction that looks through spaces starting at root space looking for
 * branch by name
 */
async function _getB2tFSBranch_ByName({
    metaspace,
    branchName,
}: {
    metaspace: MetaspaceService,
    branchName: string,
}): Promise<B2tFSBranchInfo | undefined> {
    const lc = `[${_getB2tFSBranch_ByName.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 4745712689261a665bbb26bf5b5ce523)`); }

        // todo: fix this to avoid infinite recursion with cyclical branch spaceId refs.
        const searchInIndex: (b2tfsIndex: B2tFSIndexIbGib_V1 | undefined, searchLevel: number) => Promise<B2tFSBranchInfo | undefined> =
            async (b2tfsIndex: B2tFSIndexIbGib_V1 | undefined, searchLevel: number) => {
                if (!b2tfsIndex) {
                    throw new Error(`index at search level (${searchLevel}) falsy. (E: 56ec9cab229bfcb5457ff10f7e990c23)`);
                }
                if (!b2tfsIndex.data) { throw new Error(`(UNEXPECTED) at searchLevel ${searchLevel}, index.data falsy? (E: de0e27e55a07baad34a61cd58d862c23)`); }

                const branchSpaceQualifiedAddrs = b2tfsIndex.data['$@branch'];
                const branchSpaceIdsToSearchOneLevelDeeper: SpaceId[] = [];
                for (let i = 0; i < branchSpaceQualifiedAddrs.length; i++) {
                    const branchSpaceQualifiedAddr = branchSpaceQualifiedAddrs[i];
                    const branchAddrInfo = parseSpaceQualifiedIbGibAddr({
                        spaceQualifiedAddr: branchSpaceQualifiedAddr
                    });
                    const branchIbInfo = parseB2tFSBranchIb({ ib: branchAddrInfo.ib });
                    if (branchIbInfo.name === branchName) {
                        // found a match
                        if (logalot) { console.log(`${lc} branchName match found. branchAddrInfo: ${pretty(branchAddrInfo)} (I: 19d6b8f5bf3a43b2a2b92acac85a5223)`); }
                        const branchSpaceId = branchAddrInfo.spaceId;
                        const branchSpace = await metaspace.getLocalUserSpace({ localSpaceId: branchSpaceId });
                        if (!branchSpace) { throw new Error(`branchName matched with spaceId (${branchSpaceId}) (E: 0fe1ae48f2a6bcf2dcdb515b2de43923)`); }
                        return await _getB2tFSBranch_BySpaceOrIndex({ metaspace, branchSpace });
                    } else {
                        // no match so queue for recursive search if no match found at
                        // this level
                        branchSpaceIdsToSearchOneLevelDeeper.push(branchAddrInfo.spaceId);
                    }
                }
                // not found yet, so search recursively
                for (let i = 0; i < branchSpaceIdsToSearchOneLevelDeeper.length; i++) {
                    const spaceIdToSearch = branchSpaceIdsToSearchOneLevelDeeper[i];
                    const spaceToSearch =
                        await metaspace.getLocalUserSpace({ localSpaceId: spaceIdToSearch });
                    if (!spaceToSearch) { throw new Error(`couldn't get space corresponding to spaceIdToSearch (${spaceIdToSearch}) at searchLevel ${searchLevel}. (E: 9f93375f4163cbeb1f2f2e22cbaad423)`); }
                    let nextIndex = await getB2tFSIndexIbGib({ metaspace, space: spaceToSearch });
                    const result = await searchInIndex(nextIndex, searchLevel + 1);
                    if (result) {
                        return result;
                    }
                }
                // made it this far, none found
                return undefined;
            };

        // start in the root local user space and search for branches starting from there.
        const superSpace = await metaspace.getLocalUserSpace({});
        const superIndex = await getB2tFSIndexIbGib({ metaspace, space: superSpace, throwIfNotFound: false });

        const initialSearchLevel = 0;
        const resBranchInfo = await searchInIndex(superIndex, initialSearchLevel);

        if (logalot) {
            console.log(`${lc} console.dir(resBranchInfo)... (I: cc3ac36a666eb7ad266a90392d993c23)`);
            console.dir(resBranchInfo);
        }

        return resBranchInfo;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * subfunction that looks through spaces starting at root space looking for
 * branch by id
 */
async function _getB2tFSBranch_ById({
    metaspace,
    branchId,
}: {
    metaspace: MetaspaceService,
    branchId: string,
}): Promise<B2tFSBranchInfo | undefined> {
    const lc = `[${_getB2tFSBranch_ById.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 06c6a94990664f608915888dbd02dbb3)`); }

        // todo: fix this to avoid infinite recursion with cyclical branch spaceId refs.
        const searchInIndex: (b2tfsIndex: B2tFSIndexIbGib_V1 | undefined, searchLevel: number) => Promise<B2tFSBranchInfo | undefined> =
            async (b2tfsIndex: B2tFSIndexIbGib_V1 | undefined, searchLevel: number) => {
                if (!b2tfsIndex) {
                    throw new Error(`index at search level (${searchLevel}) falsy. (E: 53b2d617ac324bf285b73107fde3bc0b)`);
                }
                if (!b2tfsIndex.data) { throw new Error(`(UNEXPECTED) at searchLevel ${searchLevel}, index.data falsy? (E: c7c8356f817f4da49b3b9006f70e4ed9)`); }

                const branchSpaceQualifiedAddrs = b2tfsIndex.data['$@branch'];
                const branchSpaceIdsToSearchOneLevelDeeper: SpaceId[] = [];
                for (let i = 0; i < branchSpaceQualifiedAddrs.length; i++) {
                    const branchSpaceQualifiedAddr = branchSpaceQualifiedAddrs[i];
                    const branchAddrInfo = parseSpaceQualifiedIbGibAddr({
                        spaceQualifiedAddr: branchSpaceQualifiedAddr
                    });
                    const branchIbInfo = parseB2tFSBranchIb({ ib: branchAddrInfo.ib });
                    if (branchIbInfo.id === branchId) {
                        // found a match
                        if (logalot) { console.log(`${lc} branchId match found. branchAddrInfo: ${pretty(branchAddrInfo)} (I: c3c43ebe0a4c4762809f4e3cc6536ebd)`); }
                        const branchSpaceId = branchAddrInfo.spaceId;
                        const branchSpace = await metaspace.getLocalUserSpace({ localSpaceId: branchSpaceId });
                        if (!branchSpace) { throw new Error(`branchId matched with spaceId (${branchSpaceId}) (E: 847e5ec33b9a4a63a9a820f4f0e0f6ef)`); }
                        return await _getB2tFSBranch_BySpaceOrIndex({ metaspace, branchSpace });
                    } else {
                        // no match so queue for recursive search if no match found at
                        // this level
                        branchSpaceIdsToSearchOneLevelDeeper.push(branchAddrInfo.spaceId);
                    }
                }
                // not found yet, so search recursively
                for (let i = 0; i < branchSpaceIdsToSearchOneLevelDeeper.length; i++) {
                    const spaceIdToSearch = branchSpaceIdsToSearchOneLevelDeeper[i];
                    const spaceToSearch =
                        await metaspace.getLocalUserSpace({ localSpaceId: spaceIdToSearch });
                    if (!spaceToSearch) { throw new Error(`couldn't get space corresponding to spaceIdToSearch (${spaceIdToSearch}) at searchLevel ${searchLevel}. (E: 7d533dbf1fc54b6fbe2de40f3ea927ff)`); }
                    let nextIndex = await getB2tFSIndexIbGib({ metaspace, space: spaceToSearch });
                    const result = await searchInIndex(nextIndex, searchLevel + 1);
                    if (result) {
                        return result;
                    }
                }
                // made it this far, none found
                return undefined;
            };

        // start in the root local user space and search for branches starting from there.
        const superSpace = await metaspace.getLocalUserSpace({});
        const superIndex = await getB2tFSIndexIbGib({ metaspace, space: superSpace, throwIfNotFound: false });

        const initialSearchLevel = 0;
        const resBranchInfo = await searchInIndex(superIndex, initialSearchLevel);

        if (logalot) {
            console.log(`${lc} console.dir(resBranchInfo)... (I: 4e9cd0d738ac473492e90b0a2ded47a8)`);
            console.dir(resBranchInfo);
        }

        return resBranchInfo;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export async function getAllB2tFSBranches({
    metaspace,
}: {
    metaspace: MetaspaceService,
}): Promise<{ [branchSQIAddr: string]: B2tFSBranchInfo }> {
    const lc = `[${getAllB2tFSBranches.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: ce9ca28a08114881b2a0521260d2d3a3)`); }

        const runningBranches: { [branchSQIAddr: string]: B2tFSBranchInfo } = {};

        // todo: fix this to avoid infinite recursion with cyclical branch spaceId refs.
        const doIndexAndSubIndexes: (b2tfsIndex: B2tFSIndexIbGib_V1 | undefined, searchLevel: number) => Promise<void> =
            async (b2tfsIndex: B2tFSIndexIbGib_V1 | undefined, searchLevel: number) => {
                if (!b2tfsIndex) {
                    throw new Error(`index at search level (${searchLevel}) falsy. (E: a4bccee37d1942feaafb7ce807072095)`);
                }
                if (!b2tfsIndex.data) { throw new Error(`(UNEXPECTED) at searchLevel ${searchLevel}, index.data falsy? (E: 44e51adf53884363b53012e60abfc400)`); }

                const branchSpaceQualifiedAddrs = b2tfsIndex.data['$@branch'];
                const branchSpaceIdsToSearchOneLevelDeeper: SpaceId[] = [];
                for (let i = 0; i < branchSpaceQualifiedAddrs.length; i++) {
                    const branchSpaceQualifiedAddr = branchSpaceQualifiedAddrs[i];
                    if (!runningBranches[branchSpaceQualifiedAddr]) {
                        // we haven't done this branch yet
                        const branchAddrInfo = parseSpaceQualifiedIbGibAddr({
                            spaceQualifiedAddr: branchSpaceQualifiedAddr
                        });
                        const branchSpaceId = branchAddrInfo.spaceId;
                        const branchSpace = await metaspace.getLocalUserSpace({ localSpaceId: branchSpaceId });
                        if (!branchSpace) { throw new Error(`couldn't get branchSpace with spaceId (${branchSpaceId}) (E: 6c2206d808254905b8f8a7e17a631d59)`); }
                        const branchInfo = await _getB2tFSBranch_BySpaceOrIndex({ metaspace, branchSpace });
                        if (!branchInfo) { throw new Error(`couldn't get branchInfo by space. branchSpaceId: ${branchSpaceId}, branchSpaceQualifiedAddr: branchSpaceQualifiedAddr}(E: 1165641b66c3a05a8c50c6b41f336523)`); }
                        runningBranches[branchSpaceQualifiedAddr] = branchInfo;

                        // queue for recursive search
                        branchSpaceIdsToSearchOneLevelDeeper.push(branchAddrInfo.spaceId);
                    } else {
                        throw new Error(`runningBranches already contains branchSpaceQualifiedAddr (${branchSpaceQualifiedAddr}). cyclic reference? check to see if we already have it, which i believe, would mean that we have a cyclic reference. for now I'm going to throw because i think this would be bad. but in the future, may turn out ok so turn this to a warning/info and just don't process/queue it for recursive search (E: 3efc8be5d87df9ae9959d5017d1f4823)`);
                    }
                }
                // search recursively
                for (let i = 0; i < branchSpaceIdsToSearchOneLevelDeeper.length; i++) {
                    const spaceIdToSearch = branchSpaceIdsToSearchOneLevelDeeper[i];
                    const spaceToSearch =
                        await metaspace.getLocalUserSpace({ localSpaceId: spaceIdToSearch });
                    if (!spaceToSearch) { throw new Error(`couldn't get space corresponding to spaceIdToSearch (${spaceIdToSearch}) at searchLevel ${searchLevel}. (E: 9d642ff6da834342922d4f3d662f2ecc)`); }
                    let nextIndex = await getB2tFSIndexIbGib({ metaspace, space: spaceToSearch });
                    await doIndexAndSubIndexes(nextIndex, searchLevel + 1);
                }
            };

        // start in the root local user space and search for branches starting from there.
        const superSpace = await metaspace.getLocalUserSpace({});
        const superIndex = await getB2tFSIndexIbGib({ metaspace, space: superSpace, throwIfNotFound: false });

        const initialSearchLevel = 0;
        const resBranchInfo = await doIndexAndSubIndexes(superIndex, initialSearchLevel);

        if (logalot) {
            console.log(`${lc} console.dir(resBranchInfo)... (I: a26c57a025834ede97139ebc744dfed8)`);
            console.dir(resBranchInfo);
        }

        return runningBranches;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * formats b2tfsInfo with respect to branches.
 * @returns formatted branch info
 */
export function getFormattedBranchInfo({
    b2tfsInfo,
    verbose,
}: {
    b2tfsInfo: B2tFSIndexReportInfo,
    verbose: boolean,
}): string {
    const lc = `[${getFormattedBranchInfo.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 328b7560cc31df719eaec16100ca5423)`); }

        let result: string;
        const { branchInfos } = b2tfsInfo;

        if (verbose) {
            result = pretty(branchInfos);
        } else {
            const branchNamesAndSpaces =
                Object.values(branchInfos ?? {})
                    .map(info => `| ${info.name.substring(0, 16).padEnd(16, ' ')}      | ${info.spaceName} : ${info.spaceId}`);
            let maxLength = branchNamesAndSpaces.at(0)?.length ?? 0;
            branchNamesAndSpaces.forEach(x => {
                if (x.length > maxLength) { maxLength = x.length; }
            })
            const branchNamesAndSpaces_padded = branchNamesAndSpaces.map(x => x.padEnd(maxLength + 1, ' ').concat('|'));
            const columnHeadings1 = `| BranchName            | SpaceName : SpaceId`.padEnd(maxLength + 1, ' ').concat('|');
            const columnSepLine = `|-----------------------|`.padEnd(maxLength + 1, '-').concat('|');
            result = [
                columnSepLine,
                columnHeadings1,
                columnSepLine,
                branchNamesAndSpaces_padded.join('\n'),
                columnSepLine,
            ].join('\n');
        }

        if (logalot) { console.log(`${lc} result: ${result} (I: 87160b97e0c2c35c040d554df91a6123)`); }
        return result;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }

}

export function spaceHasRel8dBranch({
    space,
}: {
    space: IbGibSpaceAny,
}): boolean {
    return ((space.rel8ns ?? {})[B2TFS_BRANCH_TJP_REL8N_NAME] ?? []).length > 0;
}

/**
 * gets the b2tfs item diff for the given branch's (via {@link branchInfo})
 * b2tfs item ibgib that corresponds to the branch's root folder.
 *
 * this basically has the plumbing to get that corresponding root item and
 * execute the diff against it.
 *
 * ## dev notes
 *
 * ### 12/2024
 *
 * I am trying to feel my way here on exactly what I want.
 *
 * Obviously, there is the aspect of diffing individual files of what is on
 * disk/current in FS against what is in the B2tFS graph (that corresponds to
 * the branch's root B2tFSItemIbGib of fsType 'folder').
 *
 * Less obvious is leaving ourselves open in the future to different types of
 * sources of diffs. For example, if we have a function (or file chunk probably
 * in early implementations), we must be able to expand our design in the future
 * to handle this.
 *
 * Also, in the future, we will be moving away from filesystems altogether and
 * just working with ibgibs in spaces.
 *
 * Also, there is a difference between the raw difference of a file and the dna
 * that must be applied to the item's timeline. When a new file/folder is found
 * within a folder item, then we will add rel8ns to the folder item. What to do
 * when we have a folder that does not intrinsically change but must update
 * pointers to child items? Do we just leave this to the metastones? Am I
 * currently rel8ing punctiliar ibgibs or tjps? If the latter, then metastones
 * are probably the way to go (if we assume I thought that part through well).
 *
 * Speaking of metastones, I've been thinking when we branch an existing branch,
 * we will indeed want to copy metastones.
 *
 * For now, I need to get the obvious part done. I _think_ this will entail both
 * the raw file diff as well as including the dna + subsequent punctiliar ibgib
 * in the timeline.
 *
 * @returns resulting branch diff (even if the branch's underlying root diff is undefined)
 */
export async function getB2tFSBranchDiff({
    branchInfo,
    metaspace,
    deferred,
    applyCommentIbGib,
}: {
    /**
     * branch that we're working against
     *
     * if falsy, will get the active branch from the metaspace. if that's also
     * falsy, will throw.
     */
    branchInfo: B2tFSBranchInfo,
    /**
     * it's meta
     */
    metaspace: MetaspaceService,
    /**
     * if true, will NOT apply (persist/register) the new b2tfs items when diffing.
     */
    deferred: boolean,
    /**
     * the apply diff's comment ibgib if we are applying
     *
     * if deferred is falsy, this must be defined or it throws.
     */
    applyCommentIbGib: CommentIbGib_V1 | undefined,
}): Promise<B2tFSBranchDiff> {
    const lc = `[${getB2tFSBranchDiff.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: dd740af68428a0a502f1d4e1fa772923)`); }

        // get the item for the branch
        let { ibGib: branchIbGib, space: branchSpace } = branchInfo;

        // #region validate
        if (!branchIbGib) { throw new Error(`(UNEXPECTED) branchInfo.ibGib falsy? (E: 9dab6e28f5dcc002e5b342bdfe982123)`); }
        if (!branchIbGib.data) { throw new Error(`(UNEXPECTED) branchInfo.ibGib.data falsy? (E: 1b29b4d15f1145adbcfdf082b0dcc577)`); }
        if (!branchSpace) { throw new Error(`(UNEXPECTED) branchInfo.space falsy? (E: f2b41d95070e91209741a6b12bd17623)`); }
        if (!branchSpace.data) { throw new Error(`(UNEXPECTED) branchInfo.space.data falsy? (E: b08e0fadb86f400697c53c201c34f928)`); }

        if (!deferred && !applyCommentIbGib) { throw new Error(`(UNEXPECTED) not deferred and no apply diff comment ibgib?? (E: ff6e34cb6e636197bf0281bddee67124)`); }
        // #endregion validate

        const branchRootItem = (await getBranchRootItem({
            branchIbGib,
            branchSpace,
            latest: true,
            metaspace,
            defaultIfNone: 'throw',
        }))!;

        // build the paths that should be ignored
        // const branchFSPaths = await getBranchResolvedFSPaths({ metaspace });
        if (logalot) { console.log(`${lc} todo: build paths that should be ignored based on all paths known to the user's local space b2tfs index. (I: a659d61f6f82de051b438c6b2ba32124)`); }


        const contextPath =
            pathUtils.resolve(pathUtils.join(cwd(), branchIbGib.data.relativePathFromMetaspaceToFSPath));
        // what is the contextPath? '/home/wraiford/ibgib/ibgib/apps/ibgib/test-vcs/00_1_file'
        if (logalot) { console.log(`${lc} contextPath: ${contextPath} (I: 5602de8c1c3c02133e37560289516923)`); }

        // we now have the root item. we need to iterate all items and compare
        // with current fs state. remember this item is in the branchSpace.
        const itemDiff = await getB2tFSItemDiff({
            existingItem: branchRootItem,
            filterPatterns: B2TFS_DEFAULT_ITEM_FILTER_PATTERNS,
            inputPath: contextPath,
            diffTransactionId: await getUUID(),
            deferred,
            applyCommentIbGib,
            metaspace,
            space: branchSpace,
        });

        if (logalot) {
            console.log(`${lc} console.dir(itemDiff)... (I: 153032faa8b7d02def2d80d55ea35a23)`);
            console.dir(itemDiff);
        }

        const resBranchDiff: B2tFSBranchDiff = {
            branch: branchInfo,
            rootItem: branchRootItem,
            rootItemDiff: itemDiff,
        };

        if (logalot) {
            console.log(`${lc} console.dir(resBranchDiff)... (I: 3b11cfaeebe7476baad8f7f06ba7422e)`);
            console.dir(resBranchDiff);
        }

        // if (!deferred) {
        //     // we applied the diff, so update branchIbGib with the new root item?
        //     await rel8(...)
        // }
        // for now, i just leave the branch pointing to old root item and call
        // getBranchRootItem with latest === true

        // #region TESTING ONLY - REMOVE THIS REGION
        // const branchRootItem_confirmHigherN = (await getBranchRootItem({
        //     branchIbGib,
        //     branchSpace,
        //     latest: true,
        //     metaspace,
        //     defaultIfNone: 'throw',
        // }))!;
        // debugger; // examine branch root item vs newer confirm one
        // console.dir(branchRootItem);
        // console.dir(branchRootItem_confirmHigherN);
        // #endregion TESTING ONLY - REMOVE THIS REGION

        return resBranchDiff;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

// todo: implement getBranchResolvedFSPaths
// export async function getBranchResolvedFSPaths({
//     metaspace,
// }: {
//     metaspace: MetaspaceService,
// }): Promise<string[]> {
//     const lc = `[${getBranchResolvedFSPaths.name}]`;
//     try {
//         if (logalot) { console.log(`${lc} starting... (I: b2eb6ee5eb19d69dedf63e552a52f524)`); }
//         const localIndex = await getB2tFSIndexIbGib({ metaspace, throwIfNotFound: true });
//         let branches = localIndex?.rel8ns?.branch_tjp ?? [];
//         let dataBranches = localIndex?.data?.['$@branch'] ?? [];
//         console.dir(branches);
//         console.dir(dataBranches);
//         throw new Error(`not implemented (E: 13ab4407acbabfcc43cb6b4ad9b98824)`);
//     } catch (error) {
//         console.error(`${lc} ${extractErrorMsg(error)}`);
//         throw error;
//     } finally {
//         if (logalot) { console.log(`${lc} complete.`); }
//     }
// }

// #region extractArg_Branch...

/**
 * similar to {@link extractArg_space}, this will look for branch name/id in
 * args (including as a bare arg) and then get the branch info for the branch
 * indicated.
 *
 * if args don't contain a branch name/id and {@link defaultIfNoArg} is 'active'
 * name/id, then this will look for a spaceId/Name arg in {@link argInfos} to
 * use for the root branch space. it will then look for *that* space's active
 * branch and return that.
 */
export async function extractArg_Branch_byNameOrId({
    metaspace,
    argInfos,
    defaultIfNoArg,
    nameParamInfo = PARAM_INFO_NAME,
    idParamInfo = PARAM_INFO_SRC_ID,
    nameCanBeBareArg,
    idCanBeBareArg,
}: {
    metaspace: MetaspaceService,
    argInfos: RCLIArgInfo<any>[],
    defaultIfNoArg: 'active' | 'undefined' | 'throw',
    /**
     * if provided, this will be the param info used to check for the name
     *
     * @default PARAM_INFO_NAME
     *
     * @see {@link idParamInfo}
     * @see {@link PARAM_INFO_NAME}
     */
    nameParamInfo?: RCLIParamInfo,
    /**
     * if provided, this will be the param info used to check for the id
     *
     * @default {@link PARAM_INFO_SRC_ID}
     *
     * @see {@link nameParamInfo}
     * @see {@link PARAM_INFO_SRC_ID}
     */
    idParamInfo?: RCLIParamInfo,
    nameCanBeBareArg?: boolean,
    idCanBeBareArg?: boolean,
}): Promise<B2tFSBranchInfo | undefined> {
    const lc = `[${extractArg_Branch_byNameOrId.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: fe53d6cafb0d4fadb78c781cca1817d4)`); }

        let resBranchInfo: B2tFSBranchInfo | undefined;

        nameParamInfo ??= PARAM_INFO_NAME;
        idParamInfo ??= PARAM_INFO_SRC_ID;

        // #region extract name/id from argInfos explicitly or by bare arg

        let branchName: string | undefined = extractArgValue<string>({
            paramInfo: PARAM_INFO_NAME,
            argInfos,
            throwIfNotFound: false,
        }) as string;

        let branchId: string | undefined = extractArgValue<string>({
            paramInfo: PARAM_INFO_NAME,
            argInfos,
            throwIfNotFound: false,
        }) as string;

        if (!branchName && !branchId && nameCanBeBareArg) {
            branchName = extractArgValue<string>({
                paramInfo: PARAM_INFO_BARE,
                argInfos,
                throwIfNotFound: false,
            }) as string;
        }

        if (!branchName && !branchId && idCanBeBareArg) {
            branchId = extractArgValue<string>({
                paramInfo: PARAM_INFO_BARE,
                argInfos,
                throwIfNotFound: false,
            }) as string;
        }

        // #endregion extract name/id from argInfos explicitly or by bare arg

        // prefer (first in if..else) branchName?
        if (branchName) {
            resBranchInfo = await getB2tFSBranch({ metaspace, branchName });
        } else if (branchId) {
            resBranchInfo = await getB2tFSBranch({ metaspace, branchId });
        } else {
            switch (defaultIfNoArg) {
                case 'active':
                    // get the active branch and return that
                    resBranchInfo = await extractArg_Branch_noArg_getActiveBranch({ metaspace, argInfos });
                    break;
                case 'undefined':
                    return undefined; /* <<<< returns early */
                case 'throw':
                    throw new Error(`no branchName or branchId found among args and defaultIfNoArg is '${defaultIfNoArg}'. (E: f5237cbcdd1dad864ffdb81bde902423)`);
                default:
                    throw new Error(`(UNEXPECTED) unknown defaultIfNoArg value? ('${defaultIfNoArg}') (E: 0582cd85c3de39d97deedaad71896723)`);
            }
        }
        return resBranchInfo;

    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

async function extractArg_Branch_noArg_getActiveBranch({
    metaspace,
    argInfos,
}: {
    metaspace: MetaspaceService,
    argInfos: RCLIArgInfo<any>[],
}): Promise<B2tFSBranchInfo | undefined> {
    const lc = `[${extractArg_Branch_noArg_getActiveBranch.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: ab9b9bd869b51606870a7c89d5634923)`); }

        const space = await extractArg_space({
            argInfos, metaspace,
            returnOnArgsFalsy: 'default',
            spaceIdParamInfo: PARAM_INFO_SPACE_ID,
            spaceNameParamInfo: PARAM_INFO_SPACE_NAME,
        });
        // debugger; // is space a witness at this point? yes
        if (!space) { throw new Error(`(UNEXPECTED) couldn't get any space whatsoever (even default) from metaspace? (E: 5ffe4d7c254f1bebd2c3ad6b854a0b23)`); }
        let branchInfo = await getActiveB2tFSBranchInfo({
            metaspace,
            space,
        });

        if (!branchInfo) {
            if (logalot) { console.log(`${lc} active branch falsy. just getting the branch that corresponds to the space (${space.data!.name} ${space.data!.uuid}) (I: 93e2e365f7374139dfcca4f94593e223)`); }

            if (spaceHasRel8dBranch({ space: space })) {
                // branchspace has a corresponding
                branchInfo = await getB2tFSBranch({ metaspace, branchSpace: space });
            } else {
                // this is probably the metaspace's initial local user space.
                // (all root branches added create spaces that have rel8d
                // branches). try to get the index and see if that index has any
                // space-qualified branches.

                let branchIndex = await getB2tFSIndexIbGib({ metaspace, space });
                if (!branchIndex) { throw new Error(`B2tFS not initialized in space (${space.data!.name} ${space.data!.uuid}). you must first initialize with a call to --b2tfs-init (E: eefc22d1ea8d062a0c932b737a0af223)`); }

                let branchSQAddrs = branchIndex.data?.['$@branch'] ?? [];
                if (branchSQAddrs.length > 0) {
                    // just get the frigging first one for now
                    const spaceQualifiedAddr = branchSQAddrs.at(0)!;
                    console.warn(`${lc} no active space set, but there are branches added. We'll try the first branch (${spaceQualifiedAddr}) but really you should activate the space using --b2tfs-activate-space (that's the name atow 12/2023). you can also list branches with --b2tfs-info --b2tfs-branch (or terser synonyms) (W: 7c01b498574243d5aa752198c50934af)`);
                    const info = parseSpaceQualifiedIbGibAddr({ spaceQualifiedAddr });
                    const branchSpace = await metaspace.getLocalUserSpace({ localSpaceId: info.spaceId });
                    branchInfo = await getB2tFSBranch({ metaspace, branchSpace });
                    // debugger; // does branchInfo.space have witness? yes
                } else {
                    throw new Error(`B2tFS has been initialized for space (${space.data!.name} ${space.data!.uuid}) but it has no associated branches. check out "b2tfs-add-root-branch" command (E: 1d4343aac9620afd0be6c8dc05f6e123)`);
                }
            }
        }

        if (!branchInfo) {
            if (logalot) { console.log(`${lc} couldn't get any branch whatsoever from the branchSpace (${space.data!.name} ${space.data!.uuid} ) (I: 42d36fb89c3b40ca54646ea21051ea23)`); }
        }

        return branchInfo;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

// #endregion extractArg_Branch...
