/**
 * @module b2tfs-branch respec
 *
 * we gotta test our b2tfs-branch
 */

import * as pathUtils from 'path';
import { cwd, chdir, } from 'node:process';
import { cp, mkdir, readdir, readFile, writeFile, rename, stat, } from 'node:fs/promises';

import {
    firstOfAll, firstOfEach, ifWe, ifWeMight, iReckon,
    lastOfAll, lastOfEach, respecfully, respecfullyDear
} from '@ibgib/helper-gib/dist/respec-gib/respec-gib.mjs';
const maam = `[${import.meta.url}]`, sir = maam;

import {
    extractErrorMsg, getUUID, hash
} from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../../../ibgib-constants.mjs';
import { B2TFS_BRANCH_ATOM } from './b2tfs-branch-constants.mjs';
import { getB2tFSBranchIb, parseB2tFSBranchIb } from './b2tfs-branch-helper.mjs';
import { B2tFSBranchData_V1 } from './b2tfs-branch-types.mjs';
import { B2tFSTestStartState } from '../b2tfs-respec-types.mjs';
import {
    addNewFile,
    doB2tFSPostTestStuffLikeCdBackToInitialCwd,
    doB2tFSPreTestStuffAndChdirIntoTestDir, execB2tFSCmdAndCheckAfter,
    getUniqueB2tFSTestId,
} from '../b2tfs.respec-helper.mjs';
import { B2TFS_DEFAULT_TEST_CONFIG } from '../b2tfs-respec-constants.mjs';
import { RCLICommand } from '../../rcli-constants.mjs';
import { B2TFS_DEFAULT_ITEM_FILTER_PATTERNS, B2TFS_ITEM_FILTER_PATTERN_FILENAMES } from '../b2tfs-item/b2tfs-item-constants.mjs';
import { getAggregateFilterPatternsThisFolder, getFileDataAndEncoding } from '../b2tfs-item/b2tfs-item-helper.mjs';
import { isPathFiltered } from '../common/b2tfs-common-helper.mjs';
/**
 * for verbose logging
 */
const logalot = GLOBAL_LOG_A_LOT || true;

const lcFile: string = `[${pathUtils.basename(import.meta.url)}]`;

await respecfully(maam, `helpers`, async () => {

    await ifWe(maam, `getB2tFSBranchIb`, async () => {
        const atomIn = B2TFS_BRANCH_ATOM;
        const nameIn = 'hoogle';
        const idIn = await getUUID();
        const relativePathFromMetaspaceToFSPathIn = '..';
        const data: B2tFSBranchData_V1 = {
            name: nameIn,
            uuid: idIn,
            relativePathFromMetaspaceToFSPath: relativePathFromMetaspaceToFSPathIn,
        };
        const ib = getB2tFSBranchIb({ data })
        let { atom: atomOut, name: nameOut, id: idOut, } = parseB2tFSBranchIb({ ib });
        iReckon(maam, atomIn).asTo('atom').isGonnaBe(atomOut);
        iReckon(maam, nameIn).asTo('name').isGonnaBe(nameOut);
        iReckon(maam, idIn).asTo('id').isGonnaBe(idOut);
    });
    await ifWe(maam, `parseB2tFSBranchIb`, async () => {
        const atomIn = B2TFS_BRANCH_ATOM;
        const nameIn = 'hoogle';
        const idIn = await getUUID();
        /**
         * this must match the shape in {@link getB2tFSBranchIb}
         */
        const ib = `${B2TFS_BRANCH_ATOM} ${nameIn} ${idIn}`;
        let { atom: atomOut, name: nameOut, id: idOut, } = parseB2tFSBranchIb({ ib });
        iReckon(maam, atomIn).asTo('atom').isGonnaBe(atomOut);
        iReckon(maam, nameIn).asTo('name').isGonnaBe(nameOut);
        iReckon(maam, idIn).asTo('id').isGonnaBe(idOut);
    });
});

await respecfullyDear(maam, `b2tfs branch saga`, async () => {

    /**
     * permutations of repo + .ibgib folder. each of the following tests will be
     * executed for each permutation.
     */
    const startStates: B2tFSTestStartState[] = [
        // {
        //     ibgibs: { name: '20_initialized_with_app_and_robbot', },
        //     repo: { type: 'simple', name: '00_1_file', },
        // },
        // {
        //     ibgibs: { name: '20_initialized_with_app_and_robbot', },
        //     repo: { type: 'simple', name: '01_files_only', },
        // },
        // {
        //     ibgibs: { name: '20_initialized_with_app_and_robbot', },
        //     repo: { type: 'simple', name: '02_files_and_folders', },
        // },
        // {
        //     ibgibs: { name: '20_initialized_with_app_and_robbot', },
        //     repo: { type: 'simple', name: '03_has_node_modules', },
        // },
        // {
        //     ibgibs: { name: '20_initialized_with_app_and_robbot', },
        //     repo: { type: 'simple', name: '04_ignore_file_by_name', },
        // },
        // {
        //     ibgibs: { name: '20_initialized_with_app_and_robbot', },
        //     repo: { type: 'simple', name: '05_ignore_file_by_name_multi', },
        // },
        // {
        //     ibgibs: { name: '20_initialized_with_app_and_robbot', },
        //     repo: { type: 'simple', name: '06_ignore_file_by_regexp_js', },
        // },
        // {
        //     ibgibs: { name: '20_initialized_with_app_and_robbot', },
        //     repo: { type: 'simple', name: '07_override_ignore_file', },
        // },
        {
            ibgibs: { name: '20_initialized_with_app_and_robbot', },
            repo: { type: 'complex', name: 'js-ts/helper-gib', },
        },
    ];

    /**
     * each individual test leg has this as a timeout.
     *
     * make this a large number to
     */
    // const timeoutMs = 10_000; // arbitrary for my slowish laptop
    const timeoutMs = 1000 * 60 * 30; // arbitrary long timeout when debugging tests

    for (let i = 0; i < startStates.length; i++) {
        const startState = startStates[i];

        await respecfullyDear(maam, `--fs-only`, async () => {

            await respecfullyDear(maam, `startState ${i}`, async () => {
                const lc = `${lcFile}[repo:${startState.repo.name}|ibgib:${startState.ibgibs.name}]`;
                const initialCwd = cwd();
                try {
                    if (logalot) { console.log(`${lc} starting... (I: cd550531829e40efbba125236ccee0fa)`); }

                    // create a test directory
                    const { relPathFromTestDirToPackageJson, resolvedTestOutputPath } =
                        await doB2tFSPreTestStuffAndChdirIntoTestDir({
                            uniqueTestId: getUniqueB2tFSTestId('branch'),
                            testConfig: B2TFS_DEFAULT_TEST_CONFIG,
                            startState,
                        });

                    // init
                    await execB2tFSCmdAndCheckAfter({
                        respecfulTitle: sir,
                        label: 'initial init',
                        relPathFromTestDirToPackageJson,
                        cmd: RCLICommand.b2tfs_init,
                        cmdArgs: undefined,
                        stdOutIncludes: [
                            "Created B2tFS Index special index ibgib",
                        ],
                        timeoutMs,
                        // veryPolite: true,
                    });

                    // b2tfs_branch - add root branch and activate
                    await execB2tFSCmdAndCheckAfter({
                        respecfulTitle: sir, label: 'add root branch and activate',
                        relPathFromTestDirToPackageJson,
                        cmd: RCLICommand.b2tfs_branch,
                        cmdArgs: '. --add --b2-activate --msg="initial comment (wakka great branch)"',
                        stdOutIncludes: [
                            'Created and activated root B2tFS Branch IbGib, as well as any child B2tFS Item IbGibs for child files/folders.',
                        ],
                        timeoutMs,
                        // veryPolite: true,
                    });

                    // edit file and add new file and apply diff
                    // debugger; // investigating why adding a file isn't reflected in branch --fs-only
                    let { someFilePath, someFileContents } = await getSomeDamFileInTestDir();
                    await writeFile(someFilePath, 'edited yo' + someFileContents);
                    const newFileContents: string = 'yo this is a new file\n\nhey we got some new lines.\n\twakka';
                    const newFileName = 'easy file not a pic or other binary wakka.txt';
                    const newFileRelPath = '.';
                    const { newFilePath } = await addNewFile({ newFileRelPath, newFileName, newFileContents });
                    await execB2tFSCmdAndCheckAfter({
                        respecfulTitle: sir, label: 'edit file and add new file and apply diff',
                        relPathFromTestDirToPackageJson,
                        cmd: RCLICommand.b2tfs_diff,
                        /**
                         * could use just 'apply' or 'apply-cmd' or other synonyms.
                         * @see {@link PARAM_INFO_APPLY}
                         */
                        cmdArgs: `--apply-diff --text="apply diff after edit file and new file"`,
                        stdOutIncludes: [
                            'YES, differences found, and YES they were applied.',
                        ],
                        timeoutMs,
                        // veryPolite: true,
                        // fnlogalot: true,
                    });
                    // when we branch and check files match, this edit should be reflected

                    // b2tfs_branch --fs-only
                    // cwd() is the current test case repo path
                    // e.g. 'test-b2tfs/b2tmp/1234567branch'
                    const testFSOnlyOutputFolderName = pathUtils.basename(cwd()) + '_backup';
                    // e.g. '1234567branch_backup'
                    const testFSOnlyOutputPath = `../${testFSOnlyOutputFolderName}`;
                    // e.g. 'test-b2tfs/b2tmp/1234567branch_backup'
                    await execB2tFSCmdAndCheckAfter({
                        respecfulTitle: sir, label: `b2tfs_branch --fs-only --output-path="${testFSOnlyOutputPath}"`,
                        relPathFromTestDirToPackageJson,
                        cmd: RCLICommand.b2tfs_branch,
                        cmdArgs: `--fs-only --output-path="${testFSOnlyOutputPath}"`,
                        stdOutIncludes: [
                            'Branched just the fs files/folders and wrote these to',
                            pathUtils.basename(testFSOnlyOutputPath),
                        ],
                        timeoutMs,
                        // veryPolite: true,
                    });
                    // we have not added any files/folders, so the
                    // startState.repo should contain the same files and folders
                    const { repo } = startState;
                    // console.dir(repo);

                    await ifWe(sir, 'confirm repo equals --fs-only output files', async () => {
                        const directChildEntries = await readdir(testFSOnlyOutputPath, { withFileTypes: true });
                        iReckon(sir, directChildEntries.length).asTo('fs output path has one subfolder').isGonnaBe(1);
                        const testOutputRootName = directChildEntries.at(0)?.name;
                        if (!testOutputRootName) { throw new Error(`(UNEXPECTED) testOutputRootName falsy? (E: 08acae4bb524bb88fca7de56dc138224)`); }
                        const folderEqualsRepoRoot = await fsRecursivelyIsEqualViaHashingAndLength({
                            pathA: pathUtils.join(testFSOnlyOutputPath, testOutputRootName),
                            pathB: cwd(), // current test case repo path
                            filterPatterns: B2TFS_DEFAULT_ITEM_FILTER_PATTERNS.concat(),
                        });
                        iReckon(sir, folderEqualsRepoRoot).asTo('test output files/folders equals repo files/folders').isGonnaBeTrue();
                    });



                    // await ensureBackupAndInitialRepoAreTheSame({ testOutputPath, startState });

                } catch (error) {
                    console.error(`${lc} ${extractErrorMsg(error)}`);
                    throw error;
                } finally {
                    await doB2tFSPostTestStuffLikeCdBackToInitialCwd({
                        initialCwd,
                        testConfig: B2TFS_DEFAULT_TEST_CONFIG,
                        startState,
                    });
                    if (logalot) { console.log(`${lc} complete.`); }
                }
            });

        });

    }

});

/**
 * if paths are files, checks that each's content is equal via length and
 * hashing.
 *
 * if root directory, one of these should have an .ibgib folder which we will
 * manually ignore
 *
 * NOTE THIS WILL FAIL ONCE WE START HAVING OTHER FOLDERS/FILES IGNORED INSIDE
 * REPOS. WE WILL HAVE TO DUPLICATE IGNORE PATTERN FOR TESTING BUT SEEMS
 * OVERBOARD ATOW (01/2024)
 *
 * @returns true if all FS file/folder structure of {@link pathA} and {@link pathB} is exactly the same
 */
export async function fsRecursivelyIsEqualViaHashingAndLength({
    pathA,
    pathB,
    filterPatterns = B2TFS_DEFAULT_ITEM_FILTER_PATTERNS,
}: {
    pathA: string,
    pathB: string,
    filterPatterns: string[],
}): Promise<boolean> {
    const lc = `[${fsRecursivelyIsEqualViaHashingAndLength.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: b04642de3459474b1755fb89abb1f624)`); }
        const statA = await stat(pathA);
        const statB = await stat(pathB);

        if (statA.isFile() && statB.isFile()) {
            let { dataString: contentsA } = await getFileDataAndEncoding({ inputPath: pathA });
            let { dataString: contentsB } = await getFileDataAndEncoding({ inputPath: pathB });
            if (!contentsA) { throw new Error(`(UNEXPECTED) contentsA falsy? (E: 833c570aec2f8f4ed6a13dc2157f3c24)`); }
            if (!contentsB) { throw new Error(`(UNEXPECTED) contentsB falsy? (E: dbd8c434b32a00bf185a35ba10d70f24)`); }
            // const contentsA = await readFile(pathA, { encoding: 'utf8' });
            // const contentsB = await readFile(pathB, { encoding: 'utf8' });
            const lengthA = contentsA.length;
            const lengthB = contentsB.length;
            if (lengthA !== lengthB) {
                if (logalot) { console.log(`${lc} lengthA: ${lengthA}. lengthB: ${lengthB} (I: 415af44c55278e6e5220f136469d8324)`); }
                return false; /* <<<< returns early */
            }

            const hashA = await hash({ s: contentsA });
            const hashB = await hash({ s: contentsB });
            if (hashA !== hashB) {
                if (logalot) { console.log(`${lc} hashA: ${hashA}, hashB: ${hashB} (I: 9cab8b4d190aa289cdd97facdda43524)`); }
                return false; /* <<<< returns early */
            }

            // same length and same hash
            return true; /* <<<< returns early */
        } else if (statA.isDirectory() && statB.isDirectory()) {
            // one of these should have an .ibgib folder which we will manually ignore
            // note this will fail once we start having other files ignored inside repos.

            const direntsA = await readdir(pathA, { recursive: false, withFileTypes: true });
            const direntsB = await readdir(pathB, { recursive: false, withFileTypes: true });

            // must filter down using ignore patterns

            /**
             * filter patterns both inherited and from files inside path (like
             * `.ibgibignore` files).
             *
             * note that this is what is used when calling recursively inside
             * the folder.
             */
            const aggregateFilterPatternsA: string[] =
                await getAggregateFilterPatternsThisFolder({
                    filterPatterns,
                    dirEntries: direntsA,
                    filterFilenames: B2TFS_ITEM_FILTER_PATTERN_FILENAMES,
                    inputPath: pathA,
                });
            /**
             * filter patterns both inherited and from files inside path (like
             * `.ibgibignore` files).
             *
             * note that this is what is used when calling recursively inside
             * the folder.
             */
            const aggregateFilterPatternsB: string[] =
                await getAggregateFilterPatternsThisFolder({
                    filterPatterns,
                    dirEntries: direntsB,
                    filterFilenames: B2TFS_ITEM_FILTER_PATTERN_FILENAMES,
                    inputPath: pathB,
                });

            if (aggregateFilterPatternsA.length !== aggregateFilterPatternsB.length) {
                debugger;
                if (logalot) { console.log(`${lc} aggregateFilterPatternsA.length !== aggregateFilterPatternsB.length (I: edd8c31a17c1360fb247570466217a24)`); }
                return false; /* <<<< returns early */
            } else if (aggregateFilterPatternsA.some(x => !aggregateFilterPatternsB.includes(x))) {
                if (logalot) { console.log(`${lc} aggregateFilterPatternsA.some(x => !aggregateFilterPatternsB.includes(x)) (I: 1d663631a263183f2f65b072f19d7924)`); }
                return false; /* <<<< returns early */
            }


            // const ignoredA = direntsA.filter(x => isPathFiltered({ inputPath: pathUtils.join(x.path, x.name), filterPatterns: aggregateFilterPatternsA }));
            // const ignoredB = direntsB.filter(x => isPathFiltered({ inputPath: pathUtils.join(x.path, x.name), filterPatterns: aggregateFilterPatternsB }));

            const direntsNotFilteredA = direntsA.filter(x => !isPathFiltered({ inputPath: pathUtils.join(x.path, x.name), filterPatterns: aggregateFilterPatternsA }));
            const direntsNotFilteredB = direntsB.filter(x => !isPathFiltered({ inputPath: pathUtils.join(x.path, x.name), filterPatterns: aggregateFilterPatternsB }));

            if (direntsNotFilteredA.length !== direntsNotFilteredB.length) {
                if (logalot) { console.log(`${lc} direntsNotFilteredA.length !== direntsNotFilteredB.length (I: 3c71716a0a4c7d2486c0201101124f24)`); }
                return false; /* <<<< returns early */
            } else if (direntsNotFilteredA.length === 0) {
                // both are 0
                if (logalot) { console.log(`${lc} both direntsNotFilteredA/B.length === 0.\npathA: ${pathA}\npathB: ${pathB}\n(I: a4e26b37bfd426526e2eaded9e0a8624)`); }
                return true; /* <<<< returns early */
            }

            // recursively check children of directory
            for (let i = 0; i < direntsNotFilteredA.length; i++) {
                const direntA = direntsNotFilteredA[i];
                if (direntA.name === '.ibgib') { continue; }
                const filteredB = direntsNotFilteredB.filter(x => x.name === direntA.name);
                if (filteredB.length === 0) {
                    if (logalot) { console.log(`${lc} direntA.name (${direntA.name}) not found in direntsNotFilteredB. (I: e73106cbfdaefccfe69c095e31eaa524)`); }
                    return false; /* <<<< returns early */
                }
                if (filteredB.length > 1) { throw new Error(`(UNEXPECTED) filteredB.length > 1? (E: 9b92aa47960a36d2861461f75d38cc24)`); }
                const direntB = filteredB.at(0)!;

                const childPathA = pathUtils.join(pathA, direntA.name);
                const childPathB = pathUtils.join(pathB, direntB.name);
                const childrenAreEqual = await fsRecursivelyIsEqualViaHashingAndLength({
                    pathA: childPathA,
                    pathB: childPathB,
                    filterPatterns: aggregateFilterPatternsA,
                });
                if (!childrenAreEqual) {
                    if (logalot) { console.log(`${lc} children are not equal. childPathA: ${childPathA}. childPathB: ${childPathB} (I: 0e50d4d5e782def35d28da08bbf5bc24)`); }
                    return false; /* <<<< returns early */
                }
            }

            // all children are equal
            return true; /* <<<< returns early */
        } else {
            if (logalot) { console.log(`${lc} statA.isFile(): ${statA.isFile()}, statB.isFile(): ${statB.isFile()} (I: 6d8a839039e12bb2b53316fdbea43724)`); }
            return false;
        }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

async function getSomeDamFileInTestDir(): Promise<{ someFilePath: string, someFileContents: string }> {
    const lc = `[${getSomeDamFileInTestDir.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 0e831192ad62e6a9e6d1960fd107f624)`); }
        let dirents = await readdir(cwd(), { withFileTypes: true });

        /** some dam extension that isn't going to be ignored */
        const fileExts = ['md', 'txt', 'html', 'mts', 'ts', 'css'];

        const fileEntries =
            dirents.filter(entry => entry.isFile() && fileExts.some(ext => entry.name.toLowerCase().endsWith(ext)));

        const firstFile = fileEntries.at(0);
        if (!firstFile) { throw new Error(`(UNEXPECTED) firstFile falsy? make a better dam repo (E: e96d2e50f4716dcefb8f142cba487824)`); }

        const someFilePath = pathUtils.resolve(pathUtils.join(firstFile.path, firstFile.name));
        const someFileContents = await readFile(someFilePath, { encoding: 'utf8' });
        return { someFilePath, someFileContents };
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}
