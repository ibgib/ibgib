/**
 * @module b2tfs-branch constants
 *
 * constants are in this file!
 */

// /**
//  * example enum-like type+const that I use in ibgib. sometimes i put
//  * these in the types.mts and sometimes in the const.mts, depending
//  * on context.
//  */
// export type SomeEnum =
//     'ib' | 'gib';
// export const SomeEnum = {
//     ib: 'ib' as SomeEnum,
//     gib: 'gib' as SomeEnum,
// } satisfies { [key: string]: SomeEnum };
// export const SOME_TYPE_VALUES: SomeEnum[] = Object.values(SomeEnum);

/**
 * atom used in ibs
 */
export const B2TFS_BRANCH_ATOM = 'b2tfs_branch';

/**
 * a branch uses this to point to its corresponding item
 */
export const B2TFS_ITEM_REL8N_NAME = 'b2tfs_item';
