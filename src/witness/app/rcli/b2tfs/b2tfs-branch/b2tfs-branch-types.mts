/**
 * @module b2tfs-branch types (and enums)
 */

import { IbGibData_V1, IbGibRel8ns_V1, IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { IbGibAddr, TransformResult } from '@ibgib/ts-gib/dist/types.mjs';
import { SpaceId } from '@ibgib/core-gib/dist/witness/space/space-types.mjs';
import { IbGibSpaceAny } from '@ibgib/core-gib/dist/witness/space/space-base-v1.mjs';

import { B2TFS_BRANCH_TJP_REL8N_NAME, B2TFS_NAME_REGEXP } from '../common/b2tfs-common-constants.mjs';
import { B2TFS_ITEM_REL8N_NAME } from './b2tfs-branch-constants.mjs';
import { B2tFSIndexIbGib_V1 } from '../b2tfs-index/b2tfs-index-types.mjs';
import { B2tFSItemDiffInfo, B2tFSItemIbGib_V1, B2tFSType } from '../b2tfs-item/b2tfs-item-types.mjs';
import { OrderedManifestIbGib_V1 } from '../../../../../common/ordered-manifest/ordered-manifest-types.mjs';

/**
 * This contains the data specific to roots that goes beyond other B2tFSItem
 * ibgibs.
 *
 * This includes original metadata at the root's creation (timestamp,
 * original inputPath).
 *
 * ## notes
 *
 * * this root is **NOT** responsible for tracking the output path.
 *   * this output path is relative to the local space (which will be different
 *     on different machines/physical/logical locations).
 *   * this means this must be tracked in the B2tFS Index ibgib, that provides
 *     local mapping(s) relative to the local space's dir to the output path of
 *     the B2tFS generated artifacts.
 *
 * @see {@link IbGib_V1.data}
 */
export interface B2tFSBranchData_V1 extends IbGibData_V1 {
    /**
     * name must pass regexp
     *
     * for now i'm reusing
     *
     * @see {@link B2TFS_NAME_REGEXP}
     */
    name: string;
    /**
     * Path from the local user space physical fs path to the branch's physical
     * fs path.
     *
     * ## notes
     *
     * A local user space indexes (via B2tFSIndexIbGib special ibgib) branches,
     * and this mapping provides the output directory in the fs relative to the
     * space's own folder.
     *
     * ## example walkthrough
     *
     * For example, say we start in a folder `~/data/my-repo` and run an init
     * space cmd:
     *
     * `~/data/my-repo> ibgib --init`
     *
     * This will create a `.ibgib` subfolder and some other plumbing, including
     * a new local user space that will default to the containing folder's name
     * since no `--name` arg was provided:
     *
     * `~/data/my-repo/.ibgib`
     * `~/data/my-repo/.ibgib/ibgib`
     * `~/data/my-repo/.ibgib/ibgib/000_zerospace`
     * `~/data/my-repo/.ibgib/ibgib/my-repo` // this is a space, nothing to do with B2tFS/vcs yet
     *
     * _(The my-repo space and zerospace will have multiple folders/files inside
     * as well for the actual ibgibs but we're just looking at the basic folder
     * structure right now)_
     *
     * Atow (12/2023), this does NOT do anything with b2tfs, so we must also run
     * an init specifically for the vcs (syntax may change, also synonyms exist
     * like b2-init, vcs-init, fs-init, etc. @see `RCLI_COMMAND_SYNONYMS` &
     * `RCLI_COMMAND_SYNONYMS` currently in `rcli-constants.mts`):
     *
     * `~/data/my-repo> ibgib --b2tfs-init`
     *
     * This creates a special B2tFS index ibgib in the local user space which
     * will keep track of our roots. Since we do not specify a name, the index's
     * name defaults to the containing folder's name, `my-repo`.
     *
     * Next we add a root via the add root cmd:
     *
     * `~/data/my-repo> ibgib --b2tfs-add-root-branch`
     *
     * This will create a branch ibgib, whose name was not specified so again
     * will be taken from the current directory name. (If we did provide a name,
     * we would have a different name from the space.) The cwd at the point
     * where the handle-b2tfs-add-root-branch handler code is executing will be
     * the absolute path to the metaspace basedir:
     *
     * `/home/[username]/data/my-repo/.ibgib`
     *
     * and so the vcs root that we are interested in is one level up, what in
     * code atow (12/2023) is `initialCwd` in the global ibgib data on
     * `globalThis`:
     *
     * `/home/[username]/data/my-repo`
     *
     * So the relative path from the metaspace basedir to this folder (`..`) is
     * what we want to store in this {@link relativePathFromMetaspaceToFSPath}
     * property.
     *
     * `relativePathFromMetaspaceToFSPath === '..';`
     *
     * Now say we synchronize to another outerspace ("remote"/"peer"/etc. in
     * other vcs paradigms) and someone else pulls it down to their metaspace +
     * local user space ("clone"/"fork"/etc. depending on vcs). What precise
     * *ibgibs* are we pulling down? The root? The root's associated b2tfs
     * folder item rel8d to the root?
     *
     * Atow (12/2023) I'm not 100% sure on the answer to this. I know that it is
     * **NOT** the B2tFSIndexIbGib, as this is tightly coupled with the local
     * user space (which does not get synchronized). The local user space is by
     * definition a physical location and the metaspace is responsible for
     * interspatial interactions.
     *
     * I'm thinking (and it's why I'm putting this property here in the root)
     * that the root also does NOT get synchronized. The B2tFSItem is the
     * timeline we track, and it's possible for multiple "roots" (which are
     * analogous to "repos") to reference the "same" B2tFSItem timelines. This
     * is similar to enabling symbolic links/virtual folders in the vcs domain.
     *
     * Adding a root is thus like a mounting a filesystem, which also can be
     * done multiple times. Changes to the underlying files/folders
     * (B2tFSItemIbGibs) are then reflected in all mounted positions.
     *
     * But perhaps we DO sync the roots but we give the possibility of weaving
     * in virtual folder B2tFSItems.
     *
     * Will see how it organically goes.
     */
    relativePathFromMetaspaceToFSPath: string;
}

/**
 * rel8ns (named edges/links in DAG) go here.
 *
 * @see {@link IbGib_V1.rel8ns}
 */
export interface B2tFSBranchRel8ns_V1 extends IbGibRel8ns_V1 {
    /**
     * The item that this root corresponds to.
     */
    [B2TFS_ITEM_REL8N_NAME]: IbGibAddr[];
}

/**
 * this is the ibgib object itself.
 *
 * If this is a plain ibgib data only object, this acts as a dto. You may also
 * want to generate a witness ibgib, which is slightly different, for ibgibs
 * that will have behavior (i.e. methods).
 *
 * @see {@link B2tFSBranchData_V1}
 * @see {@link B2tFSBranchRel8ns_V1}
 */
export interface B2tFSBranchIbGib_V1 extends IbGib_V1<B2tFSBranchData_V1, B2tFSBranchRel8ns_V1> {

}

// /**
//  * Data for {@link B2tFSBranchInfoIbGib_V1}
//  */
// export interface B2tFSBranchInfoData_V1 extends IbGibData_V1 {
// }

// /**
//  * Rel8ns for {@link B2tFSBranchInfoIbGib_V1}
//  */
// export interface B2tFSBranchInfoRel8ns_V1 extends IbGibRel8ns_V1 {
//     /**
//      * the root that this info ibgib describes.
//      */
//     [B2TFS_MY_BRANCH_TJP_REL8N_NAME]: IbGibAddr[];
// }

/**
 * metadata ibgib for storing configuration info regarding a root that isn't
 * stored on the root itself.
 *
 * B2tFS Indexes use these info ibGibs, and these indexes stay with their local
 * user spaces. local user spaces aren't transmitted when synchronizing/merging
 * to "remote" outerspaces. likewise, this root metadata is for things that also
 * do not travel with the root graph when sync/merge/etc operations are
 * performed.
 *
 * For example, consider Alice and Bob. On Alice's computer, she may have the
 * local user space physically stored on her filesystem at
 * `~/some/path/repo/.ibgib/ibgib/alice_cool_proj_space`. Bob meanwhile may have
 * his user space at
 * `D:\Data\Look\Mom\Im\On\Windows\.ibgib\bob_cool_proj_space`. These are ibgib
 * _local user spaces_ that contain ibgib data (hypergraphs not hierarchical
 * files/folders). Alice may have her output repo folder located at
 * `~/repos/our-cool-project`, with Bob's being `D:\Data\Our Cool Project`.
 * These are _filesystem spaces_ that have files/folders. This mapping from the
 * ibgib local user space to the projected/linked filesystem space is specific
 * to that local user space and should not travel to outerspaces when
 * synchronizing/merging/replicating/etc.
 *
 * So say Alice does a command to stage some files to be committed (actual
 * syntax may change):
 *
 * `ibgib --b2tfs-add .`
 *
 * First off, note that there is no `--data-path` so this will look in the
 * default `./.ibgib` for the base path of the local user space(s) and user's
 * zerospace/metaspace. (In the future we should most likely add a filesystem
 * parent traversal until we find an ibgib metaspace directly or in the .ibgib,
 * e.g., `../.ibgib`, `../../.ibgib`, etc., to mimic existing vcs functionality)
 */
// export interface B2tFSBranchInfoIbGib_V1
//     extends IbGib_V1<B2tFSBranchInfoData_V1, B2tFSBranchInfoRel8ns_V1> {
// }

export interface B2tFSBranchInfo_Terse {
    spaceId: SpaceId;
    spaceName: string;
    indexAddr: IbGibAddr;
    name: string;
}

/**
 * convenience interface that groups together a branch space, index and branch
 * ibgib.
 *
 * optional should be filled in when verbose is set
 */
export interface B2tFSBranchInfo extends B2tFSBranchInfo_Terse {
    space: IbGibSpaceAny;
    index: B2tFSIndexIbGib_V1;
    ibGib: B2tFSBranchIbGib_V1;
}

/**
 * diff information for an entire branch starting with the branch's root b2tfs
 * item (folder).
 */
export interface B2tFSBranchDiff {
    /**
     * the branch info (branch index, ibgib and space) for the diff.
     */
    branch: B2tFSBranchInfo;
    /**
     * the existing item against which we do the differencing.
     */
    rootItem: B2tFSItemIbGib_V1;
    /**
     * the diff produced against the existing {@link rootItem}
     *
     * if there is no difference in the branch, then this will be `undefined`.
     */
    rootItemDiff: B2tFSItemDiffInfo | undefined;
}
