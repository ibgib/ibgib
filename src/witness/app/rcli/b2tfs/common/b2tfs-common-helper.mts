/**
 * @module b2tfs-branch helper/util/etc. functions
 *
 * this is where you will find helper functions like those that generate
 * and parse ibs for b2tfs-branch.
 */

import * as pathUtils from 'path';
import { cwd } from 'process';
import { rm, stat, readFile } from 'node:fs/promises';
import { spawn, } from 'node:child_process';
// import { statSync } from 'node:fs';
// import { readFile, } from 'node:fs/promises';
// import * as readline from 'node:readline/promises';
// import { stdin, stdout } from 'node:process'; // decide if use this or not

import {
    extractErrorMsg, getSaferSubstring,
} from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { RCLIArgInfo, RCLIArgType } from '@ibgib/helper-gib/dist/rcli/rcli-types.mjs';
import { extractArgValue } from '@ibgib/helper-gib/dist/rcli/rcli-helper.mjs';
import { getIbGibAddr } from '@ibgib/ts-gib/dist/helper.mjs';
import { Gib, Ib, IbGibAddr } from '@ibgib/ts-gib/dist/types.mjs';
import { IBGIB_DELIMITER } from '@ibgib/ts-gib/dist/V1/constants.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { IbGibSpaceAny } from '@ibgib/core-gib/dist/witness/space/space-base-v1.mjs';
import { SPACE_NAME_MAX_LENGTH } from '@ibgib/core-gib/dist/witness/space/space-constants.mjs';
import { SpaceId } from '@ibgib/core-gib/dist/witness/space/space-types.mjs';
import { MetaspaceService } from '@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs';
import { getTjpAddr, toDto } from '@ibgib/core-gib/dist/common/other/ibgib-helper.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../../../ibgib-constants.mjs';
import { CreateB2tFSSpaceOpts_Branch, CreateB2tFSSpaceOpts_Source, } from './b2tfs-common-types.mjs';
import {
    B2TFS_APPLY_DIFF_COMMENT_IGNORE_REGEXP,
    B2TFS_BRANCH_PATH_PREFIX, B2TFS_EDITOR_TMP_FILENAME_FOR_APPLY_COMMENT_TEXT,
    B2TFS_IGNORE_PATTERN_PREFIX_INCLUDE, B2TFS_IGNORE_PATTERN_PREFIX_REGEXP_JS,
    B2TFS_SPACE_NAME_PREFIX,
} from './b2tfs-common-constants.mjs';
import { B2tFSIndexReportInfo } from '../b2tfs-index/b2tfs-index-types.mjs';
import { getB2tFSIndexIbGib } from '../b2tfs-index/b2tfs-index-helper.mjs';
import { getIbGibGlobalThis, promptForText } from '../../rcli-helper.mjs';
import { getAllB2tFSBranches, } from '../b2tfs-branch/b2tfs-branch-helper.mjs';
import { B2tFSBranchInfo, B2tFSBranchInfo_Terse } from '../b2tfs-branch/b2tfs-branch-types.mjs';
import { PARAM_INFO_TEXT } from '../../rcli-constants.mjs';


/**
 *
 * for verbose logging
 */
const logalot = GLOBAL_LOG_A_LOT;


/**
 * creates the space that will be dedicated to the b2tfs root/branch (or other
 * entity in the future) associated with given `name`.
 *
 * @returns newly created root/branch space (atow 12/2023 `NodeFilesystemSpace_V1`)
 */
export async function createNewB2tFSSpace({
    name,
    metaspace,
}: CreateB2tFSSpaceOpts_Source | CreateB2tFSSpaceOpts_Branch): Promise<IbGibSpaceAny> {
    const lc = `[${createNewB2tFSSpace.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: a7e6c161ce17fb7fbd19c67494c87123)`); }

        /**
         * spaceName is derived from the given root/branch `name`.
         */
        const spaceName = getSaferSubstring({
            text: name,
            length: SPACE_NAME_MAX_LENGTH - B2TFS_SPACE_NAME_PREFIX.length,
        });

        // create a space for the given root?
        const newSpace = await metaspace.createLocalSpaceAndUpdateBootstrap({
            zeroSpace: metaspace.zeroSpace,
            spaceName,
            allowCancel: false,
            createBootstrap: false,
        });

        if (!newSpace) { throw new Error(`(UNEXPECTED) newSpace is falsy after call to createLocalSpaceAndUpdateBootstrap? (E: 5519afeed7ba7aa988ba9fddd2fadd23)`); }

        // //
        // metaspace.rel8ToSpecialIbGib

        return newSpace;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export async function getB2tFSInfo({
    metaspace,
    space,
    spaceId,
    verbose,
}: {
    metaspace: MetaspaceService,
    space?: IbGibSpaceAny,
    spaceId?: SpaceId,
    verbose?: boolean,
}): Promise<B2tFSIndexReportInfo> {
    const lc = `[${getB2tFSInfo.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 176604a9f12523d341eab3ac47ef1823)`); }
        space ??= await metaspace.getLocalUserSpace({ localSpaceId: spaceId });
        if (!space) { throw new Error(`(UNEXPECTED) couldn't get any space whatsover? (E: a9798d66dda5bc53ca776a9b10efc323)`); }
        if (!space.data) { throw new Error(`(UNEXPECTED) space.data falsy? (E: 2401ed35900d3bdea9d10b6cfe07fb23)`); }
        spaceId ??= space.data.uuid;
        if (spaceId && spaceId !== space.data.uuid) { throw new Error(`(UNEXPECTED) spaceId given doesn't match space.data.uuid? (E: 9cf2da948cf3ef9887333c1647ba5b23)`); }

        const indexIbGib = await getB2tFSIndexIbGib({ metaspace, space, throwIfNotFound: false });

        const allBranchInfos =
            await getAllB2tFSBranches({ metaspace, }) as { [branchSQIAddr: string]: B2tFSBranchInfo | B2tFSBranchInfo_Terse };
        if (!verbose) {
            Object.values(allBranchInfos).forEach(info => {
                const fullInfo = info as Partial<B2tFSBranchInfo>;
                delete fullInfo.index;
                delete fullInfo.space;
                delete fullInfo.ibGib;
                // return {
                //     indexAddr: info.indexAddr,
                //     name: info.name,
                //     spaceId: info.spaceId,
                //     spaceName: info.spaceName,
                // } as B2tFSBranchInfo_Terse;
            });
        }

        const info: B2tFSIndexReportInfo = {
            cwd: cwd(),
            ibGibGlobalThis: getIbGibGlobalThis(),
            spaceId,
            spaceName: space.data.name,
            indexIbGib: indexIbGib ? toDto({ ibGib: indexIbGib }) : undefined,
            indexExists: !!indexIbGib,
            indexAddr: indexIbGib ? getIbGibAddr({ ibGib: indexIbGib }) : undefined,
            indexTjpAddr: indexIbGib ? getTjpAddr({ ibGib: indexIbGib }) : undefined,
            branchInfos: allBranchInfos,
        };

        console.warn(`${lc} todo: do additional branch analysis (W: e7f9892f7c3b48568c3b7bbf7e3af2d6)`);

        if (logalot) {
            console.log(`${lc} console.dir(info)... (I: 34f118ca9d96ab3ed65ee55d3178ed23)`);
            console.dir(info);
        }

        return info;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}


/**
 * marker type to indicate a string that is a space-qualified ibgib addr
 * @see {@link IbGibAddr}
 * @see {@link SpaceQualifiedIbGibAddrInfo}
 * @see {@link getSpaceQualifiedIbGibAddr}
 */
export type SpaceQualifiedIbGibAddr = string;
export interface SpaceQualifiedIbGibAddrInfo {
    version: number;
    spaceId: string;
    ibGibAddr: IbGibAddr;
    ib: Ib;
    gib: Gib;
}

/**
 * locates an ibgib specifically within a physical space.
 *
 * space-qualified ibgib addr atow (12/2023) is in the form of:
 *
 *   `v:1${delimiter}spaceId:${spaceId}${delimiter}${ibGibAddr}`
 *
 * ## future
 *
 * in the future, there could be other space-locating schemas, different
 * delimiters, who knows what. i'm still not 100% on identifying space's by ids
 * and not by tjp or tjpGib.
 *
 * also this is the kind of schema that needs to have a schema ibgib associated
 * with it. one thing at a time.
 *
 * @returns space-qualified ibgib address
 */
export function getSpaceQualifiedIbGibAddr({
    spaceId,
    space,
    ibGibAddr,
    ibGib,
    delimiter = IBGIB_DELIMITER
}: {
    spaceId?: string,
    space?: IbGibSpaceAny,
    ibGibAddr?: IbGibAddr,
    ibGib?: IbGib_V1,
    delimiter?: string,
}): string {
    const lc = `[${getSpaceQualifiedIbGibAddr.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: ede2f78bd1a69132db3044d2cf78ee23)`); }
        spaceId ??= space?.data?.uuid;
        if (!spaceId) { throw new Error(`(UNEXPECTED) couldn't get spaceId? either valid spaceId or space (with data.uuid) required. (E: 62f9d98886cd8887f6a713c741503b23)`); }
        delimiter ??= IBGIB_DELIMITER;
        if (!ibGib && !ibGibAddr) { throw new Error(`(UNEXPECTED) both ibGib and ibGibAddr falsy? at least one is required (E: b840fcc5e944ad8559d0f2f5afe2d123)`); }
        ibGibAddr ??= getIbGibAddr({ ibGib });
        if (!ibGibAddr.includes(delimiter)) { console.warn(`${lc} it is assumed ibGibAddr has the same delimiter as the space-qualified addr. i don't know ramifications atow (12/2023) of different delimiters. (W: 859b1b0254274cd29d56abdefacfedf1)`); }

        return `v:1${delimiter}spaceId:${spaceId}${delimiter}${ibGibAddr}`;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * parses a space-qualified ibgib address and does some basic (incomplete) validation.
 * @returns space qualified addr info
 */
export function parseSpaceQualifiedIbGibAddr({
    spaceQualifiedAddr,
    delimiter = IBGIB_DELIMITER
}: {
    spaceQualifiedAddr: SpaceQualifiedIbGibAddr,
    delimiter?: string,
}): SpaceQualifiedIbGibAddrInfo {
    const lc = `[${parseSpaceQualifiedIbGibAddr.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: c581deb1dd5558e714eba5618f1da823)`); }
        delimiter ??= IBGIB_DELIMITER;

        if (!spaceQualifiedAddr) { throw new Error(`spaceQualifiedAddr required. (E: 5edac919ceae1c5286447e840368ce23)`); }

        const pieces = spaceQualifiedAddr.split(delimiter);

        const [versionKey, versionValue] = pieces.at(0)!.split(':');
        if (versionKey !== 'v' || versionValue !== '1') { throw new Error(`unknown format. first piece is expected to be a version qualifier in form of "v:[versionNum]" with currently only versionNum === 1 supported. actual was ${pieces.at(0)} (E: 9b23ccdea315227fafc11e6c1929f623)`); }

        // version confirmed
        if (pieces.length !== 4) { throw new Error(`version 1 expected to have four pieces: version, spaceId, ib, gib. actual pieces length: ${pieces.length} (E: 1635299a2aed1c88783faf762c019323)`); }

        const [_versionInfo, spaceInfo, ib, gib] = pieces;
        const [spaceKey, spaceValue] = spaceInfo.split(':');
        if (spaceKey !== 'spaceId') {
            throw new Error(`invalid format. version 1 space info should be in form of "spaceId:abc123id". spaceInfo: ${spaceInfo} (E: 6049792da6d7e0dda8ea1b94d973b423)`);
        }
        const spaceId = spaceValue;

        return {
            version: 1,
            spaceId,
            ibGibAddr: getIbGibAddr({ ib, gib, delimiter }),
            ib, gib,
        }

    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * this prefix should be used when creating soft-rel8ns inside the ibgib.data
 * map.
 *
 * Soft rel8ns are useful to notate other ibgibs by address (and thus get the
 * security benefits of the full ib^gib address), BUT THESE LINKS WILL NOT BE
 * INCLUDED IN DEPENDENCY GRAPHS OR DEFAULT VALIDATION.
 */
export const SOFT_ADDR_DATA_KEY_PREFIX = '@';
/**
 * same as {@link SOFT_ADDR_DATA_KEY_PREFIX} but with space-qualified addresses.
 */
export const SOFT_ADDR_DATA_KEY_PREFIX_SPACE_QUALIFIED = '$@'; // san antonio represent

/**
 * when adding a soft rel8n from one ibGib to another, use this for the data key
 * to indicate that the data is a soft rel8n address.
 *
 * @returns prefixed data key to use for space-qualified soft rel8ns
 *
 * @see {@link SOFT_ADDR_DATA_KEY_PREFIX}
 * @see {@link SOFT_ADDR_DATA_KEY_PREFIX_SPACE_QUALIFIED}
 */
export function getIbGibAddrSoftRel8nDataKey({
    softRel8nName,
    spaceQualified,
}: {
    softRel8nName: string,
    spaceQualified?: boolean,
}): string {
    return spaceQualified ?
        `${SOFT_ADDR_DATA_KEY_PREFIX_SPACE_QUALIFIED}${softRel8nName}` :
        `${SOFT_ADDR_DATA_KEY_PREFIX}${softRel8nName}`;
}

/**
 * atow (01/2024) this filtering algorithm has a couple components:
 *
 * * if pattern starts with "!" OR with {@link B2TFS_IGNORE_PATTERN_PREFIX_INCLUDE}, then...
 *   * if the pattern matches, {@link inputPath} will **NOT** be filtered (i.e. it's included).
 * * else if pattern does not have one of these prefixes, then...
 *   * if pattern matches, {@link inputPath} will be **filtered out** (i.e. it's excluded).
 *
 * * if pattern starts with {@link B2TFS_IGNORE_PATTERN_PREFIX_REGEXP_JS} then...
 *   * the pattern is matched by js regexp.
 *
 * * later entries in {@link filterPatterns} array win/override previous entries.
 *   * so if multiple patterns apply to {@link inputPath}, the last one wins
 *
 *
 *
 * @returns true if `inputPath` matches the `filterPatterns` given, else false.
 *
 * @see {@link B2TFS_IGNORE_PATTERN_PREFIX_INCLUDE}
 */
export function isPathFiltered({
    inputPath,
    filterPatterns,
}: {
    inputPath: string,
    filterPatterns: string[],
}): boolean {
    const lc = `[${isPathFiltered.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: d0cd5f4126f8c960f15a37631242ac23)`); }

        // first check if the path is filtered per it being a root/branch space
        // folder.
        if (pathUtils.basename(inputPath).startsWith(B2TFS_BRANCH_PATH_PREFIX)) {
            return false;
        }

        let isFilteredByPattern = false;
        for (let i = 0; i < filterPatterns.length; i++) {
            // match the inputPath against this filter pattern, overriding
            // previous value of isFiltered if necessary.

            const rawPattern = filterPatterns[i];
            /**
             * we'll manipulate this depending on what type of pattern we're
             * dealing with.
             */
            let pattern = rawPattern.concat();
            /**
             * the pattern by default is meant to exclude. if starts with !,
             * then we NOT this which means that we are actually including the
             * pattern (overriding previous pattern).
             */
            let isInclude: boolean = false;

            if (pattern.startsWith('!')) {
                isInclude = true;
                pattern = pattern.slice(1);
            } else if (rawPattern.startsWith(B2TFS_IGNORE_PATTERN_PREFIX_INCLUDE)) {
                isInclude = true;
                pattern = pattern.slice(B2TFS_IGNORE_PATTERN_PREFIX_INCLUDE.length);
            }

            const isJSRegExp = pattern.startsWith(B2TFS_IGNORE_PATTERN_PREFIX_REGEXP_JS); // notice the space
            if (isJSRegExp) {
                pattern = pattern.slice(B2TFS_IGNORE_PATTERN_PREFIX_REGEXP_JS.length);
            }

            if (pattern.includes('*') && !isJSRegExp) {
                throw new Error(`glob pattern found (${pattern}). glob patterns not implemented yet. if this is meant to be a js regexp, use prefix ${B2TFS_IGNORE_PATTERN_PREFIX_REGEXP_JS} (E: 6d33a3c2daaeafa7d9a363c657c50423)`);
            }

            let matchesPattern: boolean;
            if (isJSRegExp) {
                // regex pattern (not literal)
                // for now we will use a simple javascript-flavored regexp string
                matchesPattern = !!inputPath.match(new RegExp(pattern));
            } else {
                // literal pattern match
                const resolvedPath = pathUtils.resolve(inputPath);
                const basename = pathUtils.basename(inputPath);
                matchesPattern = basename === pattern || resolvedPath.endsWith(pattern);
            }
            if (matchesPattern) {
                // could do ternary here but i don't want to have to analyze
                // that in maintenance. simple enough here to just do a simple
                // if..else block (my brain is mush)
                if (isInclude) {
                    isFilteredByPattern = false;
                } else {
                    isFilteredByPattern = true;
                }
            }
        }

        return isFilteredByPattern;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export async function getMessageText_maybePromptIfNotInArgs({
    argInfos,
    promptTypeIfNotFound,
    promptTitle,
    promptMsg,
}: {
    argInfos: RCLIArgInfo<RCLIArgType>[],
    promptTypeIfNotFound?: 'editor' | 'prompt' | 'all' | 'none',
    promptTitle?: string,
    promptMsg?: string,
}): Promise<string> {
    const lc = `[${getMessageText_maybePromptIfNotInArgs.name}]`;
    let resCommentText = '';
    promptTypeIfNotFound ??= 'none';
    try {
        if (logalot) { console.log(`${lc} starting... (I: 4e90fe333eafd3f522046947831c4624)`); }

        // first try just extracting from args
        resCommentText = extractArgValue<string>({ paramInfo: PARAM_INFO_TEXT, argInfos }) as string | undefined ?? '';

        // always try editor first if applicable
        if (!resCommentText && ['all', 'editor'].includes(promptTypeIfNotFound)) {

            // cwd() at this point:
            // '/home/wraiford/ibgib/ibgib/apps/ibgib/test-b2tfs/b2tmp/1155391init/.ibgib'

            const tmpFilePath = pathUtils.join(cwd(), B2TFS_EDITOR_TMP_FILENAME_FOR_APPLY_COMMENT_TEXT);
            await deleteFileIfExists({ filePath: tmpFilePath });

            /**
             * i have this as a loop. for some reason it's proceeding where it
             * should be awaiting...leaving as is for now.
             */
            const editorsToTry: [cmd: string, options: string[]][] = [
                /**
                 * -n no swap file
                 * -Z restricted mode like rvim
                 * -i "NONE" no .viminfo file
                 * --clean     Do not use any personal configuration (vimrc, plugins, etc.).
                 */
                ['vim', ['-n', '-Z', '-c', 'set relativenumber', '-i', 'NONE']],

                // [process?.env?.EDITOR ?? 'vi', ''],

                // /**
                //  *
                //  */
                // ['nano', ''],
            ];
            for (let i = 0; i < editorsToTry.length; i++) {
                const editor = editorsToTry[i];
                const [editorCmd, editorOpts] = editor;
                const lcEditor = `${lc}[${editorCmd}]`;
                try {
                    if (logalot) { console.log(`${lcEditor} starting... (I: fc31670590ba57a20e17c62220029424)`); }
                    // exec editor with tmp file
                    resCommentText = await new Promise<string>((resolve) => {
                        // for some reason, i can't use `exec` or `execSync`. no
                        // idea. so found this on SO:
                        // https://stackoverflow.com/a/17110285/3897838
                        // thanks!
                        const child = spawn(editorCmd, [tmpFilePath, ...editorOpts], {
                            stdio: 'inherit'
                        });
                        child.on('exit', async function (e, code) {
                            let resFile: string = '';
                            try {
                                resFile = await readFile(tmpFilePath, { encoding: 'utf8' });
                            } catch (error) {
                                resFile = '';
                            } finally {
                                // always delete tmp, should never throw
                                await deleteFileIfExists({ filePath: tmpFilePath });
                            }
                            resolve(resFile);
                        });
                    });
                    if (resCommentText) { break; }
                } catch (error) {
                    console.error(`${lcEditor} there was an error when trying to use the editor (${editorCmd}). logging here but continuing. error: ${extractErrorMsg(error)}`);
                    // throw error; // just log, don't rethrow
                } finally {
                    // probably doesn't exist but hey i'm getting woogy
                    await deleteFileIfExists({ filePath: tmpFilePath });
                    if (logalot) { console.log(`${lcEditor} complete.`); }
                }
            }
        }

        // fallback plain prompt if editor failed
        if (!resCommentText && ['all', 'prompt'].includes(promptTypeIfNotFound)) {
            // editor failed, so last resort
            resCommentText = await promptForText({
                title: promptTitle,
                msg: promptMsg ?? 'Hey we want some text...so uh, enter it.',
                confirm: false,
                noNewLine: false,
            });
        }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        resCommentText = '';
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }

    // return it no matter what! (no throw in this function)
    if (logalot) { console.log(`${lc} resCommentText: ${resCommentText} (I: 2b78f93d7b2e10047bb04acc40c80c24)`); }
    return resCommentText;
}

export async function deleteFileIfExists({
    filePath,
}: {
    filePath: string,
}): Promise<void> {
    const lc = `[${deleteFileIfExists.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: db013a97d7f9c77b78750e0aaa9e4b24)`); }
        const _ignoredResStat = await stat(filePath);
        await rm(filePath, { force: true });
    } catch (error) {
        // 'ENOENT: no such file or directory, stat '/home/wraiford/ibgib/ibgib/apps/ibgib/test-b2tfs/b2tmp/11122806init/.ibgib/.b2tfs-applymsg.tmp.md''
        const emsg = extractErrorMsg(error);
        if (emsg.includes('ENOENT')) {
            // doesn't exist, that's fine
        } else {
            console.error(`${lc} ignoring (no rethrow) unexpected error that doesn't have ENOENT. error: ${emsg} (E: 93acde4c290a4804a531c5fa1595db74)`);
            // does not rethrow, just ignore
        }
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}
export function stripApplyMsgCommentLines({
    applyMsgText,
}: {
    applyMsgText: string,
}): string {
    const lc = `[${stripApplyMsgCommentLines.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: b43676bea10197c42ed77a113c6a0424)`); }
        const lines = applyMsgText.split('\n');
        const resLines: string[] = [];
        for (let i = 0; i < lines.length; i++) {
            const line = lines[i];
            if (!line.match(B2TFS_APPLY_DIFF_COMMENT_IGNORE_REGEXP)) {
                resLines.push(line);
            }
        }
        return resLines.join('\n');
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}
