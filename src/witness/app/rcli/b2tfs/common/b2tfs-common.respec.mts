/**
 * @module b2tfs-common respec
 *
 * we gotta test our b2tfs-common types/helpers/constants/etc.
 */

// import { cwd, chdir, } from 'node:process';
// import { statSync } from 'node:fs';
// import { mkdir, } from 'node:fs/promises';
// import { ChildProcess, exec, ExecException } from 'node:child_process';
import * as pathUtils from 'path';

import {
    firstOfAll, firstOfEach, ifWe, ifWeMight, iReckon,
    lastOfAll, lastOfEach, respecfully, respecfullyDear
} from '@ibgib/helper-gib/dist/respec-gib/respec-gib.mjs';
const maam = `[${import.meta.url}]`, sir = maam;

import { extractErrorMsg, getTimestampInTicks, getUUID, hash, } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../../../ibgib-constants.mjs';
import { isPathFiltered } from './b2tfs-common-helper.mjs';
import { B2TFS_DEFAULT_ITEM_FILTER_PATTERNS } from '../b2tfs-item/b2tfs-item-constants.mjs';
import { B2TFS_IGNORE_PATTERN_PREFIX_INCLUDE, B2TFS_IGNORE_PATTERN_PREFIX_REGEXP_JS } from './b2tfs-common-constants.mjs';

/**
 * for verbose logging
 */
const logalot = GLOBAL_LOG_A_LOT; // change this when you want to turn off verbose logging

const lcFile: string = `[${pathUtils.basename(import.meta.url)}]`;

await respecfully(maam, `helpers`, async () => {

    await respecfully(maam, `isPathFiltered`, async () => {

        const PATHS_FILTERED_BY_DEFAULT = [
            '.ibgib',
            '.ibgib/',
            './.ibgib',
            './.ibgib/',
            '/wakka/user/my-repo/.ibgib',
            '/wakka/user/my-repo/.ibgib/',
            'wakka/user/my-repo/.ibgib',
            'wakka/user/my-repo/.ibgib/',
            'node_modules',
            'node_modules/',
            './node_modules',
            './node_modules/',
            './src/node_modules',
            './src/node_modules/',
            '/wakka/user/my-repo/src/node_modules',
            '/wakka/user/my-repo/src/node_modules/',
        ];
        const PATHS_NOT_FILTERED_BY_DEFAULT = [
            './not filtered.md',
            '/wakka/',
            '/wakka/',
            '../readme.md',
            '/wakka/user/my-repo/src/readme.md',
            '/wakka/user/my repo/src/readme.md',
            '../wakka/user/my repo/src/readme.md',
        ];
        await respecfully(maam, `default filter patterns`, async () => {
            const filterPatterns = B2TFS_DEFAULT_ITEM_FILTER_PATTERNS.concat();

            ifWe(sir, 'filtered paths', async () => {
                for (let i = 0; i < PATHS_FILTERED_BY_DEFAULT.length; i++) {
                    const inputPath = PATHS_FILTERED_BY_DEFAULT[i];
                    try {
                        const isFiltered = isPathFiltered({ filterPatterns, inputPath });
                        iReckon(maam, isFiltered).isGonnaBeTrue();
                    } catch (error) {
                        iReckon(maam, i).asTo(extractErrorMsg(error)).isGonnaBeFalse();
                    }
                }
            });
            ifWe(sir, 'not filtered paths', async () => {
                for (let i = 0; i < PATHS_NOT_FILTERED_BY_DEFAULT.length; i++) {
                    const inputPath = PATHS_NOT_FILTERED_BY_DEFAULT[i];
                    try {
                        const isFiltered = isPathFiltered({ filterPatterns, inputPath });
                        iReckon(maam, isFiltered).isGonnaBeFalse();
                    } catch (error) {
                        iReckon(maam, i).asTo(extractErrorMsg(error)).isGonnaBeFalse();
                    }
                }
            });
        });

        await respecfully(maam, `filter patterns with include prefixes`, async () => {
            const prefixes = ['!', B2TFS_IGNORE_PATTERN_PREFIX_INCLUDE];
            for (let i = 0; i < prefixes.length; i++) {
                const prefix = prefixes[i];

                ifWe(sir, prefix, async () => {
                    /**
                     * we're taking the default filter patterns and prefixing
                     * each one with the prefix that means "include"/"don't
                     * filter" if this pattern matches.
                     */
                    const filterPatterns =
                        B2TFS_DEFAULT_ITEM_FILTER_PATTERNS.map(pattern => prefix + pattern);
                    ;
                    for (let i = 0; i < PATHS_FILTERED_BY_DEFAULT.length; i++) {
                        const inputPath = PATHS_FILTERED_BY_DEFAULT[i];
                        try {
                            const isFiltered = isPathFiltered({ filterPatterns, inputPath });
                            iReckon(maam, isFiltered).isGonnaBeFalse();
                        } catch (error) {
                            iReckon(maam, i).asTo(extractErrorMsg(error)).isGonnaBeFalse();
                        }
                    }
                });
            }
        });

        await respecfully(maam, `js regexp filter patterns`, async () => {
            ifWe(sir, 'replicate default string literals', async () => {
                /**
                 * atow (01/2024) {@link B2TFS_IGNORE_PATTERN_PREFIX_REGEXP_JS}:
                 *   'regexp js '
                 *
                 * note the trailing space after "js"
                 */
                const jsPrefix = B2TFS_IGNORE_PATTERN_PREFIX_REGEXP_JS.concat();
                const filterPatterns = [
                    jsPrefix + '\\.ibgib\\/?$',
                    jsPrefix + 'node_modules\\/?$',

                    // so atow (01/2024) these entries would look like this in .ibgibignore:
                    // regexp js \\.ibgib\\/?$
                    // regexp js node_modules\\/?$
                ];
                const inputPaths = PATHS_FILTERED_BY_DEFAULT.concat();
                for (let i = 0; i < inputPaths.length; i++) {
                    const inputPath = inputPaths[i];
                    try {
                        const isFiltered = isPathFiltered({ filterPatterns, inputPath });
                        iReckon(maam, isFiltered).asTo(inputPath).isGonnaBeTrue();
                    } catch (error) {
                        iReckon(maam, i).asTo(extractErrorMsg(error)).isGonnaBeFalse();
                    }
                }
            });
        });
    });

});
