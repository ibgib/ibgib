/**
 * I'm unsure of the greatness of my implementation, but the idea is that when
 * we are doing b2tfs things, we use this lock.
 */
export const B2TFS_LOCK_SCOPE = 'B2tFS_LockScope';

/**
 * rather arbitrary choice of lock timeout atow.
 */
export const B2TFS_LOCK_TIMEOUT_MS = 60_000;

/**
 * rel8nName on the index ibgib that maps to the roots
 */
export const B2TFS_BRANCH_TJP_REL8N_NAME = 'branch_tjp';

/**
 * shared regexp used for names in b2tfs.
 *
 * for right now this is extremely restrictive. when this expands we must be
 * sure to have it fully tested and think through which encoding/parsing pieces
 * of code are affected.
 *
 * this atow (12/2023) is allows a simple (but long) string for roots, items.
 */
export const B2TFS_NAME_REGEXP = /^[a-zA-Z0-9_\-. ]{1,1024}$/;

/**
 * NOTE: THIS MUST MATCH {@link B2TFS_SAFER_NAME_REGEXP} MAX LENGTH.
 */
export const B2TFS_MAX_SAFER_NAME_LENGTH = 16;

/**
 * regexp for safer name that we will use in items' ib fields.
 *
 * NOTE: THIS MUST NOT ALLOW FOR SPACES AS B2TFS ITEM IBS ARE SPACE-DELIMITED.
 *
 * NOTE: THIS MUST MATCH {@link B2TFS_MAX_SAFER_NAME_LENGTH}
 */
export const B2TFS_SAFER_NAME_REGEXP = /^[a-zA-Z0-9_\-.]{1,16}$/;

/**
 * prefix for spaces created to be roots/branches
 */
export const B2TFS_SPACE_NAME_PREFIX = 'b2tfs-space_';

/**
 * string prepended to folder name in path for b2tfs.
 *
 * ## driving use case (12/2023)
 *
 * we want to avoid traversing other roots'/branches' projected folders, so
 * this prefix is a reserved prefix.
 *
 * @see {@link "/README.md"} simple repo step-by-step algorithmic notes
 */
export const B2TFS_BRANCH_PATH_PREFIX = 'b2b_';

/**
 * prefix to an ignore pattern in .ibgibignore to indicate that the pattern
 * should be matched using regexp. this should be the string equal to the
 * `RegExp.source`.
 *
 * NOTE: this includes a trailing space
 *
 * ## dev notes
 *
 * i have formatted it this way thinking it's similar to an ib metadata, which
 * are usually space-delimited.
 *
 * also this is js specific because thinking that eventually there will be
 * multiple languages that ibgib is implemented in and this will end up being
 * data as metadata
 */
export const B2TFS_IGNORE_PATTERN_PREFIX_REGEXP_JS = 'regexp js ';
/**
 * NOTE: this includes a trailing space
 */
export const B2TFS_IGNORE_PATTERN_PREFIX_INCLUDE = 'include ';

// /**
//  * root/branch spaces created specifically for b2tfs have additional data in
//  * their `ibGib.data` state. This key accesses that additional data, so the
//  * caller should be able to access it via:
//  *
//  * `const b2tfsInfo = space.data[B2TFS_SPACE_IBGIB_DATA_KEY] as B2tFSSpaceSpecificData`
//  *
//  * @see `B2tFSSpaceSpecificData`
//  */
// export const B2TFS_SPACE_IBGIB_DATA_KEY = 'b2tfs';

/**
 * trying to get to use vim for editing apply comment text.
 */
export const B2TFS_EDITOR_TMP_FILENAME_FOR_APPLY_COMMENT_TEXT = '.b2tfs-applymsg.tmp.md';

/**
 * default text when applying a diff (if no comment text is given)
 */
export const B2TFS_DEFAULT_APPLY_DIFF_COMMENT_TEXT = 'no comment yo';

/**
 * regexp to run against individual lines in the apply diff comment text.
 *
 * atow (01/2024) this ignores a line if its first non-whitespace character is a
 * hash (#).
 */
export const B2TFS_APPLY_DIFF_COMMENT_IGNORE_REGEXP = /^\s*#.*$/;
