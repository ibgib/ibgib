/**
 * @module b2tfs-common-types (and enums)
 */

import { IbGibData_V1, IbGibRel8ns_V1, IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { IbGibAddr } from '@ibgib/ts-gib/dist/types.mjs';
import { MetaspaceService } from '@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs';
import { SpaceId } from '@ibgib/core-gib/dist/witness/space/space-types.mjs';


/**
 * type of the enveloping/container space for the `B2tFSIndexIbGib`.  see the
 * {@link B2tFSSpaceType} enum-ish constant's individual members for more
 * details.
 *
 * @see {@link B2tFSSpaceType.creation}
 * @see {@link B2tFSSpaceType.source}
 * @see {@link B2tFSSpaceType.branch}
 */
export type B2tFSSpaceType = 'creation' | 'source' | 'branch';
/**
 * type of the enveloping/container space for the `B2tFSIndexIbGib`.  see the
 * {@link B2tFSSpaceType} enum-ish constant's individual members for more
 * details.
 *
 * @see {@link B2tFSSpaceType.creation}
 * @see {@link B2tFSSpaceType.source}
 * @see {@link B2tFSSpaceType.branch}
 */
export const B2tFSSpaceType = {
    /**
     * a creation space is an original space where a b2tfs is first initialized.
     *
     * it is largely defined as being neither a source space nor a branch space,
     * i.e., it doesn't have files/folders tracked in the version control.
     * relative to source/branch spaces (and possibly others in the future like
     * tag spaces maybe?), creation spaces are metaspaces but i didn't want to
     * overload the term as that already specifically means the coordinating
     * space among other spaces (not just b2tfs).
     */
    creation: 'creation' as B2tFSSpaceType,
    /**
     * a source space contains a B2tFSBranchIbGib, meaning it did not branch from
     * another existing ibgib space
     *
     * _note: pedantically, it branches from the ROOT ib^gib zero/infinity space._
     */
    source: 'source' as B2tFSSpaceType,
    /**
     * a branch space contains a B2tFSBranchIbGib also but this was branched from
     * another ibgib space.
     */
    branch: 'branch' as B2tFSSpaceType,
}

/**
 * shared opts for creating source/branch spaces
 */
export interface CreateB2tFSSpaceOpts {
    name: string;
    metaspace: MetaspaceService,
    // srcSpaceId?: SpaceId;
}
/**
 * options specific to creating source spaces
 */
export interface CreateB2tFSSpaceOpts_Source extends CreateB2tFSSpaceOpts {
}
/**
 * options specific to creating branch spaces
 */
export interface CreateB2tFSSpaceOpts_Branch extends CreateB2tFSSpaceOpts {
    // srcSpaceId: SpaceId;
}
