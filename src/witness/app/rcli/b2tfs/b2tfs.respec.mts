/**
 * @module b2tfs respec
 *
 * this is where we can run the big integration tests to see that b2tfs is
 * working.
 *
 * I'm including types/helpers/kitchen sinks in here because hey, yazza.
 *
 *
 * ## more insight
 *
 * remember to turn on logalot per file that you are interested in examining.
 * the GLOBAL_LOG_A_LOT might be a bit much.
 */

import { cwd, chdir, } from 'node:process';
import { cp, mkdir, readdir, writeFile, rename, } from 'node:fs/promises';
import * as pathUtils from 'path';

import {
    firstOfAll, firstOfEach, ifWe, ifWeMight, iReckon,
    lastOfAll, lastOfEach, respecfully, respecfullyDear
} from '@ibgib/helper-gib/dist/respec-gib/respec-gib.mjs';
const maam = `[${import.meta.url}]`, sir = maam;

import { extractErrorMsg, getTimestampInTicks, getUUID, } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../../ibgib-constants.mjs';
import { execRCLIInNode } from '../../../../respec-gib-helper.node.mjs';
import { getIbGibGlobalThis } from '../rcli-helper.mjs';
import { RCLICommand } from '../rcli-constants.mjs';
import { FileEncoding } from '../rcli-types.mjs';
import { addNewFile, doB2tFSPostTestStuffLikeCdBackToInitialCwd, doB2tFSPreTestStuffAndChdirIntoTestDir, execB2tFSCmdAndCheckAfter, getUniqueB2tFSTestId } from './b2tfs.respec-helper.mjs';
import { B2tFSTestStartState } from './b2tfs-respec-types.mjs';
import { B2TFS_DEFAULT_TEST_CONFIG } from './b2tfs-respec-constants.mjs';

/**
 * for verbose logging
 */
const logalot = GLOBAL_LOG_A_LOT;

const lcFile: string = `[${pathUtils.basename(import.meta.url)}]`;

await respecfully(maam, `b2tfs basics saga`, async () => {

    /**
     * permutations of repo + .ibgib folder. each of the following tests will be
     * executed for each permutation.
     */
    let startStates: B2tFSTestStartState[] = [];

    startStates = [
        {
            ibgibs: { name: '20_initialized_with_app_and_robbot', },
            repo: { type: 'simple', name: '00_1_file', },
        },
        {
            ibgibs: { name: '20_initialized_with_app_and_robbot', },
            repo: { type: 'simple', name: '01_files_only', },
        },
        {
            ibgibs: { name: '20_initialized_with_app_and_robbot', },
            repo: { type: 'simple', name: '02_files_and_folders', },
        },
    ];

    /**
     * each individual test leg has this as a timeout.
     *
     * make this a large number to
     */
    const timeoutMs = 10_000; // arbitrary for my slowish laptop
    // const timeoutMs = 1000 * 60 * 30; // arbitrary long timeout when debugging tests

    for (let i = 0; i < startStates.length; i++) {

        await respecfully(maam, `startState ${i}`, async () => {
            const startState = startStates[i];
            const lc = `${lcFile}[repo:${startState.repo.name}|ibgib:${startState.ibgibs.name}]`;
            const initialCwd = cwd();
            try {
                if (logalot) { console.log(`${lc} starting... (I: e8e9563e813dacb95f37a2814a709923)`); }

                // create a test directory
                const { relPathFromTestDirToPackageJson, resolvedTestOutputPath } =
                    await doB2tFSPreTestStuffAndChdirIntoTestDir({
                        uniqueTestId: getUniqueB2tFSTestId('basics'),
                        testConfig: B2TFS_DEFAULT_TEST_CONFIG,
                        startState,
                    });

                // init
                await execB2tFSCmdAndCheckAfter({
                    respecfulTitle: sir,
                    label: 'initial init',
                    relPathFromTestDirToPackageJson,
                    cmd: RCLICommand.b2tfs_init,
                    cmdArgs: undefined,
                    stdOutIncludes: [
                        "Created B2tFS Index special index ibgib",
                    ],
                    timeoutMs,
                    // veryPolite: true,
                });

                // info (b2tfs)
                await execB2tFSCmdAndCheckAfter({
                    respecfulTitle: sir,
                    label: 'first info after init',
                    relPathFromTestDirToPackageJson,
                    cmd: RCLICommand.b2tfs_info,
                    cmdArgs: undefined,
                    stdOutIncludes: [
                        'Here ya go, info on the B2tFS',
                        '"indexIbGib": {\n    "ib": "meta special b2tfs",', // } ide bracket matching
                        '"$@branch": []',
                        '"indexExists": true',
                        '"indexAddr": "meta special b2tfs^',
                        '"indexTjpAddr": "meta special b2tfs^',
                        '"branchInfos": {}',
                    ],
                    timeoutMs,
                    // veryPolite: true,
                });

                // b2tfs_branch - add root branch
                await execB2tFSCmdAndCheckAfter({
                    respecfulTitle: sir, label: 'add root branch',
                    relPathFromTestDirToPackageJson,
                    cmd: RCLICommand.b2tfs_branch,
                    cmdArgs: '. --add --msg="initial comment (this will be a super test root branch...really great i tell ya)"',
                    stdOutIncludes: [
                        'Created but did not activate a root B2tFS Branch IbGib, as well as any child B2tFS Item IbGibs for child files/folders.',
                    ],
                    timeoutMs,
                    // veryPolite: true,
                });

                // b2tfs_info
                await execB2tFSCmdAndCheckAfter({
                    respecfulTitle: sir, label: 'info after add root',
                    relPathFromTestDirToPackageJson,
                    cmd: RCLICommand.b2tfs_info,
                    cmdArgs: undefined,
                    stdOutIncludes: [
                        // indicates that there is a branch added
                        '"branchInfos": {\n    "v:1^spaceId', // } ide bracket matching
                    ],
                    timeoutMs,
                    // veryPolite: true,
                });

                // b2tfs_activate_branch
                /**
                 * by default, branch-name is the directory when the above
                 * `--b2tfs-branch --add` happened,
                 */
                const branchName = pathUtils.basename(cwd());
                await execB2tFSCmdAndCheckAfter({
                    respecfulTitle: sir, label: 'activate it',
                    relPathFromTestDirToPackageJson,
                    cmd: RCLICommand.b2tfs_activate_branch,
                    cmdArgs: `--name=${branchName}`,
                    stdOutIncludes: [
                        'activated. branchSpaceId: ',
                    ],
                    timeoutMs,
                    // veryPolite: true,
                });

                // b2tfs_info
                await execB2tFSCmdAndCheckAfter({
                    respecfulTitle: sir, label: 'info after activate',
                    relPathFromTestDirToPackageJson,
                    cmd: RCLICommand.b2tfs_info,
                    cmdArgs: undefined,
                    stdOutIncludes: [
                        '      "activeSpaceId": "',
                    ],
                    timeoutMs,
                    // veryPolite: true,
                });

                // b2tfs_diff before anything (no differences)
                await execB2tFSCmdAndCheckAfter({
                    respecfulTitle: sir, label: 'diff should be none',
                    relPathFromTestDirToPackageJson,
                    cmd: RCLICommand.b2tfs_diff,
                    cmdArgs: undefined,
                    stdOutIncludes: [
                        'NO differences found.',
                    ],
                    timeoutMs,
                    // veryPolite: true,
                });

                // b2tfs_diff after new file
                const newFileContents: string = 'yo this is a new file\n\nhey we got some new lines.\n\twakka';
                const newFileName = 'easy file not a pic or other binary.txt';
                const newFileRelPath = '.';
                const { newFilePath } = await addNewFile({ newFileRelPath, newFileName, newFileContents });
                await execB2tFSCmdAndCheckAfter({
                    respecfulTitle: sir, label: 'diff after new file',
                    relPathFromTestDirToPackageJson,
                    cmd: RCLICommand.b2tfs_diff,
                    cmdArgs: undefined,
                    stdOutIncludes: [
                        'YES, differences found but NOT applied.',
                        pathUtils.basename(cwd()), // lists the folder name that contains the difference
                        `${newFileName}:`,
                        `oldData:\n`,
                        `[data undefined? hmmm. (E: 1627d91fc5d84bd18a11336bdff04ad5)]`,
                        `newData:\n`,
                        newFileContents.substring(0, 16), // arbitrary substring atow 01/2024
                    ],
                    timeoutMs,
                    // veryPolite: true,
                    // fnlogalot: true,
                });

                // diff after rename folder
                if (logalot) { console.log(`${lc} resolvedTestOutputPath: ${resolvedTestOutputPath} (I: c527c6fe475c5e9f32b214dd7b943424)`); }
                const resDirents = await readdir(resolvedTestOutputPath, { withFileTypes: true });
                const fooFolder = resDirents.filter(x => x.isDirectory() && x.name === 'foo').at(0); // arbitrary test folder
                if (fooFolder) {
                    if (logalot) { console.log(`${lc} testing rename foo folder (I: 280b8e6044ebbb5599974f8fe6d31824)`); }
                    const fooFolderPath = pathUtils.join(resolvedTestOutputPath, fooFolder.name);
                    const fooFolderPath_renamed = pathUtils.join(resolvedTestOutputPath, fooFolder.name + '_renamed');
                    const fooFolderPath_renamed_parsed = pathUtils.parse(fooFolderPath_renamed);
                    await rename(fooFolderPath, fooFolderPath_renamed);
                    await execB2tFSCmdAndCheckAfter({
                        respecfulTitle: sir, label: 'diff after rename folder',
                        relPathFromTestDirToPackageJson,
                        cmd: RCLICommand.b2tfs_diff,
                        cmdArgs: undefined,
                        stdOutIncludes: [
                            'YES, differences found but NOT applied.',
                            fooFolderPath_renamed_parsed.name,
                        ],
                        timeoutMs,
                        // veryPolite: true,
                        // fnlogalot: true,
                    });
                }

                // apply diff
                await execB2tFSCmdAndCheckAfter({
                    respecfulTitle: sir, label: 'apply diff',
                    relPathFromTestDirToPackageJson,
                    cmd: RCLICommand.b2tfs_diff,
                    /**
                     * could use just 'apply' or 'apply-cmd' or other synonyms.
                     * @see {@link PARAM_INFO_APPLY}
                     */
                    cmdArgs: `--apply-diff --text="hey we're applying this diff"`, //
                    stdOutIncludes: [
                        'YES, differences found, and YES they were applied.',
                    ],
                    timeoutMs,
                    // veryPolite: true,
                    // fnlogalot: true,
                });

                // b2tfs_diff after apply (no differences)
                await execB2tFSCmdAndCheckAfter({
                    respecfulTitle: sir, label: 'b2tfs_diff after apply (no differences)',
                    relPathFromTestDirToPackageJson,
                    cmd: RCLICommand.b2tfs_diff,
                    cmdArgs: undefined,
                    stdOutIncludes: [
                        'NO differences found.',
                    ],
                    timeoutMs,
                    // veryPolite: true,
                    // fnlogalot: true,
                });

                // b2tfs_diff with unnecessary apply flag (no differences)
                await execB2tFSCmdAndCheckAfter({
                    respecfulTitle: sir, label: 'b2tfs_diff with unnecessary apply flag (no differences)',
                    relPathFromTestDirToPackageJson,
                    cmd: RCLICommand.b2tfs_diff,
                    cmdArgs: `--apply-diff --text="another apply here but no differences so this is ignored"`,
                    stdOutIncludes: [
                        'NO differences found, so NOTHING applied.',
                    ],
                    timeoutMs,
                    // veryPolite: true,
                });

            } catch (error) {
                console.error(`${lc} ${extractErrorMsg(error)}`);
                throw error;
            } finally {
                await doB2tFSPostTestStuffLikeCdBackToInitialCwd({
                    initialCwd,
                    testConfig: B2TFS_DEFAULT_TEST_CONFIG,
                    startState,
                });
                if (logalot) { console.log(`${lc} complete.`); }
            }
        });
    }
});
