import { cwd, chdir, } from 'node:process';
import { cp, mkdir, readdir, writeFile, rename, } from 'node:fs/promises';
import * as pathUtils from 'path';

import {
    firstOfAll, firstOfEach, ifWe, ifWeMight, iReckon,
    lastOfAll, lastOfEach, respecfully, respecfullyDear
} from '@ibgib/helper-gib/dist/respec-gib/respec-gib.mjs';

import { extractErrorMsg, getTimestampInTicks, } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../../ibgib-constants.mjs';
import { execRCLIInNode } from '../../../../respec-gib-helper.node.mjs';
import { RCLICommand } from '../rcli-constants.mjs';
import { FileEncoding } from '../rcli-types.mjs';
import { B2tFSTestConfig, B2tFSTestStartState } from './b2tfs-respec-types.mjs';
import { getIbGibGlobalThis } from '../rcli-helper.mjs';

/**
 * for verbose logging
 */
const logalot = GLOBAL_LOG_A_LOT;


/**
 * @see {@link FnAfterExecCmd}
 */
export interface FnAfterExecCmdArg {
    /**
     * this is the maam or sir or whatever from the caller's respec file.
     */
    respecfulTitle: string;
    std_out?: string;
    std_err?: string;
    err?: any | null;
    stdOutIncludes?: string[];
    fnlogalot?: boolean | number;
}
/**
 * this function should execute after the node cmd line one-off executes and
 * returns.
 *
 * this is expected to execute within an `ifWe` block, so you should be able to
 * use `iReckon` statements within this fn (e.g. as done atow (01/2024) in
 * {@link FN_DEFAULT_AFTER_EXEC_CMD}).
 *
 * @see {@link FnAfterExecCmdArg}
 * @see {@link FN_DEFAULT_AFTER_EXEC_CMD}
 */
export type FnAfterExecCmd = (arg: FnAfterExecCmdArg) => Promise<void>;
/**
 * @see {@link FnAfterExecCmd}
 */
export async function FN_DEFAULT_AFTER_EXEC_CMD({
    std_out, std_err, err, stdOutIncludes,
    respecfulTitle,
    fnlogalot,
}: FnAfterExecCmdArg): Promise<void> {
    const lc = `[${FN_DEFAULT_AFTER_EXEC_CMD.name}]`;
    try {
        if (fnlogalot) { console.log(`${lc} starting... (I: 13f1038d708a81806298cdae43cbd524)`); }
        if (!stdOutIncludes || stdOutIncludes.length === 0) { throw new Error(`(UNEXPECTED) stdOutIncludes falsy/empty? (atow 01/2024) default fn after is to have iReckon statements about std_out includingn certain texts. (E: cdc60e4fb79ead133c8bc91106b0fa24)`); }
        if (!std_out) { throw new Error(`(UNEXPECTED) std_out falsy? (atow 01/2024) default fn after is to have iReckon statements about std_out includingn certain texts. (E: bd208ddbd12f4889a46fb58091b562c1)`); }

        iReckon(respecfulTitle, err).isGonnaBe(null);
        for (let i = 0; i < stdOutIncludes.length; i++) {
            const includesThis = stdOutIncludes[i];
            iReckon(respecfulTitle, std_out).asTo(includesThis.substring(0, 15)).includes(includesThis);
        }
        if (fnlogalot) {
            console.log(`${lc} std_err: ${std_err} (I: 7d2a2e17253847d6a0ee30f10fbee920)`);
            console.log(`${lc} std_out: ${std_out} (I: b56294a80b335ab39c57614bafbd2724)`);
        }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (fnlogalot) { console.log(`${lc} complete.`); }
    }
};

export async function addNewFile({
    newFileRelPath,
    newFileName,
    newFileContents,
}: {
    /**
     * relative path to the folder in which we are creating a new file.
     *
     * assumes `cwd()` is the base folder of the repo.
     */
    newFileRelPath: string,
    newFileName: string,
    newFileContents: string,
}): Promise<{ newFilePath: string }> {
    const lc = `[${addNewFile.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 5e20b79ee3e8c39a624f48bb6a797824)`); }
        const newFilePath = pathUtils.join(newFileRelPath, newFileName);
        await writeFile(
            newFilePath,
            newFileContents,
            { encoding: FileEncoding.utf8, }
        )
        return { newFilePath };
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export async function execB2tFSCmdAndCheckAfter({
    relPathFromTestDirToPackageJson,
    label,
    cmd,
    cmdArgs,
    timeoutMs = 10_000,
    fnAfter = FN_DEFAULT_AFTER_EXEC_CMD,
    stdOutIncludes,
    veryPolite,
    fnlogalot,
    respecfulTitle,
}: {
    relPathFromTestDirToPackageJson: string,
    label: string,
    cmd: RCLICommand,
    cmdArgs: string | undefined,
    stdOutIncludes: string[],
    timeoutMs?: number,
    fnAfter?: FnAfterExecCmd,
    /**
     * if true, uses ifWeMight.
     *
     * Note that there must be either respecfullyDear or ifWeMight followed
     * directly by an open paren "(" to trigger the respec system to register a
     * shortcircuit respec execution. Or IOW if this `veryPolite` is true, you
     * have to have the surround `respecfully` block use `respecfullyDear`.
     */
    veryPolite?: boolean,
    fnlogalot?: boolean | number,
    /**
     * this is the maam or sir or whatever from the caller's respec file.
     */
    respecfulTitle: string,
}): Promise<void> {
    const lcTest = `--${cmd}[${label}]`;
    fnlogalot ??= logalot;
    let fnIfWe = veryPolite ? ifWeMight : ifWe;
    await fnIfWe(respecfulTitle, lcTest, async () => {
        if (fnlogalot) { console.log(`${lcTest} starting... (I: dae5cc468855a219a6e8b94eb8635a24)`); }
        fnAfter ??= FN_DEFAULT_AFTER_EXEC_CMD;
        console.time(lcTest);
        try {
            const [resNodePromise] = await execRCLIInNode({
                execPath: relPathFromTestDirToPackageJson,
                argsString: `--${cmd}${cmdArgs ? ' ' + cmdArgs : ''}`,
                timeoutMs: timeoutMs ?? 10_000,
                logContext: lcTest,
                dontLogStdErr: true,
            });
            const resNode = await resNodePromise;
            // const { std_out, std_err, err } = resNode;
            if (fnAfter) { await fnAfter({ ...resNode, stdOutIncludes, respecfulTitle, fnlogalot }); }
        } catch (error) {
            console.error(`${lcTest} error: ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (fnlogalot) { console.log(`${lcTest} complete. (I: 23913edae16107fdf2a44174bd86c224)`); }
            console.timeEnd(lcTest);
        }
    });
}

export function getUniqueB2tFSTestId(label: string): string {
    // shorter is better for pathing issues in node space
    return (new Date()).getHours().toString() +
        (new Date()).getMinutes().toString() +
        getTimestampInTicks().substring(9) +
        `${label}`;
};


/**
 * silly name because i don't know wth i'm doing yet
 */
export async function doB2tFSPreTestStuffAndChdirIntoTestDir({
    uniqueTestId,
    testConfig,
    startState,
}: {
    uniqueTestId: string,
    testConfig: B2tFSTestConfig,
    startState: B2tFSTestStartState,
}): Promise<{
    resolvedTestOutputPath: string,
    /**
     * the relative path back to the ibgib package that a call to node will use
     * (i.e. the path to this project's package.json).
     */
    relPathFromTestDirToPackageJson: string,
}> {
    const lc = `[${doB2tFSPreTestStuffAndChdirIntoTestDir.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: cb4659de26213ec927511b43e417f324)`); }

        // reuse these path functions (but i don't like them file-scoped)
        const { resolve, join } = pathUtils;

        /** we'll work with this base path for most operations here.  */
        const resolvedTestBasePath = resolve(join(cwd(), testConfig.basePath));

        // first do the .ibgibs folder (recursive creates the output folder)...
        const resolvedTestOutputPath = join(
            resolvedTestBasePath,
            testConfig.outputSubPath,
            uniqueTestId
        );
        const resolvedIbgibsInputPath = join(
            resolvedTestBasePath,
            testConfig.startState.ibgibSnapshotsSubpath,
            startState.ibgibs.name,
        );
        await mkdir(resolvedTestOutputPath, { recursive: true });
        await cp(
            resolvedIbgibsInputPath,
            resolvedTestOutputPath,
            { recursive: true, }
        );

        // copy the repo files/folders next...
        const resolvedRepoInputPath = join(
            resolvedTestBasePath,
            startState.repo.type === 'simple' ?
                testConfig.startState.repoSubpaths.simple :
                testConfig.startState.repoSubpaths.complex,
            startState.repo.name,
        );
        const resolvedRepoOutputPath = resolvedTestOutputPath.concat();
        await cp(
            resolvedRepoInputPath + '/', // children files & folders not folder itself
            resolvedRepoOutputPath,
            { recursive: true }
        );

        // cd into our test output path
        if (logalot) { console.log(`${lc} cd into ${resolvedTestOutputPath} (I: b122380d8e229089c26d85a51bb8cb24)`); }
        chdir(resolvedTestOutputPath);

        return {
            resolvedTestOutputPath,
            relPathFromTestDirToPackageJson:
                pathUtils.relative(resolvedTestOutputPath, getIbGibGlobalThis().initialCwd)
        };
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * silly name because i don't know wth i'm doing yet
 */
export async function doB2tFSPostTestStuffLikeCdBackToInitialCwd({
    initialCwd,
    testConfig,
    startState,
}: {
    initialCwd: string,
    testConfig: B2tFSTestConfig,
    startState: B2tFSTestStartState,
}): Promise<void> {
    const lc = `[${doB2tFSPostTestStuffLikeCdBackToInitialCwd.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 58f5abead5434c9d966bf8bc7c47a49f)`); }

        // cd back to the initialCwd
        if (logalot) { console.log(`${lc} done with test. cd back into ${initialCwd} (I: 37d2dacf55a841c09eeb02c0f9ed0cfc)`); }
        chdir(initialCwd);

    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}
