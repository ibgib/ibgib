import * as pathUtils from 'path';
import { execPath, cwd, } from 'node:process';
import { statSync } from 'node:fs';
import * as readline from 'node:readline/promises';
import { stdin, stdout } from 'node:process'; // decide if use this or not

import { extractErrorMsg, getTimestampInTicks, getUUID, pretty } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { PARAM_INFO_DATA_PATH, PARAM_INFO_OUTPUT_PATH } from "@ibgib/helper-gib/dist/rcli/rcli-constants.mjs";
import { RCLIArgInfo, RCLIArgType, RCLIParamInfo, } from "@ibgib/helper-gib/dist/rcli/rcli-types.mjs";
import { extractArgValue, } from "@ibgib/helper-gib/dist/rcli/rcli-helper.mjs";
import { IbGibSpaceAny } from '@ibgib/core-gib/dist/witness/space/space-base-v1.mjs';
import { InnerSpace_V1 } from '@ibgib/core-gib/dist/witness/space/inner-space/inner-space-v1.mjs';
import { toDto } from '@ibgib/core-gib/dist/common/other/ibgib-helper.mjs';
import { AppFormBuilder } from '@ibgib/core-gib/dist/witness/app/app-helper.mjs';
import { MetaspaceService } from '@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../ibgib-constants.mjs';
import { ExpectPathType, IbGibGlobalThis } from './rcli-types.mjs';
import {
    RCLI_DEFAULT_COMMAND_ESCAPE_STRING, RCLI_MAX_ESCAPE_STRING_LENGTH,
    PARAM_INFO_INIT, PARAM_INFO_IN_MEMORY,
    PARAM_INFO_SPACE_ID, PARAM_INFO_SPACE_NAME,
    DEFAULT_PROMPT_TEMPLATE,
    RCLI_DEFAULT_OUTPUT_PATH,
} from "./rcli-constants.mjs";


/**
 * used in verbose logging
 */
const logalot = GLOBAL_LOG_A_LOT;

/**
 * stubbed validation function. there is also validation
 * when building the arg info objects.
 *
 * @returns error string if found, otherwise null
 *
 * @param argInfos
 */
export function validateArgInfos({ argInfos }: { argInfos: RCLIArgInfo[] }): string | undefined {
    const lc = `[${validateArgInfos.name}]`;
    let validationErrorIfAny: string | undefined = undefined;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 9621f214bf544e03879d0e943db1d409)`); }
        if (!argInfos || argInfos.length === 0) { throw new Error(`argInfos required. (E: 40d1ce3d11ff4cbc85becd83fb1783c2)`); }

        const hasInit = argInfos.some(x => x.name === PARAM_INFO_INIT.name);
        const hasInMemory = argInfos.some(x => x.name === PARAM_INFO_IN_MEMORY.name);

        if (hasInit && hasInMemory) {
            throw new Error(`cannot have both init and in-memory args (E: a6fdbc6bf0391fa033843eacf9a98c23)`);
        }

        // todo: flesh out argInfos validation
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        validationErrorIfAny = error.message;
    } finally {
        if (logalot) { console.log(`\n${lc} complete.`); }
        return validationErrorIfAny;
    }
}

/**
 * helper that checks a given `relOrAbsPath` against expectations per
 * `expectType`.
 *
 * @returns true if path points to something as given with `expectType`, else false.
 */
export function pathIsAsExpected({
    relOrAbsPath,
    expectType,
    throwIfNotExpected,
    warnIfNotExpected,
}: {
    /**
     * path to check
     */
    relOrAbsPath: string,
    /**
     * expectations about path. is it a file, is it a folder, does it exist, is it undefined
     */
    expectType: ExpectPathType,
    /**
     * side effect on if does not meet expectation
     */
    throwIfNotExpected?: boolean,
    /**
     * side effect on if does not meet expectation
     */
    warnIfNotExpected?: boolean,
}): boolean {
    const lc = `[${pathIsAsExpected.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 057bfa1bdae6c4c7e6ee577ddc193d23)`); }

        const stat = statSync(relOrAbsPath, { throwIfNoEntry: false });
        if (stat === undefined) {
            if (expectType === undefined) {
                // good, it's as expected
                return true; /* <<<< returns early */
            } else {
                // no existing file or folder of this path? A relOrAbsPath should
                const emsg = `relOrAbsPath not found. relOrAbsPath was expected as ${expectType}. initialize relOrAbsPath before proceeding. relOrAbsPath from args: ${relOrAbsPath}`;
                if (throwIfNotExpected) {
                    throw new Error(`${emsg} (E: 09f9599e16b847139aced1a041f4b593)`);
                } else if (warnIfNotExpected) {
                    console.warn(`${lc} ${emsg} (W: 6cac4705c1a74676b41165a96d1f8b69)`);
                }
                return false; /* <<<< returns early */
            }
        } else if (expectType === 'exists') {
            // good, it's as expected
            return true; /* <<<< returns early */
        } else if (stat.isFile()) {
            if (expectType === 'file') {
                // good, it's as expected
                return true; /* <<<< returns early */
            } else {
                const emsg = `relOrAbsPath found pointed to a file. relOrAbsPath was expected as ${expectType}. initialize relOrAbsPath before proceeding. relOrAbsPath from args: ${relOrAbsPath}`;
                if (throwIfNotExpected) {
                    throw new Error(`${emsg} (E: 670759bc99f44296995a29668c87ad36)`);
                } else if (warnIfNotExpected) {
                    console.warn(`${lc} ${emsg} (W: cabee5358cee4445ad271886340072d9)`);
                }
                return false; /* <<<< returns early */
            }
        } else if (stat.isDirectory()) {
            if (expectType === 'directory') {
                // good, it's as expected
                return true; /* <<<< returns early */
            } else {
                const emsg = `relOrAbsPath found pointed to a directory. relOrAbsPath was expected as ${expectType}. initialize relOrAbsPath before proceeding. relOrAbsPath from args: ${relOrAbsPath}`;
                if (throwIfNotExpected) {
                    throw new Error(`${emsg} (E: f179dca70dbe4b78a9146e8bb518c3a9)`);
                } else if (warnIfNotExpected) {
                    console.warn(`${lc} ${emsg} (W: 67516891c4f0475a9d454005561eee74)`);
                }
                return false; /* <<<< returns early */
            }
        } else {
            throw new Error(`(UNEXPECTED) unknown stat from statSync call? (E: 4f0432af699b397753b3918cf1326c23)`);
        }

    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}


// #region extractArg functions

/**
 * atow this is for an output file
 */
export async function extractArg_outputPath({
    argInfos,
    warnIfExists,
    createUniqueSubdirIfExists,
    defaultPathIfNotFound,
    throwIfNotFound,
}: {
    argInfos: RCLIArgInfo<RCLIArgType>[],
    warnIfExists: boolean,
    createUniqueSubdirIfExists: boolean,
    defaultPathIfNotFound?: string,
    throwIfNotFound?: boolean,
}): Promise<string> {
    const lc = `[${extractArg_outputPath.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 774e61a9043a45108783671a748ee130)`); }

        let relOrAbsPath: string;
        if (argInfos.some(x => x.name === PARAM_INFO_OUTPUT_PATH.name)) {
            const argInfo = argInfos.filter(x => x.name === PARAM_INFO_OUTPUT_PATH.name)[0];
            if (!argInfo.value) {
                throw new Error(`(UNEXPECTED) output path arg found but value is falsy? (E: 054b829796b94974a34d6912c22b7387)`);
            }
            relOrAbsPath = argInfo.value! as string;
        } else if (throwIfNotFound) {
            throw new Error(`output path is required. PARAM_INFO_OUTPUT_PATH: ${pretty(PARAM_INFO_OUTPUT_PATH)} (E: 2a2de5efd12e5ec9e8bfa15af03c3a24)`);
        } else {
            // throw new Error(`no output path arg provided (E: 3d2f58b4c92d4960934de13dea7cc628)`);
            defaultPathIfNotFound ??= RCLI_DEFAULT_OUTPUT_PATH;
            console.warn(`no output path arg provided. using default path: ${defaultPathIfNotFound} (E: 3d2f58b4c92d4960934de13dea7cc628)`);
            relOrAbsPath = defaultPathIfNotFound;
        }

        const stat = statSync(relOrAbsPath, { throwIfNoEntry: false });
        if (stat === undefined) {
            // no existing file or folder of this path, so we're good
        } else if (stat.isFile()) {
            throw new Error(`data output path is a file that already exists. overwriting ain't happening yet (E: 94884edf11094bd0a2c49f1e7dd34e8b)`);
        } else if (stat.isDirectory()) {
            if (warnIfExists) { console.warn(`${lc}[WARNING] output path already exists. (W: 128790f27d074fcfb9ac97614e906689)`); }
            if (createUniqueSubdirIfExists) {
                const filename = getTimestampInTicks() + (await getUUID()).slice(0, 6);
                relOrAbsPath = pathUtils.join(relOrAbsPath, filename);
            }
        }

        // add file extension only if we're encrypting and it isn't already there
        // const isEncrypt = argInfos.some(x => x.name === PARAM_INFO_ENCRYPT.name);
        // if (isEncrypt && !relOrAbsPath.endsWith(ENCRYPTED_OUTPUT_FILE_EXT)) {
        //     console.log(`${lc} adding file extension .${ENCRYPTED_OUTPUT_FILE_EXT}`);
        //     relOrAbsPath += (relOrAbsPath.endsWith('.') ? ENCRYPTED_OUTPUT_FILE_EXT : `.${ENCRYPTED_OUTPUT_FILE_EXT}`);
        // }

        return relOrAbsPath;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export async function extractArg_dataPath({
    argInfos,
    expectType,
    throwIfNotExpected,
    defaultPath,
    promptIfNotExpected,
}: {
    argInfos: RCLIArgInfo<RCLIArgType>[],
    expectType: ExpectPathType,
    throwIfNotExpected: boolean,
    defaultPath?: string,
    promptIfNotExpected: boolean,
}): Promise<string | undefined> {
    const lc = `[${extractArg_dataPath.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 24f48ba864d646ab8c04fb4547a23ccd)`); }

        let relOrAbsPath = extractArgValue({
            paramInfo: PARAM_INFO_DATA_PATH, argInfos, throwIfNotFound: false
        }) as string;

        let tryAgain = true;
        do {
            const isAsExpected = relOrAbsPath ?
                pathIsAsExpected({
                    relOrAbsPath,
                    expectType,
                    throwIfNotExpected,
                    warnIfNotExpected: true
                }) :
                false;

            if (isAsExpected) {
                tryAgain = false;
            } else {
                // already thrown/warned if not as expected

                // first try default if it's given
                if (defaultPath && defaultPath !== relOrAbsPath) {
                    // try again with default path
                    if (logalot) { console.log(`${lc} trying defaultPath ${defaultPath} (I: 22b4ff7e1b2cd69603a072eed927cd23)`); }
                    relOrAbsPath = defaultPath;
                    tryAgain = true;
                } else if (promptIfNotExpected) {
                    if (logalot) { console.log(`${lc} data-path (${relOrAbsPath}) not as expected (${expectType}). promptIfNotExpected true (I: 7ce139a815f459d819bb01c176014723)`); }
                    relOrAbsPath = await promptForText({
                        msg: `data-path not set. please enter a valid data-path that is expected to be ${expectType}`,
                        confirm: false,
                    });
                    tryAgain = true;
                } else {
                    // already warned not as expected at this point
                    if (logalot) { console.log(`${lc} not as expected (${expectType}). (I: ebccf2f510524cd395e73fea08b66923)`); }
                    return undefined; /* <<<< returns early */
                }
            }
        } while (tryAgain);

        return relOrAbsPath || undefined;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export async function extractArg_space({
    argInfos,
    metaspace,
    returnOnArgsFalsy,
    spaceIdParamInfo = PARAM_INFO_SPACE_ID,
    spaceNameParamInfo = PARAM_INFO_SPACE_NAME,
}: {
    argInfos: RCLIArgInfo<RCLIArgType>[],
    metaspace: MetaspaceService,
    /**
     * determines what to do if there are no specifiers in {@link argInfos}.
     *
     * 'undefined': return undefined
     * 'default': return the default local user space via {@link metaspace}
     * 'throw': throw an error
     */
    returnOnArgsFalsy: 'undefined' | 'default' | 'throw',
    /**
     * the spaceId param info to use to locate the space to extract.
     *
     * @see {@link PARAM_INFO_SPACE_ID}
     * @see {@link PARAM_INFO_SPACE_NAME}
     * @see {@link PARAM_INFO_LOCAL_SPACE_ID}
     * @see {@link PARAM_INFO_LOCAL_SPACE_NAME}
     */
    spaceIdParamInfo?: RCLIParamInfo,
    spaceNameParamInfo?: RCLIParamInfo,
}): Promise<IbGibSpaceAny | undefined> {
    const lc = `[${extractArg_space.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 24f48ba864d646ab8c04fb4547a23ccd)`); }

        spaceIdParamInfo ??= PARAM_INFO_SPACE_ID;
        spaceNameParamInfo ??= PARAM_INFO_SPACE_NAME;

        const spaceName = extractArgValue({ paramInfo: spaceNameParamInfo, argInfos, throwIfNotFound: false }) as string;
        const spaceId = extractArgValue({ paramInfo: spaceIdParamInfo, argInfos, throwIfNotFound: false }) as string;

        let space: IbGibSpaceAny | undefined;
        if (spaceName) {
            // console.warn(`${lc}[NOT IMPLEMENTED YET] ${spaceNameParamInfo.name} can't be used yet. Please specify the spaceId (space.data.uuid) (W: 774c801f853343b88fff856096876913)`);
            if (logalot) { console.log(`${lc} getting by spaceName (${spaceName}) (I: 9806c932dd9b166c1c6fdcefa15b5123)`); }
            const spaces = await metaspace.getLocalUserSpaces({ lock: false });
            let nameMatch = spaces.filter(x => x.data?.name === spaceName);
            if (nameMatch.length === 1) {
                space = nameMatch[0]!;
            } else if (nameMatch.length === 0) {
                throw new Error(`spaceName (${spaceName}) not found among local spaces. (E: b1784b5592ad26e788910394bc428623)`);
            } else if (nameMatch.length > 1) {
                throw new Error(`multiple spaces match. please re-run using space id. matches: ${pretty(nameMatch)} (E: 6d8a0fcbf277e38b8ad37894753f4923)`);
            }
        } else if (spaceId) {
            if (logalot) { console.log(`${lc} getting local user space by spaceId (${spaceId}) (I: f54ab2e75b280deffcc4778bfee6a823)`); }
            space = await metaspace.getLocalUserSpace({
                lock: false, localSpaceId: spaceId
            });
            if (!space) { throw new Error(`spaceId (${spaceId}) not found (E: 161e239216db686e585ceb1a2f90b223)`); }
        } else {
            // no space specified, so use the default space
            if (logalot) { console.log(`${lc} no space specified in argInfos. returnOnArgsFalsy: ${returnOnArgsFalsy}(I: 502a34b7e4852b13cdf7bf81b0b85a23)`); }
            switch (returnOnArgsFalsy) {
                case 'default':
                    space = await metaspace.getLocalUserSpace({ lock: false });
                    break;
                case 'throw':
                    throw new Error(`no space args found. (E: 040dfb14e231c76273d667187082a423)`);
                case 'undefined':
                    space = undefined;
                    break;
                default:
                    throw new Error(`(UNEXPECTED) unknown returnOnArgsFalsy? (${returnOnArgsFalsy}) (E: 435063fb45e6ad3e5194672d3bb89a23)`);
            }
            if (!space) { throw new Error(`(UNEXPECTED) couldn't get default local user space? (E: 668d4c1a37e5997ec1138b223b038d23)`); }
        }
        return space;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

// export function extractArg_hashAlgorithm({
//     argInfos,
// }: {
//     argInfos: RCLIArgInfo<RCLIArgType>[],
// }): HashAlgorithm | undefined {
//     const lc = `[${extractArg_hashAlgorithm.name}]`;
//     try {
//         if (logalot) { console.log(`${lc} starting... (I: 6f42818e6a7a4c05a3d8a3da8e625598)`); }

//         const hashAlgorithm = extractArgValue({ paramInfo: PARAM_INFO_HASH_ALGORITHM, argInfos }) as string | undefined;

//         if (hashAlgorithm) {
//             if (!HASH_ALGORITHMS.includes(hashAlgorithm as HashAlgorithm)) {
//                 throw new Error(`unknown hashAlgorithm: ${hashAlgorithm}. Must be one of: ${HASH_ALGORITHMS.join(", ")} (E: dae1ceaf6bfd4f82b9342a51c37ab1b2)`);
//             }
//             return hashAlgorithm as HashAlgorithm;
//         } else {
//             return undefined;
//         }
//     } catch (error) {
//         console.error(`${lc} ${extractErrorMsg(error)}`);
//         throw error;
//     } finally {
//         if (logalot) { console.log(`${lc} complete.`); }
//     }
// }

/**
 * gets the path arg from given `argInfos` that corresponds to the given
 * `paramInfo`. looks for the bareArg according to `canBeBareArg`, and if not
 * there should be an explicit `paramInfo` param.
 *
 * @throws if neither or both are found, or if the path does not point to an existing valid directory.
 * @returns path pointing to an existing valid directory from `argInfos`
 */
export async function extractArg_path({
    argInfos,
    paramInfo,
    canBeBareArg,
    expectType,
    throwIfNotExpected,
}: {
    argInfos: RCLIArgInfo<RCLIArgType>[],
    paramInfo: RCLIParamInfo,
    canBeBareArg: boolean,
    expectType: ExpectPathType,
    throwIfNotExpected?: boolean,
}): Promise<string> {
    const lc = `[${extractArg_path.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 5002199a812764f7f436de0a59183823)`); }

        let inputPath: string;
        const bareArgInfo = argInfos.filter(x => x.isBare).at(0);
        if (canBeBareArg && bareArgInfo) {
            if (!bareArgInfo.value) { throw new Error(`(UNEXPECTED) bareArgInfo.value falsy? (E: 21cbff1aa02ab80bc7e4a3f7beba2e23)`); }
            inputPath = bareArgInfo.value as string;
        } else {
            inputPath = extractArgValue<string>({
                paramInfo,
                argInfos,
                throwIfNotFound: true,
            }) as string;
        }

        // ensure it's a valid folder
        // this is tricky though because this may be a one off os-commandline
        // call or inside of our repl. for now, we will consider the initialCwd
        // (which is set before anything happens in the rcli.mts init code) as
        // the official cwd to work with relative paths
        let pathToCheck = inputPath;
        if (!pathUtils.isAbsolute(inputPath)) {
            // relative path kluge here. combine this relative path with the absolute initial cwd.
            pathToCheck = pathUtils.join(getIbGibGlobalThis().initialCwd, inputPath);
            if (logalot) { console.log(`${lc}\ninputPath: ${inputPath}\npathToCheck: ${pathToCheck} (I: 263728c4526f7252263a3729269f3e23)`); }
        }
        const _isAsExpected = pathIsAsExpected({
            relOrAbsPath: pathToCheck,
            expectType,
            throwIfNotExpected,
        });

        return inputPath;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * helper for checking `RCLIArgInfo` flags.
 *
 * @returns true if the flag is set in the argInfos, else false.
 */
export function extractArg_FlagIsTrue({
    argInfos,
    flagParamInfo,
    defaultIfNotFound,
}: {
    argInfos: RCLIArgInfo<RCLIArgType>[],
    flagParamInfo: RCLIParamInfo
    defaultIfNotFound?: boolean | undefined,
}): boolean {
    let result: boolean | undefined = false;
    const lc = `[${extractArg_FlagIsTrue.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 5bb6f2841f16b94682219be948a15e24)`); }

        for (let i = 0; i < argInfos.length; i++) {
            const argInfo = argInfos[i];
            if (argInfo.name === flagParamInfo.name) {
                if (!argInfo.isFlag) { throw new Error(`(UNEXPECTED) argInfo.isFlag is falsy? if we're checking if a flag is true, then the param info should be defined as a flag (i.e. is boolean). argInfo: ${pretty(argInfo)}\nflagParamInfo: ${pretty(flagParamInfo)} (E: aaed21bf1802a2d4598f29e3bd1c9624)`); }

                if (argInfo.value === undefined) {
                    // no key/value, just the key, which is a way of setting the flag to true.
                    return true; /* <<<< returns early */
                } else {
                    return argInfo.value as boolean;
                }
            }
        }

        // if we made it this far, then the flag was not found.
        if (defaultIfNotFound !== undefined) {
            return !!defaultIfNotFound;
        } else {
            return false;
        }
        // const explicitTrue = argInfos.some(argInfo => {
        //     return argInfo.isFlag &&
        //         argInfo.name === flagParamInfo.name &&
        //         argInfo.value === true;
        // });
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

// #endregion extractArg functions

/**
 * Low-level plumbing for prompting the user for some text.
 *
 * ## notes
 *
 * * if an empty string is entered, confirm is automatic/always happens.
 * * if confirm is true, will confirm on any entry
 * * can just hit ENTER to confirm anything
 *   * this should be parameterized
 *
 * @returns the text the user entered.
 */
export async function promptForText({
    msg,
    title,
    confirm,
    noNewLine,
    rl,
    defaultValue,
}: {
    msg: string,
    title?: string,
    confirm?: boolean,
    noNewLine?: boolean,
    rl?: readline.Interface,
    defaultValue?: string,
}): Promise<string> {
    const lc = `[${promptForText.name}]`;
    try {
        if (!msg) { throw new Error(`msg required (E: 64059542d789c292a4b030d442ed9c23)`); }
        rl ??= readline.createInterface({
            input: stdin,
            output: stdout
        });
        let userText: string | undefined = undefined;
        try {
            do {
                title = title ? `[${title}] ` : '';
                const questionPromise = rl.question(`${title}${msg}${noNewLine ? ' ' : '\n'}`);
                if (defaultValue) {
                    if (defaultValue.includes('\n')) {
                        if (logalot) { console.log(`${lc} defaultValue includes newline. doesn't this trigger an enter on the readline? (I: d2596dac76578e3ccbceb84d931a9924)`); }
                        // defaultValue = defaultValue.replace(/\n/g, '\t')
                    }
                    rl.write(defaultValue);
                }
                // const userText1 = await rl.question(`${title}${msg}${noNewLine ? ' ' : '\n'}`);
                const userText1 = await questionPromise;
                if (!userText1) {
                    const userText2 = await rl.question(`empty string entered. confirm:${noNewLine ? ' ' : '\n'}`);
                    if (userText2 === userText1) {
                        userText = userText1;
                        break;
                    } else {
                        console.log(`confirm failed. please try again.`);
                    }
                } else if (confirm) {
                    const userText2 = await rl.question(`confirm by either hitting ENTER (empty string) or retype input:${noNewLine ? ' ' : '\n'}`);
                    if (userText2 === '' || userText2 === userText1) {
                        userText = userText1;
                    } else {
                        console.log(`confirm failed. please try again.`);
                    }
                } else {
                    userText = userText1;
                    continue;
                }
            } while (!userText);
            return userText;
        } catch (error) {
            throw error;
        } finally {
            rl.close();
        }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    }
}

export async function alertUser({
    msg,
    title,
}: {
    msg: string,
    title?: string,
}): Promise<void> {
    const lc = `[${alertUser.name}]`;
    try {
        if (!msg) { throw new Error(`msg required (E: 281d138de20f48148506869bd49ff865)`); }
        const rl = readline.createInterface({
            input: stdin,
            output: stdout
        });
        try {
            title = title ? `[${title}] ` : '';
            const _ignored = await rl.question(`${title}${msg}\nHit ENTER to continue...`);
        } catch (error) {
            throw error;
        } finally {
            rl.close();
        }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    }
}

export async function debugLogInnerSpace({
    space,
}: {
    space: InnerSpace_V1,
}): Promise<void> {
    const lc = `[${debugLogInnerSpace.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: adbdad68a6c104f072836297bc656d23)`); }
        console.log(`${lc} data ():\n${pretty(space.data)}`)
        console.log(`${lc} rel8ns ():\n${pretty(space.rel8ns)}`)

        // we only want the dtos, as some of these will be witnesses with
        // functions and references.
        const ibGibDtos = Object.values(space.ibGibs).map(x => toDto({ ibGib: x }));
        console.log(`${lc} ===========START ibGibs (${ibGibDtos.length})========`)
        ibGibDtos.forEach(x => { console.log(pretty(x)); });
        console.log(`${lc} ===========END ibGibs (${ibGibDtos.length})========`)
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export function getPrompt({
    id,
    promptTemplate = DEFAULT_PROMPT_TEMPLATE,
}: {
    /**
     * identifier, not necessarily a uuid.
     * so, e.g., a username or name of a witness
     */
    id: string
    /**
     * should have $id where the id will go.
     *
     * really i should be incorporating lex-gib here.
     */
    promptTemplate?: string,
}): string {
    promptTemplate ??= DEFAULT_PROMPT_TEMPLATE;
    let result: string;
    if (!!id && promptTemplate.includes('$id')) {
        result = promptTemplate.replace('$id', id);
    } else {
        result = id || '[?]>';
    }
    return result;
}

export class RCLIFormBuilder extends AppFormBuilder {
    protected lc: string = `[${RCLIFormBuilder.name}]`;

    constructor() {
        super();
        this.what = 'app';
    }

    cmdEscapeString({
        of,
        required,
        defaultValue = RCLI_DEFAULT_COMMAND_ESCAPE_STRING,
    }: {
        of: string,
        required?: boolean,
        defaultValue?: string,
    }): RCLIFormBuilder {
        this.addItem({
            name: "cmdEscapeString",
            description: `Prefix text with this escape string to indicate a command that the app should process. The default is colon (:), so if you type :ls then this will indicate to exec the "ls" request.`,
            label: "Request Escape String",
            fnValid: (x: string | number) => { return typeof x === 'string' && x.length > 0 && x.length <= RCLI_MAX_ESCAPE_STRING_LENGTH; },
            defaultErrorMsg: `Must be at least 1 character and max of ${RCLI_MAX_ESCAPE_STRING_LENGTH}. `,
            dataType: 'text',
            value: of,
            defaultValue: defaultValue || RCLI_DEFAULT_COMMAND_ESCAPE_STRING,
            required,
        });
        return this;
    }

}

export function parseCommandRawTextIntoArgs({
    rawText,
    validCommandIdentifiers,
    logErrors = false,
}: {
    rawText: string,
    validCommandIdentifiers: string[],
    logErrors?: boolean,
}): {
    /**
     * true if the incoming `rawText` was a valid args string, i.e., represented
     * something a user should type into a command line.
     */
    validArgsString: boolean,
    /**
     * firstArg should be a command.
     */
    cmd_ie_firstArgSansPrefix?: string,
    /**
     * hopefully a recreation of what would get passed to process.vargs
     */
    args?: string[],
    validationErrorMsg?: string,
} {
    const lc = `[${parseCommandRawTextIntoArgs.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: ae320ed57358b33ccc64e9e88a571423)`); }

        if (rawText === '') { throw new Error(`rawText is empty string (E: b93c6ed51b5147c49f8355be9ad1c0b7)`); }

        /**
         * we will mutate this, splicing off/combining as we go from the first arg
         */
        const mutatingNaiveSpaceDelimited = rawText.split(' ');

        // first do the first arg, which must be a valid command identifier with
        // either a command prefix or a regular arg prefix
        const doubleDashPrefix = '--';
        const commandPrefix = RCLI_DEFAULT_COMMAND_ESCAPE_STRING;
        const rawFirstArg = mutatingNaiveSpaceDelimited.splice(0, 1)[0];
        /**
         * first arg must be the command
         */
        let cmd_ie_firstArgSansPrefix: string;
        if (rawFirstArg.startsWith(commandPrefix)) {
            cmd_ie_firstArgSansPrefix = rawFirstArg.substring(commandPrefix.length);
        } else if (rawFirstArg.startsWith(doubleDashPrefix)) {
            cmd_ie_firstArgSansPrefix = rawFirstArg.substring(doubleDashPrefix.length);
        } else {
            throw new Error(`first arg must be a command without spaces that starts with either the command prefix (${commandPrefix}) or regular arg prefix (${doubleDashPrefix}) (E: 236b5a0abfaa46748382ad3f70afbff3)`);
        }

        if (!validCommandIdentifiers.includes(cmd_ie_firstArgSansPrefix)) {
            throw new Error(`first arg (${cmd_ie_firstArgSansPrefix}) must be a valid command. validCommandIdentifiers: ${validCommandIdentifiers} (E: fcb8ae7e9f674aa796215212323da42c)`);
        }

        // if we have nothing left, then we have a bare command and we're done
        if (mutatingNaiveSpaceDelimited.length === 0) {
            return {
                validArgsString: true,
                cmd_ie_firstArgSansPrefix,
                args: [`--${cmd_ie_firstArgSansPrefix}`], // args always contains double-dash
            }; /* <<<< returns early */
        }

        // const stripQuotes = (str: string) => {
        //     let s = str.concat();
        //     if (!s.includes("'") && !s.includes('"')) {
        //         // doesn't contain quotes
        //         return s; /* <<<< returns early */
        //     }
        //     if (s.includes('=')) {
        //         let [key, value] = s.split('=');
        //         if (value.startsWith('"') && !value.endsWith('"')) {
        //             throw new Error(`invalid string. value starts with " but doesn't end with it (E: 5f67392af09c469bbfc8b3ce68f6ee61)`);
        //         } else if (value.startsWith("'") && !value.endsWith("'")) {
        //             throw new Error(`invalid string. value starts with ' but doesn't end with it (E: d35c8fc64f6148008c9e5aa0b7d25dfb)`);
        //         } else {
        //             // strip the right side value
        //             value = value.substring(1);
        //             value = value.substring(0, value.length - 1);
        //         }
        //         s = `${key}=${value}`;
        //     } else {
        //         if (s.startsWith('"') && !s.endsWith('"')) {
        //             throw new Error(`invalid string. starts with " but doesn't end with it (E: 267f36b0e2434199a66e0b7acfdbb104)`);
        //         } else if (s.startsWith("'") && !s.endsWith("'")) {
        //             throw new Error(`invalid string. starts with ' but doesn't end with it (E: 8decfd854b620581eeb271ffdaf01623)`);
        //         } else {
        //             // strip the right side value
        //             s = s.substring(1);
        //             s = s.substring(0, s.length - 1);
        //         }
        //     }
        //     return s;
        // }

        // we have at least one additional arg
        /**
         * once we have a double-dash arg (not the command), we cannot have a bare arg.
         */
        let doubleDashFound = false;
        // const resArgs: string[] = [rawFirstArg.startsWith(doubleDashPrefix) ? rawFirstArg : `--${rawFirstArg}`]; // args always contains double-dash
        const resArgs: string[] = [`--${cmd_ie_firstArgSansPrefix}`]; // args always contains double-dash
        let tmpArgWhileBuilding: string = '';
        for (let i = 0; i < mutatingNaiveSpaceDelimited.length; i++) {
            const textPiece = mutatingNaiveSpaceDelimited[i];
            if (textPiece.startsWith(doubleDashPrefix)) {
                doubleDashFound = true;
                if (textPiece === doubleDashPrefix) { throw new Error(`invalid rawText. textPiece is just -- with no additional text, which is invalid. flags should be --my-flag (no spaces) and args with values should be like --key=value or --key="value here" or --key='value here' (E: 822be7519985462c9d73e68cecb64423)`); }
                if (tmpArgWhileBuilding.length > 0) {
                    // we have been building an arg and have hit a new argPrefix
                    // which means our previous building is done so flush it to
                    // the remainderArgs. this is guaranteed not to be a
                    // quote-surrounded value because that is flushed elsewhere.
                    // (so we don't need to strip the quotes at this point)
                    resArgs.push(tmpArgWhileBuilding.concat());
                    tmpArgWhileBuilding = '';
                }
                if (textPiece.includes('=')) {
                    // --identifier=value
                    // --identifier='value
                    // --identifier="value
                    // --identifier="value"
                    // --identifier='value'
                    if (textPiece.endsWith('"') || textPiece.endsWith("'")) {
                        // --identifier="value"
                        // --identifier='value'
                        resArgs.push(stripQuotes(textPiece.concat()));
                        tmpArgWhileBuilding = '';
                    } else {
                        // --identifier=value
                        // --identifier='value
                        // --identifier="value
                        tmpArgWhileBuilding = textPiece.concat();
                    }
                } else {
                    // flag arg needs no more processing, e.g., --my-flag
                    tmpArgWhileBuilding = textPiece.concat();
                }
            } else if (textPiece.startsWith("'") || textPiece.startsWith('"')) {
                if (doubleDashFound) { throw new Error(`invalid (edge case?) rawText. textPiece starts with a quote to indicate a bare arg, but bare args can only come immediately after the command. IOW You can't have a --key=value arg and then a bare arg. (E: 2fe0df8159d5a9eb030e39fcca0b9723)`); }
                if (textPiece.endsWith("'") || textPiece.endsWith('"')) {
                    // starts and ends with quote, so we have an unnecessarily quoted value
                    if (tmpArgWhileBuilding) {
                        throw new Error(`invalid (edge case?) rawText. testPiece starts and ends with quote which is only expected if we were doing a bare arg immediately after the cmd. but tmpArgWhileBuilding is truthy, which means we have a weird quote edge case. (E: fcf7846e22dc8354b3410ef60c996a23)`);
                    } else {
                        resArgs.push(stripQuotes(textPiece.concat()));
                    }
                } else {
                    // start of single or double quote entry
                    if (tmpArgWhileBuilding) {
                        // we have already started a tmp arg meaning found one with a quote and now we have another one started...so throw
                        throw new Error(`invalid (edge case?) rawText. While parsing the  (E: f12101a8d33cbf10e4e02efd66daad23)`);
                    } else {
                        // start of a tmp
                        tmpArgWhileBuilding = textPiece.concat();
                    }
                }
            } else if (textPiece.endsWith("'") || textPiece.endsWith('"')) {
                if (textPiece.length === 1) {
                    // textPiece is only a quote
                    throw new Error(`invalid (edge case?) rawText. textPiece is only a single or double quote. does the text inside the quotes end in a space? (E: 5bfac7429645c8c74415085bb5510a23)`);
                }
                // end of single or double quote entry with guaranteed text of at least length 1
                if (tmpArgWhileBuilding) {

                    // complete tmp arg
                    tmpArgWhileBuilding += ` ${textPiece}`; // add the space + textPiece

                    // at this point, tmpArgWhileBuilding has completed a quoted
                    // segment. but this might be a bare arg or a name=value arg
                    // and we need to strip the quotes

                    if (tmpArgWhileBuilding.includes('=')) {
                        if (logalot) { console.log(`${lc} key=value arg closed with quotes (I: 0114892892c96ab77143666bc8a73c23)`); }
                        const subPieces = tmpArgWhileBuilding.split('=');
                        if (subPieces.length !== 2) { throw new Error(`textPiece has more than one equals sign? (=) (E: 2b25f1f7e10172a6739892ba97d3f623)`); }
                        let [left, right] = subPieces;
                        if (!right.startsWith('"') && !right.startsWith("'")) { throw new Error(`we had a close quote in a --key=value arg but the value did not start with a quote? (E: 04c9b33d54b1a7f998c11c1619d55223)`); }
                        tmpArgWhileBuilding = `${left}=${stripQuotes(right)}`;
                    } else {
                        if (logalot) { console.log(`${lc} bare arg closed with quotes (I: eda6b6593b5963de427e9efb4df6c823)`); }
                        if (!tmpArgWhileBuilding.startsWith('"') && !tmpArgWhileBuilding.startsWith("'")) { throw new Error(`we had a close quote in a bare arg but the bare arg did not start with a quote? (E: 04c9b33d54b1a7f998c11c1619d55223)`); }
                        tmpArgWhileBuilding = stripQuotes(tmpArgWhileBuilding);
                    }

                    // now tmp arg is stripped of quotes if applicable
                    resArgs.push(tmpArgWhileBuilding.concat());
                    tmpArgWhileBuilding = '';
                } else {
                    // textPiece ends with quote but tmp hasn't started?
                    throw new Error(`invalid (edge case?) rawText. textPiece ends with quote but there wasn't a starting quote? quotes should only be used to entirely surround options (E: fab31bc4df2f7c644221b8ab00f36c23)`);
                }
            } else {
                // could be adding to bare arg, could be adding to double-dash arg
                if (tmpArgWhileBuilding.startsWith(doubleDashPrefix)) {
                    // verify that we're quoting the kv value
                    if (tmpArgWhileBuilding.includes('=')) {
                        let [left, right] = tmpArgWhileBuilding.split('=');
                        if (!right.startsWith('"') && !right.startsWith("'")) {
                            throw new Error(`non-quoted value in double-dash key-value arg followed by a space. if you have a bare arg, it has to immediately follow the first (cmd) arg. (E: 4e0db23a2b5fd868a8a9d08c0e8f2b23)`);
                        }
                        if (right.endsWith("'") || right.endsWith('"')) {
                            throw new Error(`quoted non-space value in double-dash key-value arg followed by a space. if you have a bare arg, it has to immediately follow the first (cmd) arg. (E: ffc8c4fcfc444a19ae366db29eb5a764)`);
                        }
                        // adding to quoted value in --key="value here"
                        tmpArgWhileBuilding += ` ${textPiece}`;
                    } else {
                        throw new Error(`double-dash flag cannot contain spaces (E: 73443965e5c7c5ec641a243571989323)`);
                    }
                } else {
                    // adding to bare arg
                    if (!doubleDashFound) {
                        // if we already have tmpArgWhileBuilding then we add
                        // the space, otherwise it's the first bare arg and
                        // there is no initial space
                        tmpArgWhileBuilding += tmpArgWhileBuilding ? ` ${textPiece}` : textPiece;
                    } else {
                        throw new Error(`bare arg must be immediately after the cmd atow (11/2023) (E: e51b4569f2fed215a542be6f620b9723)`);
                    }
                }
            }
        }
        if (tmpArgWhileBuilding.length > 0) { resArgs.push(tmpArgWhileBuilding); }

        const result = {
            validArgsString: true,
            cmd_ie_firstArgSansPrefix,
            args: resArgs,
        };

        return result;
        // the first first should be a command, with no spaces
        // the second arg might be a bare arg (doesn't start with --)
    } catch (error) {
        const emsg = `${lc} ${extractErrorMsg(error)}`;
        if (!!logErrors) { console.error(emsg); }
        return { validArgsString: false, validationErrorMsg: emsg };
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export function getIbGibGlobalThis(): IbGibGlobalThis {
    return (globalThis as any).ibgib as IbGibGlobalThis;
}

/**
 * main priority is to ensure that there are no duplicate param infos.
 *
 * use this function at the start of application in order to check
 * that you have built a good parameter configuration.
 */
export function validateParamInfos({
    paramInfos,
}: {
    paramInfos: RCLIParamInfo[],
}): string[] {
    const lc = `[${validateParamInfos.name}]`;
    const errors: string[] = [];
    try {
        if (logalot) { console.log(`${lc} starting... (I: 078da3ccdadef64aea6cf74357322c23)`); }

        // verify no duplicate param identifiers
        const identifiers: string[] = [];
        const doIdentifier = (identifier: string) => {
            identifier = identifier.toLowerCase();
            if (identifiers.includes(identifier)) {
                errors.push(`${lc} identifier duplicate found: ${identifier} (E: 8c7e0a781a674d8ab5d64edf0f7dfd7d)`);
            } else {
                identifiers.push(identifier);
            }
        }
        paramInfos.forEach(paramInfo => {
            doIdentifier(paramInfo.name);
            (paramInfo.synonyms ?? []).forEach(x => doIdentifier(x));
        });

        return errors;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export function stripQuotes(str: string): string {
    const lc = `[${stripQuotes.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 5e2a789d979d738dc69fb2da54ad6923)`); }
        let s = str.concat();
        if (!s.includes("'") && !s.includes('"')) {
            // doesn't contain quotes
            return s; /* <<<< returns early */
        }
        if (s.includes('=')) {
            let [key, value] = s.split('=');
            if (value.startsWith('"') && !value.endsWith('"')) {
                throw new Error(`invalid string. value starts with " but doesn't end with it (E: 5f67392af09c469bbfc8b3ce68f6ee61)`);
            } else if (value.startsWith("'") && !value.endsWith("'")) {
                throw new Error(`invalid string. value starts with ' but doesn't end with it (E: d35c8fc64f6148008c9e5aa0b7d25dfb)`);
            } else {
                // strip the right side value
                value = value.substring(1);
                value = value.substring(0, value.length - 1);
            }
            s = `${key}=${value}`;
        } else {
            if (s.startsWith('"') && !s.endsWith('"')) {
                throw new Error(`invalid string. starts with " but doesn't end with it (E: 267f36b0e2434199a66e0b7acfdbb104)`);
            } else if (s.startsWith("'") && !s.endsWith("'")) {
                throw new Error(`invalid string. starts with ' but doesn't end with it (E: 8decfd854b620581eeb271ffdaf01623)`);
            } else {
                // strip the right side value
                s = s.substring(1);
                s = s.substring(0, s.length - 1);
            }
        }
        return s;

    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * sets a globalThis.ibgib
 *
 * ## driving use case
 *
 * I want to be able to store the initial cwd() and not have to pass this around
 *
 * @see {@link IbGibGlobalThis}
 */
export async function initIbGibGlobalThis(): Promise<void> {
    const lc = `[${initIbGibGlobalThis.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: e7836caf3068bcd1e9fe5dfb2ee64f23)`); }

        if (!!(globalThis as any).ibgib) { throw new Error(`(UNEXPECTED) globalThis.ibgib already truthy? (E: a8aa7ec32c56e123585148349261ac23)`); }

        (globalThis as any).ibgib = {
            initialCwd: cwd(),
        } satisfies IbGibGlobalThis;

        if (logalot) {
            console.log(`${lc} console.dir(globalThis.ibgib)... (I: 9c573482b46ceb0977ef25ece485f623)`);
            console.dir((globalThis as any).ibgib);
        }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}
