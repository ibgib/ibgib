#!/usr/bin/env node

/**
 * @module rcli
 *
 * request/command line interface, OR robbot/command line interface
 *
 * we're building on top of the command line interface paradigm to be more
 * flexible with natural language systems, yes? that means enabling both the rigid
 * parameter structure as well as natural language with expansions like synonyms.
 * ultimately, we are streamlining interfacing with app(s), robbot(s)/chatbot(s) and
 * even other user(s).
 *
 * This module is for the interpreting of the incoming args when this package is
 * executed from the command line (bash, zsh, etc.).
 */

import { execPath, cwd, } from 'node:process';

import { extractErrorMsg, pretty, } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { RCLIArgInfo, } from "@ibgib/helper-gib/dist/rcli/rcli-types.mjs";
import { argIs, buildArgInfos, extractArgValue, } from "@ibgib/helper-gib/dist/rcli/rcli-helper.mjs";
import { PARAM_INFO_HELP } from "@ibgib/helper-gib/dist/rcli/rcli-constants.mjs";

import {
    PARAM_INFOS, PARAM_INFO_INIT, PARAM_INFO_INTERACTIVE, PARAM_INFO_IN_MEMORY,
} from './rcli-constants.mjs';
import { GLOBAL_LOG_A_LOT } from '../../../ibgib-constants.mjs';
import { createRCLICommentIbGib } from '../../../common/rcli-comment/rcli-comment-helper.mjs';
import { initIbGibGlobalThis, validateArgInfos, validateParamInfos, } from './rcli-helper.mjs';
import { execInit_inMemoryMetaspace, execInit_nodeMetaspace } from './exec/init.mjs';
import { execInteractive_RCLIApp, execOneOff_RCLIApp } from './exec/interactive.mjs';
import { showHelp } from './exec/showHelp.mjs';
import { IbGibGlobalThis } from './rcli-types.mjs';

/**
 * used in verbose logging (across all ibgib libs atow)
 */
const logalot = GLOBAL_LOG_A_LOT;


export async function execRCLI(): Promise<void> {
    const lc = `[${execRCLI.name}]`;
    try {
        console.log(`${lc} starting... (I: 9e3e7d36c0934976ae499697521f1948)\n`);

        await initIbGibGlobalThis();

        console.log(`${lc} process.execPath: ${execPath}`);
        console.log(`${lc} process.cwd(): ${cwd()}`);
        console.log(`process.argv: ${pretty(process.argv)}`);

        const args: string[] = process.argv?.slice(2) ?? [];
        console.log(`${lc} args.join(' '): ${args.join(' ')}`);

        console.warn(`${lc} todo: change logalot type to number | boolean in helper-gib`);
        const paramInfosValidationErrors = validateParamInfos({ paramInfos: PARAM_INFOS });
        if (paramInfosValidationErrors.length > 0) { throw new Error(`(UNEXPECTED) invalid PARAM_INFOS configured? errors: ${paramInfosValidationErrors.join('|')} (E: d409dcf1163fa0d89a8cb389531c7a23)`); }
        const argInfos = buildArgInfos({ args, paramInfos: PARAM_INFOS, logalot: !!logalot });

        const validationErrors = validateArgInfos({ argInfos });
        if (!validationErrors) {
            // if (argInfos.some(x => x.name === PARAM_INFO_HELP.name)) {
            if (argInfos.at(0)?.name === PARAM_INFO_HELP.name) {
                showHelp({ args });
                return;
            }

            console.log(`${lc} here are the args received`);
            for (let i = 0; i < args.length; i++) {
                const arg = args[i];
                console.log(`arg ${i}: ${arg}`);
            }

            await routeCmd({ args, argInfos });
        } else {
            console.log(`validationErrors: ${validationErrors}`);
        }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        console.log(`\n${lc} complete.`);
    }
}

/**
 * Routes the cmd to the proper starting handler.
 *
 * This is what will ultimately execute the RCLI_App witness, but first we need
 * to establish the context space (node/in-memory) and initialize.
 */
async function routeCmd({
    args,
    argInfos,
}: {
    args: string[],
    argInfos: RCLIArgInfo[]
}): Promise<void> {
    const lc = `[${routeCmd.name}]`;
    try {
        console.log(`${lc} starting... (I: 7ce8237f0c2648bbb200c3fd4b5b94e3)`);

        const resRCLICommentIbGib = await createRCLICommentIbGib({
            args,
            interpretedArgInfos: argInfos,
            // space:        // no metaspace yet
            // saveInSpace?: // no metaspace yet
        });

        // "Three" cases:
        // 1. init a new metaspace
        // 2. start an interactive repl session
        // 3. exec some other rcli cmd as a one-off

        if (argInfos.some(x => argIs({ arg: x.name, paramInfo: PARAM_INFO_INIT }))) {
            if (logalot) { console.log(`${lc} init metaspace (I: 31a6055041d6b9c564eb33bdbb17a623)`); }

            // before any work can be done at all, there must be a metaspace initialized.
            // this is like setting up initial folders/files used in all ibgib actions.

            if (argInfos.some(x => argIs({ arg: x.name, paramInfo: PARAM_INFO_IN_MEMORY }))) {
                await execInit_inMemoryMetaspace({ argInfos, resRCLICommentIbGib });
            } else {
                await execInit_nodeMetaspace({ argInfos, resRCLICommentIbGib });
            }
        } else if (argInfos.some(x => argIs({ arg: x.name, paramInfo: PARAM_INFO_INTERACTIVE }))) {
            if (logalot) { console.log(`${lc} start interactive repl session (I: 26b9be23c107467baf6b510696301923)`); }

            // if we were in an angular app, we would navigate at this point
            // to the home page. in the mvp, this would be backed by the
            // ibgib base component plumbing.

            // instantiate an RCLI app and start the repl
            await execInteractive_RCLIApp({ argInfos, resRCLICommentIbGib });
        } else {
            // instantiate an RCLI app and execute a one-off cmd or request
            await execOneOff_RCLIApp({ argInfos, resRCLICommentIbGib });
        }

    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        console.log(`\n${lc} complete.`);
    }
}
