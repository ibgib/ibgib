import { execPath, cwd, chdir, } from 'node:process';
import { statSync } from 'node:fs';
import { mkdir, readdir, } from 'node:fs/promises';
import * as readline from 'node:readline/promises';
import { stdin, stdout } from 'node:process'; // decide if use this or not
import * as pathUtils from 'path';

import { extractErrorMsg, pretty, } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';

import { PARAM_INFOS, } from '../rcli-constants.mjs';
import { RCLICommandHandler, RCLICommandHandlerArg } from '../rcli-types.mjs';
import { GLOBAL_LOG_A_LOT } from '../../../../ibgib-constants.mjs';
import { ROOT } from '@ibgib/ts-gib/dist/V1/constants.mjs';
import { showHelp } from '../exec/showHelp.mjs';

/**
 * used in verbose logging (across all ibgib libs atow)
 */
const logalot = GLOBAL_LOG_A_LOT;

export async function handleHelp(arg: RCLICommandHandlerArg): Promise<IbGib_V1 | undefined> {
    const lc = `[${handleHelp.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 99593aa166c106f0c5c03424d1c1d523)`); }
        const { cmdEscapeString, cmdInfo, contextIbGib, ibGib, metaspace } = arg;

        let { argInfos } = cmdInfo;

        showHelp({ args: argInfos?.map(x => x.name) ?? ['[no args given?]'] });

        return ROOT;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export const rcliCmdHandlerHelp: RCLICommandHandler =
    (arg) => handleHelp(arg);
