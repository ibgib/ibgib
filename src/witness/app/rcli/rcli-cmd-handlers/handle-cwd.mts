import { cwd } from 'process';

// import { clone, } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { extractErrorMsg } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { ROOT } from '@ibgib/ts-gib/dist/V1/constants.mjs';

import { RCLICommandHandler, RCLICommandHandlerArg } from '../rcli-types.mjs';
import { GLOBAL_LOG_A_LOT } from '../../../../ibgib-constants.mjs';

/**
 * used in verbose logging (across all ibgib libs atow)
 */
const logalot = GLOBAL_LOG_A_LOT;


export async function handleCwd(arg: RCLICommandHandlerArg): Promise<IbGib_V1 | undefined> {
    const lc = `[${handleCwd.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 28f21c4183fd58cb0a97f7fea7519823)`); }
        const { contextIbGib, fnAddComment } = arg;
        await fnAddComment({ contextIbGib, rel8nName: 'comment', text: cwd(), });
        return ROOT;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export const rcliCmdHandlerCwd: RCLICommandHandler =
    (arg) => handleCwd(arg);
