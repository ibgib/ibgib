import { extractErrorMsg, pretty, } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { extractArgValue } from '@ibgib/helper-gib/dist/rcli/rcli-helper.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { ROOT } from '@ibgib/ts-gib/dist/V1/constants.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../../ibgib-constants.mjs';
import { PARAM_INFO_ADD_COMMENT, PARAM_INFO_TEXT, } from '../rcli-constants.mjs';
import { RCLICommandHandler, RCLICommandHandlerArg } from '../rcli-types.mjs';
import { getFormattedCmdHelpText } from '../../../../common/rcli-comment/rcli-comment-helper.mjs';

/**
 * used in verbose logging (across all ibgib libs atow)
 */
const logalot = GLOBAL_LOG_A_LOT;

export async function handleAddComment(arg: RCLICommandHandlerArg): Promise<IbGib_V1 | undefined> {
    const lc = `[${handleAddComment.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 43f9b408581f41a38ca3120dc79e7ecd)`); }
        const { contextIbGib, fnAddComment, metaspace, cmdInfo, ibGib } = arg;
        const { rawText, argInfos, errorMsg, showHelp } = cmdInfo;

        if (!argInfos) { throw new Error(`(UNEXPECTED) argInfos required? (E: 1e7998e35dbd1deffe5e0a830a6a9d23)`); }

        if (showHelp) { return handleAddComment_showHelp(arg); /* <<<< returns early */ }

        const text = extractArgValue<string>({ paramInfo: PARAM_INFO_TEXT, argInfos, throwIfNotFound: true }) as string;
        if (!text) { throw new Error(`text required (E: ae0fda36f6bae51269c4203300983923)`); }

        await fnAddComment({ contextIbGib, rel8nName: 'comment', text, });

        return ROOT;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

async function handleAddComment_showHelp(arg: RCLICommandHandlerArg): Promise<IbGib_V1> {
    const lc = `[${handleAddComment_showHelp.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 4a32e127c574728f6a289c062fd92d23)`); }
        const { contextIbGib, fnAddComment, metaspace, cmdInfo, ibGib } = arg;
        // const { rawText, argInfos, errorMsg, showHelp } = cmdInfo;
        const helpText =
            `This command creates a comment ibgib and adds it to the current context. Use the "--text" param (${pretty(PARAM_INFO_TEXT)}).`;
        const text = getFormattedCmdHelpText({
            cmdParamInfo: PARAM_INFO_ADD_COMMENT,
            summaryText: helpText,
            paramSummaries: [
                {
                    param: PARAM_INFO_TEXT,
                    summary: `text for comment to add.`,
                }
            ],
        });
        await fnAddComment({ contextIbGib, rel8nName: 'comment', text });
        return ROOT;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export const rcliCmdHandlerAddComment: RCLICommandHandler =
    (arg) => handleAddComment(arg);
