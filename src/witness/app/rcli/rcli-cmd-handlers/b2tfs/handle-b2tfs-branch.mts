/**
 * @module rcliCmdHandler_B2tFS_branch
 *
 * this handler is for working with B2tFS branches.
 *
 * @see {@link handleB2tFS_branch}
 */

import { cwd } from 'process';
import * as pathUtils from 'path';

import { extractErrorMsg, pretty, } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { extractArgValue } from '@ibgib/helper-gib/dist/rcli/rcli-helper.mjs';
import { PARAM_INFO_INPUT_PATH, } from '@ibgib/helper-gib/dist/rcli/rcli-constants.mjs';
import { RCLIArgInfo, RCLIArgType, } from '@ibgib/helper-gib/dist/rcli/rcli-types.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { ROOT } from '@ibgib/ts-gib/dist/V1/constants.mjs';
import { getIbGibAddr } from '@ibgib/ts-gib/dist/helper.mjs';
import { createCommentIbGib } from '@ibgib/core-gib/dist/common/comment/comment-helper.mjs';
import { MetaspaceService } from '@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../../../ibgib-constants.mjs';
import {
    RCLICommandHandler, RCLICommandHandlerAddCommentFunctionArgs,
    RCLICommandHandlerArg, RCLICommandTextInfo
} from '../../rcli-types.mjs';
import {
    activateB2tFSBranch, addB2tFSBranchRefToSuperSpace,
    createNewB2tFSIndexIbGib,
} from '../../b2tfs/b2tfs-index/b2tfs-index-helper.mjs';
import {
    createNewB2tFSBranchIbGib, extractArg_Branch_byNameOrId,
    getNameFromArgInfosOrUseInputPathDirName,
} from '../../b2tfs/b2tfs-branch/b2tfs-branch-helper.mjs';
import {
    extractArg_path, extractArg_FlagIsTrue, getIbGibGlobalThis,
    extractArg_outputPath,
} from '../../rcli-helper.mjs';
import { B2TFS_DEFAULT_ITEM_FILTER_PATTERNS } from '../../b2tfs/b2tfs-item/b2tfs-item-constants.mjs';
import {
    createNewB2tFSSpace, getMessageText_maybePromptIfNotInArgs,
    stripApplyMsgCommentLines
} from '../../b2tfs/common/b2tfs-common-helper.mjs';
import {
    PARAM_INFO_ADD, PARAM_INFO_B2TFS_ACTIVATE_BRANCH, PARAM_INFO_B2TFS_BRANCH,
    PARAM_INFO_FS_ONLY, PARAM_INFO_INFO
} from '../../rcli-constants.mjs';
import { B2TFS_DEFAULT_APPLY_DIFF_COMMENT_TEXT } from '../../b2tfs/common/b2tfs-common-constants.mjs';
import { getFormattedCmdHelpText } from '../../../../../common/rcli-comment/rcli-comment-helper.mjs';
import { B2tFSItemIbGib_V1, B2tFSType } from '../../b2tfs/b2tfs-item/b2tfs-item-types.mjs';
import { getBinIbGibFromB2tFSItemIbGib, getBranchRootItem, writeItemToFS } from '../../b2tfs/b2tfs-item/b2tfs-item-helper.mjs';

/**
 * used in verbose logging (across all ibgib libs atow)
 */
const logalot = GLOBAL_LOG_A_LOT;


/**
 * handler function that adds a root branch or a forked branch per the given
 * rcli command `arg`.
 *
 * after doing some initial preparations (validation/extraction of `arg`
 * related), atow (12/2023) this does the following:
 *
 * 1. creates a new local space (`NodeFilesystemSpace_V1`) for the (root) branch
 * 2. inits a b2tfs index in that space
 * 3. creates a b2tfs branch ibgib
 * 4. adds a reference to the branch space in the current default local user
 *    (super) space
 * 5. activates the new (root) branch if --activate flag is true
 *
 * ## @example

 * **NOTE: I'm calling this my-repo just so we can ease our repo-centric heads
 * into understanding the workflow - both me in developing and the consumer in
 * consuming. THIS IS NOT A REPO. Timelines are timelines and we have to set
 * them free.**
 *
 * ### `my-repo> ibgib --b2tfs-add-root-branch ./my-project`
 *
 * this command creates a new branch that will correspond to the existing path
 * `my-repo/my-project/` but does not activate it (similar to "switch"/"checkout").
 *
 * the bare arg '.my-project' sets the relative path of the branch. but this
 * interestingly doesn't necessarily mean 'my-repo/my-project'.
 *
 * ### `my-repo> ibgib --b2tfs-add-root-branch ./my-project-2 --activate`
 *
 * similar to above example, but after the branch is added, it will be activated
 * as the current branch with which we're performing operations on.  for
 * example, if we branch from here, the source branch will be the activated one.
 * if you do a diff, again it will execute against the activated branch.
 *
 * @param arg includes `contextIbGib`, `metaspace`, `cmdInfo`, ...
 * @returns ROOT ibgib if successful, otherwise @throws error
 *
 * @see {@link RCLICommandHandlerArg}
 */
async function handleB2tFS_branch(arg: RCLICommandHandlerArg): Promise<IbGib_V1 | undefined> {
    const lc = `[${handleB2tFS_branch.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: c9e0ee6654174d77b798c66a1cd30a93)`); }

        // debugger; // trying to see why this is failing respec after refactor

        // #region prepare args/validate
        const { contextIbGib, fnAddComment, metaspace, cmdInfo, } = arg;
        const { argInfos, showHelp } = cmdInfo;

        if (!argInfos) { throw new Error(`(UNEXPECTED) argInfos falsy? (E: 122d76d157ca385e75fef6ebb4fb6523)`); }
        if (logalot) {
            console.log(`${lc} console.dir(argInfos)... (I: 6c5c2d95d2b246a93ed4775d10ad7923)`);
            console.dir(argInfos);
        }

        // #endregion prepare args/validate

        if (showHelp) {
            // show that help
            return handleB2tFS_branch_showHelp({
                contextIbGib, fnAddComment, cmdInfo
            }); /* <<<< returns early */
        } else if (extractArg_FlagIsTrue({ argInfos, flagParamInfo: PARAM_INFO_INFO })) {
            // just want info on branch(es)
            return handleB2tFS_branch_info({
                argInfos
            }); /* <<<< returns early */
        } else if (extractArg_FlagIsTrue({ argInfos, flagParamInfo: PARAM_INFO_FS_ONLY })) {
            return handleB2tFS_branch_fsonly({
                arg,
                argInfos,
                metaspace,
                contextIbGib,
                fnAddComment,
            })
        } else if (extractArg_FlagIsTrue({ argInfos, flagParamInfo: PARAM_INFO_ADD })) {
            // adding a new branch
            return handleB2tFS_branch_add({
                arg,
                argInfos,
                metaspace,
                contextIbGib,
                fnAddComment,
            }); /* <<<< returns early */
        } else {
            throw new Error(`(UNEXPECTED) unknown cmd params? expected either a help command, info, or add. actual argInfos: ${pretty(argInfos)} (E: 2e7e6734fdc26b6ebb924a5602797424)`);
        }

    } catch (error) {
        debugger; // in error block
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

async function handleB2tFS_branch_showHelp({
    contextIbGib,
    fnAddComment,
    cmdInfo,
}: {
    contextIbGib: IbGib_V1,
    fnAddComment: (arg: RCLICommandHandlerAddCommentFunctionArgs) => Promise<void>,
    cmdInfo: RCLICommandTextInfo,
}): Promise<IbGib_V1 | undefined> {
    const lc = `[${handleB2tFS_branch_showHelp.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 4ca7339966eb70ebe4cd2f1b44a23e24)`); }

        const summaryText = `[todo...]`;

        const text = getFormattedCmdHelpText({
            cmdParamInfo: PARAM_INFO_B2TFS_BRANCH,
            summaryText,
            paramSummaries: [
                // {
                //     param: PARAM_INFO_SPACE_ID,
                //     summary: `id of space to get B2tFS information on. you can also use space name. if no space is provided, will use the default local user space.`,
                // },
                // {
                //     param: PARAM_INFO_SPACE_NAME,
                //     summary: `name of space to get B2tFS information on. you can also use space id. if no space is provided, will use the default local user space.`,
                // },
                // {
                //     param: PARAM_INFO_B2TFS_ADD_BRANCH,
                //     summary: `if this flag is true, will only get branch information. (doesn't add a branch, that is just a result of a poor kluge for the time being.)`
                // },
                // {
                //     param: PARAM_INFO_VERBOSE,
                //     summary: `if this flag is true, will provide even more information.`
                // },
            ],
        })

        await fnAddComment({
            contextIbGib,
            rel8nName: 'comment',
            text,
        });
        return ROOT;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

async function handleB2tFS_branch_info({
    argInfos,
}: {
    argInfos: RCLIArgInfo<RCLIArgType>[],
}): Promise<IbGib_V1> {
    const lc = `[${handleB2tFS_branch_info.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: c56ecb2bc8472fc3c9f2893715d4a424)`); }
        throw new Error(`not implemented. use --b2tfs-info --b2tfs-branch (E: 4ef542ae0e7d81fc6eabca16f45d0d24)`);
        return ROOT;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

async function handleB2tFS_branch_add({
    arg,
    argInfos,
    metaspace,
    contextIbGib,
    fnAddComment,
}: {
    arg: RCLICommandHandlerArg,
    argInfos: RCLIArgInfo<RCLIArgType>[],
    metaspace: MetaspaceService,
    contextIbGib: IbGib_V1,
    fnAddComment: (arg: RCLICommandHandlerAddCommentFunctionArgs) => Promise<void>,
}): Promise<IbGib_V1> {
    const lc = `[${handleB2tFS_branch_add.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 49080fde961e4649a76af1a449f94349)`); }

        /**
         * this must be provided if we are adding a new branch based on the FS and not
         * branching from an existing branch.
         */
        const rawInputPath = await extractArg_path({
            argInfos,
            paramInfo: PARAM_INFO_INPUT_PATH,
            expectType: 'directory',
            canBeBareArg: true,
            throwIfNotExpected: true, // throws if inputPath not a valid existing directory
        });
        if (logalot) { console.log(`${lc} rawInputPath: ${rawInputPath} (I: b77204c795c81ee9b75baa19bc242223)`); }
        // todo: check & throw if there is already a branch with the given relative path from local user space fs folder to the branch's fs folder.

        /**
         * this is something.
         *
         * example cwds
         * test-vcs/1
         *   ../.. --vcs-add-branch
         *   cwd: /home/wraiford/ibgib/ibgib/apps/ibgib/test-vcs/1/.ibgib
         * test-vcs
         *  .. --vcs-add-branch --data-path=./1/.ibgib
         *  cwd: /home/wraiford/ibgib/ibgib/apps/ibgib/test-vcs/1/.ibgib
         *
         * so cwd is absolute path to the current metaspace --data-path (.ibgib
         * by default). this node app will be executing the same way (i.e. same
         * client) on each local space ("remote"/"peer"/etc) for the foreseeable
         * future. So it seems that instead of pathing from the space we should
         * path from the metaspace datapath.
         *
         */
        if (logalot) { console.log(`${lc} cwd(): ${cwd()}`) }
        let {
            /**
             * we want the initial cwd location that the user typed the command
             * into in the CLI.  OR if the user started an interactive repl
             * session, this is the cwd when that session started.
             *
             * in the future, we will need to adjust this algorithm to take into
             * account the ability to change the cwd in the repl.
             */
            initialCwd,
        } = getIbGibGlobalThis();
        if (logalot) { console.log(`${lc} initialCwd: ${initialCwd} (I: dab6d1f8d9e8953f47467b4c05492723)`); }

        /**
         * the default scenario is doing a root from a conventional "repo"
         * mindset. This means that we are executing within our repo's root
         * folder which is by default initialized with the ibgib-related space
         * inside the ibgib subfolder (.ibgib atow 11/2023). So if we're inside
         * /user/my-repo/.ibgib, the user actually is working from /user/my-repo
         * and that is the "branchName" that we want to get.
         */
        const absoluteAdjustedInputPath =
            pathUtils.resolve(pathUtils.join(initialCwd, rawInputPath));
        console.log(`${lc} absoluteAdjustedInputPath: ${absoluteAdjustedInputPath}`)

        const activateAfterwards =
            (extractArgValue<boolean>({ paramInfo: PARAM_INFO_B2TFS_ACTIVATE_BRANCH, argInfos, }) ?? false) as boolean;


        /**
         * the root name is either caller-specified with the --name arg or based
         * on the input path's dir name
         */
        const branchName = await getNameFromArgInfosOrUseInputPathDirName({ arg, inputPath: absoluteAdjustedInputPath });
        if (logalot) { console.log(`${lc} branchName: ${branchName} (I: f5cd177fb2169475e890a1833979d723)`); }

        /**
         * we store branches, root branches, and all their ibgibs (i.e. their
         * dependency graphs) in separate spaces. this is a broad reaching
         * design decision, which in the (relatively) far future will need to be
         * revisited (or at the very least our implementations of multi-space
         * compositions).
         */
        const branchSpace = await createNewB2tFSSpace({ name: branchName, metaspace, });

        /**
         * create and initialize a b2tfs index for the newly created branch **in
         * that branch's space**.
         */
        let branchIndex = await createNewB2tFSIndexIbGib({
            metaspace,
            space: branchSpace,
        });

        // debugger; // add root branch before prompt message text
        let applyMsgText = await getMessageText_maybePromptIfNotInArgs({
            argInfos,
            promptTypeIfNotFound: 'all',
            promptTitle: 'Initial Apply Msg',
            promptMsg: `Type/Paste/Whatever your msg to accompany the very first apply diff operation for the new branch. If you cannot afford a msg, one will be provided for you.`
        });
        applyMsgText ??= B2TFS_DEFAULT_APPLY_DIFF_COMMENT_TEXT;
        applyMsgText = stripApplyMsgCommentLines({ applyMsgText });
        const resCreateComment = await createCommentIbGib({
            text: applyMsgText,
            saveInSpace: true,
            space: branchSpace,
        });
        const applyCommentIbGib = resCreateComment.newIbGib;

        /**
         * we create the branch ibgib that will be the root of the fs dependency
         * graph. any ibgibs that need to get propagated among spaces will need
         * to be a part of this root's graph.
         *
         * The main ibgibs are the actual files and folder ibgibs. This will
         * also recursively traverse the given absoluteBranchPath and create/add
         * B2tFS file & folder items.
         *
         * this both creates the ibgib and does necessary plumbing associating
         * the new branch ibgib to the branch's index ibgib.
         */
        const branchIbGib = await createNewB2tFSBranchIbGib({
            absoluteBranchPath: absoluteAdjustedInputPath,
            branchName,
            metaspace,
            filterPatterns: B2TFS_DEFAULT_ITEM_FILTER_PATTERNS,
            branchSpace,
            branchIndex,
            applyCommentIbGib,
        });

        // add a reference to the new branch in the superspace. in this case,
        // since this is "add-root", the superspace is the current default user
        // space. this points the superspace to the new branch's space, as well
        // as other voodoo.
        const superSpace = await metaspace.getLocalUserSpace({ lock: false });
        await addB2tFSBranchRefToSuperSpace({
            metaspace,
            branchSpace,
            branchIndex,
            branchIbGib,
            superSpace,
        });

        if (activateAfterwards) {
            branchIndex = await activateB2tFSBranch({
                // contextSpace: undefined, // right now we get this from metaspace. todo: add optional spaceId param
                branchSpace: branchSpace,
                metaspace,
            });
        }

        // #region dev notes from 12/2023
        // at this point we have a root, which is a wild, unbridled timeline.
        // but what is the equivalent of a branch? when we update a file's item
        // ibgib (via stage+commit or whatever), that updates that timeline.
        // but with the current implementation it updates it per the space by
        // adding metastones. these metastones reference the new latest ibgib
        // addr by associating the n with the tjp (via tjpGib).

        // so is a branch necessarily a separate space?

        // no, not necessarily.

        // we can look at it this way. if we consider all ibgibs to always
        // exist, a branch can be seen as essentially a collection of latest
        // addrs. a space is a subset of timelines (stones are timelines of
        // length 1) which can be seen as a set of mapping pairs:

        //   tjpAddr->latestAddr
        //   or
        //   startpoint->endpoint

        // so just because we are using metastones as our implementation of
        // "latest addrs" tracking and dynamically jit getting the latest addrs,
        // we can have another implementation that is like the previous
        // implementation of an index ibgib that tracks these start->end
        // mappings.

        // in fact, we can amend `registerNewIbGib` call by adding a branch
        // name/id to the signature (and consequently the underlying cmd
        // modifiers). we can then update our metastone implementation to
        // include this in both the ibgib.ib metadata and ibgib.data proper.
        // this way, we can incorporate the branching functionality in the core
        // of the metaspace functionality and not just in the b2tfs.

        // separate spaces vs. discriminated metastones
        // * spaces will...
        //   * always require copies of ibgib data for every branch operation
        //   * necessitate that when you branch, you will have to copy all files
        //     in the space.
        //     * this copy includes metastones (?)
        //   * create a cleaner separation
        //   * can be optimized later with a larger k/v store/cache
        // * metastones will...
        //   * slow down already tenuous runtime `getLatestAddr`
        //   * mandate copy of metastones with all interspatial transfers

        // so it looks that a branch indeed doesn't have to be a separate space,
        // but the cleanest implementation right now may be a separate space.
        // so I'm going to implement this with a branching operation as creating
        // a separate space. this will also require thought on the active root
        // and mechanics on registerNewIbGib (we'll have to ensure we're
        // designating the default space...maybe changing the existing setting
        // in the bootstrap?)

        // #endregion dev notes from 12/2023

        // talk to the user/posterity/caller
        await fnAddComment({
            contextIbGib, rel8nName: 'comment',
            text: activateAfterwards ?
                `Created and activated root B2tFS Branch IbGib, as well as any child B2tFS Item IbGibs for child files/folders.` :
                `Created but did not activate a root B2tFS Branch IbGib, as well as any child B2tFS Item IbGibs for child files/folders.`,
        });

        // eventually gonna have to figure out what to do with the return ibgib
        return ROOT;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

async function handleB2tFS_branch_fsonly({
    arg,
    argInfos,
    metaspace,
    contextIbGib,
    fnAddComment,
}: {
    arg: RCLICommandHandlerArg,
    argInfos: RCLIArgInfo<RCLIArgType>[],
    metaspace: MetaspaceService,
    contextIbGib: IbGib_V1,
    fnAddComment: (arg: RCLICommandHandlerAddCommentFunctionArgs) => Promise<void>,
}): Promise<IbGib_V1> {
    const lc = `[${handleB2tFS_branch_fsonly.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 624f8758512d6ea9db578db46ceb9b24)`); }

        // which branch?
        const branchInfo = await extractArg_Branch_byNameOrId({ metaspace, argInfos, defaultIfNoArg: 'active' });
        if (!branchInfo) { throw new Error(`branch not found (E: 339719fc32ccaeea75bc85b17acbeb24)`); }

        // debugger; // looking at branchInfo

        const { ibGib: branchIbGib, space: branchSpace } = branchInfo;

        // #region branchInfo.ibGib assumptions
        if (!branchIbGib) { throw new Error(`(UNEXPECTED) branchIbGib falsy? (E: de717fc2ed5a24e9f846d239b2e12524)`); }
        if (!branchIbGib.data) { throw new Error(`(UNEXPECTED) branchIbGib.data falsy? (E: 97d37a04f16bdf5aea5b635450604f24)`); }
        if (!branchIbGib.data.relativePathFromMetaspaceToFSPath) { throw new Error(`(UNEXPECTED) !branchIbGib.data.relativePathFromMetaspaceToFSPath falsy? (E: 97d37a04f16bdf5aea5b635450604f24)`); }
        if (!branchIbGib.rel8ns?.b2tfs_item) { throw new Error(`(UNEXPECTED) !branchIbGib.rel8ns?.b2tfs_item falsy? (E: 7d3b4bef870d9ed08b1590be1e74a824)`); }
        if (branchIbGib.rel8ns.b2tfs_item.length !== 1) { throw new Error(`(UNEXPECTED) branchIbGib.rel8ns.b2tfs_item.length !== 1? (E: cedf551eb604e11d63ae467b8dc34924)`); }
        if (!branchSpace) { throw new Error(`(UNEXPECTED) branchSpace falsy? (E: 6248472302b1c266b54a1c49d6209724)`); }
        // #endregion branchInfo.ibGib assumptions


        /**
         * atow (01/2024) i don't update the branch with the latest item addr.
         * so we want to make sure we get this with `latest: true`
         */
        const branchRootItem = (await getBranchRootItem({
            branchIbGib,
            branchSpace,
            latest: true,
            metaspace,
            defaultIfNone: 'throw',
        }))!;

        // const branchRootItemAddr = branchIbGib.rel8ns.b2tfs_item.at(0)!;
        const branchRootItemAddr = getIbGibAddr({ ibGib: branchRootItem });

        if (logalot) { console.log(`${lc} branchRootItemAddr: ${branchRootItemAddr} (I: a0074bd4763c29ec081812976b435724)`); }

        // we don't care about the entire rootItem dependency graph because we
        // only want the latest binary (file) and folder info.
        // const rootItemDependencyGraph = await metaspace.getDependencyGraph({
        //     space: branchInfo.space,
        //     ibGibAddr: rootItemIbGibAddr,
        //     live: false,
        // });

        /**
         * if the output path already exists, we will shove the output FS items
         * into a unique subdirectory until we prompt for these answers when we
         * do this.
         */
        const outputPath = await extractArg_outputPath({
            argInfos, warnIfExists: true,
            createUniqueSubdirIfExists: true,
            throwIfNotFound: true,
        });

        if (logalot) { console.log(`${lc} cwd: ${cwd()} (I: ddb941fbf9cdfb2ce81d09a762483824)`); }

        /**
         * `outputPath` could be relative. we are going to pass the resolved
         * version to our write FS function.
         */
        const resolvedOutputPath = pathUtils.resolve(getIbGibGlobalThis().initialCwd, outputPath);
        // debugger;


        if (logalot) {
            console.log(`${lc} outputPath: ${outputPath} (I: b5b8795fc596dc5ae3650ec40a0abe24)`);
            console.log(`${lc} resolvedOutputPath: ${resolvedOutputPath} (I: 4f897f7c98f048159fca96262d490ca5)`);
        }
        // debugger; // want to see the resolvedOutputPath

        // todo: which address (for now assume all the latest from the root)

        // project the fs items (folders and files) to output path
        await writeItemToFS({
            // itemAddr: branchRootItemAddr,
            itemIbGib: branchRootItem,
            branchSpace: branchInfo.space,
            metaspace,
            resolvedContextOutputDirPath: resolvedOutputPath,
        });

        // talk to the user/posterity/caller
        await fnAddComment({
            contextIbGib, rel8nName: 'comment',
            text: `Branched just the fs files/folders and wrote these to ${resolvedOutputPath}. branchId: ${branchInfo?.ibGib.data?.uuid}, as well as any child B2tFS Item IbGibs for child files/folders.`
            // text: `wakka wakka`
        });

        // eventually gonna have to figure out what to do with the return ibgib
        return ROOT;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export const rcliCmdHandler_B2tFS_branch: RCLICommandHandler =
    (arg) => handleB2tFS_branch(arg);
