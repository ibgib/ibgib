/**
 * @module rcliCmdHandler_B2tFS_addBranch
 *
 * this handler is for diffing roots/branches.
 *
 * ## thanks
 *
 * * for triggering editor for apply msg (may move this code)
 *   * https://stackoverflow.com/a/17110285/3897838, thanks!
 *
 * @see {@link handleB2tFS_diff}
 */

import * as pathUtils from 'path';
import { promisify } from 'node:util';
import { rm, stat, readFile } from 'node:fs/promises';
import { cwd } from 'node:process';
import { exec, spawn, spawnSync } from 'node:child_process';

import { delay, extractErrorMsg, pretty, } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { RCLIArgInfo, RCLIArgType, RCLIParamInfo } from '@ibgib/helper-gib/dist/rcli/rcli-types.mjs';
import { extractArgValue } from '@ibgib/helper-gib/dist/rcli/rcli-helper.mjs';
import { PARAM_INFO_BARE, PARAM_INFO_NAME } from '@ibgib/helper-gib/dist/rcli/rcli-constants.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { ROOT } from '@ibgib/ts-gib/dist/V1/constants.mjs';
import { MetaspaceService } from '@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../../../ibgib-constants.mjs';
import { RCLICommandHandler, RCLICommandHandlerArg } from '../../rcli-types.mjs';
import { B2tFSBranchInfo, } from '../../b2tfs/b2tfs-branch/b2tfs-branch-types.mjs';
import { PARAM_INFO_APPLY, PARAM_INFO_SPACE_ID, PARAM_INFO_SPACE_NAME, PARAM_INFO_SRC_ID, PARAM_INFO_TEXT } from '../../rcli-constants.mjs';
import { getActiveB2tFSBranchInfo, getB2tFSIndexIbGib } from '../../b2tfs/b2tfs-index/b2tfs-index-helper.mjs';
import { extractArg_space, promptForText } from '../../rcli-helper.mjs';
import { extractArg_Branch_byNameOrId, getB2tFSBranch, getB2tFSBranchDiff, spaceHasRel8dBranch } from '../../b2tfs/b2tfs-branch/b2tfs-branch-helper.mjs';
import { getMessageText_maybePromptIfNotInArgs, parseSpaceQualifiedIbGibAddr, stripApplyMsgCommentLines } from '../../b2tfs/common/b2tfs-common-helper.mjs';
import { getDiffInfoText } from '../../b2tfs/b2tfs-item/b2tfs-item-helper.mjs';
import { B2TFS_DEFAULT_APPLY_DIFF_COMMENT_TEXT, } from '../../b2tfs/common/b2tfs-common-constants.mjs';
import { CommentIbGib_V1 } from '@ibgib/core-gib/dist/common/comment/comment-types.mjs';
import { createCommentIbGib } from '@ibgib/core-gib/dist/common/comment/comment-helper.mjs';

/**
 * used in verbose logging (across all ibgib libs atow)
 */
const logalot = GLOBAL_LOG_A_LOT;

/**
 * handler function that diffs a root/branch in a given superspace per the
 * given rcli command `arg`.
 *
 * @param arg includes
 * @returns ROOT ibgib if successful, otherwise @throws error
 *
 * @see {@link RCLICommandHandlerArg}
 */
export async function handleB2tFS_diff(arg: RCLICommandHandlerArg): Promise<IbGib_V1 | undefined> {
    const lc = `[${handleB2tFS_diff.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: d05a4b2746ef432cab764d84ea0097dd)`); }

        // #region prepare args/validate
        const { contextIbGib, fnAddComment, metaspace, cmdInfo, } = arg;
        const { argInfos } = cmdInfo;

        if (!argInfos) { throw new Error(`(UNEXPECTED) argInfos falsy? (E: c7d8563a3c0f4a79ae1bc694566470c6)`); }
        if (logalot) {
            console.log(`${lc} console.dir(argInfos)... (I: 445580f0e7b04645af681f82a7d6c394)`);
            console.dir(argInfos);
        }

        // for now, we will just diff a given branch's filesystem state with its
        // previous state. in the future, we should be able to diff any two
        // arbitrary branches
        const branchInfo = await extractArg_Branch_byNameOrId({
            metaspace, argInfos, defaultIfNoArg: 'active',
            nameParamInfo: PARAM_INFO_NAME,
            nameCanBeBareArg: true,
            idParamInfo: PARAM_INFO_SRC_ID,
            idCanBeBareArg: false,
        });
        if (!branchInfo) { throw new Error(`couldn't get branch from argInfos or the activeBranch? is there an active branch? check to see active branch with "--b2tfs-info" (E: 3115178eb8164e4698ecdea4c319ac03)`); }

        /**
         * if true, will apply the diff.
         * if false, will defer applying the diff
         *
         * IOW deferred = !apply
         */
        const applyFlagIsSet = (extractArgValue<boolean>({ paramInfo: PARAM_INFO_APPLY, argInfos, }) as boolean) ?? false; // default to false
        if (logalot) { console.log(`${lc} applyFlag: ${applyFlagIsSet}(I: 8d37eaf2c22dc2afbbb72168d6493e24)`); }

        // #endregion prepare args/validate

        const { index: branchIndex, space: branchSpace } = branchInfo;

        if (!branchSpace) { throw new Error(`(UNEXPECTED) branchSpace falsy? (E: 2b050dd1e002b5c1c32e0d9cc7cd2124)`); }

        // first things first, if applying (not deferred) then we want a message
        // associated with

        let applyMsgText: string | undefined = undefined;
        let applyCommentIbGib: CommentIbGib_V1 | undefined = undefined;
        if (applyFlagIsSet) {
            applyMsgText = await getMessageText_maybePromptIfNotInArgs({
                argInfos,
                promptTypeIfNotFound: 'all',
                promptTitle: 'Apply Msg',
                promptMsg: `Type/Paste/Whatever your msg to accompany the apply diff operation.`
            });
            applyMsgText ??= B2TFS_DEFAULT_APPLY_DIFF_COMMENT_TEXT;
            applyMsgText = stripApplyMsgCommentLines({ applyMsgText });
            const resCreateComment = await createCommentIbGib({
                text: applyMsgText,
                saveInSpace: true,
                space: branchSpace,
            });
            applyCommentIbGib = resCreateComment.newIbGib;
        }

        const branchDiff = await getB2tFSBranchDiff({
            branchInfo,
            metaspace,
            deferred: !applyFlagIsSet,
            applyCommentIbGib,
        });

        if (logalot) {
            console.log(`${lc} console.dir(branchDiff)... (I: 8794c0e4287e4ef3b087ccaf8de3639a)`);
            console.dir(branchDiff);
            const diffInfoText = await getDiffInfoText({ itemDiff: branchDiff.rootItemDiff });
            console.log(`${lc} console.dir(diffInfoText)... (I: f9ee5ccd657b4a59ad698e35672cfcc4)`);
            console.dir(diffInfoText);
        }

        // talk to the user/posterity/caller
        const differencesFound = !!branchDiff.rootItemDiff;
        if (differencesFound) {
            // YES differences found
            const diffInfoText = await getDiffInfoText({ itemDiff: branchDiff.rootItemDiff });
            if (applyFlagIsSet) {
                await fnAddComment({
                    contextIbGib, rel8nName: 'comment',
                    text: `YES, differences found, and YES they were applied.\nbranch ${branchInfo.name} (branchSpaceId: ${branchInfo.spaceId})\n${diffInfoText}\nApply Diff Comment (in case we already forgot?):\n${applyMsgText}`,
                });
            } else {
                await fnAddComment({
                    contextIbGib, rel8nName: 'comment',
                    text: `YES, differences found but NOT applied.\nbranch ${branchInfo.name} (branchSpaceId: ${branchInfo.spaceId})\n${diffInfoText}`,
                });
            }
        } else {
            // NO differences found
            if (applyFlagIsSet) {
                await fnAddComment({
                    contextIbGib, rel8nName: 'comment',
                    text: `NO differences found, so NOTHING applied.\nbranch ${branchInfo.name} (branchSpaceId: ${branchInfo.spaceId})`,
                });
            } else {
                await fnAddComment({
                    contextIbGib, rel8nName: 'comment',
                    text: `NO differences found.\nbranch ${branchInfo.name} (branchSpaceId: ${branchInfo.spaceId})`,
                });
            }
        }

        // eventually gonna have to figure out what to do with the return ibgib
        return ROOT;
    } catch (error) {
        debugger; // in error block
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export const rcliCmdHandler_B2tFS_diff: RCLICommandHandler =
    (arg) => handleB2tFS_diff(arg);
