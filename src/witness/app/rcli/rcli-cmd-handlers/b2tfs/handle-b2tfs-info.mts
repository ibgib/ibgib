/**
 * @module rcliCmdHandler_B2tFS_info
 *
 * this handler is for providing information of the current state of B2tFS,
 * i.e., probably mostly the {@link B2tFSIndexIbGib_V1}.
 */

import { extractErrorMsg, pretty, } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { extractArgValue } from '@ibgib/helper-gib/dist/rcli/rcli-helper.mjs';
import { PARAM_INFO_HELP } from '@ibgib/helper-gib/dist/rcli/rcli-constants.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { ROOT } from '@ibgib/ts-gib/dist/V1/constants.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../../../ibgib-constants.mjs';
import { DEFAULT_FORK_OPTIONS, PARAM_INFO_B2TFS_BRANCH, PARAM_INFO_B2TFS_INFO, PARAM_INFO_SPACE_ID, PARAM_INFO_SPACE_NAME, PARAM_INFO_VERBOSE, RCLICommand, } from '../../rcli-constants.mjs';
import { RCLICommandHandler, RCLICommandHandlerAddCommentFunctionArgs, RCLICommandHandlerArg, RCLICommandTextInfo } from '../../rcli-types.mjs';
import { extractArg_space } from '../../rcli-helper.mjs';
import { getB2tFSInfo } from '../../b2tfs/common/b2tfs-common-helper.mjs';
import { getFormattedBranchInfo } from '../../b2tfs/b2tfs-branch/b2tfs-branch-helper.mjs';
import { getFormattedCmdHelpText } from '../../../../../common/rcli-comment/rcli-comment-helper.mjs';

/**
 * used in verbose logging (across all ibgib libs atow)
 */
const logalot = GLOBAL_LOG_A_LOT;


export async function handleB2tFS_info(arg: RCLICommandHandlerArg): Promise<IbGib_V1 | undefined> {
    const lc = `[${handleB2tFS_info.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 33a5ceff7a064c7e88518e453bf46626)`); }
        const { contextIbGib, fnAddComment, metaspace, cmdInfo } = arg;
        const { argInfos, showHelp } = cmdInfo;

        if (!argInfos) { throw new Error(`(UNEXPECTED) argInfos falsy? (E: fb8c5bafc6e230f34158d9ea678f2e23)`); }

        if (showHelp) {
            return handleB2tFS_info_showHelp({ contextIbGib, fnAddComment, cmdInfo }); /* <<<< returns early */
        }

        const space = await extractArg_space({ argInfos, metaspace, returnOnArgsFalsy: 'default' });

        /**
         * only report branch information.
         *
         * note that this param info is a command flag, but doubles as just a
         */
        const branchFlag = (extractArgValue<boolean>({
            paramInfo: PARAM_INFO_B2TFS_BRANCH,
            argInfos,
            throwIfNotFound: false
        }) as boolean ?? false); // defaults to false

        const verbose = (extractArgValue<boolean>({
            paramInfo: PARAM_INFO_VERBOSE,
            argInfos,
            throwIfNotFound: false
        }) as boolean ?? false); // defaults to false

        const b2tfsInfo = await getB2tFSInfo({ metaspace, space, verbose })

        if (branchFlag) {
            const branchInfoTable = getFormattedBranchInfo({ b2tfsInfo, verbose });
            await fnAddComment({
                contextIbGib,
                rel8nName: 'comment',
                text: `Branch Info${verbose ? ' (Verbose)' : ''}:\n${branchInfoTable}`,
            });
        } else {
            await fnAddComment({
                contextIbGib,
                rel8nName: 'comment',
                text: `Here ya go, info on the B2tFS...\n\n${pretty(b2tfsInfo)}`,
            });
        }

        return ROOT;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export async function handleB2tFS_info_showHelp({
    contextIbGib,
    fnAddComment,
    cmdInfo,
}: {
    contextIbGib: IbGib_V1,
    fnAddComment: (arg: RCLICommandHandlerAddCommentFunctionArgs) => Promise<void>,
    cmdInfo: RCLICommandTextInfo,
}): Promise<IbGib_V1 | undefined> {
    const lc = `[${handleB2tFS_info_showHelp.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 4ca7339966eb70ebe4cd2f1b44a23e24)`); }

        const summaryText = `B2tFS Info provides information for a given space. ATOW (01/2024) this is mostly B2tFSIndexReportInfo (in b2tfs-index-types.mts):
/**
 * Shape of information presented to the user when cmd "b2tfs-info"
 * (status/report/other synonyms) is given.
 */
export interface B2tFSIndexReportInfo {
    /**
     * cwd of the given command
     */
    cwd: string;
    /**
     * global state
     */
    ibGibGlobalThis: IbGibGlobalThis;
    /**
     * id of the space which we're reporting on
     */
    spaceId: SpaceId;
    /**
     * name of the space which we're reporting on
     */
    spaceName: string;
    /**
     * true if the B2tFSIndex exists
     */
    indexExists?: boolean;
    /**
     * current index ibgib address.
     */
    indexAddr?: IbGibAddr;
    /**
     * index ibgib's temporal junction point address.
     */
    indexTjpAddr?: TjpIbGibAddr;
    /**
     * actual reference to index ibgib, for \`console.dir(info)\` usage.
     *
     * BE SURE TO DELETE THIS IF YOU NEED A NON-OBJECT, MEMOIZED ONLY INFO
     * OBJECT.
     */
    indexIbGib?: B2tFSIndexIbGib_V1;
    /**
     * branch info for all branches/subbranches in an index
     */
    branchInfos?: { [branchSQIAddr: string]: B2tFSBranchInfo | B2tFSBranchInfo_Terse };
}`;

        const text = getFormattedCmdHelpText({
            cmdParamInfo: PARAM_INFO_B2TFS_INFO,
            summaryText,
            paramSummaries: [
                {
                    param: PARAM_INFO_SPACE_ID,
                    summary: `id of space to get B2tFS information on. you can also use space name. if no space is provided, will use the default local user space.`,
                },
                {
                    param: PARAM_INFO_SPACE_NAME,
                    summary: `name of space to get B2tFS information on. you can also use space id. if no space is provided, will use the default local user space.`,
                },
                {
                    param: PARAM_INFO_B2TFS_BRANCH,
                    summary: `if this flag is true, will only get branch information. possibly same info as --branch --info ? hmm...`
                },
                {
                    param: PARAM_INFO_VERBOSE,
                    summary: `if this flag is true, will provide even more information.`
                },
            ],
        })

        await fnAddComment({
            contextIbGib,
            rel8nName: 'comment',
            text,
        });
        return ROOT;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export const rcliCmdHandler_B2tFS_info: RCLICommandHandler =
    (arg) => handleB2tFS_info(arg);
