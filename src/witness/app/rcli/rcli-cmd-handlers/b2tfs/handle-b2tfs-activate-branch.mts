/**
 * @module rcliCmdHandler_B2tFS_addBranch
 *
 * this handler is for initializing plumbing for the B2tFS aspect of the rcli.
 *
 * @see {@link handleB2tFS_activateBranch}
 */

import { cwd } from 'process';
import * as pathUtils from 'path';

import { clone, extractErrorMsg, getSaferSubstring, } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { extractArgValue } from '@ibgib/helper-gib/dist/rcli/rcli-helper.mjs';
import { PARAM_INFO_INPUT_PATH, PARAM_INFO_NAME } from '@ibgib/helper-gib/dist/rcli/rcli-constants.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { ROOT } from '@ibgib/ts-gib/dist/V1/constants.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../../../ibgib-constants.mjs';
import { RCLICommandHandler, RCLICommandHandlerArg } from '../../rcli-types.mjs';
import { activateB2tFSBranch } from '../../b2tfs/b2tfs-index/b2tfs-index-helper.mjs';
import { getB2tFSBranch, } from '../../b2tfs/b2tfs-branch/b2tfs-branch-helper.mjs';
/**
 * used in verbose logging (across all ibgib libs atow)
 */
const logalot = GLOBAL_LOG_A_LOT;


/**
 * handler function that activates a root/branch in a given superspace per the
 * given rcli command `arg`.
 *
 * after doing some initial preparations (validation/extraction of `arg`
 * related), atow (12/2023) this does the following:
 *
 *
 * @param arg includes
 * @returns ROOT ibgib if successful, otherwise @throws error
 *
 * @see {@link RCLICommandHandlerArg}
 */
export async function handleB2tFS_activateBranch(arg: RCLICommandHandlerArg): Promise<IbGib_V1 | undefined> {
    const lc = `[${handleB2tFS_activateBranch.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 7c21acbf0ffe4c6e8a473d54b34f7fe3)`); }

        // #region prepare args/validate
        const { contextIbGib, fnAddComment, metaspace, cmdInfo, } = arg;
        const { argInfos } = cmdInfo;

        if (!argInfos) { throw new Error(`(UNEXPECTED) argInfos falsy? (E: cb69c2523f8e46aeb1eb68444813764e)`); }
        if (logalot) {
            console.log(`${lc} console.dir(argInfos)... (I: c20761ebe9914b51b60eb4c1605052d0)`);
            console.dir(argInfos);
        }

        let branchName: string;
        const bareArgInfo = argInfos.filter(x => x.isBare).at(0);
        if (bareArgInfo) {
            if (logalot) { console.log(`${lc} bareArgFound. taking this for branchName (I: 013f4d07d7642cbf694ba212f860f223)`); }
            branchName = bareArgInfo.value as string;
        } else {
            branchName = extractArgValue<string>({
                paramInfo: PARAM_INFO_NAME,
                argInfos,
                throwIfNotFound: true,
            }) as string;
        }

        // #endregion prepare args/validate

        const branchInfo = await getB2tFSBranch({ metaspace, branchName });
        if (!branchInfo) { throw new Error(`branchName (${branchName}) couldn't be found...so we can't activate it. ensure the spelling is correct and that it is truly local. also perhaps the bootstrap points to a different trunk local user space. (E: 52b84c06db79d41a9d0a729d857cf123)`); }

        const { index: branchIndex, space: branchSpace } = branchInfo;

        const newBranchIndex = await activateB2tFSBranch({ metaspace, branchSpace, branchIndex });

        if (logalot) {
            console.log(`${lc} console.dir(newBranchIndex) (I: bb2c659ee65251b6f7c79d33db41d323)`);
            console.dir(newBranchIndex);
        }

        // talk to the user/posterity/caller
        await fnAddComment({
            contextIbGib, rel8nName: 'comment',
            text: `branch ${branchName} activated. branchSpaceId: ${branchSpace.data!.uuid}`,
        });

        // eventually gonna have to figure out what to do with the return ibgib
        return ROOT;
    } catch (error) {
        debugger; // in error block
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export const rcliCmdHandler_B2tFS_activateBranch: RCLICommandHandler =
    (arg) => handleB2tFS_activateBranch(arg);
