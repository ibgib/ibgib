/**
 * @module rcliCmdHandler_B2tFS_init
 *
 * this handler is for initializing plumbing for the B2tFS aspect of the rcli.
 */

import { extractErrorMsg, } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { ROOT } from '@ibgib/ts-gib/dist/V1/constants.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../../../ibgib-constants.mjs';
import { RCLICommandHandler, RCLICommandHandlerArg } from '../../rcli-types.mjs';
import { createNewB2tFSIndexIbGib, getB2tFSIndexIbGib } from '../../b2tfs/b2tfs-index/b2tfs-index-helper.mjs';
import { extractArg_space } from '../../rcli-helper.mjs';

/**
 * used in verbose logging (across all ibgib libs atow)
 */
const logalot = GLOBAL_LOG_A_LOT;


export async function handleB2tFS_init(arg: RCLICommandHandlerArg): Promise<IbGib_V1 | undefined> {
    const lc = `[${handleB2tFS_init.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 33a5ceff7a064c7e88518e453bf46626)`); }
        const { contextIbGib, fnAddComment, metaspace, cmdInfo } = arg;
        const { argInfos } = cmdInfo;

        if (!argInfos) { throw new Error(`(UNEXPECTED) argInfos falsy? (E: fb8c5bafc6e230f34158d9ea678f2e23)`); }
        const space = await extractArg_space({ argInfos, metaspace, returnOnArgsFalsy: 'default' });

        // check if the index already exists
        const b2tfsIbGib_existing = await getB2tFSIndexIbGib({
            metaspace, space, throwIfNotFound: false,
        });

        // if it DOES exist already, throw
        if (b2tfsIbGib_existing) { throw new Error(`b2tfs already initialized as  (E: b488232cf1bbca4aff01ad41840d9a23)`); }

        const b2tfsIbGib = await createNewB2tFSIndexIbGib({ metaspace, space, });

        if (!b2tfsIbGib) { throw new Error(`(UNEXPECTED) couldn't create B2tFSIndexIbGib (b2tfsIbGib falsy)? (E: e6360704c37a1fd962f3901cc13db923)`); }

        await fnAddComment({
            contextIbGib,
            rel8nName: 'comment',
            text: `Created B2tFS Index special index ibgib.\n\nThis will be used to keep track of B2tFS roots (roots are kinda like mounted repos).`
        });

        return ROOT;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export const rcliCmdHandler_B2tFS_init: RCLICommandHandler =
    (arg) => handleB2tFS_init(arg);
