/**
 * $name
 */
export const WITNESS_HELPER_TEMPLATE = `/*
 * @module $name helper/util/etc. functions
 *
 * this is where you will find helper functions like those that generate
 * and parse ibs for $name.
 */

// import * as pathUtils from 'path';
// import { statSync } from 'node:fs';
// import { readFile, } from 'node:fs/promises';
// import * as readline from 'node:readline/promises';
// import { stdin, stdout } from 'node:process'; // decide if use this or not

import {
    extractErrorMsg, delay, getSaferSubstring,
    getTimestampInTicks, getUUID, pretty,
} from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { UUID_REGEXP, CLASSNAME_REGEXP, } from '@ibgib/helper-gib/dist/constants.mjs';
import { Gib, Ib, } from '@ibgib/ts-gib/dist/types.mjs';
import { IbGib_V1, } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { GIB, } from '@ibgib/ts-gib/dist/V1/constants.mjs';
import { validateGib, validateIb, validateIbGibIntrinsically, getGib, getGibInfo, } from '@ibgib/ts-gib/dist/V1/validate-helper.mjs';
// import { IbGibSpaceAny } from '@ibgib/core-gib/dist/witness/space/space-base-v1.mjs';
// import { MetaspaceService } from '@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs';

import { WitnessFormBuilder } from '@ibgib/core-gib/dist/witness/witness-form-builder.mjs';
// import { IbGib$PascalCaseNameAny } from './$hyphenated-name-v1.mjs';
import {
    $PascalCaseNameData_V1, $PascalCaseNameRel8ns_V1, $PascalCaseNameIbGib_V1,
} from './$hyphenated-name-types.mjs';
import { $UPPER_SNAKE_NAME_NAME_REGEXP, } from './$hyphenated-name-constants.mjs';

/**
 * for logging. import this constant from your project.
 */
const logalot = GLOBAL_LOG_A_LOT; // remove the true to "turn off" verbose logging

export function validateCommon$PascalCaseNameData({
    data,
}: {
    data?: $PascalCaseNameData_V1,
}): string[] {
    const lc = \`[\${validateCommon$PascalCaseNameData.name}]\`;
    try {
        if (logalot) { console.log(\`\${lc} starting...\`); }
        if (!data) { throw new Error(\`$camelCaseName Data required (E: $GUID)\`); }
        const errors: string[] = [];
        const {
            name, uuid, classname,
        } =
            data;

        if (name) {
            if (!name.match($UPPER_SNAKE_NAME_NAME_REGEXP)) {
                errors.push(\`name must match regexp: \${$UPPER_SNAKE_NAME_NAME_REGEXP} (E: $GUID)\`);
            }
        } else {
            errors.push(\`name required.\`);
        }

        if (uuid) {
            if (!uuid.match(UUID_REGEXP)) {
                errors.push(\`uuid must match regexp: \${UUID_REGEXP} (E: $GUID)\`);
            }
        } else {
            errors.push(\`uuid required.\`);
        }

        if (classname) {
            if (!classname.match(CLASSNAME_REGEXP)) {
                errors.push(\`classname must match regexp: \${CLASSNAME_REGEXP}\`);
            }
        }

        return errors;
    } catch (error) {
        console.error(\`\${lc} \${extractErrorMsg(error)}\`);
        throw error;
    } finally {
        if (logalot) { console.log(\`\${lc} complete.\`); }
    }
}

export async function validateCommon$PascalCaseNameIbGib({
    ibGib,
}: {
    ibGib: $PascalCaseNameIbGib_V1,
}): Promise<string[] | undefined> {
    const lc = \`[\${validateCommon$PascalCaseNameIbGib.name}]\`;
    try {
        if (logalot) { console.log(\`\${lc} starting... (I: $GUID)\`); }
        const intrinsicErrors: string[] = await validateIbGibIntrinsically({ ibGib }) ?? [];

        if (!ibGib.data) { throw new Error(\`ibGib.data required (E: $GUID)\`); }
        const ibErrors: string[] = [];
        let { $camelCaseNameClassname, $camelCaseNameName, $camelCaseNameId } =
            parse$PascalCaseNameIb({ ib: ibGib.ib });
        if (!$camelCaseNameClassname) { ibErrors.push(\`$camelCaseNameClassname required (E: $GUID)\`); }
        if (!$camelCaseNameName) { ibErrors.push(\`$camelCaseNameName required (E: $GUID)\`); }
        if (!$camelCaseNameId) { ibErrors.push(\`$camelCaseNameId required (E: $GUID)\`); }

        const dataErrors = validateCommon$PascalCaseNameData({ data: ibGib.data });

        let result = [...(intrinsicErrors ?? []), ...(ibErrors ?? []), ...(dataErrors ?? [])];
        if (result.length > 0) {
            return result;
        } else {
            return undefined;
        }
    } catch (error) {
        console.error(\`\${lc} \${extractErrorMsg(error)}\`);
        throw error;
    } finally {
        if (logalot) { console.log(\`\${lc} complete.\`); }
    }
}

export function get$PascalCaseNameIb({
    data,
    classname,
}: {
    data: $PascalCaseNameData_V1,
    classname?: string,
}): Ib {
    const lc = \`[\${get$PascalCaseNameIb.name}]\`;
    try {
        const validationErrors = validateCommon$PascalCaseNameData({ data });
        if (validationErrors.length > 0) { throw new Error(\`invalid $camelCaseName data: \${validationErrors} (E: $GUID)\`); }
        if (classname) {
            if (data.classname && data.classname !== classname) { throw new Error(\`classname does not match data.classname (E: $GUID)\`); }
        } else {
            classname = data.classname;
            if (!classname) { throw new Error(\`classname required (E: $GUID)\`); }
        }

        // ad hoc validation here. should centralize witness classname validation

        const { name, uuid } = data;
        return \`witness \${$UPPER_SNAKE_NAME_ATOM} \${classname} \${name} \${uuid}\`;
    } catch (error) {
        console.error(\`\${lc} \${extractErrorMsg(error)}\`);
        throw error;
    }
}

/**
 * Current schema is 'witness [$UPPER_SNAKE_NAME_ATOM] [classname] [$camelCaseNameName] [$camelCaseNameId]'
 *
 * NOTE this is space-delimited
 */
export function parse$PascalCaseNameIb({
    ib,
}: {
    ib: Ib,
}): {
    $camelCaseNameClassname: string,
    $camelCaseNameName: string,
    $camelCaseNameId: string,
} {
    const lc = \`[\${parse$PascalCaseNameIb.name}]\`;
    try {
        if (!ib) { throw new Error(\`$camelCaseName ib required (E: $GUID)\`); }

        const pieces = ib.split(' ');

        return {
            $camelCaseNameClassname: pieces[2],
            $camelCaseNameName: pieces[3],
            $camelCaseNameId: pieces[4],
        };
    } catch (error) {
        console.error(\`\${lc} \${extractErrorMsg(error)}\`);
        throw error;
    }
}

export class $PascalCaseNameFormBuilder extends WitnessFormBuilder {
    protected lc: string = \`[\${$PascalCaseNameFormBuilder.name}]\`;

    constructor() {
        super();
        this.what = '$hyphenated-name';
    }

    // exampleSetting({
    //     of,
    //     required,
    // }: {
    //     of: string,
    //     required?: boolean,
    // }): $PascalCaseNameFormBuilder {
    //     this.addItem({
    //         // $camelCaseName.data.exampleSetting
    //         name: "exampleSetting",
    //         description: \`example description\`,
    //         label: "Example Label",
    //         regexp: EXAMPLE_REGEXP,
    //         regexpErrorMsg: EXAMPLE_REGEXP_DESC,
    //         dataType: 'textarea',
    //         value: of,
    //         required,
    //     });
    //     return this;
    // }

}
`;
