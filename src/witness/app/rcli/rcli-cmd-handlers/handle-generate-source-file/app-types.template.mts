/**
 * $name
 * $PascalCaseName
 */
export const APP_TYPES_TEMPLATE = `/**
 * @module $name app types (and some enums/constants)
 */

import { IbGibAddr } from '@ibgib/ts-gib/dist/types.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import {
    AppData_V1, AppRel8ns_V1,
    AppCmdData, AppCmdRel8ns, AppCmdIbGib,
    AppCmd, AppCmdModifier,
    AppResultData, AppResultRel8ns, AppResultIbGib,
} from "@ibgib/core-gib/dist/witness/app/app-types.mjs";
// and just to show where these things are
// import { CommentIbGib_V1 } from "@ibgib/core-gib/dist/common/comment/comment-types.mjs";
// import { MetaspaceService } from "@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs";

// /**
//  * example enum-like type+const that I use in ibgib. sometimes i put
//  * these in the types.mts and sometimes in the const.mts, depending
//  * on context.
//  */
// export type SomeEnum =
//     'ib' | 'gib';
// /**
//  * @see {@link SomeEnum}
//  */
// export const SomeEnum = {
//     ib: 'ib' as SomeEnum,
//     gib: 'gib' as SomeEnum,
// } satisfies { [key: string]: SomeEnum };
// /**
//  * values of {@link SomeEnum}
//  */
// export const SOME_ENUM_VALUES: SomeEnum[] = Object.values(SomeEnum);

export const DEFAULT_UUID_$UPPER_SNAKE_NAME_APP = '';
export const DEFAULT_NAME_$UPPER_SNAKE_NAME_APP = '$snake_name_gib';
export const DEFAULT_DESCRIPTION_$UPPER_SNAKE_NAME_APP =
    \`HMM what is our description?\`;

/**
 * ibgib's intrinsic data goes here.
 *
 * @see {@link IbGib_V1.data}
 */
export interface $PascalCaseNameAppData_V1 extends AppData_V1 {
    // ibgib data (settings/values/etc) goes here
    // /**
    //  * docs yo
    //  */
    // setting: string;
}

/**
 * rel8ns (named edges/links in DAG) go here.
 *
 * @see {@link IbGib_V1.rel8ns}
 */
export interface $PascalCaseNameAppRel8ns_V1 extends AppRel8ns_V1 {
    // /**
    //  * required rel8n. for most, put in $name-constants.mts file
    //  *
    //  * @see {@link REQUIRED_REL8N_NAME}
    //  */
    // [REQUIRED_REL8N_NAME]: IbGibAddr[];
    // /**
    //  * optional rel8n. for most, put in $name-constants.mts file
    //  *
    //  * @see {@link OPTIONAL_REL8N_NAME}
    //  */
    // [OPTIONAL_REL8N_NAME]?: IbGibAddr[];
}

/**
 * this is the ibgib DTO itself. this is NOT the ibgib app class.
 *
 * If this is a plain ibgib data only object, this acts as a dto. The app
 * witness class is slightly different, as this adds on a single method called
 * \`witness\`. The general design is to send commands and other ibgibs to the
 * witness and receive an ibgib as a result. this acts as a single point of
 * binding interaction and has many other consequences. see the \`Witness\`
 * interface in core-gib \`witness-types.mts\` for more information.
 *
 * @see {@link $PascalCaseNameAppData_V1}
 * @see {@link $PascalCaseNameAppRel8ns_V1}
 */
export interface $PascalCaseNameAppIbGib_V1 extends IbGib_V1<$PascalCaseNameAppData_V1, $PascalCaseNameAppRel8ns_V1> {
}

/**
 * Default data values for a $PascalCaseName app.
 *
 * If you change this, please bump the version
 *
 * (but of course won't be the end of the world when this doesn't happen).
 */
export const DEFAULT_$UPPER_SNAKE_NAME_APP_DATA_V1: $PascalCaseNameAppData_V1 = {
    version: '1',
    uuid: DEFAULT_UUID_$UPPER_SNAKE_NAME_APP,
    name: DEFAULT_NAME_$UPPER_SNAKE_NAME_APP,
    description: DEFAULT_DESCRIPTION_$UPPER_SNAKE_NAME_APP,
    classname: \`\$PascalCaseNameApp_V1\`,

    icon: 'code-slash',

    /**
     * if true, then the app will attempt to persist ALL calls to
     * \`app.witness\`.
     */
    persistOptsAndResultIbGibs: false,
    /**
     * allow ibgibs like 42^gib ({ib: 42, gib: 'gib'} with \`data\` and \`rel8ns\` undefined)
     */
    allowPrimitiveArgs: true,
    /**
     * witnesses should be guaranteed not to throw uncaught exceptions.
     */
    catchAllErrors: true,
    /**
     * if true, would enable logging of all calls to \`app.witness\`
     */
    trace: false,

    // put in your custom defaults here
}
export const DEFAULT_$UPPER_SNAKE_NAME_APP_REL8NS_V1: $PascalCaseNameAppRel8ns_V1 | undefined = undefined;

/**
 * Cmds for interacting with ibgib apps.
 *
 * Not all of these will be implemented for every app.
 *
 * ## todo
 *
 * change these commands to better structure, e.g., verb/do/mod, can/get/addrs
 * */
export type $PascalCaseNameAppCmd =
    'ib' | 'gib' | 'ibgib';
/** Cmds for interacting with ibgib spaces. */
export const $PascalCaseNameAppCmd = {
    /**
     * it's more like a grunt that is intepreted by context.
     */
    ib: 'ib' as $PascalCaseNameAppCmd,
    /**
     * it's more like a grunt that is intepreted by context.
     */
    gib: 'gib' as $PascalCaseNameAppCmd,
    /**
     * third placeholder command.
     *
     * I imagine this will be like "what's up", but who knows.
     */
    ibgib: 'ibgib' as $PascalCaseNameAppCmd,
}

/**
 * Flags to affect the command's interpretation.
 */
export type $PascalCaseNameAppCmdModifier =
    'ib' | 'gib' | 'ibgib';
/**
 * Flags to affect the command's interpretation.
 */
export const $PascalCaseNameAppCmdModifier = {
    /**
     * hmm...
     */
    ib: 'ib' as $PascalCaseNameAppCmdModifier,
    /**
     * hmm...
     */
    gib: 'gib' as $PascalCaseNameAppCmdModifier,
    /**
     * hmm...
     */
    ibgib: 'ibgib' as $PascalCaseNameAppCmdModifier,
}

/** Information for interacting with spaces. */
export interface $PascalCaseNameAppCmdData
    extends AppCmdData<$PascalCaseNameAppCmd, $PascalCaseNameAppCmdModifier> {
}

export interface $PascalCaseNameAppCmdRel8ns extends AppCmdRel8ns {
}

/**
 * Shape of options ibgib if used for a command-based app.
 */
export interface $PascalCaseNameAppCmdIbGib
    extends AppCmdIbGib<
        IbGib_V1,
        $PascalCaseNameAppCmd, $PascalCaseNameAppCmdModifier,
        $PascalCaseNameAppCmdData, $PascalCaseNameAppCmdRel8ns
    > {
}

/**
 * Optional shape of result data to app interactions.
 *
 * This is in addition of course to {@link $PascalCaseNameAppResultData}.
 *
 * so if you're sending a cmd to this app, this should probably be the shape
 * of the result.
 *
 * ## notes
 *
 * * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface $PascalCaseNameAppResultData extends AppResultData {
}

/**
 * Marker interface rel8ns atm...
 *
 * so if you're sending a cmd to this app, this should probably be the shape
 * of the result.
 *
 * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface $PascalCaseNameAppResultRel8ns extends AppResultRel8ns { }

/**
 * Shape of result ibgib if used for a app.
 *
 * so if you're sending a cmd to this app, this should probably be the shape
 * of the result.
 *
 * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface $PascalCaseNameAppResultIbGib
    extends AppResultIbGib<IbGib_V1, $PascalCaseNameAppResultData, $PascalCaseNameAppResultRel8ns> {
}

/**
 * used with app's ib additional metadata. most app ibs I've made have this addl
 * metadata field which is an underscore-delimited field (which is why
 * underscores are removed). the purpose of this is to have per-use-case addl
 * metadata that would be useful to someone (person, api function, whatever)
 * looking at only the ib without loading the entire ibgib data (which could be
 * very expensive).
 */
export interface $PascalCaseNameAppAddlMetadata {
    /**
     * should be $snake_name
     */
    atom?: '$snake_name';
    /**
     * classname of $name **with any underscores removed**.
     */
    classnameIsh?: string;
    /**
     * name of $name app witness **with any underscores removed**.
     */
    nameIsh?: string;
    /**
     * id of $name app witness **with any underscores removed**.
     */
    idIsh?: string;
}
`;
