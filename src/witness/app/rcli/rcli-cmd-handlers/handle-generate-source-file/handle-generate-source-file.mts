import { execPath, cwd, chdir, } from 'node:process';
import { statSync } from 'node:fs';
import { mkdir, readFile, readdir, writeFile, } from 'node:fs/promises';
import * as readline from 'node:readline/promises';
import { stdin, stdout } from 'node:process'; // decide if use this or not
import * as pathUtils from 'path';

import { extractArgValue } from '@ibgib/helper-gib/dist/rcli/rcli-helper.mjs';
import { getSaferSubstring, getUUID, pretty, extractErrorMsg, } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { GIB, ROOT } from '@ibgib/ts-gib/dist/V1/constants.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';

import { RCLICommandHandler, RCLICommandHandlerArg, FILE_ENCODINGS, FileEncoding } from '../../rcli-types.mjs';
import { extractArg_outputPath, promptForText } from '../../rcli-helper.mjs';
import {
    MAX_GENERATE_FILE_NAME_LENGTH, PARAM_INFO_APP,
    PARAM_INFO_BINARY, PARAM_INFO_FILE_ENCODING, PARAM_INFO_NO_RESPEC, PARAM_INFO_ROBBOT, PARAM_INFO_WITNESS
} from '../../rcli-constants.mjs';
import { getCompoundStringVariations } from '../../../../../ibgib-helper.mjs';

// #region templates
import { ROBBOT_V1_TEMPLATE } from './robbot-v1.template.mjs';
import { ROBBOT_CONSTANTS_TEMPLATE } from './robbot-constants.template.mjs';
import { ROBBOT_HELPER_TEMPLATE } from './robbot-helper.template.mjs';
import { ROBBOT_RESPEC_TEMPLATE } from './robbot-respec.template.mjs';
import { ROBBOT_TYPES_TEMPLATE } from './robbot-types.template.mjs';
import { APP_V1_TEMPLATE } from './app-v1.template.mjs';
import { APP_CONSTANTS_TEMPLATE } from './app-constants.template.mjs';
import { APP_HELPER_TEMPLATE } from './app-helper.template.mjs';
import { APP_RESPEC_TEMPLATE } from './app-respec.template.mjs';
import { APP_TYPES_TEMPLATE } from './app-types.template.mjs';
import { WITNESS_V1_TEMPLATE } from './witness-v1.template.mjs';
import { WITNESS_CONSTANTS_TEMPLATE } from './witness-constants.template.mjs';
import { WITNESS_HELPER_TEMPLATE } from './witness-helper.template.mjs';
import { WITNESS_RESPEC_TEMPLATE } from './witness-respec.template.mjs';
import { WITNESS_TYPES_TEMPLATE } from './witness-types.template.mjs';
import { PLAIN_CONSTANTS_TEMPLATE } from './plain-constants.template.mjs';
import { PLAIN_TYPES_TEMPLATE } from './plain-types.template.mjs';
import { PLAIN_HELPER_TEMPLATE } from './plain-helper.template.mjs';
import { PLAIN_RESPEC_TEMPLATE } from './plain-respec.template.mjs';
// #endregion templates

import { GLOBAL_LOG_A_LOT } from '../../../../../ibgib-constants.mjs';
import { RCLIArgInfo, RCLIArgType } from '@ibgib/helper-gib/dist/rcli/rcli-types.mjs';
import { CompoundStringVariations } from '../../../../../ibgib-types.mjs';
import { PARAM_INFO_NAME } from '@ibgib/helper-gib/dist/rcli/rcli-constants.mjs';

/**
 * used in verbose logging (across all ibgib libs atow)
 */
const logalot = GLOBAL_LOG_A_LOT;


/**
 * helper function that replaces tokens in templates.
 *
 * ## future
 *
 * replace this with lex-gib
 */
async function doTemplateReplacements({
    template, nameVariations
}: {
    template: string, nameVariations: CompoundStringVariations,
}): Promise<string> {
    let resTemplate: string = template.concat();

    // first replace guids
    const guidTemplateVar = '$GUID';
    while (resTemplate.includes(guidTemplateVar)) {
        const guid = (await getUUID()).substring(0, 32);
        resTemplate = resTemplate.replace(guidTemplateVar, guid);
    }

    // now we can do simple string replacements
    resTemplate = resTemplate
        .replace(/\$name/g, nameVariations.raw)
        .replace(/\$PascalCaseName/g, nameVariations.pascalCased)
        .replace(/\$camelCaseName/g, nameVariations.camelCased)
        .replace(/\$hyphenated-name/g, nameVariations.hyphenated)
        .replace(/\$snake_case_name/g, nameVariations.snakeCased)
        .replace(/\$UPPER_SNAKE_NAME/g, nameVariations.upperSnakeCased);

    // we're done
    return resTemplate;
}

/**
 * meh, probably doesn't need to be a constant
 */
const TEMPLATE_FILE_EXT = 'mts';
function getFullFilenamePath_Suffixed({
    nameVariations,
    suffix,
    dirPath,
}: {
    nameVariations: CompoundStringVariations,
    suffix: string,
    dirPath: string,
}): string {
    return pathUtils.join(dirPath, `${nameVariations.hyphenated}${suffix}.${TEMPLATE_FILE_EXT}`);
}

async function getName({ argInfos }: { argInfos: RCLIArgInfo<RCLIArgType>[] }): Promise<string> {
    const lc = `[${getName.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 1779f377d1d510c6ced25103b801b123)`); }

        let name = extractArgValue<string>({
            argInfos,
            paramInfo: PARAM_INFO_NAME,
            throwIfNotFound: false,
        }) as string | undefined;
        if (name) { name = getSaferSubstring({ text: name, length: MAX_GENERATE_FILE_NAME_LENGTH }); }

        let attemptsSoFar = 0;
        const maxAttempts = 3;
        while (!name && attemptsSoFar < maxAttempts) {
            attemptsSoFar++;
            let resName = await promptForText({ msg: 'name?' });
            if (resName) {
                name = getSaferSubstring({ text: resName, length: MAX_GENERATE_FILE_NAME_LENGTH });
                if (!name) { console.error(`${lc} invalid name. just characters, make it simple. attempts left: ${maxAttempts - attemptsSoFar}`); }
            }
        }
        if (!name) { throw new Error(`name required (E: 490197b1b86feb2c0472253f69c6cb23)`); }

        return name;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export async function handleGenerateSourceFile(arg: RCLICommandHandlerArg): Promise<IbGib_V1 | undefined> {
    const lc = `[${handleGenerateSourceFile.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: dc2fae45f95042cd80cdf408e005c635)`); }
        const { cmdEscapeString, cmdInfo, contextIbGib, ibGib, metaspace } = arg;

        let { argInfos } = cmdInfo;

        if (!argInfos) { throw new Error(`(UNEXPECTED) argInfos falsy? (E: 5a077a9054eb470f9200acb6a8083eed)`); }

        let outputPath = await extractArg_outputPath({
            argInfos, createUniqueSubdirIfExists: false, warnIfExists: false,
        });

        const name = await getName({ argInfos });
        /**
         * someFilenameYo, SomeFilenameYo, some-filename-yo, some_filename_yo
         */
        const nameVariations = getCompoundStringVariations({ rawString: name });

        // right now, we handle the following different cases
        // 1. regular ibgib
        // 2. witness ibgib
        // 3. app witness
        // 4. robbot witness

        const isWitness = extractArgValue<boolean>({
            paramInfo: PARAM_INFO_WITNESS,
            argInfos, throwIfNotFound: false,
        }) as boolean | undefined;
        console.log(`${lc} isWitness: ${isWitness}`);
        console.log(`${lc} cwd: ${cwd()}`);
        const isApp = extractArgValue<boolean>({
            paramInfo: PARAM_INFO_APP,
            argInfos, throwIfNotFound: false,
        }) as boolean | undefined;
        const isRobbot = extractArgValue<boolean>({
            paramInfo: PARAM_INFO_ROBBOT,
            argInfos, throwIfNotFound: false,
        }) as boolean | undefined;
        const noRespec = !!(extractArgValue<boolean>({
            paramInfo: PARAM_INFO_NO_RESPEC,
            argInfos, throwIfNotFound: false,
        }) as boolean | undefined);

        console.warn(`${lc} WARNING: early assumption here with cwd. auto-adding '../' to outputPath (W: a4cfc4ebed824923b873f634da10dd7a)`)
        outputPath = pathUtils.join('..', outputPath);
        console.warn(outputPath)
        console.warn(`${lc} WARNING: todo: add option for output pth assumptions (W: a4cfc4ebed824923b873f634da10dd7a)`)
        console.warn(`${lc} WARNING: early assumption here with cwd. auto-adding '../' to outputPath (W: a4cfc4ebed824923b873f634da10dd7a)`)

        const dirPath = pathUtils.join(outputPath, nameVariations.hyphenated);
        await mkdir(dirPath, { recursive: true });
        const respecFilename = `${nameVariations.hyphenated}.respec.${TEMPLATE_FILE_EXT}`;

        if (isApp) {
            await writeFiles_App({
                dirPath,
                nameVariations,
                noRespec,
                respecFilename,
            });
        } else if (isRobbot) {
            throw new Error(`not impl (E: 27bcf966df1c64786576b00676135823)`);
            await writeFiles_Robbot({
                dirPath,
                nameVariations,
                noRespec,
                respecFilename,
            });
        } else if (isWitness) {
            // make the witness class
            await writeFiles_Witness({
                dirPath,
                nameVariations,
                noRespec,
                respecFilename,
            });
        } else {
            // regular ibgib, non-witness
            await writeFiles_Plain({
                dirPath,
                nameVariations,
                noRespec,
                respecFilename,
            });
        }

        return ROOT;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export async function writeFiles_Plain({
    dirPath,
    nameVariations,
    noRespec,
    respecFilename,
}: {
    dirPath: string,
    nameVariations: CompoundStringVariations,
    noRespec: boolean,
    respecFilename: string,
}): Promise<void> {
    const lc = `[${writeFiles_Plain.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: fb487411be8d899cedf347df049f3d23)`); }

        // make the constants
        await writeFile(
            getFullFilenamePath_Suffixed({ suffix: '-constants', nameVariations, dirPath }),
            await doTemplateReplacements({ template: PLAIN_CONSTANTS_TEMPLATE, nameVariations }),
            { encoding: FileEncoding.utf8, }
        );
        await writeFile(
            getFullFilenamePath_Suffixed({ suffix: '-helper', nameVariations, dirPath }),
            await doTemplateReplacements({ template: PLAIN_HELPER_TEMPLATE, nameVariations }),
            { encoding: FileEncoding.utf8, }
        );
        await writeFile(
            getFullFilenamePath_Suffixed({ suffix: '-types', nameVariations, dirPath }),
            await doTemplateReplacements({ template: PLAIN_TYPES_TEMPLATE, nameVariations }),
            { encoding: FileEncoding.utf8, }
        );
        if (!noRespec) {
            // make the respec
            await writeFile(
                pathUtils.join(dirPath, respecFilename),
                await doTemplateReplacements({ template: PLAIN_RESPEC_TEMPLATE, nameVariations }),
                { encoding: FileEncoding.utf8, }
            );
        }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export async function writeFiles_Witness({
    dirPath,
    nameVariations,
    noRespec,
    respecFilename,
}: {
    dirPath: string,
    nameVariations: CompoundStringVariations,
    noRespec: boolean,
    respecFilename: string,
}): Promise<void> {
    const lc = `[${writeFiles_Witness.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: db8442104cf2446fba0a6642aeba007a)`); }

        // make the constants
        await writeFile(
            getFullFilenamePath_Suffixed({ suffix: '-constants', nameVariations, dirPath }),
            await doTemplateReplacements({ template: WITNESS_CONSTANTS_TEMPLATE, nameVariations }),
            { encoding: FileEncoding.utf8, }
        );
        await writeFile(
            getFullFilenamePath_Suffixed({ suffix: '-helper', nameVariations, dirPath }),
            await doTemplateReplacements({ template: WITNESS_HELPER_TEMPLATE, nameVariations }),
            { encoding: FileEncoding.utf8, }
        );
        await writeFile(
            getFullFilenamePath_Suffixed({ suffix: '-types', nameVariations, dirPath }),
            await doTemplateReplacements({ template: WITNESS_TYPES_TEMPLATE, nameVariations }),
            { encoding: FileEncoding.utf8, }
        );
        await writeFile(
            getFullFilenamePath_Suffixed({ suffix: '-v1', nameVariations, dirPath }),
            await doTemplateReplacements({ template: WITNESS_V1_TEMPLATE, nameVariations }),
            { encoding: FileEncoding.utf8, }
        );
        if (!noRespec) {
            // make the respec
            await writeFile(
                pathUtils.join(dirPath, respecFilename),
                await doTemplateReplacements({ template: WITNESS_RESPEC_TEMPLATE, nameVariations }),
                { encoding: FileEncoding.utf8, }
            );
        }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export async function writeFiles_App({
    dirPath,
    nameVariations,
    noRespec,
    respecFilename,
}: {
    dirPath: string,
    nameVariations: CompoundStringVariations,
    noRespec: boolean,
    respecFilename: string,
}): Promise<void> {
    const lc = `[${writeFiles_App.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 826b3b2c9ff74b989218cbfdcbedcf80)`); }

        // make the constants
        await writeFile(
            getFullFilenamePath_Suffixed({ suffix: '-constants', nameVariations, dirPath }),
            await doTemplateReplacements({ template: APP_CONSTANTS_TEMPLATE, nameVariations }),
            { encoding: FileEncoding.utf8, }
        );
        await writeFile(
            getFullFilenamePath_Suffixed({ suffix: '-helper', nameVariations, dirPath }),
            await doTemplateReplacements({ template: APP_HELPER_TEMPLATE, nameVariations }),
            { encoding: FileEncoding.utf8, }
        );
        await writeFile(
            getFullFilenamePath_Suffixed({ suffix: '-types', nameVariations, dirPath }),
            await doTemplateReplacements({ template: APP_TYPES_TEMPLATE, nameVariations }),
            { encoding: FileEncoding.utf8, }
        );
        await writeFile(
            getFullFilenamePath_Suffixed({ suffix: '-v1', nameVariations, dirPath }),
            await doTemplateReplacements({ template: APP_V1_TEMPLATE, nameVariations }),
            { encoding: FileEncoding.utf8, }
        );
        if (!noRespec) {
            // make the respec
            await writeFile(
                pathUtils.join(dirPath, respecFilename),
                await doTemplateReplacements({ template: APP_RESPEC_TEMPLATE, nameVariations }),
                { encoding: FileEncoding.utf8, }
            );
        }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export async function writeFiles_Robbot({
    dirPath,
    nameVariations,
    noRespec,
    respecFilename,
}: {
    dirPath: string,
    nameVariations: CompoundStringVariations,
    noRespec: boolean,
    respecFilename: string,
}): Promise<void> {
    const lc = `[${writeFiles_Robbot.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 05a6f07f8d2e46b996367bcdf57e630e)`); }

        // make the constants
        await writeFile(
            getFullFilenamePath_Suffixed({ suffix: '-constants', nameVariations, dirPath }),
            await doTemplateReplacements({ template: ROBBOT_CONSTANTS_TEMPLATE, nameVariations }),
            { encoding: FileEncoding.utf8, }
        );
        await writeFile(
            getFullFilenamePath_Suffixed({ suffix: '-helper', nameVariations, dirPath }),
            await doTemplateReplacements({ template: ROBBOT_HELPER_TEMPLATE, nameVariations }),
            { encoding: FileEncoding.utf8, }
        );
        await writeFile(
            getFullFilenamePath_Suffixed({ suffix: '-types', nameVariations, dirPath }),
            await doTemplateReplacements({ template: ROBBOT_TYPES_TEMPLATE, nameVariations }),
            { encoding: FileEncoding.utf8, }
        );
        await writeFile(
            getFullFilenamePath_Suffixed({ suffix: '-v1', nameVariations, dirPath }),
            await doTemplateReplacements({ template: ROBBOT_V1_TEMPLATE, nameVariations }),
            { encoding: FileEncoding.utf8, }
        );
        if (!noRespec) {
            // make the respec
            await writeFile(
                pathUtils.join(dirPath, respecFilename),
                await doTemplateReplacements({ template: ROBBOT_RESPEC_TEMPLATE, nameVariations }),
                { encoding: FileEncoding.utf8, }
            );
        }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export const rcliCmdHandlerGenerateSourceFile: RCLICommandHandler =
    (arg) => handleGenerateSourceFile(arg);
