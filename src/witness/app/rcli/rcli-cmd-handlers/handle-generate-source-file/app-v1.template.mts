/**
 * $name
 * $PascalCaseName
 * $UPPER_SNAKE_NAME
 * $hyphenated-name
 */
export const APP_V1_TEMPLATE = `// import * as readline from 'node:readline/promises';
// import { stdin, stdout } from 'node:process'; // decide if use this or not

import { clone, extractErrorMsg, getIdPool, getUUID, pretty } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { DEFAULT_DATA_PATH_DELIMITER } from '@ibgib/helper-gib/dist/constants.mjs';
import { getIbGibAddr } from '@ibgib/ts-gib/dist/helper.mjs';
import { IbGib_V1, Rel8n, IbGibRel8ns_V1, } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { ROOT, } from '@ibgib/ts-gib/dist/V1/constants.mjs';
import { Factory_V1 as factory, } from '@ibgib/ts-gib/dist/V1/factory.mjs';
import { IbGibAddr, TransformResult } from '@ibgib/ts-gib/dist/types.mjs';
import { AppBase_V1 } from '@ibgib/core-gib/dist/witness/app/app-base-v1.mjs';
import { ErrorIbGib_V1 } from '@ibgib/core-gib/dist/common/error/error-types.mjs';
import { RobbotCmd } from '@ibgib/core-gib/dist/witness/robbot/robbot-types.mjs';
import { CommentIbGib_V1 } from '@ibgib/core-gib/dist/common/comment/comment-types.mjs';
import { DynamicForm, FormItemInfo } from '@ibgib/core-gib/dist/common/form/form-items.mjs';
import { WitnessFormBuilder } from '@ibgib/core-gib/dist/witness/witness-form-builder.mjs';
import { getAppIb, AppFormBuilder } from '@ibgib/core-gib/dist/witness/app/app-helper.mjs';
import { DynamicFormBuilder, } from '@ibgib/core-gib/dist/common/form/form-helper.mjs';
import { WitnessArgIbGib } from '@ibgib/core-gib/dist/witness/witness-types.mjs';
import { isComment } from '@ibgib/core-gib/dist/common/comment/comment-helper.mjs';
import { WITNESS_CONTEXT_REL8N_NAME } from '@ibgib/core-gib/dist/witness/witness-constants.mjs';
import { isError } from '@ibgib/core-gib/dist/common/error/error-helper.mjs';
import { IbGibRobbotAny } from '@ibgib/core-gib/dist/witness/robbot/robbot-base-v1.mjs';

import {
    $PascalCaseNameAppData_V1, $PascalCaseNameAppRel8ns_V1, $PascalCaseNameAppIbGib_V1,
    $PascalCaseNameAppCmdData, $PascalCaseNameAppCmdRel8ns, $PascalCaseNameAppCmdIbGib,
    $PascalCaseNameAppResultData, $PascalCaseNameAppResultRel8ns, $PascalCaseNameAppResultIbGib,
    $PascalCaseNameAppAddlMetadata,
    DEFAULT_$UPPER_SNAKE_NAME_APP_DATA_V1,
    DEFAULT_DESCRIPTION_$UPPER_SNAKE_NAME_APP, DEFAULT_$UPPER_SNAKE_NAME_APP_REL8NS_V1,
} from './$hyphenated-name-types.mjs';
import { $PascalCaseNameAppFormBuilder, } from './$hyphenated-name-helper.mjs';
import { DynamicFormFactoryBase } from '../../tmp/dynamic-form-factory-base.mjs';


/**
 * for logging. import this constant from your project.
 */
const logalot = GLOBAL_LOG_A_LOT; // change this when you want to turn off verbose logging

export class $PascalCaseNameApp_V1 extends AppBase_V1<
    // in
    $PascalCaseNameAppCmdData, $PascalCaseNameAppCmdRel8ns, $PascalCaseNameAppCmdIbGib,
    // out
    $PascalCaseNameAppResultData, $PascalCaseNameAppResultRel8ns, $PascalCaseNameAppResultIbGib,
    // this
    $PascalCaseNameAppData_V1, $PascalCaseNameAppRel8ns_V1
> implements $PascalCaseNameAppIbGib_V1 {
    protected lc: string = \`[\${$PascalCaseNameApp_V1.name}]\`;

    /**
     * when receiving an update to the context, we want to know if the incoming
     * ibgib child is one that we've already handled. this is for idempotence
     * (to avoid double-handling).
     */
    protected alreadyHandledContextChildrenAddrs: IbGibAddr[] = [];

    constructor(initialData?: $PascalCaseNameAppData_V1, initialRel8ns?: $PascalCaseNameAppRel8ns_V1) {
        super(initialData, initialRel8ns);
        const lc = \`\${ this.lc }[ctor]\`;
        try {
            if (logalot) { console.log(\`\${ lc } starting...\`); }
            this.initialize();
        } catch (error) {
            console.error(\`\${ lc } \${extractErrorMsg(error)} \`);
            throw error;
        } finally {
            if (logalot) { console.log(\`\${ lc } complete.\`); }
        }
    }

    /**
     * overridden to handle non-argyfied incoming ibgibs being witnessed.
     */
    protected async doNonArg({
        ibGib,
    }: {
        ibGib: IbGib_V1,
    }): Promise<$PascalCaseNameAppResultIbGib | undefined> {
        const lc = \`\${ this.lc }[\${ this.doNonArg.name }]\`;
        try {
            if (logalot) { console.log(\`\${ lc } starting... (I: $GUID)\`); }

            throw new Error(\`not impl (E: $GUID)\`);

        } catch (error) {
            console.error(\`\${ lc } \${extractErrorMsg(error)} \`);
            throw error;
        } finally {
            if (logalot) { console.log(\`\${ lc } complete.\`); }
        }
    }

    /**
     * implement this when transitioning to a new context ibgib. be sure
     * to call \`super.initializeContext\`
     *
     * So you call this when you have an ibgib that you want to initialize the
     * app with. if the app already has a context, then this should finalize the
     * previous context and initialize the new one.
     */
    protected async initializeContext({
        arg,
        contextIbGib,
        rel8nName,
    }: {
        arg?: WitnessArgIbGib<IbGib_V1, any, any>,
        contextIbGib?: IbGib_V1,
        rel8nName: string,
    }): Promise<void> {
        const lc = \`\${this.lc}[\${this.initializeContext.name}]\`;
        try {
            if (logalot) { console.log(\`\${lc} starting... (I: $GUID)\`); }
            await super.initializeContext({ arg, contextIbGib, rel8nName });

            // do custom initialization

            // the following is an example atow from rcli app
            // // prepare
            // contextIbGib ??= await this.getContextIbGibFromArgOrAddr({ arg, latest: false });
            // if (!contextIbGib) { throw new Error(\`couldn't get contextIbGib? (E: $GUID)\`); }
            // if (!this.rliRobbot) { throw new Error(\`(UNEXPECTED) this.rliRobbot falsy? (E: $GUID)\`); }

            // // initialize the context of our helper robbot as well, as both
            // // initially start out the same
            // // in the future here, these may not be the same though...
            // const argActivate = await this.rliRobbot.argy({
            //     argData: {
            //         cmd: RobbotCmd.activate,
            //         ibGibAddrs: [getIbGibAddr({ ibGib: contextIbGib })], // context
            //     },
            //     ibGibs: [contextIbGib], // context
            // });
            // const resCmd = await this.rliRobbot.witness(argActivate);

            // if (!resCmd) { throw new Error(\`resCmd is falsy. (E: $GUID)\`); }
            // if (isError({ ibGib: resCmd })) {
            //     const errIbGib = resCmd as ErrorIbGib_V1;
            //     throw new Error(\`errIbGib: \${pretty(errIbGib)} (E: $GUID)\`);
            // }
        } catch (error) {
            console.error(\`\${lc} \${extractErrorMsg(error)}\`);
            throw error;
        } finally {
            if (logalot) { console.log(\`\${lc} complete.\`); }
        }
    }

    /**
     * this occurs when a "new" ibgib is added to the app's current context.
     *
     * technically, this occurs when a "new" ibgib is rel8'd to the context and
     * the context's timeline (per its temporal junction point) gets a new entry
     * in the local metaspace.
     *
     * @see {@link handleContextUpdate} for any update
     */
    protected async handleNewContextChild({ newChild }: { newChild: IbGib_V1 }): Promise<void> {
        const lc = \`\${this.lc}[\${this.handleNewContextChild.name}]\`;
        try {
            if (logalot) { console.log(\`\${lc} starting... (I: $GUID)\`); }

            const addr = getIbGibAddr({ ibGib: newChild });

            if (this.alreadyHandledContextChildrenAddrs.includes(addr)) {
                if (logalot) { console.log(\`\${lc} already handled, returning early: \${addr} (I: $GUID)\`); }
                return; /* <<<< returns early */
            } else {
                this.alreadyHandledContextChildrenAddrs.push(addr);
            }

        } catch (error) {
            console.error(\`\${lc} \${extractErrorMsg(error)}\`);
            throw error;
        } finally {
            if (logalot) { console.log(\`\${lc} complete.\`); }
        }
    }

    /**
     * overridden to convert comments to requests, if applicable.
     */
    protected async doComment({
        ibGib,
    }: {
        ibGib: CommentIbGib_V1,
    }): Promise<$PascalCaseNameAppResultIbGib | undefined> {
        const lc = \`\${this.lc}[\${this.doComment.name}]\`;
        let result: $PascalCaseNameAppResultIbGib | undefined = undefined;
        try {

            if (logalot) { console.log(\`\${lc} starting... (I: $GUID)\`); }

            throw new Error(\`not impl (E: $GUID)\`);
            return result;
        } catch (error) {
            console.error(\`\${lc} \${extractErrorMsg(error)}\`);
            // don't rethrow ?
            // throw error;
            return undefined;
        } finally {
            if (logalot) { console.log(\`\${lc} complete.\`); }
        }
    }

    /**
     * Initializes to default space values.
     */
    protected async initialize(): Promise<void> {
        const lc = \`\${this.lc}[\${this.initialize.name}]\`;
        try {
            if (logalot) { console.log(\`\${lc} starting...\`); }
            if (!this.data) { this.data = clone(DEFAULT_$UPPER_SNAKE_NAME_APP_DATA_V1); }
            if (!this.rel8ns && DEFAULT_$UPPER_SNAKE_NAME_APP_REL8NS_V1) {
                this.rel8ns = clone(DEFAULT_$UPPER_SNAKE_NAME_APP_REL8NS_V1);
            }
        } catch (error) {
            console.error(\`\${lc} \${extractErrorMsg(error)}\`);
        } finally {
            if (logalot) { console.log(\`\${lc} complete.\`); }
        }
    }

    protected async doDefault({
        ibGib,
    }: {
        ibGib: IbGib_V1,
    }): Promise<$PascalCaseNameAppResultIbGib> {
        const lc = \`\${this.lc}[\${this.doDefault.name}]\`;
        try {
            if (logalot) { console.log(\`\${lc} starting...\`); }
            throw new Error(\`not impl (E: $GUID)\`);
            return ROOT as $PascalCaseNameAppResultIbGib;
        } catch (error) {
            console.error(\`\${lc} \${extractErrorMsg(error)}\`);
            throw error;
        } finally {
            if (logalot) { console.log(\`\${lc} complete.\`); }
        }
    }

    /**
     * In this app, the ib command looks at given ibGib(s) and remembers
     * it/them (i.e. the app rel8s the ibgibs to itself).
     *
     * @returns ROOT if successful, else throws
     */
    protected async doCmdIb({
        arg,
    }: {
        arg: IbGib_V1,
    }): Promise<$PascalCaseNameAppResultIbGib> {
        const lc = \`\${this.lc}[\${this.doCmdIb.name}]\`;
        try {
            if (logalot) { console.log(\`\${lc} starting...\`); }
            throw new Error(\`not implemented (E: $GUID)\`);
            return ROOT as $PascalCaseNameAppResultIbGib;
        } catch (error) {
            console.error(\`\${lc} \${extractErrorMsg(error)}\`);
            throw error;
        } finally {
            if (logalot) { console.log(\`\${lc} complete.\`); }
        }
    }

    /**
     * "Speak" one of the memorized ibgibs using the given arg.data.ibGibAddrs[0] as the context
     * ibGib.
     *
     * @returns ROOT if all goes well, otherwise throws an error.
     */
    protected async doCmdGib({
        arg,
    }: {
        arg: IbGib_V1,
    }): Promise<$PascalCaseNameAppResultIbGib> {
        const lc = \`\${this.lc}[\${this.doCmdGib.name}]\`;
        try {
            if (logalot) { console.log(\`\${lc} starting...\`); }
            if (!this.ibgibsSvc) { throw new Error(\`(UNEXPECTED) this.ibgibsSvc falsy. not initialized? (E: $GUID)\`); }

            const space = await this.ibgibsSvc.getLocalUserSpace({ lock: true });

            throw new Error(\`not implemented yet (E: $GUID)\`);
            return ROOT as $PascalCaseNameAppResultIbGib;
        } catch (error) {
            console.error(\`\${lc} \${extractErrorMsg(error)}\`);
            throw error;
        } finally {
            if (logalot) { console.log(\`\${lc} complete.\`); }
        }

    }
    protected async doCmdIbgib({
        arg,
    }: {
        arg: IbGib_V1,
    }): Promise<$PascalCaseNameAppResultIbGib> {
        const lc = \`\${this.lc}[\${this.doCmdIbgib.name}]\`;
        try {
            if (logalot) { console.log(\`\${lc} starting...\`); }
            throw new Error(\`not implemented yet (E: $GUID)\`);
            return ROOT as $PascalCaseNameAppResultIbGib;
        } catch (error) {
            console.error(\`\${lc} \${extractErrorMsg(error)}\`);
            throw error;
        } finally {
            if (logalot) { console.log(\`\${lc} complete.\`); }
        }

    }

    protected async validateWitnessArg(arg: $PascalCaseNameAppCmdIbGib): Promise<string[]> {
        const lc = \`\${this.lc}[\${this.validateWitnessArg.name}]\`;
        try {
            if (logalot) { console.log(\`\${lc} starting...\`); }
            const errors = await super.validateWitnessArg(arg) ?? [];
            if (!this.ibgibsSvc) { throw new Error(\`(unexpected) this.ibgibsSvc falsy. not initialized? (E: $GUID)\`); }
            if ((arg.data as any).cmd) {
                // perform extra validation for cmds
                if ((arg.ibGibs ?? []).length === 0) {
                    errors.push(\`ibGibs required. (E: $GUID)\`);
                }
            }
            return errors;
        } catch (error) {
            console.error(\`\${lc} \${extractErrorMsg(error)}\`);
            throw error;
        } finally {
            if (logalot) { console.log(\`\${lc} complete.\`); }
        }
    }

    protected async validateThis(): Promise<string[]> {
        const lc = \`\${this.lc}[\${this.validateThis.name}]\`;
        try {
            if (logalot) { console.log(\`\${lc} starting...\`); }
            const errors = [
                ...await super.validateThis(),
            ];
            const { data } = this;
            if (data) {
                // add and push errors like so...
                // if (!data.cmdEscapeString) {
                //     errors.push('data.cmdEscapeString required (E: $GUID');
                // }
            }
            return errors;
        } catch (error) {
            console.error(\`\${lc} \${extractErrorMsg(error)}\`);
            throw error;
        } finally {
            if (logalot) { console.log(\`\${lc} complete.\`); }
        }
    }

    /**
     * @returns addlmetadata string (atow default impl in base class)
     */
    protected getAddlMetadata(): string {
        return super.getAddlMetadata();
    }
    /**
     * @returns parsed info from the given ib's addl metadata
     *
     * change this implementation to suit your use case.
     */
    protected parseAddlMetadataString<TParseResult>({ ib }: { ib: string; }): TParseResult {
        // const addlMetadataText = \`\${atom}_\${classnameIsh}_\${nameIsh}_\${idIsh}\`;
        if (!ib) { throw new Error(\`ib required (E: $GUID)\`); }
        const lc = \`[\${this.parseAddlMetadataString.name}]\`;
        try {
            const [atom, classnameIsh, nameIsh, idIsh] = ib.split('_');
            const result = { atom, classnameIsh, nameIsh, idIsh, } as $PascalCaseNameAppAddlMetadata;
            return result as TParseResult; // i'm not liking the TParseResult...hmm
        } catch (error) {
            console.error(\`\${lc} \${extractErrorMsg(error)}\`);
            throw error;
        }
    }
}

/**
 * factory for random app.
 *
 * @see {@link DynamicFormFactoryBase}
 */
export class $PascalCaseNameApp_V1_Factory
    extends DynamicFormFactoryBase<$PascalCaseNameAppData_V1, $PascalCaseNameAppRel8ns_V1, $PascalCaseNameApp_V1> {

    protected lc: string = \`[\${$PascalCaseNameApp_V1_Factory.name}]\`;

    getName(): string { return $PascalCaseNameApp_V1.name; }

    async newUp({
        data,
        rel8ns,
    }: {
        data?: $PascalCaseNameAppData_V1,
        rel8ns?: $PascalCaseNameAppRel8ns_V1,
    }): Promise<TransformResult<$PascalCaseNameApp_V1>> {
        const lc = \`\${this.lc}[\${this.newUp.name}]\`;
        try {
            if (logalot) { console.log(\`\${lc} starting...\`); }
            data ??= clone(DEFAULT_$UPPER_SNAKE_NAME_APP_DATA_V1);
            data = data!;
            rel8ns = rel8ns ?? DEFAULT_$UPPER_SNAKE_NAME_APP_REL8NS_V1 ? clone(DEFAULT_$UPPER_SNAKE_NAME_APP_REL8NS_V1) : undefined;
            data.uuid ||= await getUUID();
            let { classname } = data;

            const ib = getAppIb({ appData: data, classname });

            const resApp = await factory.firstGen({
                ib,
                parentIbGib: factory.primitive({ ib: \`app \${classname}\` }),
                data,
                rel8ns,
                dna: true,
                linkedRel8ns: [Rel8n.ancestor, Rel8n.past],
                nCounter: true,
                tjp: { timestamp: true },
            }) as TransformResult<$PascalCaseNameApp_V1>;

            // replace the newIbGib which is just ib,gib,data,rel8ns with loaded
            // witness class (that has the witness function on it)
            const appDto = resApp.newIbGib;
            let appIbGib = new $PascalCaseNameApp_V1(undefined, undefined);
            await appIbGib.loadIbGibDto(appDto);
            resApp.newIbGib = appIbGib;
            if (logalot) { console.log(\`\${lc} appDto: \${pretty(appDto)} (I: $GUID)\`); }

            return resApp as TransformResult<$PascalCaseNameApp_V1>;
        } catch (error) {
            console.error(\`\${lc} \${extractErrorMsg(error)}\`);
            throw error;
        } finally {
            if (logalot) { console.log(\`\${lc} complete.\`); }
        }
    }

    async witnessToForm({ witness }: { witness: $PascalCaseNameApp_V1; }): Promise<DynamicForm> {
        const lc = \`\${this.lc}[\${this.witnessToForm.name}]\`;
        try {
            if (logalot) { console.log(\`\${lc} starting...\`); }
            let { data } = witness;
            if (!data) { throw new Error(\`(UNEXPECTED) witness.data falsy? (E: $GUID)\`); }
            // We do the AppFormBuilder specific functions first, because of
            if (logalot) { console.log(\`\${lc} data: \${pretty(data)} (I: $GUID)\`); }
            const idPool = await getIdPool({ n: 100 });
            // type inference in TS! eesh...
            const form = new $PascalCaseNameAppFormBuilder()
                .with({ idPool })
                .name({ of: data.name, required: false, })
                .description({ of: data.description ?? DEFAULT_DESCRIPTION_$UPPER_SNAKE_NAME_APP })
                .and<$PascalCaseNameAppFormBuilder>()
                .and<AppFormBuilder>()
                .icon({ iconsList: ['alert'], of: data.icon ?? 'alert', required: true })
                .and<DynamicFormBuilder>()
                .uuid({ of: data.uuid, required: true })
                .classname({ of: data.classname! })
                .and<WitnessFormBuilder>()
                .commonWitnessFields({ data })
                .outputForm({
                    formName: 'form',
                    label: '$name',
                });
            return Promise.resolve(form);
        } catch (error) {
            console.error(\`\${lc} \${extractErrorMsg(error)}\`);
            throw error;
        } finally {
            if (logalot) { console.log(\`\${lc} complete.\`); }
        }
    }

    async formToWitness({ form }: { form: DynamicForm; }): Promise<TransformResult<$PascalCaseNameApp_V1>> {
        // let app = new $PascalCaseNameApp_V1(null, null);
        let data: $PascalCaseNameAppData_V1 = clone(DEFAULT_$UPPER_SNAKE_NAME_APP_DATA_V1);
        this.patchDataFromItems({ data, items: form.items, pathDelimiter: DEFAULT_DATA_PATH_DELIMITER });
        let resApp = await this.newUp({ data });
        return resApp;
    }

}
`;
