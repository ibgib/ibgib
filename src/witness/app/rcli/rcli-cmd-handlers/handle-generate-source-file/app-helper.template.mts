/**
 * $name
 * $PascalCaseName
 * $camelCaseName
 * $snake_case_name
 * $UPPER_SNAKE_NAME
 */
export const APP_HELPER_TEMPLATE = `/*
 * @module $name helper/util/etc. functions
 *
 * this is where you will find helper functions like those that generate and
 * parse ibs for $name.
 */

// import * as pathUtils from 'path';
// import { statSync } from 'node:fs';
// import { readFile, } from 'node:fs/promises';
// import * as readline from 'node:readline/promises';
// import { stdin, stdout } from 'node:process'; // decide if use this or not

import {
    extractErrorMsg, delay, getSaferSubstring,
    getTimestampInTicks, getUUID, pretty,
} from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { UUID_REGEXP, CLASSNAME_REGEXP, } from '@ibgib/helper-gib/dist/constants.mjs';
import { Gib, Ib, } from '@ibgib/ts-gib/dist/types.mjs';
import { IbGib_V1, Rel8n, IbGibRel8ns_V1, } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { GIB} from '@ibgib/ts-gib/dist/V1/constants.mjs';
import { validateGib, validateIb, validateIbGibIntrinsically, getGib, getGibInfo, } from '@ibgib/ts-gib/dist/V1/validate-helper.mjs';
// import { IbGibSpaceAny } from '@ibgib/core-gib/dist/witness/space/space-base-v1.mjs';
// import { MetaspaceService } from '@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs';

import { WitnessFormBuilder } from '@ibgib/core-gib/dist/witness/witness-form-builder.mjs';
// import { IbGib$PascalCaseNameAppAny } from './$hyphenated-name-v1.mjs';
import {
    $PascalCaseNameAppData_V1, $PascalCaseNameAppRel8ns_V1, $PascalCaseNameAppIbGib_V1,
} from './$hyphenated-name-types.mjs';
import { $UPPER_SNAKE_NAME_ATOM, $UPPER_SNAKE_NAME_NAME_REGEXP, } from './$hyphenated-name-constants.mjs';

/**
 * for logging. import this constant from your project.
 */
const logalot = GLOBAL_LOG_A_LOT;

export function validateCommon$PascalCaseNameAppData({
    $camelCaseNameAppData,
}: {
    $camelCaseNameAppData?: $PascalCaseNameAppData_V1,
}): string[] {
    const lc = \`[\${validateCommon$PascalCaseNameAppData.name}]\`;
    try {
        if (logalot) { console.log(\`\${lc} starting...\`); }
        if (!$camelCaseNameAppData) { throw new Error(\`$camelCaseNameAppData required (E: $GUID)\`); }
        const errors: string[] = [];
        const {
            name, uuid, classname,
        } =
            $camelCaseNameAppData;

        if (name) {
            if (!name.match($UPPER_SNAKE_NAME_APP_NAME_REGEXP)) {
                errors.push(\`name must match regexp: \${$UPPER_SNAKE_NAME_APP_NAME_REGEXP}\`);
            }
        } else {
            errors.push(\`name required. (E: $GUID)\`);
        }

        if (uuid) {
            if (!uuid.match(UUID_REGEXP)) {
                errors.push(\`uuid must match regexp: \${UUID_REGEXP}\`);
            }
        } else {
            errors.push(\`uuid required. (E: $GUID)\`);
        }

        if (classname) {
            if (!classname.match(CLASSNAME_REGEXP)) {
                errors.push(\`classname must match regexp: \${CLASSNAME_REGEXP}\`);
            }
        }

        return errors;
    } catch (error) {
        console.error(\`\${lc} \${extractErrorMsg(error)}\`);
        throw error;
    } finally {
        if (logalot) { console.log(\`\${lc} complete.\`); }
    }
}

export async function validateCommon$PascalCaseNameAppIbGib({
    $camelCaseNameIbGib,
}: {
    $camelCaseNameIbGib: $PascalCaseNameAppIbGib_V1,
}): Promise<string[] | undefined> {
    const lc = \`[\${validateCommon$PascalCaseNameAppIbGib.name}]\`;
    try {
        if (logalot) { console.log(\`\${lc} starting... (I: $GUID)\`); }
        const intrinsicErrors: string[] = await validateIbGibIntrinsically({ ibGib: $camelCaseNameIbGib }) ?? [];

        if (!$camelCaseNameIbGib.data) { throw new Error(\`$camelCaseNameIbGib.data required (E: $GUID)\`); }
        const ibErrors: string[] = [];
        let { $camelCaseNameClassname, $camelCaseNameName, $camelCaseNameId } =
            parse$PascalCaseNameAppIb({ $camelCaseNameIb: $camelCaseNameIbGib.ib });
        if (!$camelCaseNameClassname) { ibErrors.push(\`$camelCaseNameClassname required (E: $GUID)\`); }
        if (!$camelCaseNameName) { ibErrors.push(\`$camelCaseNameName required (E: $GUID)\`); }
        if (!$camelCaseNameId) { ibErrors.push(\`$camelCaseNameId required (E: $GUID)\`); }

        const dataErrors = validateCommon$PascalCaseNameAppData({ $camelCaseNameAppData: $camelCaseNameIbGib.data });

        let result = [...(intrinsicErrors ?? []), ...(ibErrors ?? []), ...(dataErrors ?? [])];
        if (result.length > 0) {
            return result;
        } else {
            return undefined;
        }
    } catch (error) {
        console.error(\`\${lc} \${extractErrorMsg(error)}\`);
        throw error;
    } finally {
        if (logalot) { console.log(\`\${lc} complete.\`); }
    }
}

export function get$PascalCaseNameAppIb({
    $camelCaseNameAppData,
    classname,
}: {
    $camelCaseNameAppData: $PascalCaseNameAppData_V1,
    classname?: string,
}): Ib {
    const lc = \`[\${get$PascalCaseNameAppIb.name}]\`;
    try {
        const validationErrors = validateCommon$PascalCaseNameAppData({ $camelCaseNameAppData });
        if (validationErrors.length > 0) { throw new Error(\`invalid $camelCaseNameAppData: \${validationErrors} (E: $GUID)\`); }
        if (classname) {
            if ($camelCaseNameAppData.classname && $camelCaseNameAppData.classname !== classname) { throw new Error(\`classname does not match $camelCaseNameAppData.classname (E: $GUID)\`); }
        } else {
            classname = $camelCaseNameAppData.classname;
            if (!classname) { throw new Error(\`classname required (E: $GUID)\`); }
        }

        // ad hoc validation here. should centralize witness classname validation

        const { name, uuid } = $camelCaseNameAppData;
        return \`app $snake_case_name \${classname} \${name} \${uuid}\`;
    } catch (error) {
        console.error(\`\${lc} \${extractErrorMsg(error)}\`);
        throw error;
    }
}

/**
 * Current schema is 'app [$UPPER_SNAKE_NAME_ATOM] [classname] [$camelCaseNameName] [$camelCaseNameId]'
 *
 * NOTE this is space-delimited
 */
export function parse$PascalCaseNameAppIb({
    $camelCaseNameIb,
}: {
    $camelCaseNameIb: Ib,
}): {
    $camelCaseNameClassname: string,
    $camelCaseNameName: string,
    $camelCaseNameId: string,
} {
    const lc = \`[\${parse$PascalCaseNameAppIb.name}]\`;
    try {
        if (!$camelCaseNameIb) { throw new Error(\`$camelCaseNameIb required (E: $GUID)\`); }

        const pieces = $camelCaseNameIb.split(' ');

        return {
            $camelCaseNameClassname: pieces[2],
            $camelCaseNameName: pieces[3],
            $camelCaseNameId: pieces[4],
        };
    } catch (error) {
        console.error(\`\${lc} \${extractErrorMsg(error)}\`);
        throw error;
    }
}

export class $PascalCaseNameAppFormBuilder extends WitnessFormBuilder {
    protected lc: string = \`[\${$PascalCaseNameAppFormBuilder.name}]\`;

    constructor() {
        super();
        this.what = '$hyphenated-name';
    }

    // exampleSetting({
    //     of,
    //     required,
    // }: {
    //     of: string,
    //     required?: boolean,
    // }): $PascalCaseNameAppFormBuilder {
    //     this.addItem({
    //         // $camelCaseName.data.exampleSetting
    //         name: "exampleSetting",
    //         description: \`example description\`,
    //         label: "Example Label",
    //         regexp: EXAMPLE_REGEXP,
    //         regexpErrorMsg: EXAMPLE_REGEXP_DESC,
    //         dataType: 'textarea',
    //         value: of,
    //         required,
    //     });
    //     return this;
    // }

}
`;
