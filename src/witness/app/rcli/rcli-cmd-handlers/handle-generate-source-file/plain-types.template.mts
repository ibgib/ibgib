/**
 * $name
 * $PascalCaseName
 */
export const PLAIN_TYPES_TEMPLATE = `/**
 * @module $name types (and enums)
 */

import { IbGibAddr } from '@ibgib/ts-gib/dist/types.mjs';
import { IbGibData_V1, IbGibRel8ns_V1, IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
// and just to show where these things are
// import { CommentIbGib_V1 } from "@ibgib/core-gib/dist/common/comment/comment-types.mjs";
// import { MetaspaceService } from "@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs";

// /**
//  * example enum-like type+const that I use in ibgib. sometimes i put
//  * these in the types.mts and sometimes in the const.mts, depending
//  * on context.
//  */
// export type SomeEnum =
//     'ib' | 'gib';
// /**
//  * @see {@link SomeEnum}
//  */
// export const SomeEnum = {
//     ib: 'ib' as SomeEnum,
//     gib: 'gib' as SomeEnum,
// } satisfies { [key: string]: SomeEnum };
// /**
//  * values of {@link SomeEnum}
//  */
// export const SOME_ENUM_VALUES: SomeEnum[] = Object.values(SomeEnum);

/**
 * ibgib's intrinsic data goes here.
 *
 * @see {@link IbGib_V1.data}
 * @see {@link $PascalCaseNameIbGib_V1}
 */
export interface $PascalCaseNameData_V1 extends IbGibData_V1 {
    // ibgib data (settings/values/etc) goes here
    // /**
    //  * docs yo
    //  */
    // setting: string;
}

/**
 * rel8ns (named edges/links in DAG) go here.
 *
 * @see {@link IbGib_V1.rel8ns}
 * @see {@link $PascalCaseNameIbGib_V1}
 */
export interface $PascalCaseNameRel8ns_V1 extends IbGibRel8ns_V1 {
    // /**
    //  * required rel8n. for most, put in $name-constants.mts file
    //  *
    //  * @see {@link REQUIRED_REL8N_NAME}
    //  */
    // [REQUIRED_REL8N_NAME]: IbGibAddr[];
    // /**
    //  * optional rel8n. for most, put in $name-constants.mts file
    //  *
    //  * @see {@link OPTIONAL_REL8N_NAME}
    //  */
    // [OPTIONAL_REL8N_NAME]?: IbGibAddr[];
}

/**
 * this is the ibgib object itself.
 *
 * If this is a plain ibgib data only object, this acts as a dto. You may also
 * want to generate a witness ibgib, which is slightly different, for ibgibs
 * that will have behavior (i.e. methods).
 *
 * @see {@link $PascalCaseNameData_V1}
 * @see {@link $PascalCaseNameRel8ns_V1}
 */
export interface $PascalCaseNameIbGib_V1 extends IbGib_V1<$PascalCaseNameData_V1, $PascalCaseNameRel8ns_V1> {

}
`;
