
/**
 * $name
 * $PascalCaseName
 */
export const WITNESS_TYPES_TEMPLATE = `/**
 * @module $name types (and some enums/constants)
 */

import { IbGibAddr } from '@ibgib/ts-gib/dist/types.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { WitnessWithContextData_V1, WitnessWithContextRel8ns_V1 } from '@ibgib/core-gib/dist/witness/witness-with-context/witness-with-context-types.mjs';
import { WitnessCmdData, WitnessCmdIbGib, WitnessCmdRel8ns } from '@ibgib/core-gib/dist/witness/witness-cmd/witness-cmd-types.mjs';
import { WitnessResultData, WitnessResultIbGib, WitnessResultRel8ns } from '@ibgib/core-gib/dist/witness/witness-types.mjs';
// and just to show where these things are
// import { CommentIbGib_V1 } from "@ibgib/core-gib/dist/common/comment/comment-types.mjs";
// import { MetaspaceService } from "@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs";

// /**
//  * example enum-like type+const that I use in ibgib. sometimes i put
//  * these in the types.mts and sometimes in the const.mts, depending
//  * on context.
//  */
// export type SomeEnum =
//     'ib' | 'gib';
// /**
//  * @see {@link SomeEnum}
//  */
// export const SomeEnum = {
//     ib: 'ib' as SomeEnum,
//     gib: 'gib' as SomeEnum,
// } satisfies { [key: string]: SomeEnum };
// /**
//  * values of {@link SomeEnum}
//  */
// export const SOME_ENUM_VALUES: SomeEnum[] = Object.values(SomeEnum);

/**
 * ibgib's intrinsic data goes here.
 *
 * @see {@link IbGib_V1.data}
 */
export interface $PascalCaseNameData_V1 extends WitnessWithContextData_V1 {
    // ibgib data (settings/values/etc) goes here
    // /**
    //  * docs yo
    //  */
    // setting: string;
}

/**
 * rel8ns (named edges/links in DAG) go here.
 *
 * @see {@link IbGib_V1.rel8ns}
 */
export interface $PascalCaseNameRel8ns_V1 extends WitnessWithContextRel8ns_V1 {
    // /**
    //  * required rel8n. for most, put in $name-constants.mts file
    //  *
    //  * @see {@link REQUIRED_REL8N_NAME}
    //  */
    // [REQUIRED_REL8N_NAME]: IbGibAddr[];
    // /**
    //  * optional rel8n. for most, put in $name-constants.mts file
    //  *
    //  * @see {@link OPTIONAL_REL8N_NAME}
    //  */
    // [OPTIONAL_REL8N_NAME]?: IbGibAddr[];
}

/**
 * this is the ibgib object itself.
 *
 * If this is a plain ibgib data only object, this acts as a dto. You may also
 * want to generate a witness ibgib, which is slightly different, for ibgibs
 * that will have behavior (i.e. methods).
 *
 * @see {@link $PascalCaseNameData_V1}
 * @see {@link $PascalCaseNameRel8ns_V1}
 */
export interface $PascalCaseNameIbGib_V1 extends IbGib_V1<$PascalCaseNameData_V1, $PascalCaseNameRel8ns_V1> {

}

/**
 * Cmds for interacting with ibgib witnesses.
 *
 * Not all of these will be implemented for every witness.
 *
 * ## todo
 *
 * change these commands to better structure, e.g., verb/do/mod, can/get/addrs
 * */
export type $PascalCaseNameCmd =
    'ib' | 'gib' | 'ibgib';
/** Cmds for interacting with ibgib spaces. */
export const $PascalCaseNameCmd = {
    /**
     * it's more like a grunt that is intepreted by context.
     */
    ib: 'ib' as $PascalCaseNameCmd,
    /**
     * it's more like a grunt that is intepreted by context.
     */
    gib: 'gib' as $PascalCaseNameCmd,
    /**
     * third placeholder command.
     *
     * I imagine this will be like "what's up", but who knows.
     */
    ibgib: 'ibgib' as $PascalCaseNameCmd,
}

/**
 * Flags to affect the command's interpretation.
 */
export type $PascalCaseNameCmdModifier =
    'ib' | 'gib' | 'ibgib';
/**
 * Flags to affect the command's interpretation.
 */
export const $PascalCaseNameCmdModifier = {
    /**
     * hmm...
     */
    ib: 'ib' as $PascalCaseNameCmdModifier,
    /**
     * hmm...
     */
    gib: 'gib' as $PascalCaseNameCmdModifier,
    /**
     * hmm...
     */
    ibgib: 'ibgib' as $PascalCaseNameCmdModifier,
}

/** Information for interacting with spaces. */
export interface $PascalCaseNameCmdData
    extends WitnessCmdData<$PascalCaseNameCmd, $PascalCaseNameCmdModifier> {
}

export interface $PascalCaseNameCmdRel8ns extends WitnessCmdRel8ns {
}

/**
 * Shape of options ibgib if used for a command-based witness.
 */
export interface $PascalCaseNameCmdIbGib
    extends WitnessCmdIbGib<
        IbGib_V1,
        $PascalCaseNameCmd, $PascalCaseNameCmdModifier,
        $PascalCaseNameCmdData, $PascalCaseNameCmdRel8ns
    > {
}

/**
 * Optional shape of result data to witness interactions.
 *
 * This is in addition of course to {@link $PascalCaseNameResultData}.
 *
 * so if you're sending a cmd to this witness, this should probably be the shape
 * of the result.
 *
 * ## notes
 *
 * * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface $PascalCaseNameResultData extends WitnessResultData {
}

/**
 * Marker interface rel8ns atm...
 *
 * so if you're sending a cmd to this witness, this should probably be the shape
 * of the result.
 *
 * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface $PascalCaseNameResultRel8ns extends WitnessResultRel8ns { }

/**
 * Shape of result ibgib if used for a witness.
 *
 * so if you're sending a cmd to this witness, this should probably be the shape
 * of the result.
 *
 * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface $PascalCaseNameResultIbGib
    extends WitnessResultIbGib<IbGib_V1, $PascalCaseNameResultData, $PascalCaseNameResultRel8ns> {
}

/**
 * shape of underscore-delimited addl metadata string that may be present in the
 * ib (i.e. available when parsing the ib)
 *
 * This is not hard and fast and can (and should?) vary greatly per use case.
 */
export interface $PascalCaseNameAddlMetadata {
    /**
     * should be $hyphenated-name
     */
    atom?: '$hyphenated-name'
    /**
     * classname of $name **with any underscores removed**.
     */
    classnameIsh?: string;
    /**
     * name of $name witness (data.name) **with any underscores removed**.
     */
    nameIsh?: string;
    /**
     * id of witness (data.uuid) **with any underscores removed**.
     *
     * may be a substring per use case...?
     */
    idIsh?: string;
}

/**
 * Default data values for a $PascalCaseName.
 *
 * If you change this, please bump the version
 *
 * (but of course won't be the end of the world when this doesn't happen).
 */
export const DEFAULT_$UPPER_SNAKE_NAME_DATA_V1: $PascalCaseNameData_V1 = {
    version: '1',
    uuid: DEFAULT_UUID_$UPPER_SNAKE_NAME,
    name: DEFAULT_NAME_$UPPER_SNAKE_NAME,
    description: DEFAULT_DESCRIPTION_$UPPER_SNAKE_NAME,
    classname: \`\$PascalCaseName_V1\`,

    icon: 'code-slash',

    /**
     * if true, then the witness will attempt to persist ALL calls to
     * \`witness.witness(...)\`.
     */
    persistOptsAndResultIbGibs: false,
    /**
     * allow ibgibs like 42^gib ({ib: 42, gib: 'gib'} with \`data\` and \`rel8ns\` undefined)
     */
    allowPrimitiveArgs: true,
    /**
     * witnesses should be guaranteed not to throw uncaught exceptions.
     */
    catchAllErrors: true,
    /**
     * if true, would enable logging of all calls to \`witness.witness(...)\`
     */
    trace: false,

    // put in your custom defaults here
}
export const DEFAULT_$UPPER_SNAKE_NAME_REL8NS_V1: $PascalCaseNameRel8ns_V1 | undefined = undefined;
`;
