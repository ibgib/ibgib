import { extractErrorMsg } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { extractArgValue, } from '@ibgib/helper-gib/dist/rcli/rcli-helper.mjs';
import { RCLIArgInfo } from '@ibgib/helper-gib/dist/rcli/rcli-types.mjs';
import { IbGibAddr, TransformOpts, TransformResult } from '@ibgib/ts-gib/dist/types.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { validateIbGibAddr, validateIbGibIntrinsically } from '@ibgib/ts-gib/dist/V1/validate-helper.mjs';
import { Factory_V1, } from '@ibgib/ts-gib/dist/V1/factory.mjs';
import { isPrimitive } from '@ibgib/ts-gib/dist/V1/transforms/transform-helper.mjs';
import { getIbAndGib, getIbGibAddr } from '@ibgib/ts-gib/dist/helper.mjs';
import { IBGIB_DELIMITER, } from '@ibgib/ts-gib/dist/V1/constants.mjs';
import { MetaspaceService } from '@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs';

import { PARAM_INFO_SRC_ADDR, RCLICommand, } from '../rcli-constants.mjs';
import { RCLICommandHandlerAddCommentFunctionArgs, RCLICommandHandlerArg } from '../rcli-types.mjs';
import { GLOBAL_LOG_A_LOT } from '../../../../ibgib-constants.mjs';

/**
 * used in verbose logging (across all ibgib libs atow)
 */
const logalot = GLOBAL_LOG_A_LOT;

export abstract class TransformHandlerBase<TOpts extends TransformOpts> {
    protected lc = `[${TransformHandlerBase.name}]`;

    protected abstract getCmd(): RCLICommand;
    protected abstract getDefaultOpts(): Promise<TOpts>;

    public async handleCommand(arg: RCLICommandHandlerArg): Promise<IbGib_V1 | undefined> {
        const lc = `${this.lc}[${this.handleCommand.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: bb14b3a4fc92759c42259a898fa3cf23)`); }

            const { cmdEscapeString, cmdInfo, contextIbGib, ibGib, metaspace, fnAddComment } = arg;
            let { argInfos, rawText, cmd, } = cmdInfo;
            argInfos ??= [];

            if (!metaspace) { throw new Error(`metaspace required (E: d927587c93a450edfa7dfb8e143a6723)`); }
            if (cmd !== this.getCmd()) { throw new Error(`(UNEXPECTED) cmd !== ${this.getCmd()}? (E: cb25b3a3658988b3c32d875705520923)`); }

            let opts = await this.getDefaultOpts();
            opts = await this.configureOpts_SrcAndSrcAddr({ opts, argInfos, metaspace });
            opts = await this.configureOpts_Other({ opts, argInfos, });

            let resTransform = await this.executeTransform(opts);
            await metaspace.persistTransformResult({
                resTransform, force: undefined,
            });

            await this.giveRCLIFeedback({
                contextIbGib, fnAddComment, resTransform,
            });

            return resTransform.newIbGib;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected abstract executeTransform(opts: TOpts): Promise<TransformResult<IbGib_V1>>;

    /**
     * by default, this creates a comment with the new addr and posts it to the chat
     * contextIbGib via a comment with `fnAddComment`.
     */
    protected async giveRCLIFeedback({
        contextIbGib,
        fnAddComment,
        resTransform,
    }: {
        contextIbGib: IbGib_V1,
        fnAddComment: (arg: RCLICommandHandlerAddCommentFunctionArgs) => Promise<void>;
        resTransform: TransformResult<IbGib_V1>,
    }): Promise<void> {
        const lc = `${this.lc}[${this.giveRCLIFeedback.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 6961b843143173c3e843004bdc689123)`); }

            // give feedback to the user
            const { newIbGib } = resTransform;
            const newIbGibAddr = getIbGibAddr({ ibGib: newIbGib });
            await fnAddComment({
                contextIbGib,
                rel8nName: 'comment',
                text: `new addr: ${newIbGibAddr}`,
            });
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }
    protected async configureOpts_SrcAndSrcAddr({
        opts,
        argInfos,
        metaspace,
    }: {
        opts: TOpts,
        argInfos: RCLIArgInfo<any>[],
        metaspace: MetaspaceService,
    }): Promise<TOpts> {
        const lc = `${this.lc}[${this.configureOpts_SrcAndSrcAddr.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 254d3f692a027e2db17613c728fc8f23)`); }

            let srcAddr = await this.extractSrcAddr({ argInfos });
            if (srcAddr) { opts.srcAddr = srcAddr; }
            srcAddr ??= opts.srcAddr; // put back in variable in case used later
            if (!srcAddr) { throw new Error(`could not get srcAddr? (E: ed2422f8db1b8ebdb22d295fb3bbb523)`); }

            const { ib: srcIb, gib: srcGib } = getIbAndGib({ ibGibAddr: opts.srcAddr });
            if (isPrimitive({ gib: srcGib })) {
                opts.src = Factory_V1.primitive({ ib: srcIb });
            } else {
                let resGet = await metaspace.get({ addr: srcAddr });
                if (resGet.success && resGet.ibGibs?.length === 1) {
                    const src = resGet.ibGibs![0];
                    const errors = await validateIbGibIntrinsically({ ibGib: src });
                    if ((errors ?? []).length > 0) {
                        throw new Error(`validation errors on src ibgib. errors:\n${errors!.join('\n')} (E: 6471e3ccd0abf3ae43f3c2cda7837f23)`);
                    }
                    opts.src = src;
                } else {
                    throw new Error(`was unable to get src from space. srcAddr: ${srcAddr}  (E: 6b814fb406211f16b7afe0b9c55bcd23)`);
                }
            }

            return opts;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * srcAddr already done before this.
     *
     * @param arg
     */
    protected abstract configureOpts_Other(arg: {
        opts: TOpts;
        argInfos: RCLIArgInfo<any>[];
    }): Promise<TOpts>;

    protected async extractSrcAddr({ argInfos }: { argInfos: RCLIArgInfo<any>[] }): Promise<IbGibAddr | undefined> {
        const lc = `${this.lc}[${this.extractSrcAddr.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 4bee5ace15da6a0ca2e4f22c14951123)`); }
            const srcAddr = extractArgValue<string>({
                argInfos,
                paramInfo: PARAM_INFO_SRC_ADDR,
                throwIfNotFound: false,
            }) as string;

            if (srcAddr) {
                const errors = validateIbGibAddr({
                    addr: srcAddr,
                    delimiter: IBGIB_DELIMITER,
                    version: undefined,
                }) ?? [];
                if (errors.length > 0) { throw new Error(`invalid srcAddr given (${srcAddr}). must be in the form of ib^gib. errors: ${errors.join('|')} (E: a9328ed79ed163d231153aa5f77f9223)`); }
                return srcAddr;
            } else {
                return undefined;
            }
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }
}
