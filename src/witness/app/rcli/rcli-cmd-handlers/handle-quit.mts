import { cwd } from 'node:process';

import { clone, extractErrorMsg, } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { fork } from '@ibgib/ts-gib/dist/V1/transforms/fork.mjs';

import { DEFAULT_FORK_OPTIONS, RCLICommand, } from '../rcli-constants.mjs';
import { RCLICommandHandler, RCLICommandHandlerArg } from '../rcli-types.mjs';
import { GLOBAL_LOG_A_LOT } from '../../../../ibgib-constants.mjs';
import { RCLIArgInfo } from '@ibgib/helper-gib/dist/rcli/rcli-types.mjs';
import { MetaspaceService } from '@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs';
import { TransformHandlerBase } from './transform-handler-base.mjs';
import { getIbAndGib } from '@ibgib/ts-gib/dist/helper.mjs';
import { GIB } from '@ibgib/ts-gib/dist/V1/constants.mjs';

/**
 * used in verbose logging (across all ibgib libs atow)
 */
const logalot = GLOBAL_LOG_A_LOT;


export function handleQuit(arg: RCLICommandHandlerArg): Promise<IbGib_V1 | undefined> {
    const lc = `[${handleQuit.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 28f21c4183fd58cb0a97f7fea7519823)`); }
        let quitGib = { ib: 'quit', gib: GIB } as IbGib_V1;
        return Promise.resolve(quitGib);
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export const rcliCmdHandlerQuit: RCLICommandHandler =
    (arg) => handleQuit(arg);
