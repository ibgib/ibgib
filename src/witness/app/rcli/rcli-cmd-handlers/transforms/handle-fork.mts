import { clone, extractErrorMsg, pretty, } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { fork } from '@ibgib/ts-gib/dist/V1/transforms/fork.mjs';

import { DEFAULT_FORK_OPTIONS, PARAM_INFO_CLONE_DATA, PARAM_INFO_CLONE_REL8NS, PARAM_INFO_IB, PARAM_INFO_TJP, RCLICommand, } from '../../rcli-constants.mjs';
import { RCLICommandHandler, RCLICommandHandlerArg } from '../../rcli-types.mjs';
import { RCLIArgInfo } from '@ibgib/helper-gib/dist/rcli/rcli-types.mjs';
import { MetaspaceService } from '@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs';
import { TransformHandlerBase } from '../transform-handler-base.mjs';
import { IbGib, TransformOpts_Fork, TransformResult } from '@ibgib/ts-gib/dist/types.mjs';
import { getIbAndGib } from '@ibgib/ts-gib/dist/helper.mjs';
import { extractArgValue } from '@ibgib/helper-gib/dist/rcli/rcli-helper.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../../../ibgib-constants.mjs';
/**
 * used in verbose logging (across all ibgib libs atow)
 */
const logalot = GLOBAL_LOG_A_LOT;

export class TransformHandler_Fork extends TransformHandlerBase<TransformOpts_Fork> {
    protected getCmd(): RCLICommand { return RCLICommand.fork; }
    protected getDefaultOpts(): Promise<TransformOpts_Fork<IbGib>> {
        return Promise.resolve(clone(DEFAULT_FORK_OPTIONS) as TransformOpts_Fork);
    }

    /**
     *
     * ## transform atow
     *
     * noTimestamp?: boolean;
     * dna?: boolean;
     * linkedRel8ns?: string[];
     * nCounter?: boolean;
     *
     * ## fork specific atow
     *
     * uuid?: boolean;
     * tjp?: TemporalJunctionPointOptions;
     * cloneRel8ns?: boolean;
     * cloneData?: boolean;
     *
     * @param param0
     * @returns
     */
    protected async configureOpts_Other({
        opts,
        argInfos,
        metaspace,
    }: {
        opts: TransformOpts_Fork,
        argInfos: RCLIArgInfo<any>[],
        metaspace: MetaspaceService,
    }): Promise<TransformOpts_Fork> {
        const lc = `${this.lc}[${this.configureOpts_Other.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 0bb733f0953d7a59555e562243339b23)`); }
            if (!opts.src) { throw new Error(`(UNEXPECTED) opts.src falsy? (E: 66ef5873eadf5ccce42af75312e88123)`); }
            if (!opts.srcAddr) { throw new Error(`(UNEXPECTED) opts.srcAddr falsy? (E: 117d46ea64768165f2f47e1b0c3e1923)`); }

            // destIb
            let { ib: srcIb, gib: srcGib } = getIbAndGib({ ibGibAddr: opts.srcAddr });
            const destIb = extractArgValue<string>({ paramInfo: PARAM_INFO_IB, argInfos, throwIfNotFound: false });
            if (destIb) {
                opts.destIb = destIb as string;
            } else {
                opts.destIb = srcIb;
            }

            const tjpOption = extractArgValue<string>({ paramInfo: PARAM_INFO_TJP, argInfos, throwIfNotFound: false, }) as string | undefined;
            if (tjpOption) {
                // 'uuid'|'timestamp'|'none'
                if (tjpOption === 'uuid') {
                    opts.tjp = { uuid: true, timestamp: false };
                    opts.noTimestamp = true;
                } else if (tjpOption === 'timestamp') {
                    opts.tjp = { uuid: false, timestamp: true };
                } else if (tjpOption === 'none') {
                    delete opts.tjp;
                    opts.noTimestamp = true;
                    delete opts.nCounter;
                } else {
                    throw new Error(`invalid tjp value (${tjpOption}). if set, should be either 'uuid', 'timestamp' or 'none' (E: 4d2a2f648d68c84c7a2f4bd3da4b4c23)`);
                }
            }

            const cloneData = extractArgValue<boolean>({
                paramInfo: PARAM_INFO_CLONE_DATA,
                argInfos, throwIfNotFound: false,
            }) as boolean | undefined;
            if (typeof cloneData === 'boolean') { opts.cloneData = cloneData; }
            // if (cloneData === undefined) { throw new Error(`cloneData is undefined ...just testing remove this (E: f90d14cda50e99c1e5791a4c2b9db423)`); }

            const cloneRel8ns = extractArgValue<boolean>({
                paramInfo: PARAM_INFO_CLONE_REL8NS,
                argInfos, throwIfNotFound: false,
            }) as boolean | undefined;
            if (typeof cloneRel8ns === 'boolean') { opts.cloneRel8ns = cloneRel8ns; }
            // if (cloneRel8ns === undefined) { throw new Error(`cloneRel8ns is undefined ...just testing remove this (E: f90d14cda50e99c1e5791a4c2b9db423)`); }

            if (logalot) { console.log(`${lc} opts: ${pretty(opts)} (I: 8167cfb83431a9f11754434eba054123)`); }

            return opts;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async executeTransform(
        opts: TransformOpts_Fork<IbGib>
    ): Promise<TransformResult<IbGib_V1>> {
        const lc = `${this.lc}[${this.executeTransform.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 9cf5d79522577a443e4995e1b4f02b23)`); }
            const resTransform = await fork(opts);
            return resTransform;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }
}

export function handleFork(arg: RCLICommandHandlerArg): Promise<IbGib_V1 | undefined> {
    const lc = `[${handleFork.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 28f21c4183fd58cb0a97f7fea7519823)`); }
        const handler = new TransformHandler_Fork();
        return handler.handleCommand(arg);
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export const rcliCmdHandlerFork: RCLICommandHandler =
    (arg) => handleFork(arg);
