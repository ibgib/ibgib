import { clone, pretty, extractErrorMsg, } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { RCLIArgInfo } from '@ibgib/helper-gib/dist/rcli/rcli-types.mjs';
import { extractArgValue } from '@ibgib/helper-gib/dist/rcli/rcli-helper.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { rel8 } from '@ibgib/ts-gib/dist/V1/transforms/rel8.mjs';
import { IbGib, TransformOpts_Rel8, TransformResult } from '@ibgib/ts-gib/dist/types.mjs';
import { getIbAndGib } from '@ibgib/ts-gib/dist/helper.mjs';
import { MetaspaceService } from '@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs';

import {
    DEFAULT_REL8_OPTIONS,
    PARAM_INFO_REL8NS_TO_ADD, PARAM_INFO_REL8NS_TO_REMOVE,
    RCLICommand,
} from '../../rcli-constants.mjs';
import { RCLICommandHandler, RCLICommandHandlerArg } from '../../rcli-types.mjs';
import { GLOBAL_LOG_A_LOT } from '../../../../../ibgib-constants.mjs';
import { TransformHandlerBase } from '../transform-handler-base.mjs';

/**
 * used in verbose logging (across all ibgib libs atow)
 */
const logalot = GLOBAL_LOG_A_LOT;

export class TransformHandler_Rel8 extends TransformHandlerBase<TransformOpts_Rel8> {
    protected getCmd(): RCLICommand { return RCLICommand.rel8; }
    protected getDefaultOpts(): Promise<TransformOpts_Rel8<IbGib>> {
        return Promise.resolve(clone(DEFAULT_REL8_OPTIONS) as TransformOpts_Rel8);
    }

    /**
     *
     * ## transform atow
     *
     * noTimestamp?: boolean;
     * dna?: boolean;
     * linkedRel8ns?: string[];
     * nCounter?: boolean;
     *
     * ## rel8 specific atow
     *
     * rel8nsToAddByAddr?: IbGibRel8ns;
     * rel8nsToRemoveByAddr?: IbGibRel8ns;
     *
     * @returns transform opts
     */
    protected async configureOpts_Other({
        opts,
        argInfos,
        metaspace,
    }: {
        opts: TransformOpts_Rel8,
        argInfos: RCLIArgInfo<any>[],
        metaspace: MetaspaceService,
    }): Promise<TransformOpts_Rel8> {
        const lc = `${this.lc}[${this.configureOpts_Other.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 9f5944707ccc461dad82d13ba6c0cf75)`); }
            if (!opts.src) { throw new Error(`(UNEXPECTED) opts.src falsy? (E: a2e7299f2265447b88f99a205bd99af6)`); }
            if (!opts.srcAddr) { throw new Error(`(UNEXPECTED) opts.srcAddr falsy? (E: 4a0a62f3154d438f9c9c35323c4013a3)`); }

            const rel8nsToAddStr = extractArgValue<string>({ paramInfo: PARAM_INFO_REL8NS_TO_ADD, argInfos, throwIfNotFound: false });
            if (rel8nsToAddStr) {
                const jsonString = rel8nsToAddStr as string;
                opts.rel8nsToAddByAddr = JSON.parse(jsonString) as any;
            }

            const rel8nsToRemoveStr = extractArgValue<string>({ paramInfo: PARAM_INFO_REL8NS_TO_REMOVE, argInfos, throwIfNotFound: false });
            if (rel8nsToRemoveStr) {
                const jsonString = rel8nsToRemoveStr as string;
                opts.rel8nsToRemoveByAddr = JSON.parse(jsonString) as any;
            }

            if (logalot) { console.log(`${lc} opts: ${pretty(opts)} (I: 3590c98b5d384b15ac66ca2e633dc28a)`); }

            return opts;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async executeTransform(
        opts: TransformOpts_Rel8<IbGib>
    ): Promise<TransformResult<IbGib_V1>> {
        const lc = `${this.lc}[${this.executeTransform.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 40f448fb05ad44aea8a67f018cfe59e9)`); }
            const resTransform = await rel8(opts);
            return resTransform;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }
}

export function handleRel8(arg: RCLICommandHandlerArg): Promise<IbGib_V1 | undefined> {
    const lc = `[${handleRel8.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: d76a9ecf096f47c185a2a10c358ea0d7)`); }
        const handler = new TransformHandler_Rel8();
        return handler.handleCommand(arg);
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export const rcliCmdHandlerRel8: RCLICommandHandler =
    (arg) => handleRel8(arg);
