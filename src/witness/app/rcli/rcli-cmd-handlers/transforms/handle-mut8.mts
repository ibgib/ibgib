import { clone, extractErrorMsg, pretty, } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { mut8 } from '@ibgib/ts-gib/dist/V1/transforms/mut8.mjs';

import { DEFAULT_MUT8_OPTIONS, PARAM_INFO_CLONE_DATA, PARAM_INFO_CLONE_REL8NS, PARAM_INFO_DATA_TO_ADD_OR_PATCH, PARAM_INFO_DATA_TO_REMOVE, PARAM_INFO_DATA_TO_RENAME, PARAM_INFO_IB, PARAM_INFO_TJP, RCLICommand, } from '../../rcli-constants.mjs';
import { RCLICommandHandler, RCLICommandHandlerArg } from '../../rcli-types.mjs';
import { GLOBAL_LOG_A_LOT } from '../../../../../ibgib-constants.mjs';
import { RCLIArgInfo } from '@ibgib/helper-gib/dist/rcli/rcli-types.mjs';
import { MetaspaceService } from '@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs';
import { TransformHandlerBase } from '../transform-handler-base.mjs';
import { IbGib, TransformOpts_Mut8, TransformResult } from '@ibgib/ts-gib/dist/types.mjs';
import { getIbAndGib } from '@ibgib/ts-gib/dist/helper.mjs';
import { extractArgValue } from '@ibgib/helper-gib/dist/rcli/rcli-helper.mjs';

/**
 * used in verbose logging (across all ibgib libs atow)
 */
const logalot = GLOBAL_LOG_A_LOT;

export class TransformHandler_Mut8 extends TransformHandlerBase<TransformOpts_Mut8> {
    protected getCmd(): RCLICommand { return RCLICommand.mut8; }
    protected getDefaultOpts(): Promise<TransformOpts_Mut8<IbGib>> {
        return Promise.resolve(clone(DEFAULT_MUT8_OPTIONS) as TransformOpts_Mut8);
    }

    /**
     *
     * ## transform atow
     *
     * noTimestamp?: boolean;
     * dna?: boolean;
     * linkedRel8ns?: string[];
     * nCounter?: boolean;
     *
     * ## mut8 specific atow
     *
     * dataToRename?: { [key: string]: any;
     * dataToRemove?: { [key: string]: any; };
     * dataToAddOrPatch?: TNewData;
     * mut8Ib?: string;
     *
     * @returns transform opts
     */
    protected async configureOpts_Other({
        opts,
        argInfos,
        metaspace,
    }: {
        opts: TransformOpts_Mut8,
        argInfos: RCLIArgInfo<any>[],
        metaspace: MetaspaceService,
    }): Promise<TransformOpts_Mut8> {
        const lc = `${this.lc}[${this.configureOpts_Other.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 0860ab060e1746949d1031dc27d99edb)`); }
            if (!opts.src) { throw new Error(`(UNEXPECTED) opts.src falsy? (E: 90dfe9cb033f4b85b2ae15170e27b015)`); }
            if (!opts.srcAddr) { throw new Error(`(UNEXPECTED) opts.srcAddr falsy? (E: 3be5550c36b242b0a0165fcc67bbe0cf)`); }

            let { ib: srcIb, } = getIbAndGib({ ibGibAddr: opts.srcAddr });

            const dataToRename = extractArgValue<string>({ paramInfo: PARAM_INFO_DATA_TO_RENAME, argInfos, throwIfNotFound: false });
            if (dataToRename) {
                const jsonString = dataToRename as string;
                opts.dataToRename = JSON.parse(jsonString) as any;
            }

            const dataToRemove = extractArgValue<string>({ paramInfo: PARAM_INFO_DATA_TO_REMOVE, argInfos, throwIfNotFound: false });
            if (dataToRemove) {
                const jsonString = dataToRemove as string;
                opts.dataToRemove = JSON.parse(jsonString) as any;
            }

            const dataToAddOrPatch = extractArgValue<string>({ paramInfo: PARAM_INFO_DATA_TO_ADD_OR_PATCH, argInfos, throwIfNotFound: false });
            if (dataToAddOrPatch) {
                const jsonString = dataToAddOrPatch as string;
                opts.dataToAddOrPatch = JSON.parse(jsonString) as any;
            }

            const mut8Ib = extractArgValue<string>({ paramInfo: PARAM_INFO_IB, argInfos, throwIfNotFound: false });
            if (mut8Ib) {
                if (mut8Ib === srcIb) { console.warn(`${lc} mut8Ib === srcIb...? This may not be a big deal but why is it set if it's the same? (W: 11a7eb88495a4604b8f19b307331118d)`) }
                opts.mut8Ib = mut8Ib as string;
            }

            if (logalot) { console.log(`${lc} opts: ${pretty(opts)} (I: 2537b6e8670142e2b8e08236e7286aa7)`); }

            return opts;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async executeTransform(
        opts: TransformOpts_Mut8<IbGib>
    ): Promise<TransformResult<IbGib_V1>> {
        const lc = `${this.lc}[${this.executeTransform.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 1000bf39aad04006b80937c3f9e07db9)`); }
            const resTransform = await mut8(opts);
            return resTransform;
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }
}

export function handleMut8(arg: RCLICommandHandlerArg): Promise<IbGib_V1 | undefined> {
    const lc = `[${handleMut8.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: b0b3c19fbf2d40fabc3efc5b97e1476a)`); }
        const handler = new TransformHandler_Mut8();
        return handler.handleCommand(arg);
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export const rcliCmdHandlerMut8: RCLICommandHandler =
    (arg) => handleMut8(arg);
