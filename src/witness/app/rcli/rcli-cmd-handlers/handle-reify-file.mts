import { execPath, cwd, chdir, } from 'node:process';
import { statSync } from 'node:fs';
import { mkdir, readFile, readdir, } from 'node:fs/promises';
import * as readline from 'node:readline/promises';
import { stdin, stdout } from 'node:process'; // decide if use this or not
import * as pathUtils from 'path';

import { extractArgValue } from '@ibgib/helper-gib/dist/rcli/rcli-helper.mjs';
import { extractErrorMsg, pretty, } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { ROOT } from '@ibgib/ts-gib/dist/V1/constants.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { Factory_V1 } from '@ibgib/ts-gib/dist/V1/factory.mjs';

import { RCLICommandHandler, RCLICommandHandlerArg, FILE_ENCODINGS } from '../rcli-types.mjs';
import { GLOBAL_LOG_A_LOT } from '../../../../ibgib-constants.mjs';
import { extractArg_dataPath } from '../rcli-helper.mjs';
import { DEFAULT_FILE_ENCODING, PARAM_INFO_BINARY, PARAM_INFO_FILE_ENCODING } from '../rcli-constants.mjs';

/**
 * used in verbose logging (across all ibgib libs atow)
 */
const logalot = GLOBAL_LOG_A_LOT;

/**
 * this is meant to be used to take an ibgib and reify
 * @param arg
 * @returns
 */
export async function handleReifyFile(arg: RCLICommandHandlerArg): Promise<IbGib_V1 | undefined> {
    const lc = `[${handleReifyFile.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: b44118f25b2a40b4b260915f3ac30b76)`); }
        const { cmdEscapeString, cmdInfo, contextIbGib, ibGib, metaspace } = arg;

        let { argInfos } = cmdInfo;

        if (!argInfos) { throw new Error(`(UNEXPECTED) argInfos falsy? (E: 02f1084061a36b9c28d0c3212c904a23)`); }

        const dataPath = (await extractArg_dataPath({
            argInfos,
            expectType: 'file',
            throwIfNotExpected: true,
            promptIfNotExpected: true,
            // defaultPath: undefined,
        }))!;
        // should throw i believe if not a valid path
        if (!dataPath) { throw new Error(`could not find valid data path. --data-path arg should be a valid path to a file. (E: f8a36b0fc3934438820995f6f3c9a637)`); }

        const isBinary = extractArgValue<boolean>({
            paramInfo: PARAM_INFO_BINARY,
            argInfos, throwIfNotFound: false,
        }) as boolean | undefined;
        if (isBinary) {
            throw new Error(`not impl atow (E: 17aa424ab13dc5ea087b2db46617c723)`);
            // binary file, so treat it like we do with pics
            let resRead: Buffer = await readFile(dataPath);
        } else {
            // get the encoding
            let encoding: BufferEncoding | undefined = undefined;
            const extractedEncoding = extractArgValue<string>({
                paramInfo: PARAM_INFO_FILE_ENCODING, argInfos,
                throwIfNotFound: false
            });
            if (extractedEncoding) {
                encoding = extractedEncoding as BufferEncoding;
                if (!FILE_ENCODINGS.includes(encoding)) { throw new Error(`invalid encoding (${encoding}). valid encodings: ${FILE_ENCODINGS.join(', ')} (E: 6826d3c2f0ac806ab8dd35b8e1d70823)`); }
            } else {
                console.log(`${lc} no file encoding specified. defaulting to ${DEFAULT_FILE_ENCODING} $(I: d527eb354caa4d4b8b9387db09a6fc94)`);
                encoding = DEFAULT_FILE_ENCODING;
            }

            // try to read it as text
            let resRead = await readFile(dataPath, { encoding, flag: 'r' }); // readonly

            // try to parse as JSON
            let parsedJson: any | undefined = undefined;
            try {
                parsedJson = JSON.parse(resRead);
            } catch (error) {
                if (logalot) { console.log(`${lc} could not parse as JSON (I: 45edadf08582ddff9a647a522cf74523)`); }
                parsedJson = undefined;
            }

            /**
             * will be set to the newly created ibgib.data property.
             */
            let initialData: any = parsedJson ?
                parsedJson :
                { "x": resRead };


            let resFirstGen = await Factory_V1.firstGen({
                ib: 'hmm',
                parentIbGib: Factory_V1.primitive({ ib: 'lskdjflkjdsj' }),
                data: initialData,
                dna: true,
                linkedRel8ns: [],
                nCounter: true,
                noTimestamp: false,
                rel8ns: undefined,
                tjp: { timestamp: true, uuid: true },
            });


        }

        return ROOT;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export const rcliCmdHandlerReifyFile: RCLICommandHandler =
    (arg) => handleReifyFile(arg);
