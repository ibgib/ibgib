import { execPath, cwd, chdir, } from 'node:process';
import { statSync } from 'node:fs';
import { mkdir, readdir, } from 'node:fs/promises';
import * as readline from 'node:readline/promises';
import { stdin, stdout } from 'node:process'; // decide if use this or not
import * as pathUtils from 'path';

import { extractErrorMsg, pretty, } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';

import { PARAM_INFOS, } from '../rcli-constants.mjs';
import { RCLICommandHandler, RCLICommandHandlerArg } from '../rcli-types.mjs';
import { GLOBAL_LOG_A_LOT } from '../../../../ibgib-constants.mjs';
import { ROOT } from '@ibgib/ts-gib/dist/V1/constants.mjs';
import { getFromSpace } from '@ibgib/core-gib/dist/witness/space/space-helper.mjs';
import { getIbGibAddr } from '@ibgib/ts-gib/dist/helper.mjs';

/**
 * used in verbose logging (across all ibgib libs atow)
 */
const logalot = GLOBAL_LOG_A_LOT;

export async function handleListChat(arg: RCLICommandHandlerArg): Promise<IbGib_V1 | undefined> {
    const lc = `[${handleListChat.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: ef6ebc7b8c5840e38e3246bff05adc24)`); }
        const { cmdEscapeString, cmdInfo, contextIbGib, ibGib, metaspace } = arg;

        let { argInfos } = cmdInfo;

        // option to not get latest
        // option to enable logging for errors/warnings

        const contextRel8ns = contextIbGib.rel8ns ?? {};
        const rel8dCommentAddrs = contextRel8ns['comment'] ?? [];
        const latestAddrPromises = rel8dCommentAddrs.map(async addr => {
            return await metaspace.getLatestAddr({ addr }) ?? addr;
        });
        const latestAddrs = (await Promise.allSettled(latestAddrPromises)).map((x, i) => {
            if (x.status === 'fulfilled') {
                return x.value;
            } else {
                // when option for logging is impl, add error here
                return rel8dCommentAddrs[i];
            }
        });
        // combine for use later
        const addrsAndLatestAddrs = rel8dCommentAddrs.map((x, i) => [x, latestAddrs[i]]);
        let resGetIbGibs = await getFromSpace({
            addrs: latestAddrs,
            space: (await metaspace.getLocalUserSpace({ lock: false }))
        });
        let comments = addrsAndLatestAddrs.map(([addr, latestAddr]) => {
            let ibGib = resGetIbGibs.ibGibs?.find(x => getIbGibAddr({ ibGib: x }) === latestAddr);
            if (ibGib?.data?.text) {
                const text = ibGib.data!.text! as string;
                return `[${ibGib.data?.timestamp}] ${text}`;
            } else {
                return `[error getting comment for ${addr}. (latestAddr we had was ${latestAddr})`;
            }
        });
        let commentBlock = comments.join('\n---\n');
        console.log(commentBlock);

        return ROOT;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export const rcliCmdHandlerListChat: RCLICommandHandler =
    (arg) => handleListChat(arg);
