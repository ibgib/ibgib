/**
 * @module handle-info
 *
 * rcli cmd handler for the info cmd.
 */

import { extractErrorMsg, pretty, } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { extractArgValue } from '@ibgib/helper-gib/dist/rcli/rcli-helper.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { ROOT } from '@ibgib/ts-gib/dist/V1/constants.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../../ibgib-constants.mjs';
import { PARAM_INFO_LATEST, PARAM_INFO_PRETTY, PARAM_INFO_SRC_ADDR, } from '../rcli-constants.mjs';
import { RCLICommandHandler, RCLICommandHandlerArg } from '../rcli-types.mjs';
import { extractArg_space } from '../rcli-helper.mjs';

/**
 * used in verbose logging (across all ibgib libs atow)
 */
const logalot = GLOBAL_LOG_A_LOT;

export async function handleInfo(arg: RCLICommandHandlerArg): Promise<IbGib_V1 | undefined> {
    const lc = `[${handleInfo.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: eb9949faadff45fdb572991d53da25c9)`); }
        const { cmdEscapeString, cmdInfo, contextIbGib, ibGib, metaspace, fnAddComment } = arg;

        let { argInfos } = cmdInfo;
        if (!argInfos) { throw new Error(`(UNEXPECTED) argInfos falsy? (E: 944bb61e2a5f44428eb02f5bb78305c2)`); }

        const getLatestFlag = extractArgValue<boolean>({ paramInfo: PARAM_INFO_LATEST, argInfos, }) as boolean;
        const prettyFlag = (extractArgValue<boolean>({ paramInfo: PARAM_INFO_PRETTY, argInfos, }) as boolean) ?? true; // default to true

        /**
         * we'll reassign this if we get the latest addr
         */
        let addr = extractArgValue<string>({ paramInfo: PARAM_INFO_SRC_ADDR, argInfos, throwIfNotFound: true }) as string;
        /**
         * use this later, otherwise addr will be a working variable that might change.
         */
        const originalAddr = addr.concat();

        const space = await extractArg_space({ argInfos, metaspace, returnOnArgsFalsy: 'default' });
        if (!space) { throw new Error(`(UNEXPECTED) space is falsy. couldn't even get local user space? (E: b5a2f3c3265a4e12a5bf9163934395b8)`); }

        if (getLatestFlag) {
            const latestAddr = await metaspace.getLatestAddr({ addr, space });
            if (latestAddr) {
                if (logalot) { console.log(`${lc} latestAddr found (${latestAddr}) for incoming addr (${addr}) (I: 8b9ed59e81eb4be6885711fc45d00e38)`); }
                addr = latestAddr;
            } else {
                if (logalot) { console.log(`${lc} latestAddr not found for incoming addr (${addr}) (I: ff13cb3b80224743886126fc2409e62d)`); }
            }
        }

        const resGet = await metaspace.get({ addr, space });
        if (resGet.success && (resGet.ibGibs ?? []).length === 1) {
            const ibGib = resGet.ibGibs![0];
            if (logalot) {
                console.log(`${lc} console.dir(ibGib)... (I: 29893845212545f89aae09f054dd46a2)`);
                console.dir(ibGib);
            }
            const ibGibText = prettyFlag ? pretty(ibGib) : JSON.stringify(ibGib);
            if (getLatestFlag) {
                await fnAddComment({
                    contextIbGib,
                    rel8nName: 'comment',
                    text: `Here is that info...\naddr: ${originalAddr}\nlatest Addr: ${addr}\n${ibGibText}`,
                });
            } else {
                await fnAddComment({
                    contextIbGib,
                    rel8nName: 'comment',
                    text: `Here is the exact punctiliar ibGib you requested...\n${ibGibText}`,
                });
            }
        } else {
            await fnAddComment({
                contextIbGib,
                rel8nName: 'comment',
                text: `couldn't get addr (${addr}) from space (${space.data?.uuid}). errorMsg: ${resGet.errorMsg}`,
            });
        }
        return ROOT;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export const rcliCmdHandlerInfo: RCLICommandHandler =
    (arg) => handleInfo(arg);
