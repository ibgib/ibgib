import { cwd, chdir, } from 'node:process';
import { statSync } from 'node:fs';
import { mkdir, } from 'node:fs/promises';
import { exec, ExecException } from 'node:child_process';
import * as pathUtils from 'path';

import {
    firstOfAll, firstOfEach, ifWe, ifWeMight, iReckon,
    lastOfAll, lastOfEach, respecfully, respecfullyDear,
} from '@ibgib/helper-gib/dist/respec-gib/respec-gib.mjs';
const maam = `[${import.meta.url}]`, sir = maam;

import { GLOBAL_LOG_A_LOT } from '../../../ibgib-constants.mjs';
import { extractErrorMsg, getTimestampInTicks, getUUID } from '@ibgib/helper-gib';
import { parseCommandRawTextIntoArgs } from './rcli-helper.mjs';
import { RCLI_COMMAND_IDENTIFIERS } from './rcli-constants.mjs';

const logalot: boolean | number = GLOBAL_LOG_A_LOT;

const lcFile: string = `[${pathUtils.basename(import.meta.url)}]`;

// not worrying about repeating key names which is valid when `allowMultiple` is
// true in the param info

const CMD_VALID = `--init`;
const CMD_INVALID = `--initializerdoodle`;
const FLAG_VALID = `--some-flag`;
const FLAG_INVALID = `--uh oh spaces`;
const BARE_ARG_NO_QUOTE_0_SPACES_VALID = `no_spaces`;
const BARE_ARG_NO_QUOTE_1_SPACES_VALID = `one space`;
const BARE_ARG_NO_QUOTE_2_SPACES_VALID = `two spaces here`;
const BARE_ARG_DBL_QUOTE_0_SPACES_VALID = `"no_spaces"`;
const BARE_ARG_DBL_QUOTE_1_SPACES_VALID = `"one space"`;
const BARE_ARG_DBL_QUOTE_2_SPACES_VALID = `"two spaces here"`;
const BARE_ARG_SINGLE_QUOTE_0_SPACES_VALID = `'no_spaces'`;
const BARE_ARG_SINGLE_QUOTE_1_SPACES_VALID = `'one space'`;
const BARE_ARG_SINGLE_QUOTE_2_SPACES_VALID = `'two spaces here'`;
const KV_NO_QUOTE_VALID = `--key=bare_value`;
const KV_DBL_QUOTE_0_SPACES_VALID = `--key="bare_value"`;
const KV_DBL_QUOTE_1_SPACES_VALID = `--key="one space"`;
const KV_DBL_QUOTE_2_SPACES_VALID = `--key="two spaces here"`;
const KV_SINGLE_QUOTE_0_SPACES_VALID = `--key='bare_value'`;
const KV_SINGLE_QUOTE_1_SPACES_VALID = `--key='one space'`;
const KV_SINGLE_QUOTE_2_SPACES_VALID = `--key='two spaces here'`;

const KV_NO_END_QUOTE_INVALID = `--key="start_no_end`;
const KV_SPACES_NO_QUOTES_INVALID = `--key="start_no_end`;

const CMD_WITH_A_FLAG = `${CMD_VALID} ${FLAG_VALID}`;
const CMD_WITH_KV_BARE = `${CMD_VALID} ${KV_NO_QUOTE_VALID}`;
const CMD_WITH_2_KV_BARE = `${CMD_VALID} ${KV_NO_QUOTE_VALID} ${KV_NO_QUOTE_VALID}`

const VALID_BARE_ARGS: string[] = [
    BARE_ARG_NO_QUOTE_0_SPACES_VALID,
    BARE_ARG_NO_QUOTE_1_SPACES_VALID,
    BARE_ARG_NO_QUOTE_2_SPACES_VALID,
    BARE_ARG_DBL_QUOTE_0_SPACES_VALID,
    BARE_ARG_DBL_QUOTE_1_SPACES_VALID,
    BARE_ARG_DBL_QUOTE_2_SPACES_VALID,
    BARE_ARG_SINGLE_QUOTE_0_SPACES_VALID,
    BARE_ARG_SINGLE_QUOTE_1_SPACES_VALID,
    BARE_ARG_SINGLE_QUOTE_2_SPACES_VALID,
];

const VALID_KV_ARGS: string[] = [
    KV_NO_QUOTE_VALID,
    KV_DBL_QUOTE_0_SPACES_VALID,
    KV_DBL_QUOTE_1_SPACES_VALID,
    KV_DBL_QUOTE_2_SPACES_VALID,
    KV_SINGLE_QUOTE_0_SPACES_VALID,
    KV_SINGLE_QUOTE_1_SPACES_VALID,
    KV_SINGLE_QUOTE_2_SPACES_VALID,
];

const VALID_RAW_TEXTS_BARE_PLUS_ONE_KV: string[] = [];
for (let bare of VALID_BARE_ARGS) {
    for (let kv of VALID_KV_ARGS) {
        VALID_RAW_TEXTS_BARE_PLUS_ONE_KV.push(`${CMD_VALID} ${bare} ${kv}`)
    }
}
const VALID_RAW_TEXTS_BARE_PLUS_TWO_KV: string[] = [];
for (let bare of VALID_BARE_ARGS) {
    for (let kv of VALID_KV_ARGS) {
        for (let kv2 of VALID_KV_ARGS) {
            VALID_RAW_TEXTS_BARE_PLUS_TWO_KV.push(`${CMD_VALID} ${bare} ${kv} ${kv2}`)
        }
    }
}

const INVALID_RAW_TEXTS_BARE_PLUS_ONE_KV: string[] = [];
for (let bare of VALID_BARE_ARGS) {
    for (let kv of VALID_KV_ARGS) {
        // invalid cmd
        INVALID_RAW_TEXTS_BARE_PLUS_ONE_KV.push(`${CMD_INVALID} ${bare} ${kv}`)

        // bare must be immediately after cmd
        INVALID_RAW_TEXTS_BARE_PLUS_ONE_KV.push(`${CMD_VALID} ${kv} ${bare}`)
    }
}
const INVALID_RAW_TEXTS_BARE_PLUS_TWO_KV: string[] = [];
for (let bare of VALID_BARE_ARGS) {
    for (let kv of VALID_KV_ARGS) {
        for (let kv2 of VALID_KV_ARGS) {
            // invalid cmd
            INVALID_RAW_TEXTS_BARE_PLUS_TWO_KV.push(`${CMD_INVALID} ${bare} ${kv} ${kv2}`)

            // bare must be immediately after cmd
            INVALID_RAW_TEXTS_BARE_PLUS_TWO_KV.push(`${CMD_VALID} ${kv} ${bare} ${kv2}`)
            INVALID_RAW_TEXTS_BARE_PLUS_TWO_KV.push(`${CMD_VALID} ${kv} ${kv2} ${bare}`)
        }
    }
}


const VALID_RAW_TEXTS: string[] = [
    CMD_VALID,
    CMD_WITH_A_FLAG,
    CMD_WITH_KV_BARE,
    CMD_WITH_2_KV_BARE,
    ...VALID_RAW_TEXTS_BARE_PLUS_ONE_KV,
    ...VALID_RAW_TEXTS_BARE_PLUS_TWO_KV,
];

const INVALID_RAW_TEXTS: string[] = [
    CMD_INVALID,
    `${CMD_VALID} ${FLAG_INVALID}`,
    ...INVALID_RAW_TEXTS_BARE_PLUS_ONE_KV,
    ...INVALID_RAW_TEXTS_BARE_PLUS_TWO_KV,
];

await respecfully(sir, `getArgsFromRawText`, async () => {
    let lc = lcFile.concat();

    await respecfully(sir, 'bulk testing...', async () => {
        await ifWe(sir, `valid test cases should not error`, async () => {
            for (let i = 0; i < VALID_RAW_TEXTS.length; i++) {
                const rawText = VALID_RAW_TEXTS[i];
                // const rawText = `${CMD_VALID} ${BARE_ARG_NO_QUOTE_0_SPACES_VALID} ${KV_DBL_QUOTE_1_SPACES_VALID}`;
                let { validArgsString, validationErrorMsg, args, cmd_ie_firstArgSansPrefix } = parseCommandRawTextIntoArgs({ rawText, validCommandIdentifiers: RCLI_COMMAND_IDENTIFIERS.concat() });
                iReckon(sir, validArgsString).asTo(rawText + ' validArgsString').isGonnaBeTrue();
                iReckon(sir, validationErrorMsg).asTo(rawText + ' validationErrorMsg').isGonnaBeFalsy();
                iReckon(sir, args?.length).asTo(rawText + ' args.length').isGonnaBeTruthy();
                iReckon(sir, args![0]).asTo(rawText + ' cmd and first arg match').isGonnaBe(`--${cmd_ie_firstArgSansPrefix}`);
            }
        });
        await ifWe(sir, `invalid test cases should error`, async () => {
            for (let i = 0; i < INVALID_RAW_TEXTS.length; i++) {
                const rawText = INVALID_RAW_TEXTS[i];
                let { validArgsString, validationErrorMsg, args } = parseCommandRawTextIntoArgs({ rawText, validCommandIdentifiers: RCLI_COMMAND_IDENTIFIERS.concat() });
                iReckon(sir, validArgsString).asTo(rawText + ' validArgsString').isGonnaBeFalse();
                iReckon(sir, validationErrorMsg).asTo(rawText + ' validation error msg').isGonnaBeTruthy();
                iReckon(sir, args).asTo(rawText + ' args').isGonnaBeFalsy();
            }
        });
    });

    await respecfully(sir, 'individual test cases...', async () => {
        await ifWe(sir, `valid cmd only`, async () => {
            const rawText = `${CMD_VALID}`;
            let { validArgsString, validationErrorMsg, args, cmd_ie_firstArgSansPrefix } =
                parseCommandRawTextIntoArgs({ rawText, validCommandIdentifiers: RCLI_COMMAND_IDENTIFIERS.concat() });
            iReckon(sir, validArgsString).asTo(rawText + ' validArgsString').isGonnaBeTrue();
            iReckon(sir, validationErrorMsg).asTo(rawText + ' validation error msg').isGonnaBeFalsy();
            iReckon(sir, args?.length).asTo(rawText + ' args').isGonnaBeTruthy();
            iReckon(sir, rawText.includes(args?.at(0) ?? 'hoogly')).asTo(rawText + 'rawText includes cmd').isGonnaBeTrue;
            iReckon(sir, args![0]).asTo(rawText + ' cmd and first arg match').isGonnaBe(`--${cmd_ie_firstArgSansPrefix}`);
        });
        await ifWe(sir, `valid cmd, 1 arg`, async () => {
            // select subsets
            const rawTexts = [
                `${CMD_VALID} ${BARE_ARG_NO_QUOTE_2_SPACES_VALID}`,
                `${CMD_VALID} ${BARE_ARG_DBL_QUOTE_2_SPACES_VALID}`,
                `${CMD_VALID} ${FLAG_VALID}`,
                `${CMD_VALID} ${KV_DBL_QUOTE_1_SPACES_VALID}`,
                `${CMD_VALID} ${KV_SINGLE_QUOTE_2_SPACES_VALID}`,
            ]
            for (let i = 0; i < rawTexts.length; i++) {
                const rawText = rawTexts[i];
                let { validArgsString, validationErrorMsg, args, cmd_ie_firstArgSansPrefix } =
                    parseCommandRawTextIntoArgs({ rawText, validCommandIdentifiers: RCLI_COMMAND_IDENTIFIERS.concat() });
                iReckon(sir, validArgsString).asTo(rawText + ' validArgsString').isGonnaBeTrue();
                iReckon(sir, validationErrorMsg).asTo(rawText + ' validation error msg').isGonnaBeFalsy();
                iReckon(sir, args?.length).asTo(rawText + ' args?.length').isGonnaBe(2);
                iReckon(sir, args![0]).asTo(rawText + ' cmd and first arg match').isGonnaBe(`--${cmd_ie_firstArgSansPrefix}`);
                const rawTextSansQuotes = rawText.replace(/['"]/g, '');
                iReckon(maam, args?.every(arg => rawTextSansQuotes.includes(arg))).asTo('rawText contains every arg').isGonnaBeTrue();
            }
        });
        await ifWe(sir, `valid cmd, 2 arg`, async () => {
            // select subsets
            const rawTexts = [
                `${CMD_VALID} ${BARE_ARG_NO_QUOTE_2_SPACES_VALID} ${FLAG_VALID}`,
                `${CMD_VALID} ${BARE_ARG_DBL_QUOTE_2_SPACES_VALID} ${FLAG_VALID}`,
                `${CMD_VALID} ${FLAG_VALID} ${KV_SINGLE_QUOTE_1_SPACES_VALID}`,
                `${CMD_VALID} ${KV_DBL_QUOTE_1_SPACES_VALID} ${KV_DBL_QUOTE_0_SPACES_VALID}`,
                `${CMD_VALID} ${KV_DBL_QUOTE_1_SPACES_VALID} ${KV_SINGLE_QUOTE_0_SPACES_VALID}`,
                `${CMD_VALID} ${KV_SINGLE_QUOTE_2_SPACES_VALID} ${KV_DBL_QUOTE_0_SPACES_VALID}`,
            ]
            for (let i = 0; i < rawTexts.length; i++) {
                const rawText = rawTexts[i];
                let { validArgsString, validationErrorMsg, args, cmd_ie_firstArgSansPrefix } =
                    parseCommandRawTextIntoArgs({ rawText, validCommandIdentifiers: RCLI_COMMAND_IDENTIFIERS.concat() });
                iReckon(sir, validArgsString).asTo(rawText + ' validArgsString').isGonnaBeTrue();
                iReckon(sir, validationErrorMsg).asTo(rawText + ' validation error msg').isGonnaBeFalsy();
                iReckon(sir, args?.length).asTo(rawText + ' args?.length').isGonnaBe(3);
                iReckon(sir, args![0]).asTo(rawText + ' cmd and first arg match').isGonnaBe(`--${cmd_ie_firstArgSansPrefix}`);
                const rawTextSansQuotes = rawText.replace(/['"]/g, '');
                iReckon(maam, args?.every(arg => rawTextSansQuotes.includes(arg))).asTo('rawText contains every arg').isGonnaBeTrue();
            }
        });
    });

}, { logalot: !!logalot });
