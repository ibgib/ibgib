import { cwd, chdir, } from 'node:process';
import { statSync } from 'node:fs';
import { mkdir, } from 'node:fs/promises';
import * as pathUtils from 'path';

import {
    firstOfAll, firstOfEach, ifWe, ifWeMight, iReckon,
    lastOfAll, lastOfEach, respecfully, respecfullyDear,
} from '@ibgib/helper-gib/dist/respec-gib/respec-gib.mjs';
const maam = `[${import.meta.url}]`, sir = maam;

import { GLOBAL_LOG_A_LOT } from '../../../ibgib-constants.mjs';
import { RCLI_TEST_PATH } from './rcli-constants.mjs';
import { extractErrorMsg, getTimestampInTicks, getUUID } from '@ibgib/helper-gib';
import { baseRespecPath, initialCwd } from './rcli.respec.mjs';
import { execRCLIInNode } from '../../../respec-gib-helper.node.mjs';

const logalot: boolean | number = GLOBAL_LOG_A_LOT;

const lcFile: string = `[${pathUtils.basename(import.meta.url)}]`;


await respecfully(sir, lcFile, async () => {
    let lc = lcFile.concat();
    if (logalot) { console.log(`${lc} cwd: ${cwd()} (I: 6d1e2c1a81cc4246b52ebd26e3fc1e11)`) }

    firstOfEach(sir, async () => {
        let subpath = pathUtils.join(baseRespecPath, `${getTimestampInTicks()}_${(await getUUID()).slice(0, 5)}`);
        await mkdir(subpath, { recursive: true });
        chdir(subpath);
        console.log(`${lc} cwd: ${cwd()} (I: 709aa70ff5d44e64a91b10df7543ec01)`)
    });

    lastOfEach(sir, async () => {
        chdir(initialCwd);
        if (logalot) { console.log(`${lc}[lastOfAll] cwd: ${cwd()} (I: 6d1e2c1a81cc4246b52ebd26e3fc1e11)`) }
    });

    await ifWe(sir, `--init --in-memory`, async () => {
        let lc = `${lcFile}[--init --in-memory]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: e8e9563e813dacb95f37a2814a709923)`); }
            const [resNode_HelpPromise] = await execRCLIInNode({
                execPath: initialCwd,
                argsString: '--init --in-memory --space-name="test_space_name_yo"',
                logContext: lc,
                timeoutMs: 20_000,
            }); // already inside unique test folder
            const resNode_Help = await resNode_HelpPromise;
            iReckon(sir, resNode_Help.err).isGonnaBe(null);
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    });

}, { logalot: !!logalot });
