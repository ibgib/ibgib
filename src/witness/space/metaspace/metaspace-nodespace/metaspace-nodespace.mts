/**
 * @module node-metaspace driven by a node+fs storage substrate
 *
 * a metaspace is a space of spaces. it adds/removes spaces and does
 * inter-spatial composition related things. it's similar to what people think
 * of in terms of a node in a p2p network.
 */

import { extractErrorMsg } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { NodeFilesystemSpace_V1 } from '@ibgib/core-gib/dist/witness/space/filesystem-space/node-filesystem-space/node-filesystem-space-v1.mjs';
import { IbGibSpaceAny } from '@ibgib/core-gib/dist/witness/space/space-base-v1.mjs';
import { IbGibCacheService } from '@ibgib/core-gib/dist/common/cache/cache-types.mjs';
import { MetaspaceBase } from '@ibgib/core-gib/dist/witness/space/metaspace/metaspace-base.mjs';
import { MetaspaceFactory } from '@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs';

import { GLOBAL_LOG_A_LOT, GLOBAL_TIMER_NAME } from '../../../../ibgib-constants.mjs';
import { fnCreateNewLocalSpace, fnDtoToSpace } from './metaspace-nodespace-helper.mjs';

const logalot = GLOBAL_LOG_A_LOT;

interface TempCacheEntry {
  /**
   * Password to the user's password.
   */
  tempMetaPassword: string;
  /**
   * Encrypted user password
   */
  encryptedPassword: string;
  /**
   * salt used in cached encryption.
   */
  salt: string;
}


/**
 * All-purpose mega service (todo: which we'll need to break up!) to interact
 * with ibgibs at the app/device level.
 *
 * This works with the local app user space.
 *
 * ## regarding special ibgibs
 *
 * Special ibgibs' behaviors are what hold in other apps configuration data.
 * Of course the difference is that most special ibgibs can leverage the "on-chain"
 * functionality of "regular" ibgibs.
 *
 * There are a couple meta ibgibs (which I also call "special"):
 *   * roots^gib
 *     * tracks other special root^gib ibgibs, which are like local app-level indexes.
 *   * tags^gib
 *     * tracks other special tag^gib ibgibs, which you can apply to any ibgib
 *   * latest^gib
 *     * tracks mappings between tjp -> latest ib^gib address
 *     * ephemeral (deletes past rel8ns and past ibGib frames)
 *   * ...
 *
 * ## regarding latest ibgibs
 *
 * The tjp (temporal junction point) defines atow the beginning of an ibGib
 * timeline.  it's like the birthday for an ibGib. (Or you can think if it as
 * the id for the stream of ibgib frames in a given timeline.)
 *
 * The latest ibGib in that timeline is also special, because it's often what
 * you want to work with.
 *
 * So ideally, when an ibgib, A, has a tjp A1, and it is updated to A2, A3, An
 * via `mut8` and/or `rel8` transforms, that ibgib creates a single timeline.
 * This service attempts to track the relationship between that starting tjp
 * address and its corresponding latest frame in that timeline, i.e., A1 -> An.
 *
 * ### mapping persistence implementation details
 *
 * The latest ibGib service is backed by a special ibgib that maintains the
 * mapping index.  It does this by rel8-ing that special backing ibgib via the
 * tjp pointer, e.g. [special latest ibgib^XXX000].rel8ns[A^TJP123] ===
 * [A^N12345] . It does this via the ib^gib content address pointer, so this
 * becomes a mapping from A^TJP123 to A^N12345.
 *
 * This backing ibGib is special (even for special ibGibs) in that:
 *   * it does not relate itself with the current root of the application
 *   * it does not maintain references to its past (i.e. rel8ns['past'] === [])
 *   * it DELETES its previous incarnation from the files service
 *
 * In other words, this service is meant to be as ephemeral as possible. I am
 * keeping it as an ibGib and not some other data format (like straight in
 * storage/some other db) because I've found this is often useful and what I end
 * up doing anyway to leverage other ibgib behavior. For example, in the future
 * it may be good to take snapshots, which is a simple copy operation of the
 * file persistence.
 *
 * ### current naive implementation notes
 *
 * questions:
 *   * What do we want to do if we can't locate an ibGib record?
 *   * How/when do we want to alert the user/our own code that we've found
 *     multiple timelines for an ibGib with a tjp (usually a thing we want to
 *     avoid)?
 *   * Who do we want to notify when new ibGibs arrive?
 *   * How often do we want to check external sources for latest?
 *   * When do we get to merging ibGib timelines?
 *
 * This is behavior that is somewhat taken care of, e.g. in git, with the HEAD
 * pointer for a repo.  But we're talking about here basically as a metarepo or
 * "repo of repos", and unlike git, we don't want our HEAD metadata living "off
 * chain" (outside of the DLT itself that it's modifying).  So eventually, what
 * we want is just like what we want with ALL ibGibs: perspective. From "the
 * app"'s perspective, the latest is mapped. But really, apps can't view slices
 * of ibGib graphs in all sorts of interesting ways and still be productive &
 * beneficial to the ecosystem as a whole.
 */
export class Metaspace_Nodespace extends MetaspaceBase {

  // we won't get an object back, only a DTO ibGib essentially
  protected lc: string = `[${Metaspace_Nodespace.name}]`;

  get zeroSpace(): IbGibSpaceAny {
    const lc = `[${this.lc}][get zeroSpace]`;
    if (this.metaspaceFactory.fnZeroSpaceFactory) {
      return this.metaspaceFactory.fnZeroSpaceFactory();
    } else {
      throw new Error(`${lc} (UNEXPECTED) this.metaspaceFactory.fnZeroSpaceFactory falsy. not initialized? (E: 9e6c6954dacd868b18c9f34a71e63523)`);
    }
  }

  constructor(
    // public modalController: ModalController,
    // public alertController: AlertController,
    protected cacheSvc: IbGibCacheService | undefined,
    // private latestCacheSvc: IonicStorageLatestIbgibCacheService,
  ) {
    super(cacheSvc);
    const lc = `${this.lc}[ctor]`;
    if (logalot) { console.log(`${lc}${GLOBAL_TIMER_NAME}`); console.timeLog(GLOBAL_TIMER_NAME); }
    console.log(`${lc} created.`);
  }

  protected async initializeMetaspaceFactory({ metaspaceFactory }: {
    metaspaceFactory: MetaspaceFactory,
  }): Promise<void> {
    const lc = `[${this.initializeMetaspaceFactory.name}]`;
    try {
      if (logalot) { console.log(`${lc} starting... (I: 5a29670439ac47634dcdfe25225c8223)`); }

      await super.initializeMetaspaceFactory({ metaspaceFactory });

      metaspaceFactory.fnDefaultLocalSpaceFactory ??= (opts) => fnCreateNewLocalSpace(opts);
      metaspaceFactory.fnDtoToSpace ??= (spaceDto) => fnDtoToSpace(spaceDto);

      this.metaspaceFactory.fnZeroSpaceFactory ??= () => {
        // default to node space
        const zeroSpace = new NodeFilesystemSpace_V1(/*initialData*/ undefined, /*initialRel8ns*/ undefined);
        zeroSpace.gib = 'gib';
        return zeroSpace;
      }
    } catch (error) {
      console.error(`${lc} ${extractErrorMsg(error)}`);
      throw error;
    } finally {
      if (logalot) { console.log(`${lc} complete.`); }
    }
  }

  async initializeZeroSpace(): Promise<void> {
    const lc = `[${this.initializeZeroSpace.name}]`;
    try {
      if (logalot) { console.log(`${lc} starting... (I: a3364e9601c70c8163e67868e5d67723)`); }

    } catch (error) {
      console.error(`${lc} ${extractErrorMsg(error)}`);
      throw error;
    } finally {
      if (logalot) { console.log(`${lc} complete.`); }
    }
  }

}
