import { extractErrorMsg, getTimestamp, getUUID, pretty } from "@ibgib/helper-gib/dist/helpers/utils-helper.mjs";
import { getGib } from "@ibgib/ts-gib/dist/V1/transforms/transform-helper.mjs";
import { GIB } from "@ibgib/ts-gib/dist/V1/constants.mjs";
import { IbGib_V1 } from "@ibgib/ts-gib/dist/V1/types.mjs";
import { IbGibSpaceAny } from "@ibgib/core-gib/dist/witness/space/space-base-v1.mjs";
import {
    DtoToSpaceFunction, LocalSpaceFactoryFunction,
} from "@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs";
import { spaceNameIsValid } from "@ibgib/core-gib/dist/witness/space/space-helper.mjs";
import { NodeFilesystemSpace_V1 } from "@ibgib/core-gib/dist/witness/space/filesystem-space/node-filesystem-space/node-filesystem-space-v1.mjs";
import { NodeFilesystemSpaceData_V1, NodeFilesystemSpaceRel8ns_V1, } from "@ibgib/core-gib/dist/witness/space/filesystem-space/node-filesystem-space/node-filesystem-space-types.mjs";
import {
    IBGIB_BASE_DIR, IBGIB_BASE_SUBPATH, IBGIB_BIN_SUBPATH,
    IBGIB_DNA_SUBPATH, IBGIB_ENCODING, IBGIB_IBGIBS_SUBPATH, IBGIB_META_SUBPATH
} from "@ibgib/core-gib/dist/witness/space/filesystem-space/filesystem-constants.mjs";
import {
    DEFAULT_LOCAL_SPACE_DESCRIPTION, DEFAULT_LOCAL_SPACE_POLLING_INTERVAL_MS,
    PERSIST_OPTS_AND_RESULTS_IBGIBS_DEFAULT
} from "@ibgib/core-gib/dist/witness/space/space-constants.mjs";

import { getFnPrompt } from "../../../../common/prompt-functions.mjs";


export const fnCreateNewLocalSpace: LocalSpaceFactoryFunction = async ({
    allowCancel,
    spaceName,
    logalot,
}) => {
    const lc = `[fnCreateNewLocalSpace]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 15bcc16f4f3f9e40e743931a3965ff22)`); }

        if (spaceName && !spaceNameIsValid(spaceName)) {
            throw new Error(`spaceName (${spaceName}) is invalid.  (E: 820b291cccf9f405f9e0cce87143e323)`);
            // throw because this is used in RCLI and automated testing...
        }
        if (!spaceName) {

            const promptName: () => Promise<void> = async () => {
                const fnPrompt = getFnPrompt();
                const resName = await fnPrompt({
                    title: 'greetings program', msg: `all of your data is stored locally on your device's "local space".

so name your space with alphanumerics & underscores, like 'phone_alice' or 'web_bob_foo', or leave blank and get a random name.

Enter space name:
`
                });

                if (resName === '' && !allowCancel) {
                    spaceName = 'space_' + (await getUUID()).slice(0, 10);
                } else {
                    if (resName && spaceNameIsValid(resName)) {
                        spaceName = resName;
                    }
                }
            };

            // ...prompt for name
            await promptName();

            if (!spaceName) { return undefined; /* <<<< returns early */ }
        }

        // ...create in memory with defaults
        const newLocalSpace = new NodeFilesystemSpace_V1(/*initialData*/ {
            version: '1',
            uuid: await getUUID(),
            isTjp: true,
            timestamp: getTimestamp(),
            name: spaceName,
            baseDir: IBGIB_BASE_DIR,
            spaceSubPath: spaceName,
            baseSubPath: IBGIB_BASE_SUBPATH,
            binSubPath: IBGIB_BIN_SUBPATH,
            dnaSubPath: IBGIB_DNA_SUBPATH,
            ibgibsSubPath: IBGIB_IBGIBS_SUBPATH,
            metaSubPath: IBGIB_META_SUBPATH,
            encoding: IBGIB_ENCODING,
            persistOptsAndResultIbGibs: PERSIST_OPTS_AND_RESULTS_IBGIBS_DEFAULT,
            validateIbGibAddrsMatchIbGibs: true,
            trace: false,
            description: DEFAULT_LOCAL_SPACE_DESCRIPTION,
            allowPrimitiveArgs: false,
            catchAllErrors: true,
            longPollingIntervalMs: DEFAULT_LOCAL_SPACE_POLLING_INTERVAL_MS,
        } as NodeFilesystemSpaceData_V1, /*initialRel8ns*/ undefined);
        if (logalot) { console.log(`${lc} localSpace.ib: ${newLocalSpace.ib}`); }
        if (logalot) { console.log(`${lc} localSpace.gib: ${newLocalSpace.gib} (before sha256v1)`); }
        if (logalot) { console.log(`${lc} localSpace.data: ${pretty(newLocalSpace.data || 'falsy')}`); }
        if (logalot) { console.log(`${lc} localSpace.rel8ns: ${pretty(newLocalSpace.rel8ns || 'falsy')}`); }

        // trying out with tjp, can't remember why didn't have it except for
        // backwards compatability which i'm no longer shooting for (will be
        // able to project old spaces to new ones i believe)
        // newLocalSpace.gib = await getGib({ ibGib: newLocalSpace, hasTjp: false });
        newLocalSpace.gib = await getGib({ ibGib: newLocalSpace, hasTjp: true });

        if (newLocalSpace.gib === GIB) { throw new Error(`localSpace.gib not updated correctly.`); }
        if (logalot) { console.log(`${lc} localSpace.gib: ${newLocalSpace.gib} (after sha256v1)`); }

        return newLocalSpace as IbGibSpaceAny;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export const fnDtoToSpace: DtoToSpaceFunction = (spaceDto) => {
    return Promise.resolve(
        NodeFilesystemSpace_V1.createFromDto(spaceDto as IbGib_V1<NodeFilesystemSpaceData_V1, NodeFilesystemSpaceRel8ns_V1>)
    );
}
