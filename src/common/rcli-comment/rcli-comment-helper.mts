import { extractErrorMsg, getSaferSubstring, getTimestamp, getUUID, pretty } from "@ibgib/helper-gib/dist/helpers/utils-helper.mjs";
import { UUID_REGEXP } from "@ibgib/helper-gib/dist/constants.mjs";
import { RCLIArgInfo, RCLIParamInfo } from '@ibgib/helper-gib/dist/rcli/rcli-types.mjs';

import { Ib, TransformResult } from "@ibgib/ts-gib/dist/types.mjs";
import { Factory_V1 as factory } from "@ibgib/ts-gib/dist/V1/factory.mjs"
import { validateIbGibIntrinsically } from '@ibgib/ts-gib/dist/V1/validate-helper.mjs';
import { IbGib_V1 } from "@ibgib/ts-gib/dist/V1/types.mjs";
import { persistTransformResult } from "@ibgib/core-gib/dist/witness/space/space-helper.mjs";
import { IbGibSpaceAny } from "@ibgib/core-gib/dist/witness/space/space-base-v1.mjs";
import { isComment } from "@ibgib/core-gib/dist/common/comment/comment-helper.mjs";

import { GLOBAL_LOG_A_LOT } from "../../ibgib-constants.mjs";
import { RCLICommentData_V1, RCLICommentIbGib_V1 } from "./rcli-comment-types.mjs";
import { stripQuotes } from "../../witness/app/rcli/rcli-helper.mjs";
import { RCLI_DEFAULT_COMMAND_ESCAPE_STRING } from "../../witness/app/rcli/rcli-constants.mjs";

const logalot = GLOBAL_LOG_A_LOT;

export function validateCommonRCLICommentData({
    rcliCommentData,
}: {
    rcliCommentData?: RCLICommentData_V1,
}): string[] {
    const lc = `[${validateCommonRCLICommentData.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting...`); }
        if (!rcliCommentData) { throw new Error(`rcliCommentData required (E: 8646734e3d494d41bd3a0951fb7338ed)`); }
        const errors: string[] = [];
        const {
            uuid,
            text,
            args,
            interpretedArgInfos,
        } = rcliCommentData;

        if (uuid) {
            if (!uuid.match(UUID_REGEXP)) {
                errors.push(`uuid must match regexp: ${UUID_REGEXP}`);
            }
        } else {
            errors.push(`uuid required.`);
        }

        if (!text) { errors.push(`text required`); }
        if (!args) { errors.push(`args required`); }
        if (!interpretedArgInfos) { errors.push(`interpretedArgInfos required`); }

        return errors;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export async function validateCommonRCLICommentIbGib({
    rcliCommentIbGib,
}: {
    rcliCommentIbGib: RCLICommentIbGib_V1,
}): Promise<string[] | undefined> {
    const lc = `[${validateCommonRCLICommentIbGib.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 4ac542ac947e4b899cae3d4b949b3708)`); }
        const intrinsicErrors: string[] = await validateIbGibIntrinsically({ ibGib: rcliCommentIbGib }) ?? [];

        if (!rcliCommentIbGib.data) { throw new Error(`rcliCommentIbGib.data required (E: 5a3133c71a9d455bb5693f66a5de396f)`); }
        const ibErrors: string[] = [];
        let { text } = parseRCLICommentIb({ rcliCommentIb: rcliCommentIbGib.ib });
        if (!text) { ibErrors.push(`ib text required (E: 1739a2c832bc42d9866111f2fefee954)`); }

        const dataErrors = validateCommonRCLICommentData({ rcliCommentData: rcliCommentIbGib.data });

        let result = [...(intrinsicErrors ?? []), ...(ibErrors ?? []), ...(dataErrors ?? [])];
        if (result.length > 0) {
            return result;
        } else {
            return undefined;
        }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

console.warn(`[TODO] do more COMMON_SPECIALS_REPLACE_MAP_RCLI`);
export const COMMON_SPECIALS_REPLACE_MAP_RCLI: { [char: string]: string } = {
    " ": "__space__",
    "/": "__slash__",
    "\\\\": "__2backslash__", // not sure if this will work
    "\\": "__backslash__",
    ".": "__dot__",
    "=": "__equals__",
    "`": "__backtick__",
    "'": "__quote__",
    "\"": "__doublequote__",
    "?": "__question__",
    "$": "__dollar__",
    "&": "__ampersand__",
    ":": "__colon__",
    ";": "__semicolon__",
    // do more of these for fuller support
}

export function getRCLICommentIb({
    rcliCommentData,
    addlMetadataText,
}: {
    rcliCommentData: RCLICommentData_V1,
    addlMetadataText?: string,
}): Ib {
    const lc = `[${getRCLICommentIb.name}]`;
    try {
        const validationErrors = validateCommonRCLICommentData({ rcliCommentData });
        if (validationErrors.length > 0) { throw new Error(`invalid rcliCommentData: ${validationErrors} (E: 70b75e7699ee439ba372aabc7e5a8618)`); }
        if (addlMetadataText?.includes(' ')) { throw new Error(`addlMetadataText should not include a space, as this is a delimiter in the rcliCommentIb (E: 9a46c72da7c8b4e30872f2a71b17c823)`); }

        const saferText = getSaferSubstring({
            text: rcliCommentData.text,
            keepLiterals: ["-", "."],
            length: 64,
            replaceMap: {
                ...COMMON_SPECIALS_REPLACE_MAP_RCLI,
            }
        });
        if (saferText.includes(' ')) { throw new Error(`saferText should not include a space, as this is a delimiter in the rcliCommentIb (E: 9a46c72da7c8b4e30872f2a71b17c823)`); }

        return addlMetadataText ?
            `comment ${saferText} ${addlMetadataText}` :
            `comment ${saferText}`;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    }
}

/**
 * Current schema is `comment ${saferText} ${addlMetadataText}`;
 *
 * NOTE this is space-delimited
 */
export function parseRCLICommentIb({
    rcliCommentIb,
}: {
    rcliCommentIb: Ib,
}): {
    text: string,
    addlMetadataText?: string,
} {
    const lc = `[${parseRCLICommentIb.name}]`;
    try {
        if (!rcliCommentIb) { throw new Error(`rcliCommentIb required (E: 944a43eeee4b4f01a764599c19421491)`); }

        const [_, text, addlMetadataText] = rcliCommentIb.split(' ');

        return {
            text,
            addlMetadataText,
        };
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    }
}

export function isRCLIComment({ ibGib }: { ibGib: IbGib_V1 }): boolean {
    const lc = `[${isRCLIComment.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 443228a77817b8e208450e9597c60223)`); }

        if (!isComment({ ibGib })) { return false; /* <<<< returns early */ }

        let { data } = ibGib;
        if (!data) { return false; /* <<<< returns early */ }

        let castedData = data as RCLICommentData_V1;
        if ((castedData.args ?? []).length === 0) {
            return false; /* <<<< returns early */
        }

        if ((castedData.interpretedArgInfos ?? []).length === 0) {
            return false; /* <<<< returns early */
        }

        return true;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * creates an RCLI comment ibgib, returning the transform result,
 * optionally saving it in a given {@link space}.
 */
export async function createRCLICommentIbGib({
    args,
    interpretedArgInfos,
    addlMetadataText,
    saveInSpace,
    space,
}: {
    /**
     * As close to the raw args as we can get.
     */
    args: string[];
    /**
     * interpreted infos for args
     */
    interpretedArgInfos: RCLIArgInfo[];
    /**
     * Optional metadata string to be included in the comment's ib.
     * Should be underscore-delimited but not a hard rule atow.
     *
     * @example "comment thisisacomm here_is_addl_metadata"
     */
    addlMetadataText?: string;
    /**
     * If true, will save the ibgibs created in the given {@link space}.
     */
    saveInSpace?: boolean,
    /**
     * If {@link saveInSpace}, all ibgibs created in this function will be stored in
     * this space.
     */
    space?: IbGibSpaceAny,
}): Promise<TransformResult<RCLICommentIbGib_V1>> {
    const lc = `[${createRCLICommentIbGib.name}]`;

    if (logalot) { console.log(`${lc} starting...`); }
    try {
        if (!args) { throw new Error(`args required (E: 6c26bc6039004a6cbabb98f1c69c9ffe)`); }
        if (args.length === 0) { throw new Error(`args.length must be at least 1 (E: 9c217b1cb77fa88c097b901d892eee23)`); }
        if (!interpretedArgInfos) { throw new Error(`interpretedArgInfos required (E: a3b1bf4898724c4f92791eae099637e5)`); }
        if (!interpretedArgInfos.length) { throw new Error(`interpretedArgInfos.length must be at least 1 (E: 2a67f5aca6ebf6bff2564f4f10352523)`); }

        // if any of the args have spaces, then we want to quote them
        // const text = args.join(' ');
        const text = getOriginalCmdTextFromStrippedArgs({ args });

        const data: RCLICommentData_V1 = {
            uuid: await getUUID(),
            text,
            args,
            interpretedArgInfos,
            textTimestamp: getTimestamp(),
        };

        // create an ibgib with the filename and ext
        const opts: any = {
            parentIbGib: factory.primitive({ ib: 'comment' }),
            ib: getRCLICommentIb({ rcliCommentData: data, addlMetadataText }),
            data,
            dna: true,
            tjp: { uuid: true, timestamp: true },
            nCounter: true,
        };

        // this makes it more difficult to share/sync ibgibs...
        // if (this.addr) { opts.rel8ns = { 'comment on': [this.addr] }; }

        if (logalot) { console.log(`${lc} opts: ${pretty(opts)}`); }
        const resCommentIbGib = await factory.firstGen(opts) as TransformResult<RCLICommentIbGib_V1>;

        if (saveInSpace) {
            if (!space) { throw new Error(`space required if saveInSpace is truthy (E: 4dc33cd38b744d42835f4550b9d37fce)`); }
            await persistTransformResult({ resTransform: resCommentIbGib, space });
        }

        return resCommentIbGib;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    }
}

export function getOriginalCmdTextFromStrippedArgs({
    args
}: {
    args: string[],
}): string {
    const lc = `[${getOriginalCmdTextFromStrippedArgs.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: e39e66d61c16543d623ae72862908723)`); }

        const adjustedArgs: string[] = [];
        for (let i = 0; i < args.length; i++) {
            const arg = args[i];
            if (arg.includes(' ')) {
                // includes space so we need to adjust

                /**
                 * helper fn to wrap
                 */
                const fnWrapInQuotesIfNeeded = (str: string) => {
                    str = str.concat();
                    let prefix = '';
                    if (str.startsWith('--')) {
                        prefix = '--';
                        str = str.substring(2)
                    } else if (str.startsWith(RCLI_DEFAULT_COMMAND_ESCAPE_STRING)) {
                        prefix = RCLI_DEFAULT_COMMAND_ESCAPE_STRING;
                        str = str.substring(RCLI_DEFAULT_COMMAND_ESCAPE_STRING.length);
                    }
                    if (str.includes(' ')) {
                        // wrap str in single or double quotes
                        if (str.startsWith('"') || str.startsWith("'")) {
                            str = stripQuotes(str);
                        }
                        if (str.includes("'") && str.includes('"')) {
                            throw new Error(`edge case detected. value (${str}) includes both single and double quote. you win. (E: 18400ddf275805d283720792b86eaf23)`);
                        } else if (str.includes('"')) {
                            // includes double quotes, so wrap in single quotes
                            str = `'${str}'`;
                        } else {
                            // includes single quotes or none at all, so wrap in
                            // double quotes
                            str = `"${str}"`;
                        }
                    }
                    return prefix ? prefix + str : str;
                }

                if (arg.includes('=')) {
                    if (arg.indexOf('=') !== arg.lastIndexOf('=')) {
                        throw new Error(`more than one equals sign (=) found. these ='s in arg must only be to separate key=value. Neither key nor value can contain an equals sign. (E: 5d810f2b323712ad18cf8e7ec244cb23)`);
                    }

                    // adjust with quotes around value
                    let [key, value] = arg.split('=');
                    value = fnWrapInQuotesIfNeeded(value);
                    if (key.includes(' ')) { throw new Error(`arg key (${key}) cannot contain a space. (E: 6ac276ea5b217c8c65173978f7173223)`); }
                    adjustedArgs.push(`${key}=${value}`);
                } else {
                    // just wrap the entire arg
                    adjustedArgs.push(fnWrapInQuotesIfNeeded(arg));
                }
            } else {
                // no space in arg so no need to adjust
                adjustedArgs.push(arg.concat());
            }
        }
        const result = adjustedArgs.join(' ');
        if (logalot) { console.log(`${lc} result: ${result} (I: 59ab6d1f448c7201d4c8af6f40dfcb23)`); }
        return result;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * formats given {@link summaryText} to show help for commands in a regular-ish
 * manner.
 *
 * note: the formatted text neither starts nor ends with a newline char (\n)
 *
 * @returns formatted help text
 */
export function getFormattedCmdHelpText({
    summaryText,
    cmdParamInfo,
    paramSummaries,
}: {
    /**
     * help text for the given {@link cmdParamInfo}
     */
    summaryText: string,
    /**
     * param info for command that we're providing help for.
     */
    cmdParamInfo: RCLIParamInfo,
    paramSummaries: { param: RCLIParamInfo, summary: string }[],
}): string {
    const lc = `[${getFormattedCmdHelpText.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 3e790514b835c4818ffdeda6ef79a624)`); }
        /**
         * dividing horizontal line
         */
        const horizontalLine = ''.padEnd(80, '-');
        summaryText = summaryText.concat().replace(/\$horizontalLine/g, horizontalLine);

        const cmdSynonymsTextIfAny = (cmdParamInfo.synonyms ?? []).length > 0 ?
            cmdParamInfo.synonyms!.join(',') :
            `[none]`;

        const paramSummariesText = paramSummaries.length > 0 ?
            paramSummaries.map(x =>
                `${x.param.name}:\n  ${x.summary}\n  synonyms: ${(x.param.synonyms ?? []).length > 0 ? x.param.synonyms!.join(', ') : '[none]'}`).join('\n') :
            `[no params]`;

        const formattedText: string = [
            `Help for: ${cmdParamInfo.name}`,
            '',
            horizontalLine,
            `Cmd synonyms:`,
            '',
            cmdSynonymsTextIfAny,
            horizontalLine,
            `Summary:`,
            '',
            summaryText,
            horizontalLine,
            `Params:`,
            '',
            paramSummariesText,
            horizontalLine
        ].join('\n');

        if (logalot) { console.log(`${lc} formattedText: ${formattedText} (I: 0d2eb12e3957f765235ddd0e08947324)`); }

        return formattedText;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}
