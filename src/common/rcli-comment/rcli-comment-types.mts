import { CommentData_V1 } from '@ibgib/core-gib/dist/common/comment/comment-types.mjs';
import { RCLIArgInfo } from '@ibgib/helper-gib/dist/rcli/rcli-types.mjs';
import { IbGibRel8ns_V1, IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';

/**
 * @see {@link RCLICommentIbGib_V1}
 */
export interface RCLICommentData_V1 extends CommentData_V1 {
    /**
     * As close to the raw args as we can get.
     */
    args: string[];
    /**
     * interpreted infos for args
     */
    interpretedArgInfos: RCLIArgInfo[];
}

export interface RCLICommentRel8ns_V1 extends IbGibRel8ns_V1 {

}

/**
 * Special Comment that acts as a "request". ("command" analog from RCLI)
 *
 * The raw text of the command is the `text` from `CommentData_V1`.
 *
 * Of course as always the timestamps are taken with a grain of salt.
 *
 * @see {@link RCLICommentData_V1}
 * @see {@link RCLICommentRel8ns_V1}
 */
export interface RCLICommentIbGib_V1 extends IbGib_V1<RCLICommentData_V1, RCLICommentRel8ns_V1> {
    /**
     * kluging this here to get one-off commandlines going for starters
     */
    oneOff: boolean;
}
