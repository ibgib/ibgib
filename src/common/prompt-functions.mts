// import { AlertController, } from '@ionic/angular';
// import { Dialog } from '@capacitor/dialog';

import * as readline from 'node:readline/promises';
import { stdin, stdout } from 'node:process';


import { extractErrorMsg } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { IbGibSpaceAny } from '@ibgib/core-gib/dist/witness/space/space-base-v1.mjs';
// import { SecretModalFormComponent } from '../modals/secret-modal-form/secret-modal-form.component';
// import { EncryptionModalFormComponent } from '../modals/encryption-modal-form/encryption-modal-form.component';
// import { OuterspaceModalFormComponent } from '../modals/outerspace-modal-form/outerspace-modal-form.component';
// import { UpdatePicModalFormComponent, UpdatePicPromptResult } from '../modals/update-pic-modal-form/update-pic-modal-form.component';
// import { UpdateCommentModalFormComponent, UpdateCommentPromptResult } from '../modals/update-comment-modal-form/update-comment-modal-form.component';
// import { EncryptionData_V1 } from '@ibgib/core-gib/dist/common/encrypt/encrypt-types.mjs';
// import { OuterSpaceIbGib } from '@ibgib/core-gib/dist/witness/space/outer-space/outer-space-types.mjs';
// import { PicIbGib_V1 } from '@ibgib/core-gib/dist/common/pic/pic-types.mjs';
// import { RobbotIbGib_V1 } from '@ibgib/core-gib/dist/witness/robbot/robbot-types.mjs';
// import {
//     RobbotModalFormComponent, RobbotPromptResult
// } from '@ibgib/core-gib/dist/modals/robbot-modal-form/robbot-modal-form.component';
// import { AppIbGib_V1 } from '@ibgib/core-gib/dist/witness/app/app-types.mjs';
// import {
//     AppModalFormComponent, AppPromptResult
// } from '@ibgib/core-gib/dist../modals/app-modal-form/app-modal-form.component';
// import { clearDoCancelModalOnBackButton, registerCancelModalOnBackButton } from '@ibgib/core-gib/dist./utils';
// import { CommentIbGib_V1 } from '@ibgib/core-gib/dist/common/comment/comment-types.mjs';
// import { SECRET_REL8N_NAME } from '@ibgib/core-gib/dist/common/encrypt/encrypt-constants.mjs';

import { GLOBAL_LOG_A_LOT } from '../ibgib-constants.mjs';

const logalot = GLOBAL_LOG_A_LOT || false;


/**
 * Wrapper for alerting via atow capacitor modals.
 *
 * @returns FUNCTION that alerts (doesn't actually do the alert)
 */
export function getFnAlert(): ({ title, msg }: { title: string, msg: string }) => Promise<void> {
    return async ({ title, msg }: { title: string, msg: string }) => {
        // await Dialog.alert({ title, message: msg });
        console.log(`${title}: ${msg}`);
    };
}

/**
 * Wrapper for prompting via atow capacitor modals.
 *
 * @returns FUNCTION that prompts (doesn't actually do the prompt)
 */
export function getFnPrompt(): ({ title, msg }: { title: string, msg: string }) => Promise<string | null> {
    return async ({ title, msg }: { title: string, msg: string }) => {
        const lc = `[${getFnPrompt.name}]`;
        let answer: string;
        try {
            if (!msg) { throw new Error(`msg required. (E: c39d1656b6b803e11bad46efe15b6823)`); }
            const rl = readline.createInterface({ input: stdin, output: stdout });
            try {
                const text = title ? `\n# ${title}:\n\n${msg}\n` : `${msg}\n`;
                answer = await rl.question(text);
                if (logalot) { console.log(`${lc} text: ${text}. answer: ${answer} (I: 7f0375b3abd7eba9c75636d31bbca323)`); }
            } catch (error) {
                throw error;
            } finally {
                rl.close();
            }
        } catch (error) {
            console.error(`${lc} ${extractErrorMsg(error)}`);
            throw error;
        }
        return answer || answer === '' ? answer : null;
    };
}

/**
 * Wrapper for confirming via commandline/terminal prompt.
 *
 * @returns FUNCTION that prompts (doesn't actually do the prompt)
 */
export function getFnConfirm():
    ({ title, msg, okButtonTitle, cancelButtonTitle }:
        { title: string, msg: string, okButtonTitle?: string, cancelButtonTitle?: string }) => Promise<boolean> {
    return async ({ title, msg, okButtonTitle, cancelButtonTitle }:
        { title: string, msg: string, okButtonTitle?: string, cancelButtonTitle?: string }) => {
        okButtonTitle ||= 'yes';
        cancelButtonTitle ||= 'no';

        if (okButtonTitle === cancelButtonTitle) { throw new Error(`okButtonTitle (${okButtonTitle}) must be different than cancelButtonTitle (${cancelButtonTitle}) (E: 254e92fed2e99edc362de337e8bf3123)`); }
        if (okButtonTitle[0] === cancelButtonTitle[0]) { throw new Error(`okButtonTitle (${okButtonTitle}) must start with a different character than cancelButtonTitle (${cancelButtonTitle}) (E: 8bdd34b0d90f479399d911af58df9190)`); }

        const promptFn = getFnPrompt();
        let result: string | null = null;
        do {
            result = await promptFn({ title, msg: `${msg} (${okButtonTitle}/${cancelButtonTitle})` });
            result = result ? result!.toLowerCase() : null;
        } while (!result);

        if (result === okButtonTitle![0].toLowerCase()) { result = okButtonTitle!; }
        return result === okButtonTitle;
    };
}

/**
 * Prompts the user for a password using an ionic capacitor (atow) alert
 * controller with an input of type `'password'`.
 *
 *
 *
 * @returns either the password or null if cancelled
 */
export function getFnPromptPassword(): (title: string, msg: string) => Promise<string | null> {
    const lc = `[${getFnPromptPassword.name}]`;
    try {
        throw new Error(`not implemented yet. i have added PromptPasswordFunction type in core-gib/core-types (E: 78c7dbb8958f8f353730d0dbb86b5423)`);
        // if (!alertController) { throw new Error('alertController required.'); }
        // let fnPromptPassword = async (title: string, msg: string) => {
        //     const alert = await alertController.create({
        //         header: title,
        //         message: msg,
        //         inputs: [
        //             { name: 'password', type: 'password', label: 'Password: ', },
        //         ],
        //         buttons: ['OK', 'Cancel'],
        //     });
        //     await alert.present();
        //     let result = await alert.onDidDismiss();
        //     if (result?.data?.values?.password) {
        //         return result!.data!.values!.password;
        //     } else {
        //         return null;
        //     }
        // };
        // return fnPromptPassword;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    }
}

/**
 * Creates a function with a single `space` arg. This fn when called shows a
 * modal to create an secret ibgib. If the user chooses to save, then the
 * modal will create the secret ibgib, save the transform result in the
 * given `space` (atow this is the local user space), and return the new
 * secret ibgib.
 */
export function getFn_promptCreateSecretIbGib(
): (space: IbGibSpaceAny) => Promise<IbGib_V1 | undefined> {
    const lc = `[${getFn_promptCreateSecretIbGib.name}]`;
    return async (space: IbGibSpaceAny) => {
        throw new Error(`not impl yet (E: 990b79f9b8c2302163cc656deceaf923)`);
        // /** hacked untyped document reference to hack a solution to the back button leaving modals open. */
        // try {
        //     const modal = await common.modalController.create({
        //         component: SecretModalFormComponent,
        //     });
        //     // have to register/clear modal for cancelling in case the user
        //     // presses the back button while the modal is still visible
        //     registerCancelModalOnBackButton(modal);
        //     await modal.present();
        //     let resModal = await modal.onWillDismiss();
        //     // clear the cancel since it dismissed naturally
        //     clearDoCancelModalOnBackButton();
        //     if (resModal.data) {
        //         const resNewSecret = <TransformResult<SecretIbGib_V1>>resModal.data;
        //         await common.ibgibs.persistTransformResult({ resTransform: resNewSecret, space });
        //         const addr = getIbGibAddr({ ibGib: resNewSecret.newIbGib });
        //         if (logalot) { console.log(`${lc} created secret. addr: ${addr}`); }
        //         await common.ibgibs.rel8ToSpecialIbGib({
        //             type: "secrets",
        //             rel8nName: SECRET_REL8N_NAME,
        //             ibGibsToRel8: [resNewSecret.newIbGib],
        //             space,
        //         });
        //         return resNewSecret.newIbGib;
        //     } else {
        //         // didn't create one
        //         console.warn(`${lc} didn't create at this time.`);
        //         return undefined;
        //     }
        // } catch (error) {
        //     console.error(`${lc} error: ${extractErrorMsg(error)}`);
        //     return undefined;
        // }
    }
}

/**
 * Creates a function with a single `space` arg. This fn when called shows a
 * modal to create an encryption ibgib. If the user chooses to save, then the
 * modal will create the encryption ibgib, save the transform result in the
 * given `space` (atow this is the local user space), and return the new
 * encryption ibgib.
 */
export function getFn_promptCreateEncryptionIbGib(
    // common: CommonService,
): (space: IbGibSpaceAny) => Promise<IbGib_V1 | undefined> {
    throw new Error(`not impl yet (E: 69c7d552de7318a97457363adfd34f23)`);
    // const lc = `[${getFn_promptCreateEncryptionIbGib.name}]`;
    // return async (space: IbGibSpaceAny) => {
    //     try {
    //         const modal = await common.modalController.create({
    //             component: EncryptionModalFormComponent,
    //         });
    //         // have to register/clear modal for cancelling in case the user
    //         // presses the back button while the modal is still visible
    //         registerCancelModalOnBackButton(modal);
    //         await modal.present();
    //         let resModal = await modal.onWillDismiss();
    //         // clear the cancel since it dismissed naturally
    //         clearDoCancelModalOnBackButton();
    //         if (resModal.data) {
    //             const resNewEncryption = <TransformResult<IbGib_V1<EncryptionData_V1>>>resModal.data;
    //             await common.ibgibs.persistTransformResult({ resTransform: resNewEncryption, space });
    //             const addr = getIbGibAddr({ ibGib: resNewEncryption.newIbGib });
    //             if (logalot) { console.log(`${lc} created encryption. addr: ${addr}`); }
    //             return resNewEncryption.newIbGib;
    //         } else {
    //             // didn't create one
    //             console.warn(`${lc} didn't create at this time.`);
    //             return undefined;
    //         }
    //     } catch (error) {
    //         console.error(`${lc} error: ${extractErrorMsg(error)}`);
    //         return undefined;
    //     }
    // }
}

/**
 * Creates a function with a single `space` arg. This fn when called shows a
 * modal to create an outerspace ibgib. If the user chooses to save, then the
 * modal will create the outerspace ibgib, save the transform result in the
 * given `space` (atow this is the local user space), and return the new
 * outerspace ibgib.
 */
export function getFn_promptCreateOuterSpaceIbGib(
    // common: CommonService,
): (space: IbGibSpaceAny) => Promise<IbGib_V1 | undefined> {
    throw new Error(`not impl yet (E: e48ef8a7b4252cfe795dde791de99523)`);
    // const lc = `[${getFn_promptCreateOuterSpaceIbGib.name}]`;
    // return async (space: IbGibSpaceAny) => {
    //     try {
    //         const modal = await common.modalController.create({
    //             component: OuterspaceModalFormComponent,
    //         });
    //         // have to register/clear modal for cancelling in case the user
    //         // presses the back button while the modal is still visible
    //         registerCancelModalOnBackButton(modal);
    //         await modal.present();
    //         let resModal = await modal.onWillDismiss();
    //         // clear the cancel since it dismissed naturally
    //         clearDoCancelModalOnBackButton();
    //         if (resModal.data) {
    //             const resOuterSpace = <TransformResult<OuterSpaceIbGib>>resModal.data;
    //             await common.ibgibs.persistTransformResult({ resTransform: resOuterSpace, space });
    //             const addr = getIbGibAddr({ ibGib: resOuterSpace.newIbGib });
    //             if (logalot) { console.log(`${lc} created outerspace. addr: ${addr}`); }
    //             return resOuterSpace.newIbGib;
    //         } else {
    //             // didn't create one
    //             console.warn(`${lc} didn't create outerspace at this time.`);
    //             return undefined;
    //         }
    //     } catch (error) {
    //         console.error(`${lc} error: ${extractErrorMsg(error)}`);
    //         return undefined;
    //     }
    // }
}

/**
 * Creates a function with a single `space` arg. This fn when called shows a
 * modal to mutate a pic ibgib. If the user chooses to save, then the modal will
 * perform the mutation, save the transform result in the given `space`, and
 * return the new pic ibgib.
 */
// export function getFn_promptUpdatePicIbGib(
//     common: CommonService,
// ): (space: IbGibSpaceAny, picIbGib: PicIbGib_V1) => Promise<UpdatePicPromptResult | undefined> {
//     const lc = `[${getFn_promptUpdatePicIbGib.name}]`;
//     return async (space: IbGibSpaceAny, picIbGib: PicIbGib_V1) => {
//         try {
//             const modal = await common.modalController.create({
//                 component: UpdatePicModalFormComponent,
//                 componentProps: { picIbGib, space },
//             });
//             // have to register/clear modal for cancelling in case the user
//             // presses the back button while the modal is still visible
//             registerCancelModalOnBackButton(modal);
//             await modal.present();
//             let resModal = await modal.onWillDismiss();
//             // clear the cancel since it dismissed naturally
//             clearDoCancelModalOnBackButton();
//             if (resModal.data) {
//                 const result = <UpdatePicPromptResult>resModal.data;
//                 const [resCreatePic, _resCreateBin] = result;
//                 const addr = getIbGibAddr({ ibGib: resCreatePic.newIbGib });
//                 if (logalot) { console.log(`${lc} updated pic. addr: ${addr}`); }
//                 return result;
//             } else {
//                 // didn't create one
//                 console.warn(`${lc} didn't create at this time.`);
//                 return undefined;
//             }
//         } catch (error) {
//             console.error(`${lc} error: ${extractErrorMsg(error)}`);
//             return undefined;
//         }
//     }
// }

/**
 * Creates a function with a single `space` arg. This fn when called shows a
 * modal to mutate a comment ibgib. If the user chooses to save, then the modal will
 * perform the mutation, save the transform result in the given `space`, and
 * return the new comment ibgib.
 */
// export function getFn_promptUpdateCommentIbGib(
//     common: CommonService,
// ): (space: IbGibSpaceAny, commentIbGib: CommentIbGib_V1) => Promise<UpdateCommentPromptResult | undefined> {
//     const lc = `[${getFn_promptUpdateCommentIbGib.name}]`;
//     return async (space: IbGibSpaceAny, commentIbGib: CommentIbGib_V1) => {
//         try {
//             const modal = await common.modalController.create({
//                 component: UpdateCommentModalFormComponent,
//                 componentProps: { commentIbGib, space },
//             });
//             // have to register/clear modal for cancelling in case the user
//             // presses the back button while the modal is still visible
//             registerCancelModalOnBackButton(modal);
//             await modal.present();
//             let resModal = await modal.onWillDismiss();
//             // clear the cancel since it dismissed naturally
//             clearDoCancelModalOnBackButton();
//             if (resModal.data) {
//                 const resCreateComment = <UpdateCommentPromptResult>resModal.data;
//                 const addr = getIbGibAddr({ ibGib: resCreateComment.newIbGib });
//                 if (logalot) { console.log(`${lc} updated comment. addr: ${addr}`); }
//                 return resCreateComment;
//             } else {
//                 // didn't create one
//                 console.warn(`${lc} didn't create at this time.`);
//                 return undefined;
//             }
//         } catch (error) {
//             console.error(`${lc} error: ${extractErrorMsg(error)}`);
//             return undefined;
//         }
//     }
// }

/**
 * Creates a function with a single `space` arg. This fn when called shows a
 * modal to create a robbot ibgib. If the user chooses to save, then the modal will
 * perform the mutation, save the transform result in the given `space`, and
 * return the new pic ibgib.
 */
// export function getFn_promptRobbotIbGib(
//     common: CommonService,
// ): (space: IbGibSpaceAny, ibGib: RobbotIbGib_V1) => Promise<RobbotPromptResult | undefined> {
//     const lc = `[${getFn_promptRobbotIbGib.name}]`;
//     return async (space: IbGibSpaceAny, ibGib: RobbotIbGib_V1) => {
//         try {
//             const modal = await common.modalController.create({
//                 component: RobbotModalFormComponent,
//                 componentProps: { ibGib, space },
//             });
//             // have to register/clear modal for cancelling in case the user
//             // presses the back button while the modal is still visible
//             registerCancelModalOnBackButton(modal);
//             await modal.present();
//             let resModal = await modal.onWillDismiss();
//             // clear the cancel since it dismissed naturally
//             clearDoCancelModalOnBackButton();
//             if (resModal.data) {
//                 const result = <RobbotPromptResult>resModal.data;
//                 return result;
//             } else {
//                 // didn't create one
//                 console.warn(`${lc} didn't create at this time.`);
//                 return undefined;
//             }
//         } catch (error) {
//             console.error(`${lc} error: ${extractErrorMsg(error)}`);
//             return undefined;
//         }
//     }
// }

/**
 * Creates a function with a single `space` arg. This fn when called shows a
 * modal to create a app ibgib. If the user chooses to save, then the modal will
 * perform the mutation, save the transform result in the given `space`, and
 * return the new pic ibgib.
 */
// export function getFn_promptAppIbGib(
//     common: CommonService,
// ): (space: IbGibSpaceAny, ibGib: AppIbGib_V1) => Promise<AppPromptResult | undefined> {
//     const lc = `[${getFn_promptAppIbGib.name}]`;
//     return async (space: IbGibSpaceAny, ibGib: AppIbGib_V1) => {
//         try {
//             const modal = await common.modalController.create({
//                 component: AppModalFormComponent,
//                 componentProps: { ibGib, space },
//             });
//             // have to register/clear modal for cancelling in case the user
//             // presses the back button while the modal is still visible
//             registerCancelModalOnBackButton(modal);
//             await modal.present();
//             let resModal = await modal.onWillDismiss();
//             // clear the cancel since it dismissed naturally
//             clearDoCancelModalOnBackButton();
//             if (resModal.data) {
//                 const result = <AppPromptResult>resModal.data;
//                 return result;
//             } else {
//                 // didn't create one
//                 console.warn(`${lc} didn't create at this time.`);
//                 return undefined;
//             }
//         } catch (error) {
//             console.error(`${lc} error: ${extractErrorMsg(error)}`);
//             return undefined;
//         }
//     }
// }
