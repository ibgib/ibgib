/**
 * @module ordered-manifest types (and enums)
 */

import { IbGibAddr, TransformResult } from '@ibgib/ts-gib/dist/types.mjs';
import { IbGibData_V1, IbGibRel8ns_V1, IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { SpaceId } from '@ibgib/core-gib/dist/witness/space/space-types.mjs';

// and just to show where these things are
// import { CommentIbGib_V1 } from "@ibgib/core-gib/dist/common/comment/comment-types.mjs";
// import { MetaspaceService } from "@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs";

// /**
//  * example enum-like type+const that I use in ibgib. sometimes i put
//  * these in the types.mts and sometimes in the const.mts, depending
//  * on context.
//  */
// export type SomeEnum =
//     'ib' | 'gib';
// /**
//  * @see {@link SomeEnum}
//  */
// export const SomeEnum = {
//     ib: 'ib' as SomeEnum,
//     gib: 'gib' as SomeEnum,
// } satisfies { [key: string]: SomeEnum };
// /**
//  * values of {@link SomeEnum}
//  */
// export const SOME_ENUM_VALUES: SomeEnum[] = Object.values(SomeEnum);

/**
 * ibgib's intrinsic data goes here.
 *
 * @see {@link IbGib_V1.data}
 * @see {@link OrderedManifestIbGib_V1}
 */
export interface OrderedManifestData_V1 extends IbGibData_V1 {
    // ibgib data (settings/values/etc) goes here
    // /**
    //  * docs yo
    //  */
    // setting: string;
    spaceId?: SpaceId;

    /**
     * If true, {@link resultAddrs} and {@link OrderedManifestIbGib_V1.results} are ordered.
     */
    resultsAreOrdered?: boolean;
    /**
     * possibly ordered
     */
    resultAddrs: TransformResultAddrs[];
}

/**
 * rel8ns (named edges/links in DAG) go here.
 *
 * @see {@link IbGib_V1.rel8ns}
 * @see {@link OrderedManifestIbGib_V1}
 */
export interface OrderedManifestRel8ns_V1 extends IbGibRel8ns_V1 {
    // /**
    //  * required rel8n. for most, put in ordered-manifest-constants.mts file
    //  *
    //  * @see {@link REQUIRED_REL8N_NAME}
    //  */
    // [REQUIRED_REL8N_NAME]: IbGibAddr[];
    // /**
    //  * optional rel8n. for most, put in ordered-manifest-constants.mts file
    //  *
    //  * @see {@link OPTIONAL_REL8N_NAME}
    //  */
    // [OPTIONAL_REL8N_NAME]?: IbGibAddr[];
}

/**
 * this is the ibgib object itself.
 *
 * If this is a plain ibgib data only object, this acts as a dto. You may also
 * want to generate a witness ibgib, which is slightly different, for ibgibs
 * that will have behavior (i.e. methods).
 *
 * @see {@link OrderedManifestData_V1}
 * @see {@link OrderedManifestRel8ns_V1}
 */
export interface OrderedManifestIbGib_V1<T extends IbGib_V1 = IbGib_V1> extends IbGib_V1<OrderedManifestData_V1, OrderedManifestRel8ns_V1> {
    results: TransformResult<T>[];
}

/**
 * addr-only information for {@link TransformResult} in ts-gib.
 * need to move this into that same lib at some point.
 *
 * @see {@link TransformResult}
 */
export interface TransformResultAddrs {
    /**
     * The final result of the transformation
     */
    newIbGibAddr: IbGibAddr;
    /**
     * DNA side effects created in the transformation.
     */
    dnaAddrs?: IbGibAddr[],
    /**
     * If you're performing a transform with multiple steps,
     * then there will be intermediate ibGibs created.
     */
    intermediateIbGibAddrs?: IbGibAddr[],
}
