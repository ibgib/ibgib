/**
 * @module ordered-manifest helper/util/etc. functions
 *
 * this is where you will find helper functions like those that generate
 * and parse ibs for ordered-manifest.
 */

// import * as pathUtils from 'path';
// import { statSync } from 'node:fs';
// import { readFile, } from 'node:fs/promises';
// import * as readline from 'node:readline/promises';
// import { stdin, stdout } from 'node:process'; // decide if use this or not

import {
    extractErrorMsg, delay, getSaferSubstring,
    getTimestampInTicks, getUUID, pretty,
} from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { UUID_REGEXP, CLASSNAME_REGEXP, } from '@ibgib/helper-gib/dist/constants.mjs';
import { Gib, Ib, } from '@ibgib/ts-gib/dist/types.mjs';
import { validateGib, validateIb, validateIbGibIntrinsically } from '@ibgib/ts-gib/dist/V1/validate-helper.mjs';

import { GLOBAL_LOG_A_LOT } from '../../ibgib-constants.mjs';
import {
    OrderedManifestData_V1, OrderedManifestRel8ns_V1, OrderedManifestIbGib_V1,
} from './ordered-manifest-types.mjs';
import { ORDERED_MANIFEST_ATOM, ORDERED_MANIFEST_NAME_REGEXP, } from './ordered-manifest-constants.mjs';

/**
 * for verbose logging
 */
const logalot = GLOBAL_LOG_A_LOT;

export function validateCommonOrderedManifestData({
    data,
}: {
    data?: OrderedManifestData_V1,
}): string[] {
    const lc = `[${validateCommonOrderedManifestData.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting...`); }
        if (!data) { throw new Error(`data required (E: c6d3f2853f19b5d75bbb9ba214abece3)`); }
        const errors: string[] = [];
        const {
            name, uuid, classname,
        } =
            data;

        if (name) {
            if (!name.match(ORDERED_MANIFEST_NAME_REGEXP)) {
                errors.push(`name must match regexp: ${ORDERED_MANIFEST_NAME_REGEXP} (E: 850adbb1eb4bacb82bd3f81579850697)`);
            }
        } else {
            errors.push(`name required. (E: 3d752b8f6b3fd7788031e7a30c28b80a)`);
        }

        if (uuid) {
            if (!uuid.match(UUID_REGEXP)) {
                errors.push(`uuid must match regexp: ${UUID_REGEXP} (E: 9098f79bed12c7cf20770bd77d3ac567)`);
            }
        } else {
            errors.push(`uuid required. (E: b79efe9658bd61b3311b72aa7129868c)`);
        }

        if (classname) {
            if (!classname.match(CLASSNAME_REGEXP)) {
                errors.push(`classname must match regexp: ${CLASSNAME_REGEXP} (E: 9280cfcb2f9cdd3c2db0b3d9844d2b9b)`);
            }
        }

        return errors;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export async function validateCommonOrderedManifestIbGib({
    OrderedManifestIbGib,
}: {
    OrderedManifestIbGib: OrderedManifestIbGib_V1,
}): Promise<string[] | undefined> {
    const lc = `[${validateCommonOrderedManifestIbGib.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 83735ec4e36ca1dfe6d8ba305eeeccdd)`); }
        const intrinsicErrors: string[] = await validateIbGibIntrinsically({ ibGib: OrderedManifestIbGib }) ?? [];

        if (!OrderedManifestIbGib.data) { throw new Error(`OrderedManifestIbGib.data required (E: 76cf52cfa56a5b2f3282dc45f075090e)`); }
        const ibErrors: string[] = [];
        let { OrderedManifestClassname, OrderedManifestName, OrderedManifestId } =
            parseOrderedManifestIb({ ib: OrderedManifestIbGib.ib });
        if (!OrderedManifestClassname) { ibErrors.push(`OrderedManifestClassname required (E: 84fc05285c87a03a5e21d755c5ad3b96)`); }
        if (!OrderedManifestName) { ibErrors.push(`OrderedManifestName required (E: 1249c967148b45a03ed668188d5da588)`); }
        if (!OrderedManifestId) { ibErrors.push(`OrderedManifestId required (E: d5cbb5a7c6fc8b6a067ae2587682b654)`); }

        const dataErrors = validateCommonOrderedManifestData({ data: OrderedManifestIbGib.data });

        let result = [...(intrinsicErrors ?? []), ...(ibErrors ?? []), ...(dataErrors ?? [])];
        if (result.length > 0) {
            return result;
        } else {
            return undefined;
        }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export function getOrderedManifestIb({
    data,
    classname,
}: {
    data: OrderedManifestData_V1,
    classname?: string,
}): Ib {
    const lc = `[${getOrderedManifestIb.name}]`;
    try {
        const validationErrors = validateCommonOrderedManifestData({ data });
        if (validationErrors.length > 0) { throw new Error(`invalid OrderedManifest data: ${validationErrors} (E: afd370a154863b3b655d4c10cac56e50)`); }
        if (classname) {
            if (data.classname && data.classname !== classname) { throw new Error(`classname does not match data.classname (E: 9d7cf8414355caa4ef1e1f62ca1a932b)`); }
        } else {
            classname = data.classname;
            if (!classname) { throw new Error(`classname required (E: 6c6f886b7c7bb236c4d60681d7c1bfbc)`); }
        }

        // ad hoc validation here.

        const { name, uuid } = data;
        return `${ORDERED_MANIFEST_ATOM} ${classname} ${name} ${uuid}`;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    }
}

/**
 * Current schema is '[ORDERED_MANIFEST_ATOM] [classname] [OrderedManifestName] [OrderedManifestId]'
 *
 * NOTE this is space-delimited
 */
export function parseOrderedManifestIb({
    ib,
}: {
    ib: Ib,
}): {
    OrderedManifestClassname: string,
    OrderedManifestName: string,
    OrderedManifestId: string,
} {
    const lc = `[${parseOrderedManifestIb.name}]`;
    try {
        if (!ib) { throw new Error(`OrderedManifest ib required (E: 6fef48ad3968d4bbf39bb09654fe5389)`); }

        const pieces = ib.split(' ');

        return {
            OrderedManifestClassname: pieces[2],
            OrderedManifestName: pieces[3],
            OrderedManifestId: pieces[4],
        };
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    }
}
