# ibgib (rcli front end)

This is the RCLI front end for ibgib. An RCLI is:

* Robbotic Command Line Interface, or
* Request/Command Line Interface

Interestingly both are equivalent. A command line interface can be too rigid for
our more robust future. Instead of attempting to "command" robbots, we will be
requesting just as we would request from other humans. In general, commands have
fewer synonyms (like --version/-v are "synonyms") whereas "requests" will be
more natural language.

The repo's name is bare "ibgib" to make things easier for npm packages
with the `"bin"` entry. (We want the rcli request to be named "ibgib").

## 🚧 wip.

This is still very much a 🚧 wip 🚧. I have created a working MVP web app front
end for ibgib that shows some of the potential for the ibgib architecture, which
can be found at https://ibgib.space which is built with Angular + Ionic. but
this repo's project specifically focuses on the RCLI + B2tFS (Semantic Version
Control System).

## quickstart

to get code to build and tests to run, you will need

* `helper-gib`: https://gitlab.com/ibgib/helper-gib
  * helpers and utils
* `ts-gib`: https://gitlab.com/ibgib/ts-gib/-/tree/master
  * base ibgib protocol graphing substrate lib on GitLab
* `core-gib`: https://gitlab.com/ibgib/core-gib
  * engine driving lowest layer of ibgib-specific code
* `encrypt-gib`: https://github.com/wraiford/encrypt-gib
  * experimental encryption lib

You can find my current monorepo-ish super repo structure at
https://github.com/wraiford/ibgib .

_note: this is GitHub whereas this RCLI project and most other libs are on GitLab. these will soon (by Q4 2024) be condensed into the dogfooded ibgib B2tFS vcs._

## build/dev/contribute tips

### when you want to test some behavior manually

sometimes it's nice to open up a repl at some state with some assumptions. the
respec's do this well and by default (atow 01/2024) do not delete the output
test folders under [b2tmp](/test-b2tfs/b2tmp/).

so you can execute an `npm test` run and navigate to these folders and within
one of those do your manual testing.  i often will use a `node ../../.. --repl` or
whatever.

_note: you may want to set the desired test only to run by changing its unit
test feature. change its `respecfully` block to `respecfullyDear` and turn on
the `veryPolite` flag._

## overall approach using ibgib's architecture

So this RCLI app has two primary parts:

1. RCLIApp_V1
2. RollyRobbot_V1

The interesting 10,000-foot aspect of this is that the RCLI's interactive REPL
functions as a chat among the user, the robbot and the RCLI app itself as
another participant. The RCLI's executions will produce side effects (both
in-band ibgib data and possibly others out-of-band). Since it is implemented as
a chat, this enables scaling up to multiple robbots and users, as well as
integrating with other "witnesses".

So a user can either enter commands or "speak" with Rolly to resolve to command
lines. And both the RCLI app and Rolly leverage ibgib's unique architecture:

* unique "ib^gib" universal content addressing system
* unique semi-structured data
* unique "witness" class structure
* much more...

Ibgib's architecture will then in the future further enable distributed
collaboration with DRYer code than other implementations. This will enable
reduced surface attack area, as well increased efficiency in a larger
distributed system and many more dynamics.

### what this enables

So we have a combination of RCLI app + RLI Robbot (Rolly) inside a chat-like
context built on a Merkle DAG DLT substrate, with the possibility of expanding
and interoperating with other apps, robbots and users. The RCLI app acts similar
to a standard RCLI in spirit. BUT we use the chat-style context ibgib for the
interactive REPL and convert "command lines" to comments in this chat context.
The RCLI strictly interprets escaped comments, while Rolly can then also
interpret other chat requests. These **could** resolve to command lines that the
RCLI then interprets. But this doesn't have to be the case. You could chat with
Rolly similar to interacting with a `man` or `--help`, but more in a
chatbot-style. **OR** you can add "comments" in your RCLI interaction to document
intent. **OR** you could be interacting with multiple witness apps. **OR many
other things.**

Basically you are **capturing context** and **enabling features** as a matter of
course of using the architecture, like other implementations of ibgib
beyond existing paradigms.

### ibgib review - skip this if you're already familiar with ibgib's architecture

A quick reference on the `IbGib` v1 interface exposed in ts-gib :

```typescript
export interface IbGib {
  /** per use-case metadata */
  ib: string;
  /** cryptographic hash info of other three props */
  gib?: string;
  /** intrinisic data */
  data?: any;
  /** named links/edges to other ibgib nodes for decoupled extrinsic data */
  rel8ns?: { [key: string]: IbGibAddr[] };
}
```

> _note: this is simplified and condensed pseudocode from multiple interfaces with generics and other complexities._

Each address is composed of the `ib` and `gib` properties concatenated using a
delimiter (`^` by default).

#### quick background on ibgib a la code as data

Ibgib is largely about meta-programming. The raw ibgib data protocol shape
enables cryptographic content-addressing, building cryptographic DAGs with the
ability to both:

1. (re)hydrate via dna transforms as in event sourcing requests/events
2. apply dna in different contexts, similar to applying diffs in git's version control in different branches when performing merges.

So both of these essentially boil down to code as data meta-programming.

#### `Witness` interface: add behavior to ibgibs

Witnesses in ibgib are those that have this same data protocol semi-structure
shape (`IbGib` interface), and a single behavior/function exposed: `witness`.

```typescript
export interface Witness<
    TIbGibIn extends IbGib,
    TIbGibOut extends IbGib,
>
    extends IbGib {
    witness(arg: TIbGibIn): Promise<TIbGibOut | undefined>;
}
```

> _note: this is simplified and condensed pseudocode from multiple interfaces with generics and other complexities._

So a `Witness` is like a universally content-addressed function. The witness
itself has an `ib^gib` address, intrinsic `data` and extrinsic `rel8ns`, and it
has a single function that takes a single incoming ibgib argument and returns a
single ibgib result.

### brainstorming development options for rcli (archive section)

These were written when trying to first develop this app.

AFAICT there are a couple options for implementing this RCLI:

* incoming text gets parsed like a normal rli request and starts motions
  of some low-ish level of ibgib graph + space.
  * this parsing and routing is done just via functions, without any kind
    of ibgib conversion, both the incoming args, the produced output, as well
    as no witness handling those incoming args.
* I build out a new witness class structure that descends directly Witness
  * does NOT descend from: space, robbot, or app
  * incoming "request" (it is an "R"LI after all) is assumed to be a text, i.e.
    CommentIbGib_V1.
* I descend from **app** witness `AppBase_V1`
  * does have `routeAndDoArg`, `doComment` already going.
  * the rli sounds like an app to me
* I descend from **robbot** witness `RobbotBase_V1`
  * does have `routeAndDoArg` already going
  * plumbing in place for listening to a timeline stream context for
    incoming comments.
    * for now, we don't need pic interpretation though that's definitely in the
      pipeline.
  * already has lex setup for parsing incoming requests
  * CONS
    * rli doesn't sound like a robbot, but rather an environment for a robbot.
    * this may be down to our paradigm shift from apps to assistants (robbots)
      as layers of indirection to apps. So instead of thinking of a robbot as
      "part of" an "app", with ibgib we're streamlining interfaces to shared
      data universes (metaverse projections).
      * as a side note, a "metaverse" is more likely a projection of **the**
        "universe".

Ultimately, I think the path forward is an amalgam of these approaches. I would
prefer to keep it simple, but the raw parsing of the incoming requests doesn't
work with my ideas for local spaces (similar/isomorphic to "nodes" in other
paradigms). And I would prefer even to use either an app or a robbot. But I'm
leaning towards both an app that gets bootstrapped and a robbot that the app
creates by default and routes requestline comments to.

UPDATE: I have moved toward the amalgam of a RCLI app + RLI Robbot (Rolly). The
RCLI app acts similar to a standard CLI in spirit. BUT it leverages ibgib's
architecture and uses a chat-style context ibgib for the interactive REPL and
converts "command lines" to comments in this chat context. Rolly can then also
interpret other chat requests that could resolve to command lines that the RCLI
interprets. But this doesn't have to be the case. You could chat with Rolly
similar to interacting with a `man` or `--help`, but more in a chatbot-style.
ALSO you can add "comments" in your RCLI interaction to document intent.
Basically you are **capturing context** and **enabling features** as a matter of
course, much like other implementations of ibgib beyond existing paradigms.

### bootstrapping - spaces, apps & robbots

#### A space...

* is a colocation of ibgib.
  * Essentially is the equivalent to a mathematical set or group.
* that colocates spaces is a metaspace.
  * starts off with zero memory as a zero-space.
  * over time, ultimately given monotonically increasing metaspaces,
    each one becomes "the same as" or "effectively indistinguishable from" the
    other except in their pasts? (all roads lead to rome)

#### An app...

* is one layer "on top of"/"orthogonal to" a space.
* provides the execution mechanism that changes/mutates ibgibs within spaces.
* is equivalent to the physics of spaces.
* can itself "move" from space to space,
  * which is to say, when you change which space(s) the app is acting against,
    it has moved.
* as a witness is not a subtype of a space, but a separate branch.
* commonly witnesses only a single ibgib arg upon startup, with some occasional
  update witness args when changing settings, preferences, etc.
* that acts upon other app ibgibs is a metaapp.

#### A robbot...

* is a witness with agency that acts within an app+space(s) context.
  * it has agency in that it acts within an app as an execution context (scheduler), as
    the app is determining the "physics".
* can "move" from app+space(s) context to app+space(s) context.
* commonly witnesses many ibgib args relative to the frequency that an app
  witnesses ibgibs.
  * this frequency can be similar in scope to the frequency that a human
    witnesses ibgibs.


#### differentiating apps and robbots...

* is tough.
  * is an app the "flesh" of a robbot?
  * can an app exist outside of a robbot?
  * am i confusing a robbot with a witness?
    * especially in light of ai and specifically ChatGPT (chatbot ai) in news
      atow.

#### bootstrapping

So in order for anything to happen, first you have to start with a space. And ultimately
the first space is a metaspace. But we don't have any previous spaces to look at to
get parameters, so the first metaspace has to be a zerospace.

##### ionic/capacitor mvp bootstrap

first run...

* angular app component is constructed
* all powerful ibgibs service is initialized
* local space initialized
  * zerospace is created (IonicSpace_V1 with default values)
  * bootstrap is looked for (but NOT found) using that zerospace
  * local space is created
    * prompts user for a name for the local space
  * bootstrap is created
    * uses zerospace
    * points to local space
* special ibgibs are initialized/created
* all powerful ibgibs service initialized fires
  * app is ready to go

successive runs

* angular app component is constructed
* all powerful ibgibs service starts
* local space initialized
  * zerospace is created (IonicSpace_V1 with default values)
  * bootstrap is looked for (and YES found) using that zerospace
    * and validated
  * local space addr is gotten from bootstrap
    * not cached and regotten most times space is needed
* all powerful ibgibs service initialized fires
  * app is ready to go



##### bootstrap in this rli

* user types in cmd in the form of `ibgib [...args]`
  * args list
    * `--b2tfs-root-dir`
      * short: `-fsrd`
      * path to root of ibgib spaces
      * also is the default unnamed argument (doesn't start with - or --)
      * default to `./.ibgib`
    * `--zerospace-dir`
      * short: `-zsd`
      * synonym: `--filesystemzerospace`
      * path to zerospace directory
      * defaults to `__zerospace__`
      * relative or absolute [^1]
    * `--memzerospace`
      * short: `-mzs`
      * synonym: `--in-memory-zerospace`
      * boolean flag indicates if we're using an in-memory only zerospace (transient metaspace)
    * `-b` / `--bootstrap-file`
      * path to the bootstrap^gib file
      * defaults to `bootstrap^gib.json`
      * relative or absolute [^1]
    * `--interactive`
      * flag to indicate if repl-like experience with robbot
      * to be implemented once we get a robbot (chatbot) going
    * `--addl-metadata`
      * optional additional metadata to be attached to the ib
* zerospace created
  * if `-fzs`/`--fszerospace`/`--filesystemzerospace`, look in that folder
  * if `-mzs`/`--memzerospace`, use in-memory zerospace.
  * no arg defaults to `-fzs="ibgib"`
  * zerospace is created in subfolder `__zerospace__`
    * note: in mvp, this is `000_default_space` I think

[^1] relative to `fs-root-dir` (e.g. `[fs-root-dir]/somepathorfile`) OR absolute path e.g. `/Volumes/my/path/somepathorfile`


### simplest request: `ibgib`

Simply executing this rli main function without arguments should bootstrap a
local space, app & robbot.

> _note: If at some point we want a wizard bootstrapper, then that will be a robbot specifically for intepreting requests and overall chatting with the user to create what is desired._

#### generalized local space (node/app?) bootstrap

## b2tfs-gib (fs+vcs)

**:under_construction: b2tfs-gib is not yet implemented. so right now, these are notes on getting this going.**
> _Also note: i use the term directory and folder mostly interchangeably._

Filesystems are a subset of ibgib's data architecture. Each fs directory has
intrinsic data (that we call metadata), but its primary purpose is to contain
children.  This is equivalent to a rel8n with a rel8nName like `'child'`. Each
file in an fs has two types of intrinsic data: the file's contents and the
file's metadata (used by os often). But there are no "rel8ns", i.e. no children,
because this is the role of a directory. Each folder/file has an address that
can be considered part of each's metadata, which is called the path.

So, ibgib is a more powerful superset, enabling all of these qualities. The path
is a constrained `ib` with no inherent `gib` hashing metadata. This `ib` is a
slash-delimited (backslash in Windows). All intrinsic data can be represented in
the internal `data` property, and all directories' contents can be represented
by a named `'child'` `rel8n` in the `rel8ns` field.

...something about vcs -> time that is inherent in ibgib with `past` and
`ancestor` rel8nNames.

When we _combine_ the version control system aspect to the fs, we effectively are
introducing **TIME** as a first-class citizen in what we previously

**TIMELINE BASED -- NOT FILE/FOLDER**

This entire process is to make it easy for users of previous technologies, e.g. src code like git or document editors like textpad/notepad/docx/docs or image/video editors like illustrator/whatever else.
This means we are working at a TIMELINE level, no other. With that in mind, and restricting ourselves to src code version control transcendence:

* I want a mapping of files/folders to keep track of timelines.
  * (folders/directories used interchangeably here)
* There is a single timeline that is the composite of these other timelines.
  * this is assumed to be a folder/directory in git/fs.
* In git, this is solved at the repo level.
  * The repo acts as the "root" directory of all other files/folders (trees) children.
  * These are the only types of timelines and each diff is an action upon one of these items.
* a commit is a collection of these diffs
  * though git doesn't store deltas
* so an "add-root" is like an "add-repo"
  * "add-repo" could be a synonym
  * the "root" must map to a hierarchical ibgib "folder"
  * we don't really care where these files are though on disk
    * but people nowadays think in these hierarchical paradigm
  * we want to reserve the ability to dynamically compose these repo "folders" from various ibgibs that can live wherever the hell they want to live
    * ibgibs can "work remotely"
  * even though we are working within a single "root" "repo", we want "files" (and simlink-like "folders") to be able to belong to multiple other "repos".
    * i.e. if a timeline gets updated in one repo, we want the ability to update that timeline if it exists in the ibgib space (not just in the repo).
    * but we should also be able to specify if we only want intra-repo changes?
      * "heterogenous mini-merges"
    * this means we must have a wrapper for the file/folder and the concept of the inner ibgib payload(s).
  * we want the ability to dynamically compose individual files from multiple ibgibs.
    * e.g. we have a function ibgib that we want to webpack/rollup/whatever into a single src code text file.
    * e.g. we have multiple chapter ibgibs that we want to compose into a "book" src code text file.
    * e.g. we have multiple visual objects/transforms/filters/layers that we want to compose into a single "image" file.
* should be able to have multiple spaces share (sync) roots.
  *

* root has a `rootName` and a `rootPath`, and of course the root's `tjp` addr.
  * `rootName` maps to the root's `dirName` or `--name` override when root is initialized.
  * `rootPath` maps from the ibgib's space to the root's `outputPath`
    * `rootPath` is always different per space.
    * the rcli binary has a cwd for the space (often `./.ibgib`)
    * don't want to have to deal with saving mappings of space->rootPath all over.
      * can't save the mapping on the root ibgib itself as we want to be able to share
        this root ibgib among multiple spaces.
      * can't save this on the `RCLIApp` for the same reason that the app should
        be portable among spaces.
      * don't want to save this on the space itself because it's busy enough as it is.
    * @examples
      * ibgib space is "inside" the repo
        * `~/my-repo/.ibgib`
        * `rootName` of `my-repo`
        * `rootPath` for `of `..`
    * so if ibgib space is at ~/my-repo/.ibgib and the
* paths act from [rootName]/fake/ibgib/path/[filename/dir-name]
* paths act from [rootName]/fake/ibgib/path/[filename/dir-name]

### b2tfs rcli commands

these commands can be executed as one-offs or in the interactive repl
(`--interactive` flag).

_note: This documentation is generated as I'm creating these, so atow (01/2024)
they are doubling as dev notes. Needless to say, these might be (and probably
will be) out of date.  With each command, I am including where to look at in
code for the real deal. In addition to the actual code, you should find also a
lot of comments/docs for individual functions/modules (But any documentation can
always be out of date)_

* many commands have synonyms, some longer and more descriptive, others more
  terse.
  * i am not of the religion that there should be one and only one command
    syntax.
* all commands have an implied/optional `--data-path` param that specifies what
  our working metaspace datapath is.
  * similar to the existing `--interactive`command, this defaults to ".ibgib".
  * also similar to the `--generate-source-file` command, the actual "cwd" for
    relative paths will be one level up from this metaspace `--data-path`.
    * this stems from placing the metaspace path inside the root cwd folder,
      similar to how existing tools like git place the .[tool] folder inside
      their root cwd folders.

#### `--b2tfs-init`

* [`RCLICommand.b2tfs_init`](/src/witness/app/rcli/rcli-constants.mts)
* [Command Handler](/src/witness/app/rcli/rcli-cmd-handlers/b2tfs/handle-b2tfs-init.mts)

```
> ibgib --b2tfs-init [--data-path=.ibgib]
```

* initializes the b2tfs index in a space.
* remember, this is different from `--init` command (no b2tfs)
  * `--init` must occur first (it's what sets up the overall ibgib local space)
* atow (01/2024) this does not create a root branch
  * so the next command should probably be a

#### b2tfs-info

* [`RCLICommand.b2tfs_info`](/src/witness/app/rcli/rcli-constants.mts)
* [`handle-b2tfs-info` Command Handler](/src/witness/app/rcli/rcli-cmd-handlers/b2tfs/handle-b2tfs-info.mts)
  * see show help info in command handler

* get info specific to the B2tFS version control system

#### b2tfs-add-root-branch

* [`RCLICommand.b2tfs_add_root_branch`](/src/witness/app/rcli/rcli-constants.mts)
* [`handle-b2tfs-add-root-branch` Command Handler](/src/witness/app/rcli/rcli-cmd-handlers/b2tfs/handle-b2tfs-add-root-branch.mts)

```
> ibgib --b2tfs-add-root-branch [path to folder] [--data-path=.ibgib]
> ibgib --b2tfs-add-root-branch --input-path=[path to folder] [--data-path=.ibgib]
```

* bare arg: `--input-path`

##### examples

```
my-repo> ibgib --b2tfs-add-root-branch .
```

```
my-repo> ibgib --b2tfs-add-root-branch ./my-project
```

##### dev notes (previous thoughts when doing this cmd)

`--b2tfs-add-dir [--root]`

create the

> ibgib --b2tfs-init --root --data-path=.ibgib --name=[optional-name] --dir-path=[path/to/dir]

synonyms: [`--b2tfs-mkdir`]

* if existing "fs" tag is not created/found, will throw an error.
* `--name` is optional override of `--dir-path` directory name
  * IOW will use dir name if `--name` isn't provided
  * if the dir name already exists, throw an error explaining using the --name option
* you can look at fs tag to list fs roots
  * this is somewhat analogous to looking at repos (source/fork) & gists

So after this is run, we will be at the point where we have an fs-root ibgib
that is dedicated to a composition of timelines. We now need the ability to
compose these timelines in such a way where we can meaningfully "version
control" the entire "repo".

I'm thinking we will use the "file" and "folder" names as rel8nNames but not
necessarily to "file" and "folder" ibGibs. Or IOW we are going to add ibgibs -
which do not necessarily have an ib like "file [filename] [n] [...]". but we do
need a way when projecting onto an FS a way of maintaining the mapping.

This is because when we go and "import" files/folders into our ibgib space, we
may want to import other non-file/folder items as well into the space. Remember
these files live in an ibgib space that is not as restrictive as the FS
subspace.

I'm thinking we will need a subspace mapping that maps rel8nNames to folder
names. The "file" and "folder" rel8nNames will map explicitly to children to
mimic existing fs-based vcs behavior (e.g. git). Then when we go to add in
super-FS behavior (such as importing individual function/class timelines), we
can still map back and forth to/from file system directory structures.

Here is what the tag that gets initialized on the tags special index:

```
ib: special tag fs-roots
gib: abc123
rel8ns: {
  x: [fs root my_repo^abc123, ..., fs root some-other-repo^abc123, ...]
}
```

The my_repo directory structure in the fs might look like this:

- my_repo
  - dir1
    - dir1.1
      - file_a.txt
    - ...[other files]
  - dir2
    - file B.md
    - ...[other files]
  - ...[other dirs]
  - root-file1.txt
  - ...

> _note: these are example file/folder names not ibs with metadata attached. 1/2/a/b included for pedantic purposes_

So this would map to an ibgib structure like this

```
fs_root my_repo tjp123^abc123
data: {
}
rel8ns: {
  folder: [fs_folder dir1^abc123, fs_folder dir2^abc123],
  file: [fs_file root-file1.txt type=text^abc123, fs_file text root-file2.txt^abc123]
}
```

#### b2tfs-activate-branch

* [`RCLICommand.b2tfs_activate_branch`](/src/witness/app/rcli/rcli-constants.mts)
* [`handle-b2tfs-activate-branch` Command Handler](/src/witness/app/rcli/rcli-cmd-handlers/b2tfs/handle-b2tfs-activate-branch.mts)

#### b2tfs-add-branch

NOT IMPLEMENTED (YET?)

I may end up combining this idea with `b2tfs-add-root-branch` with a `--root`
param.

* [`RCLICommand.b2tfs_add_branch`](/src/witness/app/rcli/rcli-constants.mts)

* the idea is to create branches of existing branches/snapshots/whatever i end
  up calling these kinds of things.
* this is different than adding a root branch in that a root branch does not
  have a previous b2tfs item

#### b2tfs-diff

* [`RCLICommand.b2tfs_diff`](/src/witness/app/rcli/rcli-constants.mts)
* [`handle-b2tfs-diff` Command Handler](/src/witness/app/rcli/rcli-cmd-handlers/b2tfs/handle-b2tfs-diff.mts)

* provides a diff of the current FS state against what is contained in
  the target branch ()

* params (exact names not guaranteed atow 01/2024)
  * `--b2tfs-branch`
    * if none, will use the current active branch
    * if no active branch, will use the first root branch
  * `--apply`
    * if set, will calculate the diff **and** subsequently apply it
      * in ibgib b2tfs, this means that new B2tFSItemIbGib timelines will be
        made and existing ones will be extended if different.
    * if not set, then the diff will only be calculated and shown to the user
      but not applied.

> ibgib --fs--add-root [path/to/dir]

#### `--b2tfs-cwd`

NOT IMPLEMENTED. JUST THINKING ABOUT THIS...

> ibgib --b2tfs-cwd

synonyms: [`--b2tfs-pwd`]

shows the current working directory when doing fs commands.

### on mental shift from existing file/folder version control

Version control systems like CVS and variants, subversion, and of course the
most widely used of our day git, were developed with one primary goal in mind:
meticulously track changes to filesystem-based text files and the folder
structure organizing them.

This is an extremely focused special case of what can be more generally seen as
enabling timeline composition. The absolute most important mental shift is this:

**Time is the domain - not files, not folders, not text.**

In making this metonymous mental shift to a more general system, a few notes may
be helpful.

#### there are no "repos"

A repository is itself not self-similar enough. How do we organize repositories?
Today, we use PaaS's like GitHub, GitLab, BitBucket, Atlassian, etc. But these
do not dogfood the timeline dynamics and essentially are kluged, out-of-band
metarepos themselves.

Instead of "repos", Ibgib's general case timeline composition uses spaces. Very
similar, there is the distinction of being able to create interspatial dynamics
among spaces. This is somewhat analogous to what is currently done with DevOps
which includes not only build steps but also even the package manager
functionality. So we will empower ourselves to have both finer-grained
semantic-level dynamics, like sharing individual versioned functions, as well as
the macroscopic versioned workflows. All of this using the same metasystem, to
enable metaversal dynamics.

### simple repo step-by-step

_(atow 12/2023 top-down, user pov, algorithmic notes while working through implementation)_

right now i'm envisioning the b2tfs vcs to create different folders when
branching (a root is a type of branch whose creation source is out-of-band). so
when we first create a new root inside of a metaspace, i am thinking it will
look like this for a "simple repo" folder:

```bash
my-repo/
```

we do a non-b2tfs init on this folder:

```bash
// metadata and data ibgibs, including one local user space (no name)
my-repo/.ibgib/*
```

we add a few files and folders:

```bash
// no changes
my-repo/.ibgib/*

// files/folders created
my-repo/b2r_main/file1.ext
my-repo/b2r_main/file2.ext
my-repo/b2r_main/subfolder1/file3.ext
```

at this point we have not synced any of the changes in the file/folder
projection to the ibgib data, so now we do so. unsure of exact commands.
perhaps something similar to existing vcs's or perhaps a simpler workflow. other
vcs's can always be mimicked later if prudent.

```bash
// metadata and data ibgibs, including spaces and source ibgibs for projected files
my-repo/.ibgib/*

my-repo/b2r_main/file1.ext
my-repo/b2r_main/file2.ext
my-repo/b2r_main/subfolder1/file3.ext
```

at this point, file data is essentially doubled in this example as the
b2r_main folder's files/folders all also exist inside .ibgib database as
`BinIbGib_V1` ibgibs.

now say we want to execute a B2tFS branch operation.

first, it should execute a check that there are no changes. if there are, then
the branch operation fails and either the changes must be committed or rolled
back (in the future should also be able to transfer changes to new branch with
an explicit flag - otherwise this is an unexpected state). once this is taken
care of, in order to branch, all b2 "main" root **ibgibs** will be copied
(**not** b2r_main files+folders). this is done by getting the entire dependency
graph of the single (in our simple repo example) `B2tFSRootIbGib` as well as all
associated metastones (metastones are also ibgibs) for all timelines. these
**ibgibs** (importantly, this leaves out the main B2tFSIndexIbGib and
any space ibgibs, e.g., `NodeFilesystemIbGib_V1`) are then copied into the newly
created "mybranch" **space** (not b2b_mybranch folder). Nothing is "replayed" or
"merged" at this point because the "mybranch" space is fresh (guaranteed no
conflicts), and no `registerNewIbGib` functions are needed because those are
what generate the metastones. Once these exist in the new branch space, **only
then** do we iterate through the graph and create all filesystem artifacts
(folders/files) in the new "b2b_mybranch" fs folder.

After the branch op completes, our filesystem will look something like this:

```bash
// has new content ibgibs, as well as new mybranch space
my-repo/.ibgib/*

// b2r_main stays the same
my-repo/b2r_main/file1.ext
my-repo/b2r_main/file2.ext
my-repo/b2r_main/subfolder1/file3.ext

my-repo/b2b_mybranch/file1.ext
my-repo/b2b_mybranch/file2.ext
my-repo/b2b_mybranch/subfolder1/file3.ext
```

at this point, we should have to (nearly?) identical directories.

> _note: we can also mimic existing vcs behavior by replacing a single working
folder, but why do this? the benefits of this separate folder approach are many,
whereas the the replacement method can be confusing/scary - both for users and
for tools (a hot reload local server for example). note that there is extra code
to be maintained now that shows the current branch in people's
commandlines/ide's. the drawback of increased disk storage in a world where
space is so cheap seems arbitrary, **especially since this is a legacy
filesystem projection algorithm in the first place**. we "should" be using a
native ibgib "filesystem" in the future in which we have a os-native, global
uuid-based key/value store where local content-addressed data is not
duplicated._

now that we have a new branch, say we update/delete some files/folders by

* updating `file1.ext`,
* adding a new files `newFileA.ext` and `subfolder1/newFileB.ext`, and
* deleting `file2.ext`.

```bash
// new content ibgibs, including new and updated B2tFSItem ibgibs corresponding to files/folders
my-repo/.ibgib/*

// b2r_main stays the same
my-repo/b2r_main/file1.ext
my-repo/b2r_main/file2.ext
my-repo/b2r_main/subfolder1/file3.ext

// b2b_mybranch changes
my-repo/b2b_mybranch/file1.ext (modified)
~~my-repo/b2b_mybranch/file2.ext~~ (deleted)
my-repo/b2b_mybranch/newfileA.ext
my-repo/b2b_mybranch/subfolder1/file3.ext
my-repo/b2b_mybranch/subfolder2/newfileB.ext
```

These changes are again synced to the appropriate ibgib timelines. This includes
creating new B2tFSItem ibgibs for each new file & folder and performing
`mut8`/`rel8` transforms on existing B2tFSItem ibgibs.

> _note: we are not explicitly setting any paths here as the only path stored is in the B2tFSRoot ibgib, which keeps a relative path from the local metaspace. all other hierarchical pathing is done via `B2tfsItemIbGib.rel8ns` using the `B2TFS_CHILD_ITEM_REL8N_NAME` named edge._

when recursively traversing folder paths to sync/add files/folders, folders
prefixed with `b2r_` (root) and `b2b_` (branch) are skipped.

> _note: these prefixes must be terse/cryptic to help mitigate legacy os pathing constraints._

## ibgib - belief vs. knowledge, p vs. np

you are walking on a path towards your home. suddenly, you come to a fork in
the path: one path leads to the land of truth and the other path leads to the
land of lies. you have two guides, each from those lands. as such, one guide
always speaks truths and one always speaks lies. you can ask one of them one
question. what do you ask?

this is a variant of a classical thought experiment that i have been asked
since as early as i can remember. the standard "solution" itself is to ask
either guide _how the other guide would answer_. (there is an equivalent
variant with a single guide and you don't know which land he/she comes from,
where the "solution" is for the guide to ask him/herself, i.e., "how would you
answer if i would ask you...").

but there are intrinsically interesting qualities of this thought experiment.
this mostly resolves around the "definition" (which implies a past tense
solution) vs. the ongoing "defining" of "solving" itself (present progressive
tense without necessarily itself being resolved).

for example, take the sock drawer question: say you have a sock drawer with two
black socks and two white socks in it. how many would you need to blindly pull
out in the dark to guarantee a matching pair? again, there is the meta question
of our initial question statement: the "say you have" part. how do you know you
have that state? even if you were to check, how do you know that state hasn't
changed?  how many people would agree that each white sock is still white? what
if it was in the machine with a red sock and got dyed? or how many washes until
it was frayed beyond being a sock?

in both thought experiments, and i say in all thought experiments, there is
always the conscious decision to apply a metric to quantize thought.

tabling knowledge past tense, ibgib however focuses on the union of both guides:

i believe g/God is being.

ib G/g ib. or ibG ib. or ib Gib. or ibgib. or ibGib...

to say that i "know" something, is now shorthand for "i am believing" some
state.  "i" provides a local space. g/G and/or g/Gib provides some ontological
state, implying that it's above me, as "i" can only get a metric projection of
it into that local space. this includes statements (including questions) that
local space, i.e., "myself".

the effect of this paradigm shift from "knowledge" (past tense) to this
marriage of present and past tense of believing is to focus on
infinitely-branching time-based dynamics. instead of just reasoning in terms
of "space", we are now working with the analog to "spacetime".

so let us revisit the fork in the road thought experiment. each word in this
formulation is now itself a timeline. indeed, "word" itself can be a misnomer
here in that other words may themselves be more apt, according to some metric,
such as combining two words into a single one, e.g. a contraction. so when i
say "word", i mean the logical construct not necessarily the reified text used
to represent that logic.

but with this framing, how can we possibly make progress towards might be
thought of as a "solution"? well, here is my "solution". i myself am the one
doing the solving. while i still live, the solution can never be past tense.
so instead of providing the image of a "solution", i focus on words that
provide dynamics to iterate on to solving processes. while i am still living, i
continually iterate on the _process of defining_.
