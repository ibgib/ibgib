## opportunity...?

first, to buy your time to read this in full, i ask you to consider this: is
there a metric of git's technical debt, and if so how might that impact all
current software development in the world?

https://stackoverflow.com/questions/2003505/how-do-i-delete-a-git-branch-locally-and-remotely

* asked 14 years ago, but last edited only jan 3, 2024
* 20k+ upvotes for the question alone
* 25k+ upvotes for the accepted answer
* 2 pages of answers
  * 41 total answers
  * 5 answers with graphical aids(!)
* viewed 11m+ Times

if these numbers don't land hard with you, please stop reading. we are wasting
each other's Time.

but if these numbers, and their implications which i strongly encourage you to
consider, do indeed resonate with you, read on for our joint opportunity.

# opportunity

hi. i am william raiford and i write you regarding my unique distributed
computation protocol called ibgib. it is a "do all teh things" protocol that i
have been developing well over 20 years now - before bitcoin and before git. i
am now finally (!) nearing the point of viability and am preparing for the next
step in its development, hence your opportunity.

## tl;dr

the ibgib protocol enables a system that i can only describe tersely to you as:

> git : special relativity :: ibgib : general relativity

ibgib is quite literally (in code) a spacetime distributed computation mechanism
ith **Time** being a first-class citizen. the protocol is meant to pull us
kicking and screaming out of our current local maximum and into the next world
of willy wonka's paradise (or harry potter's world of magic if you're more into
that kind of thing).

## not fanciful, rather practical dogfooding

this tl;dr sounds lofty, but most importantly in today's economy this is
entirely feasible and does not require huge seed funding. part of the protocol's
magic is to empower tactical micro-seed funding - think gig economy for
software. dogfooding this aspect will not only enable our own efficient internal
collaboration (so far beyond irc/slack it's hard to overstate), but also provide
the framework for solving the open problem of open source funding (and much
more).

this is all made possible by focusing on **Time** as a first-class citizen at a
fundamental level. because indeed, **Time** is _the_ meta language of _the_
metaverse. and the metaverse

## **Time**

how do you currently incorporate **Time** into your business?

most are probably aware of **Time** wrt version control systems (vcs) and source
code. also many are utilizing some form of vcs used in concert with devops, e.g.
technologies like docker. many also re-use their vcs for tracking documentation.
and almost all utilize out-of-band methods for tracking issues and bugs, with
git + github being by far the most popular solution (git does not have native
issue tracking, so github and similar services implement their own).

in addition to source code, some in the bleeding edge of ai are now becoming
aware of versioning models, and subsequently, versioning training sets. these
two are mostly "solved". but what about versioning permutations of models and
training sets? custom training sets create custom "personalities" of model
families. what about keeping up with user interactions of these personalities?

these solutions are still thinking of today's trends (where the puck is) which
is cloud-based. there is no unified approach that considers logistics of small
models (where the puck will be). how do you track a "single" model's journey
from physical device location (phone, wearable, edge/iot device) to physical
device location? how do you version that device? where do you place that model's
memory, especially as online-learning ("online" as in actively being refined,
not "on the internet") methods grow? how do you handle merging that memory?

## current state of ibgib dev

ibgib's implementation is not a finished product. but it is close - 1-2 years if
i continue to work solo - to a viable distributed computation mechanism. i
should be dogfooding basic version control within a couple months. after this, i
have to complete an improved spaceTime adapter for reproducing cloud-like
functionality (something i've already done in the https://ibgib.space prototype
but too inefficiently with dynamodb + s3). this will take another couple months,
at which point i will have a fully dogfooded, bespoke git replacement.

i have been breaking out code from that prototype for the past year or so,
utilizing both gitlab and github for various pieces. but you can find the bulk
of my bleeding edge, front end rcli + vcs commits - currently utilizing the
still-excellent git - at https://gitlab.com/ibgib/ibgib.

this is in addition to various other support libs and experiments at both
https://gitlab.com/ibgib and https://github.com/wraiford

do note that i am currently using `npm link` functionality to reduce iteration
drag, so you may have to build dependent libraries from source, using each lib's
`npm run relink` scripts. when i get interest i will address this to make it
easier to collaborate going forward.

## opportunity

of course, as stated, i am still using git. why do i reach out now?

well before now, it would have been useless to expand. indeed, there is still
risk wrt two key aspects of my design, namely, keystone authentication
integration (a post quantum pki + ca subsuming candidate) and overall
performance optimization.

but even using my current approach of access-is-authorization (similar to git
without using signatures), there are clear paths forward once vcs functionality
alone is completed. it is now both worth my time, and within my capabilities, to
coordinate with others to create real value from my lifelong passion.

and so, i invite you again to review my stated "technical debt metric". if you
are honest with yourself (a rare quality!), then you will come to the
realization that there indeed is an opportunity here. that you did not
previously recognize this will lead you inexorably to conclude that you owe it
to yourself to at the very least explore ramifications of this. once you have
done so, just reach out to me.

the protocol - and most but not all derivative code - must inevitably stay open.
initiative is where the enormous profits will lie.

- william raiford
- ibgib@ibgib.com
