export function labelize(value: any): string {
    let resLabel: string;
    if (value === undefined) {
        resLabel = 'undefined';
    } else if (value === null) {
        resLabel = 'null';
    } else {
        let raw: string = value.toString();

        const max = 512;
        if (raw.length > max) {
            resLabel = raw.substring(0, max) + '...';
        } else {
            resLabel = raw;
        }
    }
    return resLabel;
}
