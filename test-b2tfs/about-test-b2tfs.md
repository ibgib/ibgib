# test-vcs setup folder details

this folder contains resources for testing the b2tfs version control
system-esque behavior in ibgib with great respec.

## overall workflow

each respecful vcs test has the following parts:

* test-filesystem-repos
  * this mimics users' initial repos
  * different shapes of repos, such as files, folders, subfolders, large files,
    small files, numerous files, etc.
* test-.ibgib-folders
  * contains initialized '.ibgib' folders at various states (atow 01/2024):
    * just after a user has initialized the ibgib space
      * has a named local user space
      * has a named rcli_app
      * has a rolly robbot
    * just after the user has initialized b2tfs
      * after `--b2tfs-init` command (atow 01/2024)
      * has at least one b2tfs index
    * possibly others...
  * remember this also includes one or more local user spaces.

the workflow executes like this:

1. a unique uuid.substring(...) + timestamp-based subfolder is created for the
  test.
2. executes a `cp` command to the shell (via node) that copies the users' test filesystem structure.
3. executes a `cp` command that copies the initialied .ibgib folder, depending on test case.
4. executes one or more b2tfs commands and examines the resulting file structure and ibgib

## notes on filesystem repos

### the composition of the test filesystem repos should include...

* contrived, simplest-case repos.
  * files will be text-only
  * one file
  * multiple files
  * one file, one folder, (empty)
  * one file, one folder, one file
  * etc.
* demo projects from known frameworks
  * files will be javascript, typescript, html, css, etc.
  * angular, react, ionic, svelte, astro, etc.
  * in the future, will test other languages/frameworks

### the changes will all be contrived permutations of changes, including

* adding one or more lines
* removing one or more lines
* replacing one or more lines
* adding files/folders
* moving files/folders
  * including simple renames which for now will only be registered as
    deletes/additions.

### the commands tests should include **all** b2tfs commands.

this includes all `b2tfs-` prefixed commands.

depending on resources, i may implement shortcut convenience that scopes b2tfs
mode with autoprefixing. still working on the thoughts behind this.

### once these are done, dogfood ibgib's source

ibgib's own source control will be the "real world" dogfooding test.  currently,
i don't have a merge/sync/replicate space implemented for any cloud providers,
so i will have to manually implement a script that does the backing up. i'll
have to use multiple file replication providers.

once i am sure that this is working, i can then implement those
merge/sync/replicate spaces to enable more genuine cloud-like behavior.

once this is implemented, i can focus on commenting and integrating keystones.


## a note on the future of "version control"

right now, we think in terms of filesystems and repos. this does not have to be
the case though. so just keep in mind that this structure specifically relates
to the use case of transitioning from a standard filesystem + repo mindset, but
the end goal is to have a more living ecosystem of ibgib.
